/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.mucosa.genomepool.ws.data;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;
/**
 *
 * @author Sirintra Nakjang
 */
public class GeneProductWs
        implements Serializable
{
  private String guid;

  private String proteinTagId;

  private int gi;

  private String proteinId;

  private String productDescr;

  private String geneGuid;
  //private Gene gene;

  private int taxonId;

  private String fragmentGuid;

  private String productType;

  private String productSequence;

  private Set<GeneProductSynonymWs> synonyms;

  private Set<ProductAnnotationWs> annotations;

  public GeneProductWs()
  {
    synonyms = new HashSet<GeneProductSynonymWs>();
    annotations = new HashSet<ProductAnnotationWs>();
  }

  @Override
  public boolean equals(Object obj)
  {
    if (obj == null)
    {
      return false;
    }
    if (getClass() != obj.getClass())
    {
      return false;
    }
    final GeneProductWs other = (GeneProductWs) obj;
    if ((this.guid == null) ? (other.guid != null) : !this.guid.equals(other.guid))
    {
      return false;
    }
    return true;
  }

  @Override
  public int hashCode()
  {
    int hash = 3;
    hash = 29 * hash + (this.guid != null ? this.guid.hashCode() : 0);
    return hash;
  }

  public String getFragmentGuid()
  {
    return fragmentGuid;
  }

  public void setFragmentGuid(String fragmentGuid)
  {
    this.fragmentGuid = fragmentGuid;
  }

  public String getGuid()
  {
    return guid;
  }

  public void setGuid(String guid)
  {
    this.guid = guid;
  }

  public int getTaxonId()
  {
    return taxonId;
  }

  public void setTaxonId(int taxonId)
  {
    this.taxonId = taxonId;
  }

  public String getProteinTagId()
  {
    return proteinTagId;
  }

  public void setProteinTagId(String ProteinTagId)
  {
    this.proteinTagId = ProteinTagId;
  }

  public String getGeneGuid()
  {
    return geneGuid;
  }

  public void setGeneGuid(String geneGuid)
  {
    this.geneGuid = geneGuid;
  }

  
//  public Gene getGene()
//  {
//    return gene;
//  }
//
//  public void setGene(Gene gene)
//  {
//    this.gene = gene;
//  }

  public String getProductSequence()
  {
    return productSequence;
  }

  public void setProductSequence(String productSequence)
  {
    this.productSequence = productSequence;
  }

  public String getProductType()
  {
    return productType;
  }

  public void setProductType(String productType)
  {
    this.productType = productType;
  }

  public Set<GeneProductSynonymWs> getSynonyms()
  {
    return synonyms;
  }

  public void setSynonyms(Set<GeneProductSynonymWs> synonyms)
  {
    this.synonyms = synonyms;
  }

  public Set<ProductAnnotationWs> getAnnotations()
  {
    return annotations;
  }

  public void setAnnotations(Set<ProductAnnotationWs> annotations)
  {
    this.annotations = annotations;
  }

  public int getGi()
  {
    return gi;
  }

  public void setGi(int gi)
  {
    this.gi = gi;
  }

  public String getProductDescr()
  {
    return productDescr;
  }

  public void setProductDescr(String productDescr)
  {
    this.productDescr = productDescr;
  }

  public String getProteinId()
  {
    return proteinId;
  }

  public void setProteinId(String proteinId)
  {
    this.proteinId = proteinId;
  }

}
