/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: 28-Sep-2012, 23:20:47
 */

package com.microbasecloud.blast.common;

import java.io.Serializable;
import uk.org.microbase.filesystem.spi.MBFile;

/**
 *
 * @author Keith Flanagan
 */
public class BlastJobDescription
    implements Serializable
{
  public enum BlastType {
    blastn, blastp;
  };
  
  private BlastType jobType;
  
  private MBFile queryFile;
  private MBFile databaseZipFile;
  private String databaseName;
  
  public BlastJobDescription()
  {
  }

  public BlastType getJobType()
  {
    return jobType;
  }

  public void setJobType(BlastType jobType)
  {
    this.jobType = jobType;
  }

  public MBFile getDatabaseZipFile()
  {
    return databaseZipFile;
  }

  public void setDatabaseZipFile(MBFile databaseZipFile)
  {
    this.databaseZipFile = databaseZipFile;
  }

  public String getDatabaseName()
  {
    return databaseName;
  }

  public void setDatabaseName(String databaseName)
  {
    this.databaseName = databaseName;
  }

  public MBFile getQueryFile()
  {
    return queryFile;
  }

  public void setQueryFile(MBFile queryFile)
  {
    this.queryFile = queryFile;
  }

}
