/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.microbasecloud.tmhmm;

import java.io.Serializable;
import javax.persistence.*;

/**
 *
 * @author Sirintra Nakjang
 */
@Entity
@Table(name = "tmhmm_reports")
public class TmhmmReport implements Serializable{

  @Id
  @GeneratedValue(strategy=GenerationType.AUTO)
  private Long id;
 
  /**
   * Unique ID of this report
   */
  private String guid;
 
  /**
   * The unique ID of the notification message that triggered the generation
   * of this report.
   */
  private String incomingMessageId;
 
  /**
   * Details of the input file used for this report.
   */
  private String sourceFileBucket;
  private String sourceFilePath;
  private String sourceFileName;
 
  public TmhmmReport()
  {
  }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public String getIncomingMessageId() {
        return incomingMessageId;
    }

    public void setIncomingMessageId(String incomingMessageId) {
        this.incomingMessageId = incomingMessageId;
    }

    public String getSourceFileBucket() {
        return sourceFileBucket;
    }

    public void setSourceFileBucket(String sourceFileBucket) {
        this.sourceFileBucket = sourceFileBucket;
    }

    public String getSourceFilePath() {
        return sourceFilePath;
    }

    public void setSourceFilePath(String sourceFilePath) {
        this.sourceFilePath = sourceFilePath;
    }

    public String getSourceFileName() {
        return sourceFileName;
    }

    public void setSourceFileName(String sourceFileName) {
        this.sourceFileName = sourceFileName;
    }
  
  
}   
