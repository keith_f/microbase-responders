/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: 25-Mar-2012, 12:40:24
 */

package uk.ac.ncl.mucosa.genomepool.lite.fasta.data;

import java.io.Serializable;
import javax.persistence.*;

/**
 *
 * @author Keith Flanagan
 */
@Entity
@Table( name = "fasta_entries" )
public class FastaEntry implements Serializable
{
  @Id
  @GeneratedValue(strategy= GenerationType.AUTO)
  private Long id;
  
  private String guid;
  
  private String seqeunce;

  public FastaEntry()
  {
  }

  public String getGuid()
  {
    return guid;
  }

  public void setGuid(String guid)
  {
    this.guid = guid;
  }

  public Long getId()
  {
    return id;
  }

  public void setId(Long id)
  {
    this.id = id;
  }

  public String getSeqeunce()
  {
    return seqeunce;
  }

  public void setSeqeunce(String seqeunce)
  {
    this.seqeunce = seqeunce;
  }

  @Override
  public String toString()
  {
    return "FastaEntry{" + "id=" + id + ", guid=" + guid + ", seqeunce=" + seqeunce + '}';
  }

}
