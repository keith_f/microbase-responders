/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: 25-Mar-2012, 12:40:24
 */

package uk.ac.ncl.mucosa.genomepool.lite.fastq.data;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;

/**
 *
 * @author Keith Flanagan
 */
@Entity
@Table( name = "fastq_reports" )
public class FastqReport implements Serializable
{
  @Id
  @GeneratedValue(strategy= GenerationType.AUTO)
  private Long id;
  
  private String fastqFileURI;

  private int numReads;
  
  @Temporal(javax.persistence.TemporalType.DATE)
  private Date added;
  
  private String description;

  public FastqReport()
  {
  }

  public String getDescription()
  {
    return description;
  }

  public void setDescription(String description)
  {
    this.description = description;
  }

  public Date getAdded()
  {
    return added;
  }

  public void setAdded(Date added)
  {
    this.added = added;
  }

  public String getFastqFileURI()
  {
    return fastqFileURI;
  }

  public void setFastqFileURI(String fastqFileURI)
  {
    this.fastqFileURI = fastqFileURI;
  }

  public int getNumReads()
  {
    return numReads;
  }

  public void setNumReads(int numReads)
  {
    this.numReads = numReads;
  }
  
  public Long getId()
  {
    return id;
  }

  public void setId(Long id)
  {
    this.id = id;
  }

}
