/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.microbase.responder.abacascommon;

import java.io.Serializable;
import uk.org.microbase.filesystem.spi.MBFile;

/**
 *
 * @author prisni
 */
public class AlignmentJobDescription implements Serializable {
    
    private MBFile queryFile;
    private MBFile referenceFile;
 
  private String databaseName;
  
  public AlignmentJobDescription()
  {
  }

   public String getDatabaseName()
  {
    return databaseName;
  }

  public void setDatabaseName(String databaseName)
  {
    this.databaseName = databaseName;
  }

  public MBFile getQueryFile()
  {
    return queryFile;
  }

  public void setQueryFile(MBFile queryFile)
  {
    this.queryFile = queryFile;
  }

    public MBFile getReferenceFile() {
        return referenceFile;
    }

    public void setReferenceFile(MBFile referenceFile) {
        this.referenceFile = referenceFile;
    }
    
}
