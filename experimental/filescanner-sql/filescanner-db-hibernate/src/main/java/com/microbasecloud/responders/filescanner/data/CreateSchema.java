/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: 26-Mar-2012, 02:01:31
 */
package com.microbasecloud.responders.filescanner.data;

import com.microbasecloud.filescanner.common.Constants;
import org.hibernate.cfg.Configuration;
import org.hibernate.dialect.MySQLDialect;


/**
 *
 * @author Keith Flanagan
 */
public class CreateSchema
{

  public static void main(String[] args)
  {
    Configuration cfg = new Configuration().configure(Constants.HIBERNATE_RESOURCE);
    String[] lines = cfg.generateSchemaCreationScript(new MySQLDialect());

    System.out.println("\n\nSQL Schema follows:");
    for (int i = 0; i < lines.length; i++)
    {
      System.out.println(lines[i] + ";");
    }
  }
}
