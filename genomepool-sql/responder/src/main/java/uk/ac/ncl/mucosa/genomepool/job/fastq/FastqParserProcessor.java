/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: Feb 9, 2012, 9:07:59 PM
 */

package uk.ac.ncl.mucosa.genomepool.job.fastq;

import com.microbasecloud.filescanner.common.FileStatus;
import com.microbasecloud.responders.filescanner.data.nodes.FileNode;
import com.microbasecloud.responders.filescanner.notifications.FileStateChangedNotification;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.Mongo;
import com.torrenttamer.hibernate.SessionProvider;
import uk.ac.ncl.mucosa.genomepool.job.gbk.*;
import com.torrenttamer.jdbcutils.DistributedJdbcPool;
import com.torrenttamer.jdbcutils.JdbcUtils;
import com.torrenttamer.mongodb.MongoDbFactory;
import com.torrenttamer.mongodb.dbobject.DbObjectMarshaller;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.sql.Connection;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Logger;
import java.util.zip.GZIPInputStream;
import org.codehaus.jackson.map.ObjectMapper;
import org.hibernate.Session;
import uk.ac.ncl.aries.entanglement.ObjectMarshallerFactory;
import uk.ac.ncl.aries.entanglement.graph.EdgeDAO;
import uk.ac.ncl.aries.entanglement.graph.GraphDAOFactory;
import uk.ac.ncl.aries.entanglement.graph.NodeDAO;
import uk.ac.ncl.aries.entanglement.player.GraphCheckoutNamingScheme;
import uk.ac.ncl.aries.entanglement.player.LogPlayer;
import uk.ac.ncl.aries.entanglement.player.LogPlayerMongoDbImpl;
import uk.ac.ncl.aries.entanglement.revlog.RevisionLog;
import uk.ac.ncl.aries.entanglement.revlog.RevisionLogDirectToMongoDbImpl;
import uk.ac.ncl.aries.entanglement.revlog.commands.CreateEdge;
import uk.ac.ncl.aries.entanglement.revlog.commands.CreateNode;
import uk.ac.ncl.aries.entanglement.revlog.commands.GraphOperation;
import uk.ac.ncl.aries.entanglement.util.TxnUtils;
import uk.ac.ncl.aries.entanglement.util.experimental.GraphOpPostCommitPlayer;
import uk.ac.ncl.mucosa.genomepool.full.dao.GenomeFragmentInserter;
import uk.ac.ncl.mucosa.genomepool.full.dao.GenomePoolDAO;
import uk.ac.ncl.mucosa.genomepool.graph.fastq.edges.HasAssociatedDataFile;
import uk.ac.ncl.mucosa.genomepool.graph.fastq.nodes.FastqReportNode;
import uk.ac.ncl.mucosa.genomepool.lite.fastq.dao.FastqReportDAO;
import uk.ac.ncl.mucosa.genomepool.notification.GbkEntryAvailableMessageParser;
import uk.ac.ncl.mucosa.genomepool.notification.NewFastqFileNotification;
import uk.ac.ncl.mucosa.genomepool.notification.NewGbkFileMessageParser;
import uk.ac.ncl.mucosa.genomepool.notification.NotificationConstants;
import uk.ac.ncl.mucosa.genomepool.ws.GenomePoolConfig;
import uk.ac.ncl.mucosa.genomepool.ws.GenomePoolException;
import uk.org.microbase.dist.responder.RegistrationException;
import uk.org.microbase.dist.responder.ResponderInfo;
import uk.org.microbase.filesystem.spi.FileMetaData;
import uk.org.microbase.filesystem.spi.MBFile;
import uk.org.microbase.notification.data.Message;
import uk.org.microbase.responder.spi.AbstractMessageProcessor;
import uk.org.microbase.responder.spi.ProcessingException;
import uk.org.microbase.responder.spi.ResponderInfoPropertiesParser;
import uk.org.microbase.runtime.ConfigurationException;
import uk.org.microbase.util.UidGenerator;
import uk.org.microbase.util.file.FileUtils;

/**
 * A Microbase job implementation that takes a FASTQ-format file as 
 * input and parses the content to the Genome Pool (lite) database. 
 * In addition, two output resources are created containing the DNA and 
 * 6-frame translations of each read in FASTA format.
 * 
 * @author Keith Flanagan
 */
public class FastqParserProcessor
    extends AbstractMessageProcessor
{
  private static final Logger l = Logger.getLogger(FastqParserProcessor.class.getName());
  
  /**
   * Configuration resource for this responder. A file with this name must be
   * located on the classpath.
   */
  static final String CONFIG_RESOURCE = FastqParserProcessor.class.getSimpleName()+".properties";
  
  static final String CONFIG_PROP_MONGO_HOST = "mongodb.hostname";
  static final String CONFIG_PROP_DATABASE = "mongodb.database";
  static final String CONFIG_PROP_GRAPH_NAME = "entanglement.graph_id";
  static final String CONFIG_PROP_GRAPH_BRANCH = "entanglement.graph_branch";
  
  
  /*
   * Configuration options (these should be treated as 'final'
   */
  private Properties config;
  
  private DbObjectMarshaller marshaller;
  private RevisionLog revLog;
  private NodeDAO nodeDao;
  private EdgeDAO edgeDao;
  
  private String mongoHost;
  private String database;
  private String graphName;
  private String graphBranch;
    
  
  private GenomePoolConfig gpConfig;
  private SessionProvider gpLiteProvider;
  private GenomePoolDAO dao;
  
  public FastqParserProcessor()
  {
  }
  
  @Override
  protected ResponderInfo createDefaultResponderInfo()
          throws RegistrationException
  {
    try
    {
      config = new Properties();
      // Use properties from the Microbase global configuration file, if present
      config.putAll(getRuntime().getRawConfigProperties());
      try (InputStream is = getRuntime().getClassLoader().getResourceAsStream(CONFIG_RESOURCE)) {
        if (is == null) {
          throw new RegistrationException("Failed to find configuration resource: "+CONFIG_RESOURCE);
        }
        config.load(is);
      }

      ResponderInfo info = ResponderInfoPropertiesParser.parseFromProperties(config);

      l.info("Responder config: "+info);
      return info;
    }
    catch(Exception e) 
    {
      throw new RegistrationException(
          "Failed to load responder configuration", e);
    }
  }

  
  @Override
  public void preRun(Message message)
      throws ProcessingException
  {
    try
    {
      marshaller = ObjectMarshallerFactory.create(getRuntime().getClassLoader());
      
      mongoHost = getRuntime().getRawConfigProperties().get(CONFIG_PROP_MONGO_HOST);
      database = getRuntime().getRawConfigProperties().get(CONFIG_PROP_DATABASE);
      graphName = getRuntime().getRawConfigProperties().get(CONFIG_PROP_GRAPH_NAME);
      graphBranch = getRuntime().getRawConfigProperties().get(CONFIG_PROP_GRAPH_BRANCH);
      
      MongoDbFactory dbFactory = new MongoDbFactory(mongoHost, database);
      Mongo mongo = dbFactory.createMongoConnection();
      DB db = mongo.getDB(database);

      revLog = new RevisionLogDirectToMongoDbImpl(getRuntime().getClassLoader(), mongo, db);
      GraphCheckoutNamingScheme collectionNamer = new GraphCheckoutNamingScheme(graphName, graphBranch);
      DBCollection nodeCol = db.getCollection(collectionNamer.getNodeCollectionName());
      DBCollection edgeCol = db.getCollection(collectionNamer.getEdgeCollectionName());
      nodeDao = GraphDAOFactory.createDefaultNodeDAO(getRuntime().getClassLoader(), mongo, db, nodeCol, edgeCol);
      edgeDao = GraphDAOFactory.createDefaultEdgeDAO(getRuntime().getClassLoader(), mongo, db, nodeCol, edgeCol);
      
      LogPlayer player = new LogPlayerMongoDbImpl(
              getRuntime().getClassLoader(), marshaller, graphName, graphBranch, 
              revLog, nodeDao, edgeDao);
      GraphOpPostCommitPlayer autoUpdater = new GraphOpPostCommitPlayer(player);
      revLog.addListener(autoUpdater);
      
    }
    catch(Exception e)
    {
      throw new ProcessingException(
          "Failed the preRun stage while configuring a connection to the database.", e);
    }
  }

  @Override
  public void cleanupPreviousResults(Message message)
      throws ProcessingException
  {
//    Session session = null;
//    try
//    {
//      session = gpLiteProvider.getWriteableSession();
//      session.beginTransaction();
//            
//      FastqReportDAO reportDao = new FastqReportDAO();
//
//      session.getTransaction().commit();
//    }
//    catch(Exception e)
//    {
//      SessionProvider.silentRollback(session);
//      e.printStackTrace();
//    }
//    finally
//    {
//      SessionProvider.silentClose(session);
//    }    
//    
//    throw new RuntimeException("Not yet implemented!");
  }

  @Override
  public Set<Message> processMessage(Message incomingMsg)
      throws ProcessingException
  {
    Session session = null;
    InputStream fastqStream;
    FileOutputStream dnaFastaStream = null;
    FileOutputStream translationFastaStream = null;
    try
    {
      //temporarilly commented until we fix filescanner
//      FileStateChangedNotification stateChangedNotif = 
//              this.parseMessageWithJsonContent(incomingMsg, FileStateChangedNotification.class);
       String jobDescrRawJson = incomingMsg.getContent().get("file_state_changed.new");
       ObjectMapper mapper2 = new ObjectMapper();
       FileStateChangedNotification stateChangedNotif = mapper2.readValue(jobDescrRawJson, FileStateChangedNotification.class);
      
      

      if (stateChangedNotif.getStatusChange() != FileStatus.NEW) {
        l.info("Ignoring file state change that's not relevant here: "
                + stateChangedNotif.getStatusChange());
        return new HashSet<>();
      }
      if (stateChangedNotif.getNodeUid() == null) {
        throw new ProcessingException(
                "The graph node UID of incoming file notification: "
                +incomingMsg.getGuid()+" was null.");
      }
      
      // Download input file
      FileMetaData fastqFileDescr = stateChangedNotif.getFileMetaData();
      File fastqFile = downloadFileToWorkingDirectory(fastqFileDescr.getLocation());
      
      //Open input file
      boolean gz = fastqFileDescr.getLocation().getName().endsWith(".gz");
      if (gz) {
        fastqStream = new GZIPInputStream(new FileInputStream(fastqFile));
      } else {
        fastqStream = new FileInputStream(fastqFile);
      }

      // Define output files
      File dnaFastaFile = new File(getWorkingDirectory(), fastqFile.getName()+".dna.fasta");
      dnaFastaStream = new FileOutputStream(dnaFastaFile);
      File translationFastaFile = new File(getWorkingDirectory(), fastqFile.getName()+".protein.fasta");
      translationFastaStream = new FileOutputStream(translationFastaFile);

      // Parse and store sequences / scores in database.
      // Also populate a DNA FASTA file, and a translation FASTA file.
      FastqParserToEntanglement parser = 
              new FastqParserToEntanglement(marshaller, revLog, stateChangedNotif.getNodeUid());
      FastqReportNode reportNode = parser.parse(fastqStream, graphName, graphBranch, 
              dnaFastaStream, translationFastaStream, null);
      
      dnaFastaStream.flush();
      dnaFastaStream.close();
      translationFastaStream.flush();
      translationFastaStream.close();
      
      MBFile remoteDnaFile = new MBFile(
              fastqFileDescr.getLocation().getBucket(),
              FastqParserProcessor.class.getSimpleName()+"/"+incomingMsg.getGuid(),
              dnaFastaFile.getName());
      getRuntime().getMicrobaseFS().upload(dnaFastaFile, remoteDnaFile, null);
      
      MBFile remoteProteinFile = new MBFile(
              fastqFileDescr.getLocation().getBucket(),
              FastqParserProcessor.class.getSimpleName()+"/"+incomingMsg.getGuid(),
              translationFastaFile.getName());
      getRuntime().getMicrobaseFS().upload(translationFastaFile, remoteProteinFile, null);
      
      /********** Quick hack - link file nodes to report *****/
      //FIXME: do this properly
      try {
        String debugTxnId = TxnUtils.beginNewTransaction(revLog, graphName, graphBranch);
        List<GraphOperation> graphOps = new LinkedList<>();
        //Create and link file noes for the FASTA files
        FileNode dnaFileNode = new FileNode(remoteDnaFile.getBucket(), remoteDnaFile.getPath(), remoteDnaFile.getName());
        dnaFileNode.setUid(UidGenerator.generateUid());
        FileNode protFileNode = new FileNode(remoteProteinFile.getBucket(), remoteProteinFile.getPath(), remoteProteinFile.getName());
        protFileNode.setUid(UidGenerator.generateUid());
        
        HasAssociatedDataFile hasDnaFile = new HasAssociatedDataFile(reportNode, dnaFileNode);
        HasAssociatedDataFile hasProteinFile = new HasAssociatedDataFile(reportNode, protFileNode);
        
        graphOps.add(new CreateNode(marshaller.serialize(dnaFileNode)));
        graphOps.add(new CreateNode(marshaller.serialize(protFileNode)));
        graphOps.add(new CreateEdge(marshaller.serialize(hasDnaFile)));
        graphOps.add(new CreateEdge(marshaller.serialize(hasProteinFile)));
        
        revLog.submitRevisions(graphName, graphBranch, debugTxnId, 1, graphOps);
        
        TxnUtils.commitTransaction(revLog, graphName, graphBranch, debugTxnId);
      }
      catch(Exception e) {
        e.printStackTrace();
      }
      
      
      
      NewFastqFileNotification successContent = new NewFastqFileNotification();
      successContent.setFastqFileNodeUid(stateChangedNotif.getNodeUid());
      successContent.setFastqParserReportNodeUid(reportNode.getUid());
      successContent.setParsedDnaFastaFile(remoteDnaFile);
      successContent.setParsedProteinFastaFile(remoteProteinFile);
      
      Message successMsg = createMessageWithJsonContent(
          incomingMsg,                      //The message to use as a parent
          getResponderInfo().getTopicOut(), //The topic of the new message
          incomingMsg.getWorkflowStepId(),  //The workflow step ID
          successContent,                   //The result content item
          //An optional human-readable description
          "Successfully completed a FASTQ parser process.");
      Set<Message> toSend = new HashSet<>();
      toSend.add(successMsg);
      return toSend;
    }
    catch (Exception e)
    {
//      e.printStackTrace();
      SessionProvider.silentRollback(session);
      /*
       * Throwing a JobProcessingException here informs microbase that the job
       * execution has failed. In this case, Microbase will retry executing this
       * job again at some point in the future.
       */
      throw new ProcessingException(
          "Failed the processMessage stage while attempting to parse a "
          + "FASTQ file. Message ID: "+incomingMsg.getGuid(), e);
    }
    finally
    {
      SessionProvider.silentClose(session);
//      FileUtils.closeInputStreams(fastqStream);
      FileUtils.closeOutputStreams(dnaFastaStream, translationFastaStream);
    }
  }
}
