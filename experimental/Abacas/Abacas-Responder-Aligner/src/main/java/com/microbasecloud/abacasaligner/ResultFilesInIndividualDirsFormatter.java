/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.microbasecloud.abacasaligner;

/**
 *
 * @author prisni
 */
public class ResultFilesInIndividualDirsFormatter implements OutputFilePathReformatter
{

  @Override
  public String getBucket(String configuredBucketName, String configuredPath,
      String outputFilename)
  {
    return configuredBucketName;
  }

  @Override
  public String getPath(String configuredBucketName, String configuredPath,
      String outputFilename)
  {
    StringBuilder path = new StringBuilder(configuredPath);
    path.append("/").append(outputFilename);
    return path.toString();
  }

  @Override
  public String getName(String configuredBucketName, String configuredPath,
      String outputFilename)
  {
    return outputFilename;
  }


}
