/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: 15-Oct-2012, 22:05:09
 */

package com.microbasecloud.responders.adapters;

import com.microbasecloud.filescanner.notification.FileStateChanged;
import java.util.HashSet;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.concurrent.TimeUnit;
import java.util.concurrent.locks.Lock;
import java.util.logging.Logger;
import uk.org.microbase.dist.responder.RegistrationException;
import uk.org.microbase.dist.responder.ResponderInfo;
import uk.org.microbase.notification.data.Message;
import uk.org.microbase.responder.spi.AbstractMessageProcessor;
import uk.org.microbase.responder.spi.ProcessingException;
import uk.org.microbase.util.serialization.JsonSerializer;

/**
 *
 * @author Keith Flanagan
 */
public class FileScannerToProdigalAdapter
    extends AbstractMessageProcessor
{
  private static final Logger l = 
      Logger.getLogger(FileScannerToProdigalAdapter.class.getName());
  
  private boolean enabled;
  private String incomingTopic;
  private String outgoingTopic;
  
  public FileScannerToProdigalAdapter()
  {
  }

  @Override
  public void initialise()
      throws RegistrationException
  {
    l.info("Initialising responder");
    super.initialise();
    
    try {
      Properties allConfigProps = new Properties();
      allConfigProps.load(FileScannerToProdigalAdapter.
          class.getResourceAsStream(Constants.ADAPTORS_CONFIG_RESOURCE));
      
      Map<String, String> props = ReadConfig.
          loadConfig(allConfigProps, FileScannerToProdigalAdapter.class.getName());
      enabled = Boolean.parseBoolean(props.get("enabled"));
      incomingTopic = props.get("incoming_topic");
      outgoingTopic = props.get("outgoing_topic");
    }
    catch(Exception e) {
      throw new RegistrationException("Failed to configure responder", e);
    }
  }

  
  
  @Override
  protected ResponderInfo createDefaultResponderInfo()
      throws RegistrationException
  {
    ResponderInfo info = new ResponderInfo();
    info.setEnabled(enabled);
    info.setMaxDistributedCores(1);
    info.setMaxLocalCores(1);
    info.setPreferredCpus(1);
    info.setQueuePopulatorLastRunTimestamp(0);
    info.setQueuePopulatorRunEveryMs(30000);
    info.setResponderName(FileScannerToProdigalAdapter.class.getSimpleName());
    info.setResponderVersion("1.0");
    info.setResponderId(info.getResponderName()+"-"+info.getResponderVersion());
    info.setTopicOfInterest(incomingTopic);
    info.setTopicOut(outgoingTopic);
    return info;
  }

  @Override
  public void preRun(Message message)
      throws ProcessingException
  {
  }

  @Override
  public void cleanupPreviousResults(Message message)
      throws ProcessingException
  {
  }

  @Override
  public Set<Message> processMessage(Message message)
      throws ProcessingException
  {
    try {
      String contentItem = message.getContent().get("file_state_changed.new");
      FileStateChanged msgContent = JsonSerializer.deserializeFromString(
          contentItem, FileStateChanged.class);
      
      Message outgoing = createMessage(
          message, outgoingTopic, null, "Automatically converted message");
      outgoing.getContent().put("seq_bucket", msgContent.getFileMetaData().getLocation().getBucket());
      outgoing.getContent().put("seq_path", msgContent.getFileMetaData().getLocation().getPath());
      outgoing.getContent().put("seq_filename", msgContent.getFileMetaData().getLocation().getName());
      
      Set<Message> outgoingSet = new HashSet<>();
      outgoingSet.add(outgoing);
      return outgoingSet;
    } catch(Exception e) {
      throw new ProcessingException(
          "Failed to process message: "+message.getGuid(), e);
    }
  }
  
  
}
