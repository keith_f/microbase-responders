/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.microbasecloud.genbankresponder;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

/**
 *
 * @author prisni
 */
public class TblOutWriter {
      
    public void writeToFeaturesTableFile(String filename) {
        BufferedWriter bufferedWriter = null;

        try {
            bufferedWriter = new BufferedWriter(new FileWriter(filename));

            bufferedWriter.write(">hello123");
            bufferedWriter.newLine();
            bufferedWriter.write("Writing line two to file");
        } catch (IOException e) {
        } finally {
            try {
                if (bufferedWriter != null) {
                    bufferedWriter.flush();
                    bufferedWriter.close();
                }
            } catch (IOException ex) {
            }

        }

    }
    
    public static void main(String args[]){
        
        new TblOutWriter().writeToFeaturesTableFile("myFile.txt");

    }
  
}
