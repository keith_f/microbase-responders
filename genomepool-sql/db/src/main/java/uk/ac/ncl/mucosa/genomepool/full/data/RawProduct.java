/*
 * Copyright 2010 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: 28-Sep-2010, 13:01:31
 */

package uk.ac.ncl.mucosa.genomepool.full.data;

import java.io.Serializable;

/**
 * Stores raw protein data retrieved from .faa files as an independent source
 * of information not derived from difficult to parse .gbk files.
 *
 * @author Keith Flanagan
 */
public class RawProduct
    implements Serializable
{
  /*
   * Provenance data regarding the Microbase message that produced this result.
   * This may move somewhere else eventually.
   */
  private String resultSetId;
  private String messageId;

  private String idLine;
  private String gi;
  private String accession;
  private String translation;
  private String organismName;


  public RawProduct()
  {
  }

  public String getAccession()
  {
    return accession;
  }

  public void setAccession(String accession)
  {
    this.accession = accession;
  }

  public String getGi()
  {
    return gi;
  }

  public void setGi(String gi)
  {
    this.gi = gi;
  }

  public String getIdLine()
  {
    return idLine;
  }

  public void setIdLine(String idLine)
  {
    this.idLine = idLine;
  }

  public String getTranslation()
  {
    return translation;
  }

  public void setTranslation(String translation)
  {
    this.translation = translation;
  }

  public String getOrganismName()
  {
    return organismName;
  }

  public void setOrganismName(String organismName)
  {
    this.organismName = organismName;
  }

  public String getMessageId()
  {
    return messageId;
  }

  public void setMessageId(String messageId)
  {
    this.messageId = messageId;
  }

  public String getResultSetId()
  {
    return resultSetId;
  }

  public void setResultSetId(String resultSetId)
  {
    this.resultSetId = resultSetId;
  }

  
}
