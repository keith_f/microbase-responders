/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.mucosa.genomepool.job.gbk;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import uk.ac.ncl.mucosa.genomepool.full.data.Gene;
import uk.ac.ncl.mucosa.genomepool.full.data.GenomeFragment;

/**
 *
 * @author Keith Flanagan
 */
public class GbkParsedData
{
  private GenomeFragment genomeFragment;
  private String dnaSequence;
  private Map<String, String> productGuidToTranslation;
  private List<Gene> genes;

  public GbkParsedData()
  {
    genomeFragment = new GenomeFragment();
    genes = new ArrayList<Gene>();
    dnaSequence = "";
    productGuidToTranslation = new HashMap<String, String>();
  }

  public String getDnaSequence()
  {
    return dnaSequence;
  }

  public void setDnaSequence(String dnaSequence)
  {
    this.dnaSequence = dnaSequence;
  }

  public GenomeFragment getGenomeFragment()
  {
    return genomeFragment;
  }

  public void setGenomeFragment(GenomeFragment genomeFragment)
  {
    this.genomeFragment = genomeFragment;
  }

  public Map<String, String> getProductGuidToTranslation()
  {
    return productGuidToTranslation;
  }

  public void setProductGuidToTranslation(Map<String, String> productGuidToTranslation)
  {
    this.productGuidToTranslation = productGuidToTranslation;
  }

  public List<Gene> getGenes()
  {
    return genes;
  }

  public void setGenes(List<Gene> genes)
  {
    this.genes = genes;
  }


}
