/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package com.microbasecloud.blast;

import com.microbasecloud.blast.common.BlastJobDescription;
import com.microbasecloud.blast.common.BlastJobResult;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Logger;
import uk.org.microbase.dist.responder.RegistrationException;
import uk.org.microbase.dist.responder.ResponderInfo;
import uk.org.microbase.filesystem.spi.MBFile;
import uk.org.microbase.filesystem.spi.MicrobaseFS;
import uk.org.microbase.notification.data.Message;
import uk.org.microbase.responder.spi.AbstractMessageProcessor;
import uk.org.microbase.responder.spi.ProcessingException;
import uk.org.microbase.responder.spi.ResponderInfoPropertiesParser;
import uk.org.microbase.util.cli.NativeCommandExecutor;

/**
 * A MessageProcessor implementation capable of executing tools from
 * the NCBI BLAST package.
 * 
 * 
 * @author Keith Flanagan
 */
public class BlastExeResponder
  extends AbstractMessageProcessor
{
  private static final Logger l = Logger.getLogger(BlastExeResponder.class.getName());
  
  /**
   * Configuration resource for this responder. A file with this name must be
   * located on the classpath.
   */
  static final String CONFIG_RESOURCE = BlastExeResponder.class.getSimpleName()+".properties";
  
  private static final String CONFIG_RESULT_NAME_REFORMATTER = "result_name_reformatter";
  
  /*
   * Configuration options (these should be treated as 'final'
   */
  private Properties config;
  private BlastOutputPathReformatter resultFilenameReformatter;
  private BlastOutputPathReformatter stdOutFilenameReformatter;
  private BlastOutputPathReformatter stdErrFilenameReformatter;
  
  /*
   * Per-job variables. These are set every time <code>preRun</code> is called.
   */
  private BlastJobDescription jobDescr;
  private MBFile jobRemoteResultFile;
  private MBFile jobStdOutLogFile;
  private MBFile jobStdErrLogFile;
    

  public BlastExeResponder()
  {
    /*
     * Set some default file name formatters here.
     * TODO eventually, we'll want these to be configurable.
     */
    resultFilenameReformatter = new BlastResultInIndividualDirsFormatter();
    stdOutFilenameReformatter = new BlastStdOutLogFileFormatter();
    stdErrFilenameReformatter = new BlastStdErrLogFileFormatter();
  }
  
  
  
  @Override
  protected ResponderInfo createDefaultResponderInfo()
      throws RegistrationException
  {
    try
    {
      config = new Properties();
      // Use properties from the Microbase global configuration file, if present
      config.putAll(getRuntime().getRawConfigProperties());
      try (InputStream is = getRuntime().getClassLoader().getResourceAsStream(CONFIG_RESOURCE)) {
        if (is == null) {
          throw new RegistrationException("Failed to find configuration resource: "+CONFIG_RESOURCE);
        }
        config.load(is);
      }

      ResponderInfo info = ResponderInfoPropertiesParser.parseFromProperties(config);

      l.info("Responder config: "+info);
      return info;
    }
    catch(Exception e) 
    {
      throw new RegistrationException(
          "Failed to load responder configuration", e);
    }
  }

  @Override
  public void preRun(Message message)
      throws ProcessingException
  {
    l.info("Doing preRun");
    try {
      /*
       * Parse message as a responder specific job description.
       */
      jobDescr = parseMessageWithJsonContent(message, BlastJobDescription.class);
      
      /*
       * Configure the result file formatters
       */
      resultFilenameReformatter.configure(getRuntime(), getResponderInfo(),getProcessInfo(), message);
      resultFilenameReformatter.setJobDescription(jobDescr);
      stdOutFilenameReformatter.configure(getRuntime(), getResponderInfo(),getProcessInfo(), message);
      stdOutFilenameReformatter.setJobDescription(jobDescr);
      stdErrFilenameReformatter.configure(getRuntime(), getResponderInfo(),getProcessInfo(), message);
      stdErrFilenameReformatter.setJobDescription(jobDescr);
      
      jobRemoteResultFile = resultFilenameReformatter.getFormattedRemoteFile();
      jobStdOutLogFile = stdOutFilenameReformatter.getFormattedRemoteFile();
      jobStdErrLogFile = stdErrFilenameReformatter.getFormattedRemoteFile();
      
//      /*
//       * We know what the result file will be ahead of time, so may as well define
//       * it here. The remote result file path and name, can be determined partly
//       * based on configuration options, and partly on the name reformatter in use.
//       */
//      jobRemoteResultFile = determineRemoteResultFile(jobDescr);
//      
//      /*
//       * Similarly, determine the remote log file locations
//       */
//      jobStdOutLogFile = determineRemoteLogStdOutFile(message, jobDescr);
//      jobStdErrLogFile = determineRemoteLogStdErrFile(message, jobDescr);
//      
//      l.info("Parsed job from message: "+message.getGuid()+": "+jobDescr
//          + ". Remote result file will be: "+jobRemoteResultFile);
    }
    catch(Exception e) {
      throw new ProcessingException("Failed to parse message", e);
    }
  }

  @Override
  public void cleanupPreviousResults(Message message)
      throws ProcessingException
  {
    l.info("Doing cleanup");
    try
    {
      MicrobaseFS fs = getRuntime().getMicrobaseFS();
//      MBFile jobRemoteResultFile = resultFilenameReformatter.getFormattedRemoteFile();
      
      if (fs.exists(jobRemoteResultFile))
      {
        l.info("Found an existing result file "+jobRemoteResultFile
            +", generated by a previous "
            + "execution. Deleting...");
        fs.deleteRemoteCopy(jobRemoteResultFile);
      }
    }
    catch(Exception e)
    {
      throw new ProcessingException("Failed to perform cleanup", e);
    }
  }

  @Override
  public Set<Message> processMessage(Message inMsg)
      throws ProcessingException
  {
   l.info("Performing computational work");
    try
    { 
      MBFile queryRemoteFile = jobDescr.getQueryFile();
      MBFile subjectDbRemoteFile = jobDescr.getDatabaseZipFile();
  
      l.info("Query file is: "+queryRemoteFile);
      l.info("Subject DB file is: "+subjectDbRemoteFile);
      // Download the query seqeunce to the scratch space for this job.
      File querySeqFile = downloadFileToSharedSpace(queryRemoteFile);
      
      /*
       * The database is potentially huge, so download and extract it to the
       * shared scratch space for two reasons:
       * 1) other responders/instances may require the same DB concurrently
       * 2) the shared scratch space persists beyond individual job executions
       */
      File dbDir = downloadAndUnzipFileToSharedSpace(subjectDbRemoteFile);
      
      l.info("Running command ...");
      File alignmentFile = runCommand(querySeqFile, dbDir, 
          jobDescr.getDatabaseName(), jobDescr.getJobType());
      
//      MBFile jobRemoteResultFile = resultFilenameReformatter.getFormattedRemoteFile();
      l.info("Uploading completed alignment: "+alignmentFile.getAbsolutePath()
              + " to: "+jobRemoteResultFile);
      getRuntime().getMicrobaseFS().upload(
          alignmentFile, jobRemoteResultFile, null);
      
      Message successMsg = generateSuccessNotification(inMsg, jobDescr);
      
      Set<Message> messages = new HashSet<Message>();
      messages.add(successMsg);
      return messages;
    }
    catch(Exception e)
    {
      throw new ProcessingException(
          "Failed to run one or more commands: "
          + " on: " + getRuntime().getNodeInfo().getHostname() 
          + ", in: " + getWorkingDirectory(), e);
    }
  }
  
  
  private File runCommand(File seqFile, File dbDir, String dbName,
      BlastJobDescription.BlastType blastType)
      throws ProcessingException
  {
    String cmdLineText = null;
    try
    {
      File alignmentOutput = new File(getWorkingDirectory(), seqFile.getName()+"-vs-"+dbName+"."+blastType.name());
      String[] command = new String[]
      {
        "blastall",
        "-p", blastType.name(),
        //"-e", String.valueOf(eValue),
        "-m", String.valueOf(getProcessInfo().getAssignedLocalCores()),
        "-a", String.valueOf(getProcessInfo().getAssignedLocalCores()),
        "-i", seqFile.getAbsolutePath(),
        // The '-d' option below doesn't necessarilly refer to a real file
        "-d", new File(dbDir, dbName).getAbsolutePath(),
        "-o", alignmentOutput.getAbsolutePath()
      };
      cmdLineText = Arrays.asList(command).toString();

      executeCommand(command, blastType.name());
   
      return alignmentOutput;
    }
    catch(Exception e)
    {
      throw new ProcessingException(
          "Failed to execute the command line: "+cmdLineText, e);
    }
  }

  
  private void executeCommand(String[] command, String commandType)
      throws ProcessingException
  {
    File workDir = getWorkingDirectory();
    StringBuilder namePrefix = new StringBuilder();
    namePrefix.append(getProcessInfo().getMessageId()).append(".");
    namePrefix.append(getProcessInfo().getProcessGuid()).append(".");
    namePrefix.append(getProcessInfo().getHostname()).append(".");
    namePrefix.append(commandType).append(".");
    File stdOutFile = new File(workDir, namePrefix.toString()+"stdout.txt");
    File stdErrFile = new File(workDir, namePrefix.toString()+"stderr.txt");
    try {
      FileWriter stdOut = new FileWriter(stdOutFile);
      FileWriter stdErr = new FileWriter(stdErrFile);

      int exitStatus = NativeCommandExecutor.executeNativeCommand(
          workDir, command, stdOut, stdErr);

      stdOut.flush();
      stdOut.close();
      stdErr.flush();
      stdErr.close();

      if (exitStatus != 0) {
        throw new ProcessingException("Exit status of the command: "
            + Arrays.asList(command)
            + " \nwas "+exitStatus
            + ". Assuming that non-zero means that execution failed.");
      }
    }
    catch(Exception e) {
      throw new ProcessingException("Failed to execute command: "+command[0], e);
    }
    finally {
      try {
        getRuntime().getMicrobaseFS().upload(stdOutFile, jobStdOutLogFile, null);
        getRuntime().getMicrobaseFS().upload(stdErrFile, jobStdErrLogFile, null);
      }
      catch(Exception e) {
        throw new ProcessingException(
            "Failed to upload STDOUT/STDERR content", e);
      }
    }
  }
  

  private Message generateSuccessNotification(Message parent, BlastJobDescription job)
      throws IOException
  {
    /*
     * Prepare the content for the 'success' notification message.
     */
    
    BlastJobResult resultContent = new BlastJobResult();
    resultContent.setJob(job);
    resultContent.setResultFile(jobRemoteResultFile);
    resultContent.setStdErrLogFile(jobStdErrLogFile);
    resultContent.setStdOutLogFile(jobStdOutLogFile);
    
    /*
     * Create the success message with the above content. Link this success
     * message to the parent message (the job descriptor) for provenance.
     */
    Message successMsg = createMessageWithJsonContent(
        parent,                           //The message to use as a parent
        getResponderInfo().getTopicOut(), //The topic of the new message
        parent.getWorkflowStepId(),       //The workflow step ID
        resultContent,                    //The result content item
        //An optional human-readable description
        "Successfully completed a BLAST analysis.");
    
    /*
     * Old, manual way of serialising content - can be deleted?
     */
//    ObjectMapper mapper = new ObjectMapper();
//    String resultContentJson = mapper.writeValueAsString(resultContent);
//    successMsg.getContent().put(PROP__MAIN_MSG_CONTENT, resultContentJson);
//    successMsg.getContent().put(OUT_MSG_PROB__RESULT_FILE, mapper.writeValueAsString(jobRemoteResultFile));
//    successMsg.getContent().put(OUT_MSG_PROB__STDERR_FILE, mapper.writeValueAsString(jobStdErrLogFile));
//    successMsg.getContent().put(OUT_MSG_PROB__STDOUT_FILE, mapper.writeValueAsString(jobStdOutLogFile));

    
    return successMsg;
  }
  
  
}
