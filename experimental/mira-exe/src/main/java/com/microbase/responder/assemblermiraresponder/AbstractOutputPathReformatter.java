/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.microbase.responder.assemblermiraresponder;

import uk.org.microbase.dist.processes.ProcessInfo;
import uk.org.microbase.dist.responder.ResponderInfo;
import uk.org.microbase.filesystem.spi.MBFile;
import uk.org.microbase.notification.data.Message;
import uk.org.microbase.runtime.ClientRuntime;

/**
 *
 * @author prisni
 */
public abstract class AbstractOutputPathReformatter implements OutputPathReformatter{
    
    protected ClientRuntime runtime;
  protected ProcessInfo processInfo;
  protected ResponderInfo responderInfo;
  protected Message message;

  public AbstractOutputPathReformatter() {
  }

  public AbstractOutputPathReformatter(
          ClientRuntime runtime, ResponderInfo responderInfo,
          ProcessInfo processInfo, Message message) {
    this.runtime = runtime;
    this.processInfo = processInfo;
    this.responderInfo = responderInfo;
    this.message = message;
  }
  
  @Override
  public void configure(ClientRuntime runtime, ResponderInfo responderInfo,
          ProcessInfo processInfo, Message message) {

    this.runtime = runtime;
    this.processInfo = processInfo;
    this.responderInfo = responderInfo;
    this.message = message;
  }
  
  public ClientRuntime getRuntime() {
    return runtime;
  }

  @Override
  public void setRuntime(ClientRuntime runtime) {
    this.runtime = runtime;
  }

  public ProcessInfo getProcessInfo() {
    return processInfo;
  }

  @Override
  public void setProcessInfo(ProcessInfo processInfo) {
    this.processInfo = processInfo;
  }

  public ResponderInfo getResponderInfo() {
    return responderInfo;
  }

  @Override
  public void setResponderInfo(ResponderInfo responderInfo) {
    this.responderInfo = responderInfo;
  }

  public Message getMessage() {
    return message;
  }

  @Override
  public void setMessage(Message message) {
    this.message = message;
  }

    
    
}
