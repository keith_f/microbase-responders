/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.microbasecloud.prodigal.dataparser;

import com.microbasecloud.prodigal.datamodel.ProdigalCDS;
import com.microbasecloud.prodigal.datamodel.ProdigalDAO;
import com.microbasecloud.prodigal.datamodel.ProdigalReport;
import com.microbasecloud.prodigal.datamodel.ProdigalSequence;
import com.torrenttamer.hibernate.HibernateUtilException;
import com.torrenttamer.hibernate.SessionProvider;
import com.torrenttamer.util.UidGenerator;
import java.io.*;
import java.util.*;
import org.hibernate.Session;

public class ProdigalDatabaseParser {

    public static void main(String[] args) throws FileNotFoundException, ProdigalParserException, HibernateUtilException, Exception {

        ProdigalReport report = new ProdigalReport();
        report.setCdsListSourceFileName("1234");
        File file = new File("/home/prisni/new_outputs.txt");
        System.out.println("Found your file...hooray!\n");
        
        parse(file, report);
    }
    
    public static void parse(File file, ProdigalReport report) 
            throws HibernateUtilException, Exception
    {
        
        SessionProvider prodigalSessionProvider =
                new SessionProvider("/ProdigalResponder-hibernate.cfg.xml");
        Session session = null;
        List<ProdigalCDS> cdsList = new ArrayList<ProdigalCDS>();

        List<ProdigalSequence> sequences = new ArrayList<ProdigalSequence>();
        Map<String, ProdigalCDS> cdss = new HashMap<String, ProdigalCDS>();
        parseCDSFile(report, file, sequences, cdss);
        cdsList.addAll(cdss.values());
        try {

            System.out.println("Reading file\n");
            session = prodigalSessionProvider.getWriteableSession();
            session.beginTransaction();
            ProdigalDAO dao = new ProdigalDAO();
            dao.storeCdss(session, cdsList);
            System.out.println("Committing...");
            session.getTransaction().commit();
            System.out.println("Committed.");

            System.out.println("STORED " + cdsList.size());
        
        } catch (Exception e) {
            SessionProvider.silentRollback(session);
            throw new Exception("Failed to save "
                    + cdsList.size() + " hits to the DB", e);
        } finally {
            SessionProvider.silentClose(session);
        }

    }

    private static void parseCDSFile(ProdigalReport report, File cdsFile, List<ProdigalSequence> sequences, Map<String, ProdigalCDS> cdss)
            throws FileNotFoundException, ProdigalParserException, HibernateUtilException, Exception {

        ProdigalOutputFileDetailExtractor proD = new ProdigalOutputFileDetailExtractor();
        Scanner sc = new Scanner(cdsFile);
        System.out.println("Reading file\n");
        ProdigalSequence currentSequence = null;
        ProdigalCDS currentCds = null;

        while (sc.hasNextLine()) {
            String line = sc.nextLine();

            if (proD.isCurrentLineHeader(line)) {
                currentSequence = new ProdigalSequence();
                currentSequence.setReport(report);
                sequences.add(currentSequence);
                proD.extractHeaderInfo(currentSequence, line);
            } else if (proD.isCurrentLineStrandInfo(line.replaceAll("\\s", ""))) {
                currentCds = new ProdigalCDS();
                currentCds.setParentSequence(currentSequence);
                currentCds.setUniqueID(UidGenerator.generateUid());
                proD.extractStrandInfo(currentCds, line.replaceAll("\\s", ""));
            } else if (proD.isCurrentLineCDSInfo(line.replaceAll("\\s", ""))) {
               
                proD.extractCDSInfo(currentCds, line.replaceAll("\\s", ""));
                cdss.put(currentCds.getCdsID(), currentCds);
            }
        }

        sc.close();

        System.out.println("Found " + sequences.size() + " sequence definitions");
        System.out.println("Found " + cdss.size() + " CDS entries");
    }
}
