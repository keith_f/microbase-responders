/*
 * Copyright 2011 Keith Flanagan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * File created: 20-Sep-2010, 17:48:17
 */

package uk.ac.ncl.mucosa.genomepool.cli;

import com.torrenttamer.hibernate.HibernateUtilException;
import com.torrenttamer.hibernate.SessionProvider;
import java.io.*;
import java.util.Arrays;
import java.util.logging.Logger;
import org.apache.commons.cli.CommandLine;
import org.apache.commons.cli.CommandLineParser;
import org.apache.commons.cli.HelpFormatter;
import org.apache.commons.cli.OptionBuilder;
import org.apache.commons.cli.Options;
import org.apache.commons.cli.ParseException;
import org.apache.commons.cli.PosixParser;
import org.hibernate.Session;
import uk.ac.ncl.mucosa.genomepool.job.ParseDataException;
import uk.ac.ncl.mucosa.genomepool.job.fastq.FastqParserToHibernate;
import uk.ac.ncl.mucosa.genomepool.ws.GenomePoolException;
import uk.org.microbase.filesystem.spi.FSException;
import uk.org.microbase.filesystem.spi.FSOperationNotSupportedException;
import uk.org.microbase.util.file.FileUtils;

/**
 * This program runs the GenBank parser manually, without any involvement
 * with Microbase. This is useful for stand-alone runs, or for testing
 * purposes.
 *
 * @author Keith Flanagan
 */
public class RunGbkParser
{
  private static final Logger logger =
      Logger.getLogger(RunGbkParser.class.getName());

//  private static ClientRuntime runtime;
  private static final String DEFAULT_DB_CONFIG_FILE = "gp-full.properties";

  private static void printHelpExit(Options options)
  {
    printHelpExit(options, null);
  }

  private static void printHelpExit(Options options, String additionalFooter)
  {
    HelpFormatter formatter = new HelpFormatter();
    String cmdSyntax = "run_gbk_parser.sh";
    String header = "";
    StringBuilder footer = new StringBuilder();
    footer.append("This program runs the GenBank parser manually, without any "
        + "involvement with Microbase. This is useful for stand-alone runs, or "
        + "for testing purposes.\n\n"
        + "  - You must specify an input file to use in GBK format\n"
        + "Depending on your output requirements, you may specifiy one or more "
        + "of the following options:\n"
        + "  - a Java resource path to a configuration file somewhere on your "
        + "CLASSPATH. This file should contain configuration for a relational "
        + "database if you require parsed output to be inserted into a "
        + "structured database. (optional)\n");
//        + "  - a FASTA output file for holding the extracted DNA sequences (optional)\n"
//        + "  - a FASTA output file for holding the protein sequences of "
//        + "annotated CDS features (optional).");
    if (additionalFooter != null)
    {
      footer.append("\n").append(additionalFooter);
    }
    int width = 80;
    //formatter.printHelp( "notification.sh", options );
    formatter.printHelp(width, cmdSyntax, header, options, footer.toString());
    System.exit(0);
  }


  public static void main(String[] args)
      throws GenomePoolException, FSOperationNotSupportedException, FSException
  {
    CommandLineParser parser = new PosixParser();

    Options options = new Options();
    
    options.addOption(OptionBuilder
        .withLongOpt("gbk-in-file")
        //.withArgName( "ID" )
        .hasArgs(1)
        .withDescription(
        "Specifies the FASTQ input file to use")
        .create( "q" ));

    options.addOption(OptionBuilder
        .withLongOpt("db-config")
        //.withArgName( "ID" )
        .hasArgs(1)
        .withDescription(
        "Specifies a resouce path to a configuration file for a relational database for storing"
        + " parsed output. "
        + "(default: "+DEFAULT_DB_CONFIG_FILE+")")
        .create( "D" ));
    
//    options.addOption(OptionBuilder
//        .withLongOpt("fasta-out")
//        //.withArgName( "ID" )
//        .hasArgs(1)
//        .withDescription(
//        "Specifies a FASTA output file to create")
//        .create( "f" ));
    
//    options.addOption(OptionBuilder
//        .withLongOpt("dna-fasta-out")
//        //.withArgName( "ID" )
//        .hasArgs(1)
//        .withDescription(
//        "Specifies a DNA FASTA output file to create for storing parsed reads")
//        .create( "f" ));

//    options.addOption(OptionBuilder
//        .withLongOpt("translated-fasta-out")
//        //.withArgName( "ID" )
//        .hasArgs(1)
//        .withDescription(
//        "Specifies a protein FASTA-formatted output file to create for "
//        + "storing 6-frame translations of the parsed reads")
//        .create( "t" ));
    
    if (args.length == 0)
    {
      printHelpExit(options);
    }

    File gbkInFile = null;
    String dbConfigResource = null;
//    File dnaFastaOutFile = null;
//    File translationFastaOutFile = null;
    try
    {
      CommandLine line = parser.parse(options, args);

      System.out.println(Arrays.asList(line.getArgs()));
      System.out.println(Arrays.asList(line.getOptions()));

      if (line.hasOption("gbk-in-file"))
      {
        gbkInFile= new File(line.getOptionValue("gbk-in-file"));
      }
      if (line.hasOption("db-config"))
      {
        dbConfigResource = line.getOptionValue("db-config");
      }
//      if (line.hasOption("dna-fasta-out"))
//      {
//        dnaFastaOutFile = new File(line.getOptionValue("dna-fasta-out"));
//      }
//      if (line.hasOption("translated-fasta-out"))
//      {
//        translationFastaOutFile = new File(line.getOptionValue("translated-fasta-out"));
//      }

      if (gbkInFile == null)
      {
        printHelpExit(options, "You must specify a GenBank-formatted input file");
      }
      if (dbConfigResource == null)
      {
        printHelpExit(options, "You must specify a database configuration "
            + "resource file");
      }
      
      doParse(gbkInFile, dbConfigResource);
    }
    catch(ParseException e)
    {
      //Command line parse failure
      e.printStackTrace();
      printHelpExit(options);
    }
    catch(ParseDataException e)
    {
      //Data parse failure
      e.printStackTrace();
      System.out.println("Something went wrong while trying to parse the input file.");
    }
    catch(HibernateUtilException e)
    {
      e.printStackTrace();
      System.out.println("Failed to perform a database operation.");
    }
    finally
    {
    }

  }

  private static void doParse(
      File gbkInFile, String dbConfigResource)
      throws ParseDataException, HibernateUtilException
  {
    InputStream fastqStream = null;
    OutputStream dnaFastaStream = null;
    OutputStream translationFastaStream = null;
    Session session = null;
    throw new RuntimeException("Not yet implemented");
//    try
//    {
//      
//      if (fastqInFile != null)
//      {
//        fastqStream = new FileInputStream(fastqInFile);
//      }
//      if (dnaFastaOutFile != null)
//      {
//        dnaFastaStream = new FileOutputStream(dnaFastaOutFile);
//      }
//      if (translationFastaOutFile != null)
//      {
//        translationFastaStream = new FileOutputStream(translationFastaOutFile);
//      }
//      
//      
//      if (dbConfigResource != null)
//      {
//        SessionProvider gpSessionProvider = new SessionProvider(dbConfigResource);
//        session = gpSessionProvider.getWriteableSession();
//      }
//
//      if (session != null)
//      {
//        session.beginTransaction();
//      }
//      FastqParser parser = new FastqParser();
//      parser.parse(
//          fastqStream, session, dnaFastaStream, translationFastaStream,
//          fastqInFile.getName(), fastqInFile.getAbsolutePath());
//      if (session != null)
//      {
//        session.getTransaction().commit();
//      }
//    }
//    catch(IOException e)
//    {
//      throw new ParseDataException("Failed to create file streams", e);
//    }
//    catch(HibernateUtilException e)
//    {
//      throw new ParseDataException("Failed to create a database connection", e);
//    }
//    finally
//    {
//      FileUtils.closeInputStreams(fastqStream);
//      FileUtils.closeOutputStreams(dnaFastaStream, translationFastaStream);
//      SessionProvider.silentClose(session);
//    }
  }

}
