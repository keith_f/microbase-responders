/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: Jan 22, 2012, 2:01:23 PM
 */

package uk.ac.ncl.mucosa.genomepool.ws;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.codehaus.jackson.map.ObjectMapper;
import uk.ac.ncl.mucosa.genomepool.full.data.Gene;
import uk.ac.ncl.mucosa.genomepool.full.data.GenomeFragment;
import uk.ac.ncl.mucosa.genomepool.ws.data.GeneWs;
import uk.ac.ncl.mucosa.genomepool.ws.data.GenomeFragmentWs;

/**
 *
 * @author Keith Flanagan
 */
public class DbToWsConverters
{
  private static ObjectMapper mapper = new ObjectMapper();
  
  public static GenomeFragmentWs dbToWs(GenomeFragment db) throws IOException
  {
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    mapper.writeValue(baos, db);
    return mapper.readValue(baos.toByteArray(), GenomeFragmentWs.class);
  }
  
  public static List<GenomeFragmentWs> fragmentDbToWs(List<GenomeFragment> dbs) 
      throws IOException
  {
    List<GenomeFragmentWs> wss = new ArrayList<GenomeFragmentWs>(dbs.size());
    for (GenomeFragment db : dbs)
    {
      wss.add(dbToWs(db));
    }
    return wss;
  }
  
  public static GeneWs dbToWs(Gene db) throws IOException
  {
    ByteArrayOutputStream baos = new ByteArrayOutputStream();
    mapper.writeValue(baos, db);
    return mapper.readValue(baos.toByteArray(), GeneWs.class);
  }
  
  public static List<GeneWs> geneDbToWs(List<Gene> dbs) 
      throws IOException
  {
    List<GeneWs> wss = new ArrayList<GeneWs>(dbs.size());
    for (Gene db : dbs)
    {
      wss.add(dbToWs(db));
    }
    return wss;
  }
}
