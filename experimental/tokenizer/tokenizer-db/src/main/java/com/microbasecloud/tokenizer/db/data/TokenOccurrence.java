/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package com.microbasecloud.tokenizer.db.data;

import java.io.Serializable;

/**
 * Stores information about the location of a particular protein window
 *
 * @author Keith Flanagan
 */
public class TokenOccurrence
    implements Serializable
{
  private String token;
  private int tokenId;
  private int taxonId;
//  private String fragmentGuid;
//  private String geneGuid;
//  private String geneProductGuid;

  
  private int productGi;
  private int startCoord; //Start coordinate within product sequence
//  private int tokenLength;

  public TokenOccurrence()
  {
    tokenId = -1;
  }

  /**
   * Builds a String that uniquely identifies this instance. Typically useful
   * for use as Cassandra column names, where the serialised form of the
   * instances can be used as a Cassandra column value.
   *
   * @return
   */
//  public String uniqueString()
//  {
//    StringBuilder sb = new StringBuilder("[");
//    sb.append(token).append(";").
//        append(taxonId).append(";").append(fragmentGuid).append(";").
//        append(startCoord).append(";").append(tokenLength);
//    sb.append("]");
//    return sb.toString();
//  }

  public int getStartCoord()
  {
    return startCoord;
  }

  public void setStartCoord(int startCoord)
  {
    this.startCoord = startCoord;
  }

  public int getTokenId()
  {
    return tokenId;
  }

  public void setTokenId(int tokenId)
  {
    this.tokenId = tokenId;
  }
  
  

  public String getToken()
  {
    return token;
  }

  public void setToken(String token)
  {
    this.token = token;
  }

//  public int getTokenLength()
//  {
//    return tokenLength;
//  }
//
//  public void setTokenLength(int tokenLength)
//  {
//    this.tokenLength = tokenLength;
//  }

//  public String getFragmentGuid()
//  {
//    return fragmentGuid;
//  }
//
//  public void setFragmentGuid(String fragmentGuid)
//  {
//    this.fragmentGuid = fragmentGuid;
//  }
//
//  public String getGeneGuid()
//  {
//    return geneGuid;
//  }
//
//  public void setGeneGuid(String geneGuid)
//  {
//    this.geneGuid = geneGuid;
//  }

  public int getTaxonId()
  {
    return taxonId;
  }

  public void setTaxonId(int taxonId)
  {
    this.taxonId = taxonId;
  }

  public int getProductGi()
  {
    return productGi;
  }

  public void setProductGi(int productGi)
  {
    this.productGi = productGi;
  }

//  public String getGeneProductGuid()
//  {
//    return geneProductGuid;
//  }
//
//  public void setGeneProductGuid(String geneProductGuid)
//  {
//    this.geneProductGuid = geneProductGuid;
//  }

  

}
