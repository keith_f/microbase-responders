/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.microbasecloud.prodigal.responder;

import com.microbase.responder.prodigalcdscommon.ProdigalJobDescription;
import uk.org.microbase.responder.util.OutputPathReformatter;

/**
 *
 * @author prisni
 */
public interface ProdigalOutputPathReformatter 
extends OutputPathReformatter
{
  public void setJobDescription(ProdigalJobDescription job);
}

