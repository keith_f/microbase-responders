/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.microbase.responder.aligner;

import com.microbase.responder.abacascommon.AlignmentJobDescription;
import com.microbase.responder.abacascommon.AlignmentJobResult;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Logger;
import uk.org.microbase.dist.responder.RegistrationException;
import uk.org.microbase.dist.responder.ResponderInfo;
import uk.org.microbase.filesystem.spi.MBFile;
import uk.org.microbase.filesystem.spi.MicrobaseFS;
import uk.org.microbase.notification.data.Message;
import uk.org.microbase.responder.spi.AbstractMessageProcessor;
import uk.org.microbase.responder.spi.ProcessingException;
import uk.org.microbase.responder.spi.ResponderInfoPropertiesParser;
import uk.org.microbase.util.cli.NativeCommandExecutor;

/**
 *
 * @author prisni
 */
public class AbacasResponder extends AbstractMessageProcessor {

    private static final Logger l = Logger.getLogger(AbacasResponder.class.getName());
    /**
     * Configuration resource for this responder. A file with this name must be
     * located on the classpath.
     */
    static final String CONFIG_RESOURCE = AbacasResponder.class.getSimpleName() + ".properties";
    private static final String CONFIG_RESULT_NAME_REFORMATTER = "result_name_reformatter";
    /*
     * Configuration options (these should be treated as 'final'
     */
    private Properties config;
    private AlignmentOutputPathReformatter resultFilenameReformatter;
    private AlignmentOutputPathReformatter stdOutFilenameReformatter;
    private AlignmentOutputPathReformatter stdErrFilenameReformatter;
    /*
     * Per-job variables. These are set every time <code>preRun</code> is called.
     */
    private AlignmentJobDescription jobDescr;
    private MBFile jobRemoteResultFile;
    private MBFile jobStdOutLogFile;
    private MBFile jobStdErrLogFile;

    public AbacasResponder() {

        /*
         * Set some default file name formatters here.
         * TODO eventually, we'll want these to be configurable.
         */
        resultFilenameReformatter = new AlignmentResultInIndividualDirsFormatter();
        stdOutFilenameReformatter = new AlignmentStdOutLogFileFormatter();
        stdErrFilenameReformatter = new AlignmentStdErrLogFileFormatter();

    }

    @Override
    protected ResponderInfo createDefaultResponderInfo() throws RegistrationException {
        try {
            config = new Properties();
            // Use properties from the Microbase global configuration file, if present
            config.putAll(getRuntime().getRawConfigProperties());
            try (InputStream is = getRuntime().getClassLoader().getResourceAsStream(CONFIG_RESOURCE)) {
                if (is == null) {
                    throw new RegistrationException("Failed to find configuration resource: " + CONFIG_RESOURCE);
                }
                config.load(is);
            }

            ResponderInfo info = ResponderInfoPropertiesParser.parseFromProperties(config);

            l.info("Responder config: " + info);
            return info;
        } catch (Exception e) {
            throw new RegistrationException(
                    "Failed to load responder configuration", e);
        }
    }

    public void preRun(Message message) throws ProcessingException {
        l.info("Doing preRun");
        try {
            /*
             * Parse message as a responder specific job description.
             */
            jobDescr = parseMessageWithJsonContent(message, AlignmentJobDescription.class);

            /*
             * Configure the result file formatters
             */
            resultFilenameReformatter.configure(getRuntime(), getResponderInfo(), getProcessInfo(), message);
            resultFilenameReformatter.setJobDescription(jobDescr);
            stdOutFilenameReformatter.configure(getRuntime(), getResponderInfo(), getProcessInfo(), message);
            stdOutFilenameReformatter.setJobDescription(jobDescr);
            stdErrFilenameReformatter.configure(getRuntime(), getResponderInfo(), getProcessInfo(), message);
            stdErrFilenameReformatter.setJobDescription(jobDescr);

            jobRemoteResultFile = resultFilenameReformatter.getFormattedRemoteFile();
            jobStdOutLogFile = stdOutFilenameReformatter.getFormattedRemoteFile();
            jobStdErrLogFile = stdErrFilenameReformatter.getFormattedRemoteFile();

        } catch (Exception e) {
            throw new ProcessingException("Failed to parse message", e);
        }
    }

    public void cleanupPreviousResults(Message msg) throws ProcessingException {
        l.info("Doing cleanup");
        try {
            MicrobaseFS fs = getRuntime().getMicrobaseFS();

            if (fs.exists(jobRemoteResultFile)) {
                l.info("Found an existing result file generated by a previous "
                        + "execution. Deleting...");
                fs.deleteRemoteCopy(jobRemoteResultFile);
            }
        } catch (Exception e) {
            throw new ProcessingException("Failed to perform cleanup", e);
        }
    }

    /**
     *
     * @param msg
     * @return
     * @throws ProcessingException
     */
    @Override
    public Set<Message> processMessage(Message msg) throws ProcessingException {
        {
            l.info("Performing computational work");
            try {
                MBFile queryRemoteFile = jobDescr.getQueryFile();
                MBFile referenceRemoteFile = jobDescr.getReferenceFile();
               
                l.info("Query file is: " + queryRemoteFile);
                
                File querySeqFile = downloadFileToSharedSpace(queryRemoteFile);
                File referenceSeqFile = downloadFileToSharedSpace(referenceRemoteFile);

                l.info("Running command ...");

                File[] outputsLocalFile = runAbacasAligner(referenceSeqFile, querySeqFile);

                l.info("Uploading completed alignments: " + outputsLocalFile[0].getAbsolutePath()
                        + " to: " + jobRemoteResultFile);

                getRuntime().getMicrobaseFS().upload(
                        outputsLocalFile[0], jobRemoteResultFile, null);

                getRuntime().getMicrobaseFS().upload(
                        outputsLocalFile[1], jobRemoteResultFile, null);

                getRuntime().getMicrobaseFS().upload(
                        outputsLocalFile[2], jobRemoteResultFile, null);

                getRuntime().getMicrobaseFS().upload(
                        outputsLocalFile[3], jobRemoteResultFile, null);

                getRuntime().getMicrobaseFS().upload(
                        outputsLocalFile[4], jobRemoteResultFile, null);

                getRuntime().getMicrobaseFS().upload(
                        outputsLocalFile[5], jobRemoteResultFile, null);

                Message alignmentMsg = generateSuccessNotification(msg, jobDescr); //generate success notification
                Set<Message> messages = new HashSet(); //create a hash set of notification messages 
                messages.add(alignmentMsg); //add new success messages to the hash set
                return messages;
            } catch (Exception e) {
                throw new ProcessingException("Failed to run one or more commands: "
                        + "on: " + getRuntime().getNodeInfo().getHostname()
                        + "in: " + getWorkingDirectory(), e);

            }

        }
    }

    private File[] runAbacasAligner(File refFile, File qryFile) throws ProcessingException {

        String[] abacasCommand = new String[]{
            "/mnt/responder_data/abacas-mer/runabacas.sh", refFile.getAbsolutePath(),
            qryFile.getAbsolutePath()};

        executeCommand(abacasCommand, "abacas");
        File[] resultFiles = getWorkingDirectory().listFiles();
        return resultFiles;
    }

    private Message generateSuccessNotification(Message parent, AlignmentJobDescription job)
            throws IOException {
        /*
         * Prepare the content for the 'success' notification message.
         */

        AlignmentJobResult resultContent = new AlignmentJobResult();
        resultContent.setJob(job);
        resultContent.setResultFile(jobRemoteResultFile);
        resultContent.setStdErrLogFile(jobStdErrLogFile);
        resultContent.setStdOutLogFile(jobStdOutLogFile);

        /*
         * Create the success message with the above content. Link this success
         * message to the parent message (the job descriptor) for provenance.
         */
        Message successMsg = createMessageWithJsonContent(
                parent, //The message to use as a parent
                getResponderInfo().getTopicOut(), //The topic of the new message
                parent.getWorkflowStepId(), //The workflow step ID
                resultContent, //The result content item
                //An optional human-readable description
                "Successfully completed alignment of reads.");

        return successMsg;
    }

    private void executeCommand(String[] command, String commandType)
            throws ProcessingException {
        File workDir = getWorkingDirectory();
        StringBuilder namePrefix = new StringBuilder();
        namePrefix.append(getProcessInfo().getMessageId()).append(".");
        namePrefix.append(getProcessInfo().getProcessGuid()).append(".");
        namePrefix.append(getProcessInfo().getHostname()).append(".");
        namePrefix.append(commandType).append(".");
        File stdOutFile = new File(workDir, namePrefix.toString() + "stdout.txt");
        File stdErrFile = new File(workDir, namePrefix.toString() + "stderr.txt");
        try {
            FileWriter stdOut = new FileWriter(stdOutFile);
            FileWriter stdErr = new FileWriter(stdErrFile);

            int exitStatus = NativeCommandExecutor.executeNativeCommand(
                    workDir, command, stdOut, stdErr);

            stdOut.flush();
            stdOut.close();
            stdErr.flush();
            stdErr.close();

            if (exitStatus != 0) {
                throw new ProcessingException("Exit status of the command: "
                        + Arrays.asList(command)
                        + " \nwas " + exitStatus
                        + ". Assuming that non-zero means that execution failed.");
            }
        } catch (Exception e) {
            throw new ProcessingException("Failed to execute command: " + command[0], e);
        } finally {
            try {
                getRuntime().getMicrobaseFS().upload(stdOutFile, jobStdOutLogFile, null);
                getRuntime().getMicrobaseFS().upload(stdErrFile, jobStdErrLogFile, null);
            } catch (Exception e) {
                throw new ProcessingException(
                        "Failed to upload STDOUT/STDERR content", e);
            }
        }
    }
}
