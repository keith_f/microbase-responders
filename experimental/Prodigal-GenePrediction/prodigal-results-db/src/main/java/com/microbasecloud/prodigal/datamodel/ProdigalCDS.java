/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.microbasecloud.prodigal.datamodel;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author prisni
 */

@Entity
@Table (name = "prodigal_cds")

public class ProdigalCDS implements Serializable {
  @Id
  @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;
    private String uniqueID;

    public String getUniqueID() {
        return uniqueID;
    }

    public void setUniqueID(String uniqueID) {
        this.uniqueID = uniqueID;
    }
    
  
    private String cdsStartLoc = new String();
    private String cdsEndLoc = new String();
    private String cdsID = new String();
    private String cdsPartial;
    private String cdsStartCodon = new String();
    private String cdsRBSmotif = new String();
    private String cdsRBSspacer = new String();
    private String cdsGCcontent;
    private String cdsConf;
    private String cdsScore;
    private String cdsCscore;
    private String cdsSscore;
    private String cdsRscore;
    private String cdsUscore;
    private String cdsTscore;
    private boolean compliment;

    
    
    /*
     * 
     */
    @ManyToOne(cascade = CascadeType.ALL, optional = false)
    private ProdigalSequence parentSequence; 

    @Override
    public String toString() {
        return "ProdigalCDS{" + "id=" + id + ", cdsStartLoc=" + cdsStartLoc +
                ", cdsEndLoc=" + cdsEndLoc + ", cdsID=" + cdsID +
                ", cdsPartial=" + cdsPartial + ", cdsStartCodon=" +
                cdsStartCodon + ", cdsRBSmotif=" + cdsRBSmotif + 
                ", cdsRBSspacer=" + cdsRBSspacer + ", cdsGCcontent=" +
                cdsGCcontent + ", cdsConf=" + cdsConf + ", cdsScore=" +
                cdsScore + ", cdsCscore=" + cdsCscore + ", cdsSscore=" +
                cdsSscore + ", cdsRscore=" + cdsRscore + ", cdsUscore=" +
                cdsUscore + ", cdsTscore=" + cdsTscore + '}';
    }

    public String getCdsStartLoc() {
        return cdsStartLoc;
    }

    public void setCdsStartLoc(String cdsStartLoc) {
        this.cdsStartLoc = cdsStartLoc;
    }

    public String getCdsEndLoc() {
        return cdsEndLoc;
    }

    public void setCdsEndLoc(String cdsEndLoc) {
        this.cdsEndLoc = cdsEndLoc;
    }

    public String getCdsID() {
        return cdsID;
    }

    public void setCdsID(String cdsID) {
        this.cdsID = cdsID;
    }

    public String getCdsPartial() {
        return cdsPartial;
    }

    public void setCdsPartial(String cdsPartial) {
        this.cdsPartial = cdsPartial;
    }

    public String getCdsStartCodon() {
        return cdsStartCodon;
    }

    public void setCdsStartCodon(String cdsStartCodon) {
        this.cdsStartCodon = cdsStartCodon;
    }

    public String getCdsRBSmotif() {
        return cdsRBSmotif;
    }

    public void setCdsRBSmotif(String cdsRBSmotif) {
        this.cdsRBSmotif = cdsRBSmotif;
    }

    public String getCdsRBSspacer() {
        return cdsRBSspacer;
    }

    public void setCdsRBSspacer(String cdsRBSspacer) {
        this.cdsRBSspacer = cdsRBSspacer;
    }

    public String getCdsGCcontent() {
        return cdsGCcontent;
    }

    public void setCdsGCcontent(String cdsGCcontent) {
        this.cdsGCcontent = cdsGCcontent;
    }

    public String getCdsConf() {
        return cdsConf;
    }

    public void setCdsConf(String cdsConf) {
        this.cdsConf = cdsConf;
    }

    public String getCdsScore() {
        return cdsScore;
    }

    public void setCdsScore(String cdsScore) {
        this.cdsScore = cdsScore;
    }

    public String getCdsCscore() {
        return cdsCscore;
    }

    public void setCdsCscore(String cdsCscore) {
        this.cdsCscore = cdsCscore;
    }

    public String getCdsSscore() {
        return cdsSscore;
    }

    public void setCdsSscore(String cdsSscore) {
        this.cdsSscore = cdsSscore;
    }

    public String getCdsRscore() {
        return cdsRscore;
    }

    public void setCdsRscore(String cdsRscore) {
        this.cdsRscore = cdsRscore;
    }

    public String getCdsUscore() {
        return cdsUscore;
    }

    public void setCdsUscore(String cdsUscore) {
        this.cdsUscore = cdsUscore;
    }

    public String getCdsTscore() {
        return cdsTscore;
    }

    public void setCdsTscore(String cdsTscore) {
        this.cdsTscore = cdsTscore;
    }
    

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ProdigalSequence getParentSequence() {
        return parentSequence;
    }

    public void setParentSequence(ProdigalSequence parentSequence) {
        this.parentSequence = parentSequence;
    }

    public boolean isCompliment() {
        return compliment;
    }

    public void setCompliment(boolean compliment) {
        this.compliment = compliment;
    }
    
    
}
