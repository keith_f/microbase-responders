package uk.ac.ncl.mucosa.genomepool.full.data;

import java.io.Serializable;

/**
 * May be used for querying the genome pool database. 
 */
public enum FragmentType
    implements Serializable
{
  plasmid ("plasmid"),
  chromosome ("chromosome"),
  organelle ("organelle"), 
  /**
   * NCBI uses this for bacterial chromosomes
   */
  genome ("genome"),
  unknown ("unknown");


  private final String databaseText;
  FragmentType(String databaseText)
  {
    this.databaseText = databaseText;
  }

  public String getDatabaseText()
  {
    return databaseText;
  }

}
