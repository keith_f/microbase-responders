package uk.ac.ncl.mucosa.genomepool.full.dao;
import uk.ac.ncl.mucosa.genomepool.GenomePoolDbException;
import java.sql.Connection;
import java.util.List;
import java.util.logging.Logger;
import uk.ac.ncl.mucosa.genomepool.full.data.Gene;
import uk.ac.ncl.mucosa.genomepool.full.data.GenomeFragment;


/**
 *
 * @author Keith Flanagan
 *
 */
public class GenomePoolDAO
    //extends CollectionDAO<?>
{
  private static final Logger logger =
      Logger.getLogger(GenomePoolDAO.class.getName());

  public GenomePoolDAO()
  {
  }

  public void insert(Connection txn, String msgGuid,
      String workflowExeId, String stepId,
      String bucket, String path, String filename,
      GenomeFragment frag, List<Gene> genes, String dnaData)
      throws GenomePoolDbException
  {
    try
    {
      GenomeFragmentInserter.insert(txn, msgGuid, 
          workflowExeId, stepId, 
          bucket, path, filename,
          frag, genes, dnaData);
//      txn.commit();
    }
    catch(Exception e)
    {
      throw new GenomePoolDbException("Failed to insert fragment: "+frag, e);
    }
  }
  
  public void removeFragment(Connection txn, String fragmentGuid)
      throws GenomePoolDbException
  {
    try
    {
      GenomeFragmentRemover.removeGenomeFragment(txn, fragmentGuid);
//      txn.commit();
    }
    catch(Exception e)
    {
      throw new GenomePoolDbException(
          "Failed to remove fragment: "+fragmentGuid, e);
    }
  }

  public void removeFragmentByMessage(Connection txn, String msgGuid)
      throws GenomePoolDbException
  {
    try
    {
      GenomeFragmentRemover.removeByMessageGuid(txn, msgGuid);
//      txn.commit();
    }
    catch(Exception e)
    {
      throw new GenomePoolDbException(
          "Failed to remove fragment generated from message: "+msgGuid, e);
    }
  }
}
