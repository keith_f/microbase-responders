/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.microbasecloud.genbankresponder;

import com.microbase.responder.genbankcommon.GenbankJobDescription;
import uk.org.microbase.filesystem.spi.MBFile;

/**
 *
 * @author prisni
 */
public class GenbankResultInIndividualDirsFormatter 
extends AbstractOutputPathReformatter
    implements GenbankOutputPathReformatter {

    private GenbankJobDescription jobDescription;
  
  @Override
  public String getFormattedBucketName() {
    return responderInfo.getResultDestinationBucket();
  }

  @Override
  public String getFormattedPath() {
    StringBuilder path = new StringBuilder(responderInfo.getResultDestinationBasePath());
    path.append("/").append(getFormattedFilename());
    return path.toString();
  }

  @Override
  public String getFormattedFilename() {
    StringBuilder resultFilename = new StringBuilder();
//    resultFilename.append(jobDescription.getInputFiles().getName())
//        .append("-").append(jobDescription.getDatabaseName())
//        .append(".").append(jobDescription.getJobType().name());
    return resultFilename.toString();
  }
  
  @Override
  public MBFile getFormattedRemoteFile() {
    MBFile file = new MBFile(getFormattedBucketName(), getFormattedPath(), getFormattedFilename());
    return file;
  }
  

  public GenbankJobDescription getJobDescription() {
    return jobDescription;
  }

  @Override
  public void setJobDescription(
          GenbankJobDescription jobDescription) {
    this.jobDescription = jobDescription;
  }
    
}
