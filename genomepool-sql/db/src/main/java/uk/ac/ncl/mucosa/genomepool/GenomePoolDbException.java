/*
 * Copyright 2010 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: 17-Sep-2010, 16:03:30
 */

package uk.ac.ncl.mucosa.genomepool;

/**
 *
 * @author Keith Flanagan
 */
public class GenomePoolDbException
    extends Exception
{
  public GenomePoolDbException(Throwable cause)
  {
    super(cause);
  }

  public GenomePoolDbException(String message, Throwable cause)
  {
    super(message, cause);
  }

  public GenomePoolDbException(String message)
  {
    super(message);
  }

  public GenomePoolDbException()
  {
  }

}
