/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package com.microbasecloud.tokenizer.cli;

import com.microbasecloud.tokenizer.common.TokenJobDescription;
import com.torrenttamer.jdbcutils.DatabaseConfiguration;
import com.torrenttamer.jdbcutils.DistributedJdbcPool;
import com.torrenttamer.jdbcutils.DistributedJdbcPoolFactory;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import org.codehaus.jackson.map.ObjectMapper;
import uk.org.microbase.filesystem.s3.AmazonFS;
import uk.org.microbase.filesystem.spi.FSException;
import uk.org.microbase.filesystem.spi.FSOperationNotSupportedException;
import uk.org.microbase.filesystem.spi.FileMetaData;
import uk.org.microbase.filesystem.spi.MBDirectory;
import uk.org.microbase.filesystem.spi.MBFile;
import uk.org.microbase.filesystem.spi.MicrobaseFS;
import uk.org.microbase.notification.db.MicrobaseNotificationSQLImpl;
import uk.org.microbase.notification.spi.MicrobaseNotification;
import uk.org.microbase.notification.spi.NotificationException;
import uk.org.microbase.responder.spi.AbstractMessageProcessor;

/**
 * Generates notification messages for each protein FASTA file found in a
 * set of subdirectories under a specified root.
 * The subdirectory name is take as the organism name.
 * The filename is taken as the fragment name.
 * @author Keith Flanagan
 */
public class GenMessagesForDir
{
  private static final String NOTIFICATION_CONFIG_RESOURCE = "NotificationSqlConfiguration.properties";
  
  private static ObjectMapper mapper = new ObjectMapper();
  private static MicrobaseNotification notif;
  
  public static void main(String[] args) throws FSOperationNotSupportedException, FSException, Exception
  {
    if (args.length != 4) {
      System.out.println("USAGE:");
      System.out.println("  * AWS access key");
      System.out.println("  * AWS secret key");
      System.out.println("  * Bucket name");
      System.out.println("  * Root path");
      System.exit(1);
    }
    
    String accessKey = args[0];
    String secretKey = args[1];
    String bucketName = args[2];
    String rootPath = args[3];
    
    notif = connectNotification();
    
    Properties customConfig = new Properties();
    customConfig.setProperty("access_key", accessKey);
    customConfig.setProperty("secret_key", secretKey);
    customConfig.setProperty("cache_directory", ".");
    MicrobaseFS mbfs = new AmazonFS(customConfig);
    
    MBDirectory root = new MBDirectory(bucketName, rootPath);
//    Set<FileMetaData> listing = mbfs.listRemoteFileMetaData(root);
    Set<String> dirPaths = mbfs.listRemoteFilenames(root, true, true);
    System.out.println("Found "+dirPaths.size()+" remote directories.");
    for (String dirPath : dirPaths) {
      MBDirectory orgDir = new MBDirectory(bucketName, dirPath);
      if (dirPath.endsWith("/")) {
        dirPath = dirPath.substring(0, dirPath.length()-1);
      }
      String dirName = dirPath.substring(dirPath.lastIndexOf("/")+1);
//      System.out.println("Dirname: "+dirName);
      
      String organismName = dirName;
      
      System.out.println("Organism: "+organismName);
      //List content of organisms directory
      for (FileMetaData fastaFile: mbfs.listRemoteFileMetaData(orgDir)) {
        String filename = fastaFile.getLocation().getName();
        System.out.println("  * "+filename);
        generateNotification(organismName, fastaFile.getLocation());
      }
    }
    
  }
  
  private static void generateNotification(String orgName, MBFile fragmentFile)
          throws IOException, NotificationException
  {
    TokenJobDescription job = new TokenJobDescription();
    job.setFragmentFile(fragmentFile);
    job.setFragmentGuid(fragmentFile.getName());
    job.setGraphName("apta_test");
    job.setGraphBranch("trunk");
    job.setOrganismGuid(orgName);
    job.setTokenSize(15);
    
    String publisherGuid = GenMessagesForDir.class.getName();
    String topicGuid = "TOKEN-JOB-DESCRIPTION";
    Map<String, String> content = new HashMap<>();
    
    String jsonContent = mapper.writeValueAsString(job);
    content.put(AbstractMessageProcessor.PROP__MAIN_MSG_CONTENT, jsonContent);
    System.out.println("publishing: "+jsonContent);
    notif.publish(publisherGuid, topicGuid, content, null);
  }
  
  private static MicrobaseNotification connectNotification() throws Exception
  {
    ClassLoader cl = GenMessagesForDir.class.getClassLoader();

    try {
      System.out.println("Reading resource: "+NOTIFICATION_CONFIG_RESOURCE);
      DatabaseConfiguration dbConf = DistributedJdbcPoolFactory.
            readConfigurationFromPropertyResource(cl, NOTIFICATION_CONFIG_RESOURCE);
      DistributedJdbcPool pool = DistributedJdbcPoolFactory.createSingleWriteMultipleReplicaPool(dbConf);
      MicrobaseNotification notif = new MicrobaseNotificationSQLImpl(GenMessagesForDir.class.getName(), pool);
      System.out.println("Successfully configured the notification system.");
      return notif;
    }
    catch(Exception e) {
      System.out.println("Failed to configure the Notification system based on "
              + "the configuration files in this directory: "+e.getMessage());
      throw e;
    }
  }
}
