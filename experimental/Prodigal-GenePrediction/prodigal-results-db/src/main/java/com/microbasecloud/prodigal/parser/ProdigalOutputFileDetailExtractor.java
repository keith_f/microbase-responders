/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.microbasecloud.prodigal.parser;

import com.microbasecloud.prodigal.datamodel.ProdigalCDS;
import com.microbasecloud.prodigal.datamodel.ProdigalReport;
import com.microbasecloud.prodigal.datamodel.ProdigalSequence;
import com.microbasecloud.prodigal.datamodel.ProdigalTranslation;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.*;

/**
 *
 * @author prisni
 */


public class ProdigalOutputFileDetailExtractor {

    String str = new String();
   
    //extracts header info about the diffeent sequences queried 
    public boolean isCurrentLineHeader(String line1) {
        return line1.startsWith("DEFINITION");
    }

    public void extractHeaderInfo(ProdigalSequence seqData, String s) throws ProdigalParserException {
        if (!s.startsWith("DEFINITION")) {
            throw new ProdigalParserException("Expected a DEFINITION line. Recieved a: " + s);
        }
        System.out.println("\n \nFile contains information about the sequences labelled: \n");

        seqData.setSeqNum(Integer.parseInt(seqDat(s, "seqnum=")));
        System.out.println("Sequence number is: " + seqData.getSeqNum() + "\n");
        seqData.setSeqLength(Integer.parseInt(seqDat(s, "seqlen=")));
        System.out.println("Sequence length is: " + seqData.getSeqLength() + "\n");
        String seqS = seqDat(s, "seqhdr=").replaceAll("\"", "");
        seqData.setGiNum(Long.parseLong(extractGI(seqS)));
        System.out.println("GI no. is: " + seqData.getGiNum() + "\n");
        seqData.setGbkNum(extractGBKNo(seqS));
        System.out.println("GBK accession no: " + seqData.getGbkNum() + "\n");
//        seqData.setSpecies(extractSeqSpecies(seqS));
//        System.out.println("Species details are: " + seqData.getSpecies() + "\n");
//        seqData.setContigNum(extractContigInfo(seqS));
//        System.out.println("Contig no. : " + seqData.getContigNum() + "\n");
        seqData.setProdigalVer(seqDat(s, "version="));
        System.out.println("Prodigal version is: " + seqData.getProdigalVer() + "\n");
        seqData.setRunType(seqDat(s, "run_type="));
        System.out.println("Sequence run type is: " + seqData.getRunType() + "\n");
        seqData.setModel(seqDat(s, "model=").replaceAll("\"", ""));
        System.out.println("Sequence model is: " + seqData.getModel() + "\n");
        seqData.setSeqGCcontent(Double.parseDouble(seqDat(s, "gc_cont=")));
        System.out.println("Sequence gc content is: " + seqData.getSeqGCcontent() + "\n");
        seqData.setTransTable(Integer.parseInt(seqDat(s, "transl_table=")));
        System.out.println("Sequence uses translation table no. : " + seqData.getTransTable() + "\n");
        seqData.setUsesSd(Boolean.parseBoolean(seqDat(s, "uses_sd=")));
        System.out.println("Sequence uses sd: " + seqData.isUsesSd() + "\n");
        System.out.println("\n\n--------------\n");




    }

    public boolean isCurrentLineStrandInfo(String line) {
        return line.startsWith("CDS");
    }
    /*
     * extracts details about the CDS strands in each sequence queried
     */

    public void extractStrandInfo(ProdigalCDS infoCDS, String s) throws ProdigalParserException {
        if (!s.startsWith("CDS")) {
            throw new ProdigalParserException("Expected a CDS line. Recieved a: " + s);
        }

        String tmp = s.replaceAll("\\s", "");
        if (strandChecker(tmp)) {
            System.out.println("\nCDS Located on the complementary strand: 3-->5 \n");
            infoCDS.setCompliment(true);
            int i = tmp.indexOf("(");
            int j = tmp.indexOf("..");
            infoCDS.setCdsStartLoc(tmp.substring(i + 1, j));
            System.out.println("Start location is: " + infoCDS.getCdsStartLoc() + "\t");
            infoCDS.setCdsEndLoc(tmp.substring(j + 2).replace(")", ""));
            System.out.println("End location is: " + infoCDS.getCdsEndLoc() + "\t");
        } else {
            System.out.println("\nCDS located on the forward strand: 5-->3 \n");
            infoCDS.setCompliment(false);
            int i = tmp.indexOf("CDS");
            int j = tmp.indexOf("..");
            infoCDS.setCdsStartLoc(tmp.substring(i + 3, j).replaceAll("<", ""));
            System.out.println("Start location is: " + infoCDS.getCdsStartLoc() + "\t");
            infoCDS.setCdsEndLoc(tmp.substring(j + 2));
            System.out.println("End location is: " + infoCDS.getCdsEndLoc() + "\t");
        }

    }

   
    

    /**
     *
     * @param str
     * @return true if the strand is complementary false otherwise
     */
    public static boolean strandChecker(String str) {
        if (str.contains("complement")) {
            return true;
        } else {
            return false;
        }
    }

    /**
     *
     * @param s
     * @param str
     * @return detailed string containing information about the sequences
     * queried
     */
    public String seqDat(String s, String str) {
        int i = s.indexOf(str);
        int j = i + str.length();
        String tmp = s.substring(j);
        String finalStr = tmp.split(";")[0];
        return finalStr;

    }

   
    /**
     *
     * @param s prints CDS details
     */
    public void extractCDSInfo(ProdigalCDS infoCDS, String s) throws ProdigalParserException {
        if (!s.startsWith("/note=")) {
            throw new ProdigalParserException("Expected a /note= line. Recieved a: " + s);
        }

        infoCDS.setCdsID(seqDat(s, "ID="));
        System.out.println("CDS ID: " + infoCDS.getCdsID() + " ");
        infoCDS.setCdsPartial(seqDat(s, "partial="));
        System.out.println("CDS partial: " + infoCDS.getCdsPartial() + " ");
        infoCDS.setCdsStartCodon(seqDat(s, "start_type="));
        System.out.println("CDS start type: " + infoCDS.getCdsStartCodon() + " ");
        infoCDS.setCdsRBSmotif(seqDat(s, "rbs_motif="));
        System.out.println("CDS RBS motif: " + infoCDS.getCdsRBSmotif() + " ");
        infoCDS.setCdsRBSspacer(seqDat(s, "rbs_spacer="));
        System.out.println("CDS RBS spacer: " + infoCDS.getCdsRBSspacer() + " ");
        infoCDS.setCdsGCcontent(seqDat(s, "gc_cont="));
        System.out.println("CDS gc content: " + infoCDS.getCdsGCcontent() + " ");
        infoCDS.setCdsConf(seqDat(s, "conf="));
        System.out.println("CDS conf: " + infoCDS.getCdsConf() + " ");
        infoCDS.setCdsScore(seqDat(s, "score="));
        System.out.println("CDS score: " + infoCDS.getCdsScore() + " ");
        infoCDS.setCdsCscore(seqDat(s, "cscore="));
        System.out.println("CDS cscore: " + infoCDS.getCdsCscore() + " ");
        infoCDS.setCdsSscore(seqDat(s, "sscore="));
        System.out.println("CDS sscore: " + infoCDS.getCdsSscore() + " ");
        infoCDS.setCdsRscore(seqDat(s, "rscore="));
        System.out.println("CDS rscore: " + infoCDS.getCdsRscore() + " ");
        infoCDS.setCdsUscore(seqDat(s, "uscore="));
        System.out.println("CDS uscore: " + infoCDS.getCdsUscore() + " ");
        infoCDS.setCdsTscore(seqDat(s, "tscore="));
        System.out.println("CDS tscore: " + infoCDS.getCdsTscore() + "\n ");

    }

    /**
     *
     * @param str
     * @return species name
     */
//    public static String extractSeqSpecies(String str) {
//
//        int i = str.lastIndexOf("|");
//        int j = str.indexOf("contig");
//        return str.substring(i + 1, j);
//    }

    /**
     *
     * @param s
     * @return contig no.
     */
//    public static String extractContigInfo(String s) {
//        int i = s.indexOf("contig");
//        int j = s.indexOf(",");
//        String contig = s.substring(i + 6, j);
//        return contig;
//    }
    
    /**
     *
     * @param str
     * @return gi number
     */
    public String extractGI(String str) {
        int i = str.indexOf("gi|");
        int j = str.indexOf("|emb|");
        return str.substring(i + 3, j);
    }
    /*
     * returns GBK accession id
     */

    public String extractGBKNo(String str) {
        int i = str.indexOf("|emb|");
        int j = str.lastIndexOf("|");
        return str.substring(i + 4, j);
    }

    /**
     *
     * @param str
     */
    public boolean extractProteinSeqHeaderDetails(ProdigalTranslation proteinFile, String line2) {

        if (line2.startsWith(">")) {
            int i = line2.indexOf(">");
            String Str = line2.substring(i);
            proteinFile.setProteinGInumber(Integer.parseInt(extractGI(Str)));
            proteinFile.setProteinGBKnumber(extractGBKNo(Str));
            proteinFile.setProteinID(seqDat(line2, "ID="));
            System.out.println(seqDat(line2, "ID="));
            proteinFile.setProteinPartial(Boolean.parseBoolean(seqDat(line2, "partial=")));
            proteinFile.setProteinStartCodon(seqDat(line2, "start_type="));
            proteinFile.setProteinRBSmotif(seqDat(line2, "rbs_motif="));
            proteinFile.setProteinRBSspacer(seqDat(line2, "rbs_spacer="));
            proteinFile.setProteinGCcontent(Double.parseDouble(seqDat(line2, "gc_cont=")));
            System.out.println("GI number is: " + proteinFile.getProteinGInumber()
                    + "\n" + "GBK number is: " + proteinFile.getProteinGBKnumber()
                    + "\nProtein ID is: " + proteinFile.getProteinID());
            System.out.println("partial: " + proteinFile.getProteinPartial()
                    + "\nstart codon is: " + proteinFile.getProteinStartCodon()
                    + "\nRBS motif: " + proteinFile.getProteinRBSmotif()
                    + "\nRBS spacer: " + proteinFile.getProteinRBSspacer()
                    + "\nGC content: " + proteinFile.getProteinGCcontent());

            return true;
        }
        return false;
    }

    public boolean isCurrentLineProtein(String line2) {
        if (line2.startsWith(">")) {
            return true;
        }
        return false;
    }
    StringBuilder translation = new StringBuilder();

    public List<ProdigalTranslation> extractProteins(ProdigalReport report, File file) throws FileNotFoundException {
        Scanner sc = new Scanner(file);
        System.out.println("Reading the protein file\n");
        ProdigalTranslation currentProtein = null;
        List<ProdigalTranslation> allProteins = new ArrayList<ProdigalTranslation>();
        StringBuilder currentTranslation = new StringBuilder();
        while (sc.hasNextLine()) {

            String nxtLine = sc.nextLine();

            /*
             * If we find a new protein sequecnce entry, then create a new
             * ProteinData object to hold its content. Immediately parse the
             * header line.
             */
            if (nxtLine.startsWith(">")) {
                currentProtein = new ProdigalTranslation();
                currentProtein.setReport(report);
                allProteins.add(currentProtein);
                extractProteinSeqHeaderDetails(currentProtein, nxtLine);
            } else /*
             * Otherwise, the line must be part of a protein translation -
             * append it to a temporary StringBuilder.
             */ {
                currentTranslation.append(nxtLine);
            }

            /*
             * If we find a new protein sequence entry (identified by a line
             * beginning with '>'), OR we reach the end of the file, then
             * make sure the protein translation string is set on the 'current'
             * ProteinData (if there is one).
             */
            if ((nxtLine.startsWith(">") || !sc.hasNextLine()) && (currentProtein != null)) {
                String tempProtein = currentTranslation.toString().replace("*", "");
                currentProtein.setProteinTranslation(tempProtein);
                currentTranslation = new StringBuilder();
            }

        }
        return allProteins;
    }

   public boolean isCurrentLineCDSInfo(String line) {
        return line.startsWith("/note=");
    }
}
