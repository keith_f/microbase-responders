/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: 30-Sep-2012, 12:55:35
 */

package com.microbasecloud.tokenizer.exe;

import com.microbasecloud.shell.messageutil.ShellMsgGenerator;
import com.microbasecloud.shell.messageutil.ShellMsgGeneratorException;
import com.microbasecloud.tokenizer.common.TokenJobDescription;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import org.codehaus.jackson.map.ObjectMapper;
import uk.org.microbase.filesystem.spi.MBFile;

/**
 * This program generates a Microbase notification message containing
 * a BlastJobDescription, given a suitable database and query file.
 * 
 * This program is provided for convenience and produces both the text for a
 * message, and a sample notification system command line that can be used to
 * initiate the blast responder.
 * 
 * @author Keith Flanagan
 */
public class TokenJobMessageGenerator
    implements ShellMsgGenerator<TokenJobDescription>
{
  
  @Override
  public String getMessageProcessorClassname() {
    return TokenExeResponder.class.getName();
  }

  @Override
  public Map<String, String> getReqPropertyNames() {
    Map<String, String> nameToDesc = new HashMap<>();
    
    nameToDesc.put("token.input.bucket", "MBFS bucket of the input protein FASTA file");
    nameToDesc.put("token.input.path", "Path to the input protein FASTA file");
    nameToDesc.put("token.input.filename", "Filename of the input protein FASTA file");
    
    nameToDesc.put("token.organism.guid", "ID of the organism for which the input file belongs");
    nameToDesc.put("token.fragment.guid", "ID of the fragment whose entire content are present in the input file");
    
    
    nameToDesc.put("token.graph.name", "Target BigGraph name");
    nameToDesc.put("token.graph.branch", "Target BigGraph branch");
    
    nameToDesc.put("token.window.size", "The size of the protein tokens to generate.");
    
    
    return nameToDesc;
  }

  @Override
  public String generateJson(Map<String, String> properties) 
          throws ShellMsgGeneratorException {
    try {
      TokenJobDescription bean = generateMsgBean(properties);

      // Serialise message content to JSON
      ObjectMapper mapper = new ObjectMapper();
      String json = mapper.writeValueAsString(bean);
      return json;
    }
    catch(Exception e) {
      throw new ShellMsgGeneratorException(
              "Failed to generate and serialise a message content bean "
              + "from properties: "+properties, e);
    }
  }

  @Override
  public TokenJobDescription generateMsgBean(Map<String, String> properties) 
          throws ShellMsgGeneratorException {
    try {
      TokenJobDescription bean = new TokenJobDescription();
      bean.setGraphName(properties.get("token.graph.name"));
      bean.setGraphBranch(properties.get("token.graph.branch"));
      
      bean.setTokenSize(Integer.parseInt(properties.get("token.window.size")));
      
      bean.setOrganismGuid(properties.get("token.organism.guid"));
      bean.setFragmentGuid(properties.get("token.fragment.guid"));
      
      bean.setFragmentFile(
              new MBFile(
              properties.get("token.input.bucket"), 
              properties.get("token.input.path"), 
              properties.get("token.input.filename")));

      return bean;
    }
    catch(Exception e) {
      throw new ShellMsgGeneratorException(
              "Failed to generate message content bean "
              + "from properties: "+properties, e);
    }
  }

}
