#!/bin/bash

# Microbase launcher script
# Author: Keith Flanagan

# The Java class to run
EXE_CLASSNAME="uk.ac.ncl.mucosa.genomepool.cli.ManualSubmitFaa"

# Get the filename of this script
SCRIPT_NAME=$0
SCRIPT_PATH=`which $0`
PROG_HOME=`dirname $SCRIPT_PATH`
#echo "Program home directory: $PROG_HOME"

# Set a default location of MICROBASE_HOME if it is not already set
if [ -z $MICROBASE_HOME ] ; then
    MICROBASE_HOME="$PROG_HOME/.."
fi


if [ -z $MB_TOMCAT ] ; then
    echo "Please set environment variable MB_TOMCAT to your local Tomcat installation"
    exit 1
fi

MB_CONFIG_DIR=$MB_TOMCAT/mb-config

#MVN_OPTS="-o"

MVN="mvn $MVN_OPTS"

if [ ! -d $PROG_HOME/target/dependency ] ; then
    # Install dependencies into the program's target directory if necessary
    ( cd $PROG_HOME ; $MVN dependency:copy-dependencies )
fi

# Configure CLASSPATH
# Include program's compiled classes
CLASSPATH=$PROG_HOME/target/classes
# Include .jar dependencies
for LIB in `find $PROG_HOME/target/dependency -name "*.jar"` ; do
    CLASSPATH=$CLASSPATH:$LIB
done
# Add configuration file directory to the CLASSPATH
#CLASSPATH=$CLASSPATH:$MICROBASE_HOME/config
CLASSPATH=$CLASSPATH:$MB_CONFIG_DIR

# Finally, start the application
java -Xmx768M -Dhazelcast.config=$MB_CONFIG_DIR/hazelcast.xml -cp $CLASSPATH $EXE_CLASSNAME $@



