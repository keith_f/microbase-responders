/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: 25-Mar-2012, 12:40:32
 */

package com.microbasecloud.responders.filescanner.data;

import java.io.Serializable;
import java.util.Date;
import javax.persistence.*;
import org.hibernate.annotations.Index;

/**
 *
 * @author Keith Flanagan
 */
@Entity
@Table(name = "scans")
public class ScanReport 
    implements Serializable
{
  @Id
  @GeneratedValue(strategy= GenerationType.AUTO)
  private Long id;
  
  @OneToOne(cascade= CascadeType.REMOVE)
  private ScanReport previousReport;

  @Column(nullable=false)
  @Temporal(javax.persistence.TemporalType.TIMESTAMP)
  private Date scanTimestamp;
  
  @Column(nullable=false)
  @Index(name="targetNameIdx")
  private String targetName;
  
  @Column(nullable=false)
  private String bucket;
  @Column(nullable=false)
  private String filePath;
  
  private int totalFileStatusChanges;
  
  private int totalFiles;
  private int newFiles;
  private int updatedFiles;
  private int deletedFiles;
  private int unchangedFiles;
  
      
  public ScanReport()
  {
  }

  @Override
  public String toString()
  {
    return "ScanReport{" + "id=" + id 
        + ", targetName=" + targetName 
        + ", previousReport=" + previousReport 
        + ", scanTimestamp=" + scanTimestamp + ", bucket=" + bucket 
        + ", filePath=" + filePath 
        + ", totalFileStatusChanges=" + totalFileStatusChanges 
        + ", totalFiles=" + totalFiles + ", newFiles=" + newFiles 
        + ", updatedFiles=" + updatedFiles + ", deletedFiles=" + deletedFiles 
        + ", unchangedFiles=" + unchangedFiles + '}';
  }
  
  

  public Long getId()
  {
    return id;
  }

  public void setId(Long id)
  {
    this.id = id;
  }

  public Date getScanTimestamp()
  {
    return scanTimestamp;
  }

  public void setScanTimestamp(Date scanTimestamp)
  {
    this.scanTimestamp = scanTimestamp;
  }

  public String getBucket()
  {
    return bucket;
  }

  public void setBucket(String bucket)
  {
    this.bucket = bucket;
  }

  public int getTotalFiles()
  {
    return totalFiles;
  }

  public void setTotalFiles(int totalFiles)
  {
    this.totalFiles = totalFiles;
  }

  public int getNewFiles()
  {
    return newFiles;
  }

  public void setNewFiles(int newFiles)
  {
    this.newFiles = newFiles;
  }

  public int getUpdatedFiles()
  {
    return updatedFiles;
  }

  public void setUpdatedFiles(int updatedFiles)
  {
    this.updatedFiles = updatedFiles;
  }

  public String getFilePath()
  {
    return filePath;
  }

  public void setFilePath(String filePath)
  {
    this.filePath = filePath;
  }

  public ScanReport getPreviousReport()
  {
    return previousReport;
  }

  public void setPreviousReport(ScanReport previousReport)
  {
    this.previousReport = previousReport;
  }

  public int getDeletedFiles()
  {
    return deletedFiles;
  }

  public void setDeletedFiles(int deletedFiles)
  {
    this.deletedFiles = deletedFiles;
  }

  public int getUnchangedFiles()
  {
    return unchangedFiles;
  }

  public void setUnchangedFiles(int unchangedFiles)
  {
    this.unchangedFiles = unchangedFiles;
  }

  public int getTotalFileStatusChanges()
  {
    return totalFileStatusChanges;
  }

  public void setTotalFileStatusChanges(int totalFileStatusChanges)
  {
    this.totalFileStatusChanges = totalFileStatusChanges;
  }

  public String getTargetName()
  {
    return targetName;
  }

  public void setTargetName(String targetName)
  {
    this.targetName = targetName;
  }
}
