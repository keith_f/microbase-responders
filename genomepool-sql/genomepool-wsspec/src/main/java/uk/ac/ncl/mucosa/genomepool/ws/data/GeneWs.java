/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.mucosa.genomepool.ws.data;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 *
 * @author Sirintra Nakjang
 */
public class GeneWs
        implements Serializable
{
  private String guid;

  private String geneId; //Used to uniquely identify a gene

  private int taxonId;

  private String fragmentGuid;

  private Set<GeneProductWs> products;

  private String locusTag;

  private String geneName;

  private int startLocus;

  private int endLocus;

  private String orientation;


  public GeneWs()
  {
    products = new HashSet<GeneProductWs>();
  }

  @Override
  public boolean equals(Object obj)
  {
    if (obj == null)
    {
      return false;
    }
    if (getClass() != obj.getClass())
    {
      return false;
    }
    final GeneWs other = (GeneWs) obj;
    if ((this.guid == null) ? (other.guid != null) : !this.guid.equals(other.guid))
    {
      return false;
    }
    return true;
  }

  @Override
  public int hashCode()
  {
    int hash = 7;
    hash = 29 * hash + (this.guid != null ? this.guid.hashCode() : 0);
    return hash;
  }

  

  @Override
  public String toString()
  {
    StringBuilder sb = new StringBuilder("[");
    sb.append("guid:").append(guid)
        .append("gene ID:").append(geneId)
        .append(", genomeFragment:").append(fragmentGuid)
        .append(", products:").append(products)
        .append(", locus:").append(locusTag)
        .append(", name:").append(geneName)
        .append(", startLocus:").append(startLocus)
        .append(", endLocus:").append(endLocus)
        .append(", orientation:").append(orientation)
        .append("]");
    return sb.toString();
  }

  /**
   * Convenience method that retrieves the first product of a gene. This is
   * useful if you can assume a 1:1 ratio between genes:products as is the
   * case in bacteria.
   *
   * @return
   */
  public GeneProductWs firstProduct()
  {
    if (products == null)
    {
      return null;
    }
    return products.iterator().next();
  }
  

  public int getEndLocus()
  {
    return endLocus;
  }

  public void setEndLocus(int endLocus)
  {
    this.endLocus = endLocus;
  }

  public String getFragmentGuid()
  {
    return fragmentGuid;
  }

  public void setFragmentGuid(String fragmentGuid)
  {
    this.fragmentGuid = fragmentGuid;
  }

  public String getOrientation()
  {
    return orientation;
  }

  public void setOrientation(String orientation)
  {
    this.orientation = orientation;
  }

  public int getStartLocus()
  {
    return startLocus;
  }

  public void setStartLocus(int startLocus)
  {
    this.startLocus = startLocus;
  }

  public Set<GeneProductWs> getProducts()
  {
    return products;
  }

  public void setProducts(Set<GeneProductWs> products)
  {
    this.products = products;
  }

  public String getLocusTag()
  {
    return locusTag;
  }

  public void setLocusTag(String locusTag)
  {
    this.locusTag = locusTag;
  }

  public String getGeneId()
  {
    return geneId;
  }

  public void setGeneId(String geneId)
  {
    this.geneId = geneId;
  }

  public String getGeneName()
  {
    return geneName;
  }

  public void setGeneName(String geneName)
  {
    this.geneName = geneName;
  }

  public String getGuid()
  {
    return guid;
  }

  public void setGuid(String guid)
  {
    this.guid = guid;
  }

  public int getTaxonId()
  {
    return taxonId;
  }

  public void setTaxonId(int taxonId)
  {
    this.taxonId = taxonId;
  }
}
