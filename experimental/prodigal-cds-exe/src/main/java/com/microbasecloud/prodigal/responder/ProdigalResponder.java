/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.microbasecloud.prodigal.responder;

import com.microbase.responder.prodigalcdscommon.ProdigalJobDescription;
import com.microbase.responder.prodigalcdscommon.ProdigalJobResult;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Logger;
import uk.org.microbase.dist.responder.RegistrationException;
import uk.org.microbase.dist.responder.ResponderInfo;
import uk.org.microbase.filesystem.spi.FileMetaData;
import uk.org.microbase.filesystem.spi.MBFile;
import uk.org.microbase.filesystem.spi.MicrobaseFS;
import uk.org.microbase.notification.data.Message;
import uk.org.microbase.responder.spi.AbstractMessageProcessor;
import uk.org.microbase.responder.spi.ProcessingException;
import uk.org.microbase.responder.spi.ResponderInfoPropertiesParser;
import uk.org.microbase.util.cli.NativeCommandExecutor;

/**
 *
 * @author prisni
 */
public class ProdigalResponder
extends AbstractMessageProcessor {
    
    private static final Logger l = Logger.getLogger(ProdigalResponder.class.getName());
  
  /**
   * Configuration resource for this responder. A file with this name must be
   * located on the classpath.
   */
  static final String CONFIG_RESOURCE = ProdigalResponder.class.getSimpleName()+".properties";
  
  private static final String CONFIG_RESULT_NAME_REFORMATTER = "result_name_reformatter";
  
  /*
   * Configuration options (these should be treated as 'final'
   */
  private Properties config;
  private ProdigalOutputPathReformatter resultFilenameReformatter;
  private ProdigalOutputPathReformatter stdOutFilenameReformatter;
  private ProdigalOutputPathReformatter stdErrFilenameReformatter;
  
  /*
   * Per-job variables. These are set every time <code>preRun</code> is called.
   */
  private ProdigalJobDescription jobDescr;
  private MBFile jobRemoteResultFile;
  private MBFile jobStdOutLogFile;
  private MBFile jobStdErrLogFile;
    


  public ProdigalResponder()
  {
    /*
     * Set some default file name formatters here.
     * TODO eventually, we'll want these to be configurable.
     */
    resultFilenameReformatter = new ProdigalResultInIndividualDirsFormatter();
    stdOutFilenameReformatter = new ProdigalStdOutLogFileFormatter();
    stdErrFilenameReformatter = new ProdigalStdErrLogFileFormatter();
  }

  
  @Override
  protected ResponderInfo createDefaultResponderInfo()
      throws RegistrationException
  {
    try
    {
      config = new Properties();
      // Use properties from the Microbase global configuration file, if present
      config.putAll(getRuntime().getRawConfigProperties());
      try (InputStream is = getRuntime().getClassLoader().getResourceAsStream(CONFIG_RESOURCE)) {
        if (is == null) {
          throw new RegistrationException("Failed to find configuration resource: "+CONFIG_RESOURCE);
        }
        config.load(is);
      }

      ResponderInfo info = ResponderInfoPropertiesParser.parseFromProperties(config);

      l.info("Responder config: "+info);
      return info;
    }
    catch(Exception e) 
    {
      throw new RegistrationException(
          "Failed to load responder configuration", e);
    }
  }


    @Override
    public void preRun(Message message) throws ProcessingException 
        {
    l.info("Doing preRun");
    try {
      /*
       * Parse message as a responder specific job description.
       */
      jobDescr = parseMessageWithJsonContent(message, ProdigalJobDescription.class);
      
      /*
       * Configure the result file formatters
       */
      resultFilenameReformatter.configure(getRuntime(), getResponderInfo(),getProcessInfo(), message);
      resultFilenameReformatter.setJobDescription(jobDescr);
      stdOutFilenameReformatter.configure(getRuntime(), getResponderInfo(),getProcessInfo(), message);
      stdOutFilenameReformatter.setJobDescription(jobDescr);
      stdErrFilenameReformatter.configure(getRuntime(), getResponderInfo(),getProcessInfo(), message);
      stdErrFilenameReformatter.setJobDescription(jobDescr);
      
      jobRemoteResultFile = resultFilenameReformatter.getFormattedRemoteFile();
      jobStdOutLogFile = stdOutFilenameReformatter.getFormattedRemoteFile();
      jobStdErrLogFile = stdErrFilenameReformatter.getFormattedRemoteFile();
      
//      /*
//       * We know what the result file will be ahead of time, so may as well define
//       * it here. The remote result file path and name, can be determined partly
//       * based on configuration options, and partly on the name reformatter in use.
//       */
//      jobRemoteResultFile = determineRemoteResultFile(jobDescr);
//      
//      /*
//       * Similarly, determine the remote log file locations
//       */
//      jobStdOutLogFile = determineRemoteLogStdOutFile(message, jobDescr);
//      jobStdErrLogFile = determineRemoteLogStdErrFile(message, jobDescr);
//      
//      l.info("Parsed job from message: "+message.getGuid()+": "+jobDescr
//          + ". Remote result file will be: "+jobRemoteResultFile);
    }
    catch(Exception e) {
      throw new ProcessingException("Failed to parse message", e);
    }
        }
//       
    

    @Override
    public void cleanupPreviousResults(Message msg) throws ProcessingException {
    l.info("Doing cleanup");
    try
    {
      MicrobaseFS fs = getRuntime().getMicrobaseFS();
//      MBFile jobRemoteResultFile = resultFilenameReformatter.getFormattedRemoteFile();
      
      if (fs.exists(jobRemoteResultFile))
      {
        l.info("Found an existing result file "+jobRemoteResultFile
            +", generated by a previous "
            + "execution. Deleting...");
        fs.deleteRemoteCopy(jobRemoteResultFile);
      }
    }
    catch(Exception e)
    {
      throw new ProcessingException("Failed to perform cleanup", e);
    }
  }

    @Override
    public Set<Message> processMessage(Message msg) throws ProcessingException 
            {
   l.info("Performing computational work");
    try
    { 
      MBFile queryFastaRemoteFile = jobDescr.getQueryFastaFile();
      MBFile subjectDbRemoteFile = jobDescr.getDatabaseZipFile();
  
      l.info("Query Fasta file is: "+queryFastaRemoteFile);
      l.info("Subject DB file is: "+subjectDbRemoteFile);
      // Download the query seqeunce to the scratch space for this job.
      File queryFastaFile = downloadFileToSharedSpace(queryFastaRemoteFile);
      
      /*
       * The database is potentially huge, so download and extract it to the
       * shared scratch space for two reasons:
       * 1) other responders/instances may require the same DB concurrently
       * 2) the shared scratch space persists beyond individual job executions
       */
      File dbDir = downloadAndUnzipFileToSharedSpace(subjectDbRemoteFile);
      
      l.info("Running command ...");
      //CDS prediction
      File predictedCdsFile = predictProdigal(queryFastaFile);
      
//      MBFile jobRemoteResultFile = resultFilenameReformatter.getFormattedRemoteFile();
      l.info("Uploading predicted CDS: "+predictedCdsFile.getAbsolutePath()
              + " to: "+jobRemoteResultFile);
      getRuntime().getMicrobaseFS().upload(
          predictedCdsFile, jobRemoteResultFile, null);
      
      //translations predicted
      File predictedTranslationsFile = predictTranslations(queryFastaFile);
      
//      MBFile jobRemoteResultFile = resultFilenameReformatter.getFormattedRemoteFile();
      l.info("Uploading predicted Translations: "+predictedTranslationsFile.getAbsolutePath()
              + " to: "+jobRemoteResultFile);
      getRuntime().getMicrobaseFS().upload(
          predictedTranslationsFile, jobRemoteResultFile, null);
      
      Message successMsg = generateSuccessNotification(msg, jobDescr);
      
      Set<Message> messages = new HashSet<Message>();
      messages.add(successMsg);
      return messages;
    }
    catch(Exception e)
    {
      throw new ProcessingException(
          "Failed to run one or more commands: "
          + " on: " + getRuntime().getNodeInfo().getHostname() 
          + ", in: " + getWorkingDirectory(), e);
    }
  }


    private File predictProdigal(File seqFile) throws ProcessingException {
        try {
            File predictedResultFile = new File(getWorkingDirectory(), seqFile.getName());
//            File predictedResultFile = new File(getWorkingDirectory(),
//                    genePredictionS3File.getLocation().getName());
            String[] prodigalCommand = new String[]{"/mnt/responder_data/prodigal_260/prodigal.v2_60.linux", "-c", "-i",
                seqFile.getAbsolutePath(), "-o", predictedResultFile.getAbsolutePath()};

            executeCommand(prodigalCommand, "prodigal");


            return predictedResultFile;
        } catch (Exception e) {
            throw new ProcessingException("Failed to execute prodigal 2.0", e);
        }
    }

    private File predictTranslations(File seqFile) throws ProcessingException {
        try {
            File predictedResultFile = new File(getWorkingDirectory(), seqFile.getName());

            String[] prodigalCommandTranslations = new String[]{"/mnt/responder_data/prodigal_260/prodigal.v2_60.linux", "-c", "-i",
                seqFile.getAbsolutePath(), "-a", predictedResultFile.getAbsolutePath()};

            executeCommand(prodigalCommandTranslations, "translations");

            return predictedResultFile;
        } catch (Exception e) {
            throw new ProcessingException("Failed to execute prodigal 2.0", e);
        }
    }
private Message generateSuccessNotification(Message parent, ProdigalJobDescription job)
      throws IOException
  {
    /*
     * Prepare the content for the 'success' notification message.
     */
    
    ProdigalJobResult resultContent = new ProdigalJobResult();
    resultContent.setJob(job);
    resultContent.setResultCDSFile(jobStdOutLogFile);
    resultContent.setResultTranslationsFile(jobStdOutLogFile);
    resultContent.setStdErrLogFile(jobStdErrLogFile);
    resultContent.setStdOutLogFile(jobStdOutLogFile);
    
    /*
     * Create the success message with the above content. Link this success
     * message to the parent message (the job descriptor) for provenance.
     */
    Message successMsg = createMessageWithJsonContent(
        parent,                           //The message to use as a parent
        getResponderInfo().getTopicOut(), //The topic of the new message
        parent.getWorkflowStepId(),       //The workflow step ID
        resultContent,                    //The result content item
        //An optional human-readable description
        "Successfully completed a gene prediction using Prodigal.");
    
    /*
     * Old, manual way of serialising content - can be deleted?
     */
//    ObjectMapper mapper = new ObjectMapper();
//    String resultContentJson = mapper.writeValueAsString(resultContent);
//    successMsg.getContent().put(PROP__MAIN_MSG_CONTENT, resultContentJson);
//    successMsg.getContent().put(OUT_MSG_PROB__RESULT_FILE, mapper.writeValueAsString(jobRemoteResultFile));
//    successMsg.getContent().put(OUT_MSG_PROB__STDERR_FILE, mapper.writeValueAsString(jobStdErrLogFile));
//    successMsg.getContent().put(OUT_MSG_PROB__STDOUT_FILE, mapper.writeValueAsString(jobStdOutLogFile));

    
    return successMsg;
  }


    private void executeCommand(String[] command, String stdOutPrefix) throws ProcessingException {
        File workDir = getWorkingDirectory();
        File stdOutFile = new File(workDir, stdOutPrefix + "-stdout.txt");
        File stdErrFile = new File(workDir, stdOutPrefix + "-stderr.txt");
        try {
            FileWriter stdOut = new FileWriter(stdOutFile);
            FileWriter stdErr = new FileWriter(stdErrFile);

            int exitStatus = NativeCommandExecutor.executeNativeCommand(workDir, command, stdOut, stdErr);

            stdOut.flush();
            stdOut.close();
            stdErr.flush();
            stdErr.close();

            if (exitStatus != 0) {
                throw new ProcessingException("Exit status of the command: "
                        + Arrays.asList(command) + "\nwas " + exitStatus
                        + ". Assuming that non-zero means that execution failed.");
            }
        } catch (Exception e) {
            throw new ProcessingException("Failed to execute command: "
                    + command[0], e);
        } finally {
            try {
                getRuntime().getMicrobaseFS().upload(stdOutFile, jobStdOutLogFile, null);
                getRuntime().getMicrobaseFS().upload(stdErrFile, jobStdErrLogFile, null);

            } catch (Exception e) {
                throw new ProcessingException("Failed to upload output file", e);
            }
        }
    }
}
