/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.microbasecloud.prodigal.dataparser;

/**
 *
 * @author prisni
 */
public class ProdigalParserException extends Exception {

    public ProdigalParserException() {
    }

    public ProdigalParserException(String message) {
        super(message);
    }

    public ProdigalParserException(String message, Throwable cause) {
        super(message, cause);
    }

    public ProdigalParserException(Throwable cause) {
        super(cause);
    }
    
}
