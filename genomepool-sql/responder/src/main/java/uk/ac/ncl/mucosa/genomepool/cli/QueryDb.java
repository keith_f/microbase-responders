/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.mucosa.genomepool.cli;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;
import uk.ac.ncl.mucosa.genomepool.full.dao.GeneDAO;
import uk.ac.ncl.mucosa.genomepool.full.dao.GenomePoolDAO;
import uk.ac.ncl.mucosa.genomepool.GenomePoolDbException;
import uk.ac.ncl.mucosa.genomepool.full.dao.OrganismDAO;
import uk.ac.ncl.mucosa.genomepool.full.dao.ProductDAO;
import uk.ac.ncl.mucosa.genomepool.full.data.Gene;
import uk.ac.ncl.mucosa.genomepool.full.data.GeneProduct;
import uk.ac.ncl.mucosa.genomepool.full.data.GenomeFragment;

/**
 *
 * @author Keith Flanagan
 */
public class QueryDb
{
//  private static final Logger logger =
//      Logger.getLogger(QueryDb.class.getName());
//
//  private static final String USAGE = "Usage:\n"
//      + "  --hostname <mongos> - Mongos hostname\n"
//      + "  --database <database name> - Database name\n"
//      + "  --list-organisms - Prints a table of organism information\n"
//      + "  --list-fragments - Prints a table of fragment information\n"
//      + "  --list-fragment-genes <fragment guid> - Prints genes info for the specified fragment\n"
//      + "  --list-fragment-genes-allinfo <fragment guid> - Prints genes info for the specified fragment\n"
//      + "  --count-genes - prints the sum of the genes of each fragment\n"
//      + "  --list-gis - prints the GI of each protein for each fragment\n"
//      + "  --find-seqs-length-less-than <length> - prints the GI of each protein for each fragment whose length is less than the specified value\n"
//      + "  --test - Attempts to perform a selection of query operations\n"
//      + "  --list-products-from-file <file containing list of GIs> - outputs a FASTA file containing the specified sequences\n"
//      + "  --list-organisms-by-gi <list of GIs> - Given a list of product GIs, outputs a list of organisms they appear in\n"
//      + "\n";
//
//  private static enum Command
//  {
//
//    LIST_ORGANISMS,
//    LIST_FRAGMENTS,
//    LIST_FRAGMENT_GENES,
//    LIST_FRAGMENT_GENES_ALL_INFO,
//    COUNT_PROTEINS,
//    LIST_GIS,
//    FIND_SEQS_LEN_LESS_THAN,
//    TEST,
//    LIST_PRODUCTS_FROM_FILE,
//    LIST_ORGANISMS_BY_GI;
//  }
//
//  public static void main(String[] args)
//      throws DBException, GenomePoolDbException, IOException
//
//  {
//    if (args.length == 0)
//    {
//      System.out.println(USAGE);
//      System.exit(1);
//    }
//
//    Command cmd = null;
//    Set<String> hosts = new HashSet<String>();
//    String databaseName = null;
//    String fragmentGuid = null;
//    int length = -1;
//    String inputFile = null;
//    List<String> searchStrings = new ArrayList<String>();
//
//    for (int i = 0; i < args.length; i++)
//    {
//      if (args[i].equalsIgnoreCase("--hostname"))
//      {
//        hosts.add(args[++i]);
//      }
//      else if (args[i].equals("--database"))
//      {
//        databaseName = args[++i];
//      }
//      else if (args[i].equals("--list-organisms"))
//      {
//        cmd = Command.LIST_ORGANISMS;
//      }
//      else if (args[i].equals("--list-fragments"))
//      {
//        cmd = Command.LIST_FRAGMENTS;
//      }
//      else if (args[i].equals("--list-fragment-genes"))
//      {
//        cmd = Command.LIST_FRAGMENT_GENES;
//        fragmentGuid = args[++i];
//      }
//      else if (args[i].equals("--list-fragment-genes-allinfo"))
//      {
//        cmd = Command.LIST_FRAGMENT_GENES_ALL_INFO;
//        fragmentGuid = args[++i];
//      }
//      else if (args[i].equals("--count-genes"))
//      {
//        cmd = Command.COUNT_PROTEINS;
//      }
//      else if (args[i].equals("--list-gis"))
//      {
//        cmd = Command.LIST_GIS;
//      }
//      else if (args[i].equals("--find-seqs-length-less-than"))
//      {
//        cmd = Command.FIND_SEQS_LEN_LESS_THAN;
//        length = Integer.parseInt(args[++i]);
//      }
//      else if (args[i].equals("--list-products-from-file"))
//      {
//        cmd = Command.LIST_PRODUCTS_FROM_FILE;
//        inputFile = args[++i];
//      }
//      else if (args[i].equals("--list-organisms-by-gi"))
//      {
//        cmd = Command.LIST_ORGANISMS_BY_GI;
//        for (int j=i+1; j<args.length && !args[j].startsWith("--"); i++, j++)
//        {
//          searchStrings.add(args[j]);
//        }
//      }
//      else if (args[i].equals("--test"))
//      {
//        cmd = Command.TEST;
//      }
//    }
//
//    if (databaseName == null)
//    {
//      throw new DBException("No database name was specified!");
//    }
//    DB db = DBUtils.connectDB(hosts.iterator().next(), databaseName);
//
//    GenomePoolDAO dao = new GenomePoolDAO(db);
//    OrganismDAO organismDao = new OrganismDAO(db);
//    ProductDAO productDao = new ProductDAO(db);
//    GeneDAO geneDao = new GeneDAO(db);
//    switch (cmd)
//    {
//      case LIST_ORGANISMS:
//        listOrganisms(dao);
//        break;
//      case LIST_FRAGMENTS:
//        listFragments(dao);
//        break;
//      case LIST_FRAGMENT_GENES:
//        listFragmentGenes(dao, fragmentGuid);
//        break;
//      case LIST_FRAGMENT_GENES_ALL_INFO:
//        listFragmentGenesAllInfo(dao, fragmentGuid);
//        break;
//      case COUNT_PROTEINS:
//        countGenes(dao);
//        break;
//      case LIST_GIS:
//        listGis(dao);
//        break;
//      case FIND_SEQS_LEN_LESS_THAN:
//        findSequenceLengthsLessThan(dao, length);
//        break;
//      case TEST:
//        test(dao);
//        break;
//      case LIST_PRODUCTS_FROM_FILE:
//        listProductsFromFile(geneDao, productDao, inputFile);
//        break;
//      case LIST_ORGANISMS_BY_GI:
//        listOrganismsByGi(organismDao, geneDao, productDao, searchStrings);
//        break;
//      default:
//        System.out.println("Unknown command: " + cmd);
//    }
//    db.getMongo().close();
//  }
//
//  private static void listFragments(GenomePoolDAO dao)
//      throws GenomePoolDbException
//  {
//    StringBuilder sb = new StringBuilder();
//    sb.append("Frag GUID\t").append("Organism\t").append("Accession\t").append("DNA Seq Length\t").append("Fragment description\n");
//    List<String> guids = dao.listFragmentGuids();
//    for (String fragGuid : guids)
//    {
//      GenomeFragment frag = dao.findFragmentByGuid(fragGuid);
//
//      sb.append(frag.getGuid()).append("\t");
//      sb.append(frag.getOrganismGuid()).append("\t");
//      sb.append(frag.getAccession()).append("\t");
//      sb.append(frag.getDnaSequenceLength()).append("\t");
//      sb.append(frag.getDescription()).append("\t");
//      //sb.append(frag.getOrganismName()).append("\t");
//
//
//      sb.append("\n");
//    }
//    sb.append("\nTotal fragments: ").append(guids.size()).append("\n");
//    System.out.println(sb.toString());
//  }
//
//  private static void listFragmentGenes(GenomePoolDAO dao, String fragmentGuid)
//      throws GenomePoolDbException
//  {
//    StringBuilder sb = new StringBuilder();
//    sb.append("Gene GUID\t").append("Locus\t").append("Start\t").append("End\t").append("Prod GUID\t").append("Descr\n");
//    GenomeFragment fragment = dao.findFragmentByGuid(fragmentGuid);
//    List<Gene> genes = dao.findGenesForFragment(fragmentGuid);
//    for (Gene gene : genes)
//    {
//      sb.append(gene.getGuid()).append("\t");
//      sb.append(gene.getLocusTag()).append("\t");
//      sb.append(gene.getStartLocus()).append("\t");
//      sb.append(gene.getEndLocus()).append("\t");
//
//      GeneProduct prod = gene.firstProduct();
//      sb.append(prod.getGuid()).append("\t");
//      sb.append(prod.getProductDescr()).append("\t");
//
//      sb.append("\n");
//    }
//    sb.append("\nTotal genes: ").append(genes.size()).append("\n");
//    System.out.println(sb.toString());
//  }
//
//  private static void listFragmentGenesAllInfo(
//      GenomePoolDAO dao, String fragmentGuid)
//      throws GenomePoolDbException
//  {
//    StringBuilder sb = new StringBuilder();
//    sb.append("Gene GUID\t").append("Locus\t").append("Start\t").append("End\t").append("Prod GUID\t").append("Descr\t").append("Sequence\n");
//    GenomeFragment fragment = dao.findFragmentByGuid(fragmentGuid);
//    List<Gene> genes = dao.findGenesForFragment(fragmentGuid);
//    for (Gene gene : genes)
//    {
//      sb.append(gene.getGuid()).append("\t");
//      sb.append(gene.getLocusTag()).append("\t");
//      sb.append(gene.getStartLocus()).append("\t");
//      sb.append(gene.getEndLocus()).append("\t");
//
//      GeneProduct prod = gene.firstProduct();
//      sb.append(prod.getGuid()).append("\t");
//      sb.append(prod.getProductDescr()).append("\t");
//      sb.append(prod.getProductSequence()).append("\t");
//
//      sb.append("\n");
//    }
//    sb.append("\nTotal genes: ").append(genes.size()).append("\n");
//    System.out.println(sb.toString());
//  }
//
//  private static void listOrganisms(GenomePoolDAO dao)
//      throws GenomePoolDbException
//  {
//    StringBuilder sb = new StringBuilder();
//    sb.append("GUID\t").append("Taxon\t").append("Short name\t").append("Name\n");
//    List<String> guids = dao.listOrganismGuids();
//    for (String fragGuid : guids)
//    {
//      Organism org = dao.findOrganismByGuid(fragGuid);
//
//      sb.append(org.getGuid()).append("\t");
//      sb.append(org.getTaxonId()).append("\t");
//      sb.append(org.getShortName()).append("\t");
//      sb.append(org.getName()).append("\t");
//      //sb.append(frag.getOrganismName()).append("\t");
//
//
//      sb.append("\n");
//    }
//    sb.append("\nTotal organisms: ").append(guids.size()).append("\n");
//    System.out.println(sb.toString());
//  }
//
//  private static void countGenes(GenomePoolDAO dao)
//      throws GenomePoolDbException
//  {
//    StringBuilder sb = new StringBuilder();
//    sb.append("Frag GUID\t").append("Organism\t").append("Accession\t").append("DNA Seq Length\t").append("Fragment description\n");
//    List<String> guids = dao.listFragmentGuids();
//    int totalGenes = 0;
//    for (String fragGuid : guids)
//    {
//      GenomeFragment frag = dao.findFragmentByGuid(fragGuid);
//      List<Gene> genes = dao.findGenesForFragment(fragGuid);
//      totalGenes += genes.size();
//
//      sb.append(frag.getGuid()).append("\t");
//      sb.append(frag.getOrganismGuid()).append("\t");
//      sb.append(frag.getAccession()).append("\t");
//      sb.append(frag.getDnaSequenceLength()).append("\t");
//      sb.append(frag.getDescription()).append("\t");
//      sb.append("genes: ").append(genes.size()).append("\t");
//
//      sb.append("\n");
//    }
//    sb.append("\nTotal fragments: ").append(guids.size()).append("\n");
//    sb.append("\nTotal genes: ").append(totalGenes).append("\n");
//    System.out.println(sb.toString());
//  }
//
//  private static void listGis(GenomePoolDAO dao)
//      throws GenomePoolDbException
//  {
//    StringBuilder sb = new StringBuilder();
////    sb.append("Frag GUID\t")
////        .append("Organism\t")
////        .append("Accession\t")
////        .append("DNA Seq Length\t")
////        .append("Fragment description\n");
//    List<String> guids = dao.listFragmentGuids();
//    int totalGenes = 0;
//    for (String fragGuid : guids)
//    {
//      GenomeFragment frag = dao.findFragmentByGuid(fragGuid);
//      List<Gene> genes = dao.findGenesForFragment(fragGuid);
//      totalGenes += genes.size();
//
//      for (Gene gene : genes)
//      {
//        GeneProduct prod = gene.firstProduct();
//        int gi = prod.getGi();
//        sb.append(frag.getGuid()).append("\t");
//        //      sb.append(frag.getOrganismGuid()).append("\t");
//        //      sb.append(frag.getAccession()).append("\t");
//        //      sb.append(frag.getDnaSequenceLength()).append("\t");
//        sb.append(frag.getDescription()).append("\t");
//        sb.append("GI: ").append(gi).append("\n");
//      }
//
////      sb.append("\n");
//
//      //New string builder for each fragment
//      System.out.print(sb);
//      sb = new StringBuilder();
//    }
//    sb.append("\nTotal fragments: ").append(guids.size()).append("\n");
//    sb.append("\nTotal genes: ").append(totalGenes).append("\n");
//    System.out.println(sb.toString());
//  }
//
//  private static void findSequenceLengthsLessThan(GenomePoolDAO dao, int length)
//      throws GenomePoolDbException
//  {
//    logger.info("Looking for protein sequences of length < "+length);
//    StringBuilder sb = new StringBuilder();
////    sb.append("Frag GUID\t")
////        .append("Organism\t")
////        .append("Accession\t")
////        .append("DNA Seq Length\t")
////        .append("Fragment description\n");
//    List<String> guids = dao.listFragmentGuids();
//    int totalGenes = 0;
//    int totalWithinRange = 0;
//    for (String fragGuid : guids)
//    {
//      GenomeFragment frag = dao.findFragmentByGuid(fragGuid);
//      List<Gene> genes = dao.findGenesForFragment(fragGuid);
//      totalGenes += genes.size();
//
//      for (Gene gene : genes)
//      {
//        GeneProduct prod = gene.firstProduct();
//        if (prod.getProductSequence().length() < length)
//        {
//          totalWithinRange++;
//          int gi = prod.getGi();
//          sb.append(frag.getGuid()).append("\t");
//          //      sb.append(frag.getOrganismGuid()).append("\t");
//          //      sb.append(frag.getAccession()).append("\t");
//          //      sb.append(frag.getDnaSequenceLength()).append("\t");
//          sb.append(frag.getDescription()).append("\t");
//          sb.append("GI: ").append(gi).append("\t");
//          sb.append("Seq length: ").append(prod.getProductSequence().length());
//          sb.append("\n");
//        }
//      }
//
////      sb.append("\n");
//
//      //New string builder for each fragment
//      System.out.print(sb);
//      sb = new StringBuilder();
//    }
//    sb.append("\nTotal fragments: ").append(guids.size()).append("\n");
//    sb.append("\nTotal genes/products: ").append(totalGenes).append("\n");
//    sb.append("\nTotal genes/products with a sequence less than: ").
//        append(length).append(": ").append(totalWithinRange).append("\n");
//    System.out.println(sb.toString());
//  }
//
//  private static void test(GenomePoolDAO dao)
//      throws GenomePoolDbException
//  {
//    List<String> orgGuids = dao.listOrganismGuids();
//    List<Integer> orgTaxons = dao.listOrganismTaxonIds();
//    System.out.println("Organism GUIDs: " + orgGuids);
//    System.out.println("Organism Taxons: " + orgTaxons);
//    System.out.println("Organism taxon count: " + dao.countTaxons());
//
//    System.out.println("\n-----------------------------------------------------");
//    System.out.println("Querying organisms by GUID\n");
//    for (String orgGuid : orgGuids)
//    {
//      System.out.println("Querying organism guid: " + orgGuid);
//      Organism org = dao.findOrganismByGuid(orgGuid);
//      System.out.println("Org: " + org);
//    }
//
//    System.out.println("\n-----------------------------------------------------");
//    System.out.println("Querying organisms by Taxon ID\n");
//    for (int orgTaxon : orgTaxons)
//    {
//      System.out.println("Querying organism taxon: " + orgTaxon);
//      Organism org = dao.findOrganismByTaxonId(orgTaxon);
//      System.out.println("Org: " + org);
//    }
//
//    System.out.println("\n-----------------------------------------------------");
//    System.out.println("Fragment queries\n");
//
//    List<String> fragmentGuids = dao.listFragmentGuids();
//    System.out.println("Fragment guids: " + fragmentGuids.size());
//    System.out.println("Fragment guids: " + dao.countFragments());
//    System.out.println("Fragment guids: " + fragmentGuids);
//
//    for (String fragGuid : fragmentGuids)
//    {
//      GenomeFragment frag = dao.findFragmentByGuid(fragGuid);
//      System.out.println("Fragment: " + frag);
//
//
//      String dna = dao.getDNAForFragment(fragGuid);
//      System.out.println("DNA seq len: " + dna.length());
//
//      List<Gene> genes = dao.findGenesForFragment(fragGuid);
//      System.out.println("GeneGuid test: " + genes.iterator().next().
//          getProducts().iterator().next().getGeneGuid());
//      break;
//    }
//
//    System.out.println("\n-----------------------------------------------------");
//    System.out.println("Raw GBK queries\n");
//
//    for (String fragGuid : fragmentGuids)
//    {
//      byte[] gbk = dao.getRawGbkFileForFragment(fragGuid);
//      System.out.println("Bytes: " + gbk.length);
////      System.out.println("Content:");
//      //System.out.println(new String(gbk));
//      break;
//    }
//  }
//
//  private static void listProductsFromFile(
//      GeneDAO geneDao, ProductDAO productDao, String inputFilename)
//      throws IOException, GenomePoolDbException
//  {
//    File inputFile = new File(inputFilename);
//    File outputFile = new File(inputFilename+".fasta");
//
//    BufferedReader br = new BufferedReader(new FileReader(inputFile));
//    BufferedWriter out = new BufferedWriter(new FileWriter(outputFile));
//
//    for (String line = br.readLine(); line != null; line = br.readLine())
//    {
//      int gi = Integer.parseInt(line);
//      GeneProduct product = productDao.findByGi(gi);
//      Gene gene = geneDao.findByGuid(product.getGeneGuid());
//      out.append(">").append(line).
//          append("|").append(product.getProductDescr()).
//          append("|").append(gene.getGeneName()).
//          append("\n");
//      out.append(product.getProductSequence()).append("\n");
//    }
//
//    out.flush();
//    out.close();
//  }
//
//  private static void listOrganismsByGi(
//      OrganismDAO organismDao, GeneDAO geneDao, ProductDAO productDao,
//      List<String> searchStrings)
//      throws GenomePoolDbException
//  {
//    for (String gi : searchStrings)
//    {
//      GeneProduct product = productDao.findByGi(Integer.parseInt(gi));
//      if (product == null)
//      {
//        System.out.println("GI "+gi+" not found in genome pool database!");
//        continue;
//      }
//      Gene gene = geneDao.findByGuid(product.getGeneGuid());
//      Organism org = organismDao.findByGuid(product.getOrganismGuid());
//
//      StringBuilder sb = new StringBuilder();
//      sb.append(gi).append(", ").append(gene.getGeneName()).
//          append(", ").append(product.getProductDescr()).
//          append(", ").append(org.getName());
//      System.out.println(sb);
//    }
//  }
}
