/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.microbasecloud.genbankresponder;

import com.microbase.responder.genbankcommon.GenbankJobDescription;
import com.microbase.responder.genbankcommon.GenbankJobResult;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import uk.org.microbase.dist.responder.RegistrationException;
import uk.org.microbase.dist.responder.ResponderInfo;
import uk.org.microbase.filesystem.spi.FileMetaData;
import uk.org.microbase.filesystem.spi.MBFile;
import uk.org.microbase.filesystem.spi.MicrobaseFS;
import uk.org.microbase.notification.data.Message;
import uk.org.microbase.responder.spi.AbstractMessageProcessor;
import uk.org.microbase.responder.spi.ProcessingException;
import uk.org.microbase.responder.spi.ResponderInfoPropertiesParser;
import uk.org.microbase.util.cli.NativeCommandExecutor;

/**
 *
 * @author prisni
 */
public class GenBankGenerator extends AbstractMessageProcessor{
    
    
      private static final Logger l = Logger.getLogger(GenBankGenerator.class.getName());
  
  /**
   * Configuration resource for this responder. A file with this name must be
   * located on the classpath.
   */
  static final String CONFIG_RESOURCE = GenBankGenerator.class.getSimpleName()+".properties";
  
  private static final String CONFIG_RESULT_NAME_REFORMATTER = "result_name_reformatter";
  
  private Properties config;
  
  private GenbankJobDescription jobDescr;
  private MBFile jobRemoteResultFile;
  private MBFile jobStdOutLogFile;
  private MBFile jobStdErrLogFile;
  
  
  
    
    private static final String TOPIC_IN = "GENBANK-FILE-CREATION-JOB";
    private static final String TOPIC_OUT = "GENBANK-REPORT-CREATED";
    
    private FileMetaData templateFile;
    private FileMetaData[] inputFiles;
    private FileMetaData[] outputS3File;
    private String logFileS3Bucket;
    private String logFileS3Path;

    public GenBankGenerator() {
        outputS3File = new FileMetaData[3];
    }
    
    

    @Override
    protected ResponderInfo createDefaultResponderInfo() throws RegistrationException{
        try{
        config = new Properties();
      // Use properties from the Microbase global configuration file, if present
      config.putAll(getRuntime().getRawConfigProperties());
      try (InputStream is = getRuntime().getClassLoader().getResourceAsStream(CONFIG_RESOURCE)) {
        if (is == null) {
          throw new RegistrationException("Failed to find configuration resource: "+CONFIG_RESOURCE);
        }
        config.load(is);
      }
        
      ResponderInfo info = ResponderInfoPropertiesParser.parseFromProperties(config);

      l.info("Responder config: "+info);
      return info;
        }catch (Exception e) {
            
      throw new RegistrationException(
          "Failed to load responder configuration", e);
    
        }
//        ResponderInfo info = new ResponderInfo();
//        info.setEnabled(true);
//        info.setMaxDistributedCores(300);
//        info.setMaxLocalCores(50);
//        info.setPreferredCpus(1);
//        info.setQueuePopulatorLastRunTimestamp(0);
//        info.setQueuePopulatorRunEveryMs(30000);
//        info.setResponderName(GenBankGenerator.class.getSimpleName());
//        info.setResponderVersion("1.0");
//        info.setResponderId(info.getResponderName() + "-"
//                + info.getResponderVersion());
//        info.setTopicOfInterest(TOPIC_IN);
//        return info;
    }

    public void preRun(Message msg) throws ProcessingException {
         System.out.println("Doing pre-run");
         
         l.info("Doing preRun");
    try {
      /*
       * Parse message as a responder specific job description.
       */
      jobDescr = parseMessageWithJsonContent(msg, GenbankJobDescription.class);
      
      
//      //................... to be ignored later
//        templateFile = parseTemplateMessage(msg);
//        inputFiles = parseInputMessage(msg);
                // set remote location for output
        String fileName = inputFiles[0].getLocation().getName();
        int i = 0;
        int j = fileName.length()-4;
        String actualFileName = fileName.substring(i,j);
        String remoteS3Bucket = inputFiles[0].getLocation().getBucket();
        String remoteS3Path = "genbank file for: " + inputFiles[0].getLocation().getName();
        String remoteGenbankS3Name = actualFileName + ".gbf";
        System.out.println(remoteGenbankS3Name);
        outputS3File[0] = new FileMetaData(remoteS3Bucket, remoteS3Path,
                remoteGenbankS3Name);
        String remoteValS3Name =  actualFileName + ".val";
        outputS3File[1] = new FileMetaData(remoteS3Bucket, remoteS3Path, remoteValS3Name);
        String remoteSqnS3Name =  actualFileName + ".sqn";
        outputS3File[2] = new FileMetaData(remoteS3Bucket, remoteS3Path, remoteSqnS3Name);
        logFileS3Bucket = remoteS3Bucket;
        logFileS3Path = remoteS3Path;
        
       //................................ content above to be ignored later
        
        }
    catch(Exception e) {
      throw new ProcessingException("Failed to parse message", e);
    }
    }

    public void cleanupPreviousResults(Message msg) throws ProcessingException {
    l.info("Doing cleanup");
    try
    {
      MicrobaseFS fs = getRuntime().getMicrobaseFS();
//      MBFile jobRemoteResultFile = resultFilenameReformatter.getFormattedRemoteFile();
      
      if (fs.exists(jobRemoteResultFile))
      {
        l.info("Found an existing result file "+jobRemoteResultFile
            +", generated by a previous "
            + "execution. Deleting...");
        fs.deleteRemoteCopy(jobRemoteResultFile);
      }
    }
    catch(Exception e)
    {
      throw new ProcessingException("Failed to perform cleanup", e);
    }
  }

    public Set<Message> processMessage(Message msg) throws ProcessingException {
        l.info("Performing computational work");
        try {
            MBFile remoteTemplateFile = jobDescr.getTemplateFile();
            MBFile[] remoteInputFiles = jobDescr.getInputFiles();
            
            l.info("Template file is: "+remoteTemplateFile);
            l.info("Subject input files are: "+remoteInputFiles);
            
            File subjectFastaFile = downloadFileToSharedSpace(remoteInputFiles[0]);
            File subjectFeatureTableFile = downloadFileToSharedSpace(remoteInputFiles[1]);
            File subjectTemplateFile = downloadFileToSharedSpace(remoteTemplateFile);
            
//            // ..... to be ignored
//            File template = downloadInputFile(templateFile);
//            File fastaFile = downloadInputFile(inputFiles[0]);
//            File featureTable = downloadInputFile(inputFiles[1]);
//            // ... content above is to be ignore later
            
            l.info("Running command ...");
            File[] gbkFiles = runTbl2AsnCommand(subjectTemplateFile, subjectFastaFile);
            
//            File[] outputsLocalFile = runTbl2Asn(template);
                    
//            getRuntime().getMicrobaseFS().upload(
//                    outputsLocalFile[0],
//                    outputS3File[0].getLocation(), null);
//            getRuntime().getMicrobaseFS().upload(
//                    outputsLocalFile[1],
//                    outputS3File[1].getLocation(), null);
//            getRuntime().getMicrobaseFS().upload(
//                    outputsLocalFile[2],
//                    outputS3File[2].getLocation(), null);
            Message genbankFileCreatedMsg = generateSuccessNotification(msg);

            Set<Message> messages = new HashSet<Message>();
            messages.add(genbankFileCreatedMsg);
            return messages;
        } catch (Exception e) {
            throw new ProcessingException("Failed to run one or more commands: "
                    + "on: " + getRuntime().getNodeInfo().getHostname()
                    + "in: " + getWorkingDirectory(), e);
        }
    }
   

//    private File downloadInputFile(FileMetaData remoteFile)
//            throws ProcessingException {
//        try {
//            //get a reference to the working temp dir
//            File tmpDir = getWorkingDirectory();
//
//            //downloading the gbk file
//            File destinationFile = new File(tmpDir, remoteFile.getLocation().getName());
//
//            getRuntime().getMicrobaseFS().downloadFileToSpecificLocation(
//                    remoteFile.getLocation(), destinationFile, true);
//
//            return destinationFile;
//        } catch (Exception e) {
//            throw new ProcessingException("Failed to download remote file" + 
//                    remoteFile.getLocation().getBucket() + " , " + remoteFile.getLocation().getPath());
//        }
//    }

    private FileMetaData[] parseInputMessage(Message m) 
            throws ProcessingException {
        try {

            FileMetaData fastaFile = new FileMetaData(m.getContent().
                    get("fasta_bucket"),
                    m.getContent().get("fasta_path"),
                    m.getContent().get("fasta_filename"));
            FileMetaData featureFile = new FileMetaData(m.getContent().
                    get("feature_bucket"),
                    m.getContent().get("feature_path"),
                    m.getContent().get("feature_filename"));

            return new FileMetaData[]{fastaFile, featureFile};
        } catch (Exception e) {
            throw new ProcessingException("Failed to generate parse message", e);
        }

    }


    private FileMetaData parseTemplateMessage(Message m) 
            throws ProcessingException {
        try{
            
              FileMetaData template = new FileMetaData(m.getContent().
                      get("template_bucket"),
                    m.getContent().get("template_path"),
                    m.getContent().get("template_filename"));
              return template;
        } catch (Exception e) {
            throw new ProcessingException("Failed to generate parse message");
        }
        
    }
//
//    private File[] runTbl2Asn(File template)
//        throws ProcessingException {
//        try {
////            File currDirFile = getWorkingDirectory();
//             String[] genbankCommand = new String[]{
//               "/mnt/responder_data/linux64.tbl2asn",
//               "-t", template.getAbsolutePath(),
//               "-p", getWorkingDirectory().getAbsolutePath(), 
//               "-V", "vb"};
//            executeCommand(genbankCommand, "genbankgenerator");
//
//            File genbankResultFile = new File(getWorkingDirectory(), 
//                    outputS3File[0].getLocation().getName());
//            File validateResultFile = new File(getWorkingDirectory(),
//                    outputS3File[1].getLocation().getName());
//            File sequinResultFile = new File(getWorkingDirectory(), 
//                    outputS3File[2].getLocation().getName());
//
//             return new File[]{genbankResultFile, validateResultFile, 
//                 sequinResultFile};
//        } catch (Exception e) {
//            throw new ProcessingException("Failed to execute Tbl2Asn", e);
//        }
//    }

    private void executeCommand(String[] command, String stdOutPrefix) 
            throws ProcessingException {
        System.out.println("*********************: Executing command: "+
                Arrays.asList(command));
        File workDir = getWorkingDirectory();
        File stdOutFile = new File(workDir, stdOutPrefix + "-stdout.txt");
        File stdErrFile = new File(workDir, stdOutPrefix + "-stderr.txt");
        try {
            FileWriter stdOut = new FileWriter(stdOutFile);
            FileWriter stdErr = new FileWriter(stdErrFile);

            int exitStatus = NativeCommandExecutor.executeNativeCommand(workDir,
                    command, stdOut, stdErr);
            System.out.println("Exit status of command: "+exitStatus);

            stdOut.flush();
            stdOut.close();
            stdErr.flush();
            stdErr.close();

            if (exitStatus != 0) {
                throw new ProcessingException("Exit status of the command: " +
                        Arrays.asList(command) + "\nwas " + exitStatus + 
                        ". Assuming that non-zero means that execution failed.");
            }
        } catch (Exception e) {
            throw new ProcessingException("Failed to execute command: " +
                    command[0], e);
        } finally {
            try {
                getRuntime().getMicrobaseFS().upload(stdOutFile, jobStdOutLogFile, null);
                getRuntime().getMicrobaseFS().upload(stdErrFile, jobStdErrLogFile, null);
//               MBFile standardOutputFile = new MBFile(logFileS3Bucket, logFileS3Path, stdOutFile.getName());
//                getRuntime().getMicrobaseFS().upload(stdOutFile, standardOutputFile, null);
//                      
//                MBFile standardErrorFile = new MBFile(logFileS3Bucket, logFileS3Path, stdErrFile.getName());
//                getRuntime().getMicrobaseFS().upload(stdErrFile, standardErrorFile, null);
            } catch (Exception e) {
                throw new ProcessingException("Failed to upload output file", e);
            }
        }
    }

       private Message generateSuccessNotification(Message parent) throws IOException {
        {
             /*
     * Prepare the content for the 'success' notification message.
     */
    
    GenbankJobResult resultContent = new GenbankJobResult();
    resultContent.setJob(job);
    resultContent.setResultFile(jobRemoteResultFile);
    resultContent.setStdErrLogFile(jobStdErrLogFile);
    resultContent.setStdOutLogFile(jobStdOutLogFile);
            Message successMsg = createMessage(
                    parent,
                    //The message to use as a parent
                    TOPIC_OUT,
                    //The topic of the new message
                    parent.getWorkflowStepId(),
                    //The workflow step ID
                    //An optional human-readable description
                    "Successfully generated a genbank file for the provided fasta file "
                    + inputFiles[0].getLocation().getName() + " and features table file " + 
                    inputFiles[1].getLocation().getName());
            successMsg.getContent().put("fasta_bucket", inputFiles[0].getLocation().getBucket());
            successMsg.getContent().put("fasta_path", inputFiles[0].getLocation().getPath());
            successMsg.getContent().put("fasta_filename", inputFiles[0].getLocation().getName());
            successMsg.getContent().put("features_bucket", inputFiles[1].getLocation().getBucket());
            successMsg.getContent().put("features_path", inputFiles[1].getLocation().getPath());
            successMsg.getContent().put("features_filename", inputFiles[1].getLocation().getName());
            successMsg.getContent().put("genbank_result_bucket",
                    outputS3File[0].getLocation().getBucket());
            successMsg.getContent().put("genbank_result_path", 
                    outputS3File[0].getLocation().getPath());
            successMsg.getContent().put("genbank_result_filename",
                    outputS3File[0].getLocation().getName());
            successMsg.getContent().put("validate_result_bucket",
                    outputS3File[1].getLocation().getBucket());
            successMsg.getContent().put("validate_result_path", 
                    outputS3File[1].getLocation().getPath());
            successMsg.getContent().put("validate_result_filename",
                    outputS3File[1].getLocation().getName());
            successMsg.getContent().put("sqn_result_bucket",
                    outputS3File[2].getLocation().getBucket());
            successMsg.getContent().put("sqn_result_path", 
                    outputS3File[2].getLocation().getPath());
            successMsg.getContent().put("sqn_result_filename",
                    outputS3File[2].getLocation().getName());
            return successMsg;
        }

    }

    private File[] runTbl2AsnCommand(File subjectTemplateFile, File subjectFastaFile) throws ProcessingException {
        String cmdLineText = null;
        String fileName = subjectFastaFile.getName();
        int i = 0;
        int j = fileName.length()-4;
        String actualFileName = fileName.substring(i,j);
        try{
        
        File genbankResultFile = new File(getWorkingDirectory(), actualFileName + ".gbf");
        File validateResultFile = new File(getWorkingDirectory(),
                actualFileName + ".val");
        File sequinResultFile = new File(getWorkingDirectory(),
                actualFileName + ".sqn");
     
               String[] genbankCommand = new String[]{
               "/mnt/responder_data/linux64.tbl2asn",
               "-t", subjectTemplateFile.getAbsolutePath(),
               "-p", getWorkingDirectory().getAbsolutePath(), 
               "-V", "vb"};
            executeCommand(genbankCommand, "genbankgenerator");
            return new File[]{genbankResultFile, validateResultFile, 
                 sequinResultFile};
  
}catch(Exception e){
    throw new ProcessingException("Failed to execute the command line: "+cmdLineText, e);
}
    }
}
