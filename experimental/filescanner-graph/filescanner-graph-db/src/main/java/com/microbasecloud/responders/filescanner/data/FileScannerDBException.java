/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: 13-Oct-2012, 23:04:53
 */

package com.microbasecloud.responders.filescanner.data;

/**
 *
 * @author Keith Flanagan
 */
public class FileScannerDBException
    extends Exception
{

  public FileScannerDBException()
  {
  }

  public FileScannerDBException(String message)
  {
    super(message);
  }

  public FileScannerDBException(String message, Throwable cause)
  {
    super(message, cause);
  }

  public FileScannerDBException(Throwable cause)
  {
    super(cause);
  }

}
