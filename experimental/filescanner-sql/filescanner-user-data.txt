!install_jar=http://microbase.responders.s3.amazonaws.com/microbase-2.0/filescanner/filescanner-exe-FILESCANNER-SNAPSHOT.jar

!install_jar=http://microbase.responders.s3.amazonaws.com/microbase-2.0/filescanner/antlr-2.7.7.jar
!install_jar=http://microbase.responders.s3.amazonaws.com/microbase-2.0/filescanner/commons-cli-1.2.jar
!install_jar=http://microbase.responders.s3.amazonaws.com/microbase-2.0/filescanner/concurrent-TC-SNAPSHOT.jar
!install_jar=http://microbase.responders.s3.amazonaws.com/microbase-2.0/filescanner/distributed-data-spi-MB-SNAPSHOT.jar
!install_jar=http://microbase.responders.s3.amazonaws.com/microbase-2.0/filescanner/dom4j-1.6.1.jar
!install_jar=http://microbase.responders.s3.amazonaws.com/microbase-2.0/filescanner/filescanner-common-FILESCANNER-SNAPSHOT.jar
!install_jar=http://microbase.responders.s3.amazonaws.com/microbase-2.0/filescanner/filescanner-db-hibernate-FILESCANNER-SNAPSHOT.jar
!install_jar=http://microbase.responders.s3.amazonaws.com/microbase-2.0/filescanner/filesystem-spi-MB-SNAPSHOT.jar
!install_jar=http://microbase.responders.s3.amazonaws.com/microbase-2.0/filescanner/hazelcast-2.3.1.jar
!install_jar=http://microbase.responders.s3.amazonaws.com/microbase-2.0/filescanner/hazelcast-cloud-2.3.1.jar
!install_jar=http://microbase.responders.s3.amazonaws.com/microbase-2.0/filescanner/hibernate-commons-annotations-4.0.1.Final.jar
!install_jar=http://microbase.responders.s3.amazonaws.com/microbase-2.0/filescanner/hibernate-core-4.1.1.Final.jar
!install_jar=http://microbase.responders.s3.amazonaws.com/microbase-2.0/filescanner/hibernate-entitymanager-4.1.1.Final.jar
!install_jar=http://microbase.responders.s3.amazonaws.com/microbase-2.0/filescanner/hibernate-jpa-2.0-api-1.0.1.Final.jar
!install_jar=http://microbase.responders.s3.amazonaws.com/microbase-2.0/filescanner/hibernate-util-TC-SNAPSHOT.jar
!install_jar=http://microbase.responders.s3.amazonaws.com/microbase-2.0/filescanner/jackson-core-lgpl-1.9.3.jar
!install_jar=http://microbase.responders.s3.amazonaws.com/microbase-2.0/filescanner/jackson-mapper-lgpl-1.9.3.jar
!install_jar=http://microbase.responders.s3.amazonaws.com/microbase-2.0/filescanner/javassist-3.15.0-GA.jar
!install_jar=http://microbase.responders.s3.amazonaws.com/microbase-2.0/filescanner/jboss-logging-3.1.0.GA.jar
!install_jar=http://microbase.responders.s3.amazonaws.com/microbase-2.0/filescanner/jboss-transaction-api_1.1_spec-1.0.0.Final.jar
!install_jar=http://microbase.responders.s3.amazonaws.com/microbase-2.0/filescanner/mysql-connector-java-5.1.18.jar
!install_jar=http://microbase.responders.s3.amazonaws.com/microbase-2.0/filescanner/notification-spi-MB-SNAPSHOT.jar
!install_jar=http://microbase.responders.s3.amazonaws.com/microbase-2.0/filescanner/postgresql-9.0-801.jdbc4.jar
!install_jar=http://microbase.responders.s3.amazonaws.com/microbase-2.0/filescanner/runtime-MB-SNAPSHOT.jar
!install_jar=http://microbase.responders.s3.amazonaws.com/microbase-2.0/filescanner/sql-util-TC-SNAPSHOT.jar
!install_jar=http://microbase.responders.s3.amazonaws.com/microbase-2.0/filescanner/utils-MB-SNAPSHOT.jar
!install_jar=http://microbase.responders.s3.amazonaws.com/microbase-2.0/filescanner/util-TC-SNAPSHOT.jar
!install_jar=http://microbase.responders.s3.amazonaws.com/microbase-2.0/filescanner/xml-apis-1.0.b2.jar

# Make sure this file ends with a blank line
