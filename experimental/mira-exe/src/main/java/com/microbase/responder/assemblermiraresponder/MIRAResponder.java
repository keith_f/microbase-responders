/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.microbase.responder.assemblermiraresponder;

import com.microbase.responder.assemblermiracommon.AssemblerMIRAJobDescription;
import com.microbase.responder.assemblermiracommon.AssemblerMIRAJobResult;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Logger;
import uk.org.microbase.dist.responder.RegistrationException;
import uk.org.microbase.dist.responder.ResponderInfo;
import uk.org.microbase.filesystem.spi.FSException;
import uk.org.microbase.filesystem.spi.FSOperationNotSupportedException;
import uk.org.microbase.filesystem.spi.MBFile;
import uk.org.microbase.filesystem.spi.MicrobaseFS;
import uk.org.microbase.notification.data.Message;
import uk.org.microbase.responder.spi.AbstractMessageProcessor;
import uk.org.microbase.responder.spi.ProcessingException;
import uk.org.microbase.responder.spi.ResponderInfoPropertiesParser;
import uk.org.microbase.util.cli.NativeCommandException;
import uk.org.microbase.util.cli.NativeCommandExecutor;

/**
 *
 * @author prisni
 */
public class MIRAResponder extends AbstractMessageProcessor {
    
    /*
   * Configuration options (these should be treated as 'final'
   */
  private Properties config;
  private AssemblerMIRAOutputPathReformatter resultFilenameReformatter;
  private AssemblerMIRAOutputPathReformatter stdOutFilenameReformatter;
  private AssemblerMIRAOutputPathReformatter stdErrFilenameReformatter;
  
    
    private static final Logger l = Logger.getLogger(MIRAResponder.class.getName());
  
  /**
   * Configuration resource for this responder. A file with this name must be
   * located on the classpath.
   */
  static final String CONFIG_RESOURCE = MIRAResponder.class.getSimpleName()+".properties";
  private static final String CONFIG_RESULT_NAME_REFORMATTER = "result_name_reformatter";
  
     /*
     * Per-job variables. These are set every time <code>preRun</code> is called.
     */
    private AssemblerMIRAJobDescription jobDescr;
    private MBFile jobRemoteResultFile;
    private MBFile jobStdOutLogFile;
    private MBFile jobStdErrLogFile;
  
  
    private String projectName="Strain-4"; // set project name as "Strain-4"
    private String remoteS3Bucket;

    public MIRAResponder() {
//        outputAssemblyfiles = new FileMetaData[9];
        resultFilenameReformatter = new AssemblerMIRAResultInIndividualDirsFormatter();
        stdOutFilenameReformatter = new AssemblerMIRAStdOutLogFileFormatter();
        stdErrFilenameReformatter = new AssemblerMIRAStdErrLogFileFormatter();
    }
    
    /**
     * Create a default responder with pre-set configuration properties
     *
     * @return responder information
     */
    @Override
    protected ResponderInfo createDefaultResponderInfo() throws RegistrationException { 
        try{
            config = new Properties();
      // Use properties from the Microbase global configuration file, if present
      config.putAll(getRuntime().getRawConfigProperties());
      try (InputStream is = getRuntime().getClassLoader().getResourceAsStream(CONFIG_RESOURCE)) {
        if (is == null) {
          throw new RegistrationException("Failed to find configuration resource: "+CONFIG_RESOURCE);
        }
        config.load(is);
      }
        
      ResponderInfo info = ResponderInfoPropertiesParser.parseFromProperties(config);

      l.info("Responder config: "+info);
      return info;
        }catch (Exception e) {
            
      throw new RegistrationException(
          "Failed to load responder configuration", e);
    
        }
        }

    @Override
    public void preRun(Message message) throws ProcessingException {
        
        System.out.println("Doing pre-run");
         
         l.info("Doing preRun");
    try {
      /*
       * Parse message as a responder specific job description.
       */
      jobDescr = parseMessageWithJsonContent(message, AssemblerMIRAJobDescription.class);
      
      /*
       * Configure the result of file formatters
       */
      resultFilenameReformatter.configure(getRuntime(), getResponderInfo(), getProcessInfo(), message);
      resultFilenameReformatter.setJobDescription(jobDescr);
      stdOutFilenameReformatter.configure(getRuntime(), getResponderInfo(), getProcessInfo(), message);
      stdOutFilenameReformatter.setJobDescription(jobDescr);
      stdErrFilenameReformatter.configure(getRuntime(), getResponderInfo(), getProcessInfo(), message);
      stdErrFilenameReformatter.setJobDescription(jobDescr);

      jobRemoteResultFile = resultFilenameReformatter.getFormattedRemoteFile();
      jobStdOutLogFile = stdOutFilenameReformatter.getFormattedRemoteFile();
      jobStdErrLogFile = stdErrFilenameReformatter.getFormattedRemoteFile();

    }
    catch(Exception e) {
      throw new ProcessingException("Failed to parse message", e);
    }
    }
   
    //cleaning the entire directory
    @Override
    public void cleanupPreviousResults(Message msg)
            throws ProcessingException {
        l.info("Doing cleanup");
    try
    {
      MicrobaseFS fs = getRuntime().getMicrobaseFS();
//      MBFile jobRemoteResultFile = resultFilenameReformatter.getFormattedRemoteFile();
      
      if (fs.exists(jobRemoteResultFile))
      {
        l.info("Found an existing result file "+jobRemoteResultFile
            +", generated by a previous "
            + "execution. Deleting...");
        fs.deleteRemoteCopy(jobRemoteResultFile);
      }
    }
    catch(Exception e)
    {
      throw new ProcessingException("Failed to perform cleanup", e);
    }
    }

    @Override
    public Set<Message> processMessage(Message msg)
            throws ProcessingException {
                   
             l.info("Performing computational work");
        try {
           MBFile remoteFastQFile = jobDescr.getFastQFile();
           l.info("Subject FASTQ input file is: "+remoteFastQFile);
           
           MBFile remoteTraceInfoXMLFile = jobDescr.getTraceInfoXmlFile();
           l.info("Subject trace_info XML fils is: "+remoteTraceInfoXMLFile);
            
            File subjectFastQFile = downloadFileToSharedSpace(remoteFastQFile);
            File subjectTraceInfoXMLFile = downloadFileToSharedSpace(remoteTraceInfoXMLFile);
           
            l.info("Running command ...");
            miraAssembler(); //runs MIRA

            uploadRelevantResults(projectName); //uploads the resulting directory to the S3 bucket
            
            Message genbankFileCreatedMsg = generateSuccessNotification(msg, jobDescr);

            Set<Message> messages = new HashSet<Message>();
            messages.add(genbankFileCreatedMsg);
            return messages;

        } catch (Exception e) {
            throw new ProcessingException("Failed to run one or more commands: "
                    + "on: " + getRuntime().getNodeInfo().getHostname()
                    + "in: " + getWorkingDirectory(), e);

        }

    }

  
     private Message generateSuccessNotification(Message parent, AssemblerMIRAJobDescription job)
            throws IOException {
        /*
         * Prepare the content for the 'success' notification message.
         */

        AssemblerMIRAJobResult resultContent = new AssemblerMIRAJobResult();
        resultContent.setJob(job);
        resultContent.setResultFile(jobRemoteResultFile);
        resultContent.setStdErrLogFile(jobStdErrLogFile);
        resultContent.setStdOutLogFile(jobStdOutLogFile);

        /*
         * Create the success message with the above content. Link this success
         * message to the parent message (the job descriptor) for provenance.
         */
        Message successMsg = createMessageWithJsonContent(
                parent, //The message to use as a parent
                getResponderInfo().getTopicOut(), //The topic of the new message
                parent.getWorkflowStepId(), //The workflow step ID
                resultContent, //The result content item
                //An optional human-readable description
                "Successfully completed assembly job.");

        return successMsg;
    }

    private void executeCommand(String[] command, String stdOutPrefix) throws ProcessingException {
        File workDir = getWorkingDirectory();
        File stdOutFile = new File(workDir, stdOutPrefix + "-stdout.txt");
        File stdErrFile = new File(workDir, stdOutPrefix + "-stderr.txt");
        try {
            FileWriter stdOut = new FileWriter(stdOutFile);
            FileWriter stdErr = new FileWriter(stdErrFile);

            int exitStatus = NativeCommandExecutor.executeNativeCommand(workDir, command, stdOut, stdErr);

            stdOut.flush();
            stdOut.close();
            stdErr.flush();
            stdErr.close();

            if (exitStatus != 0) {
                throw new ProcessingException("Exit status of the command: "
                        + Arrays.asList(command) + "\nwas " + exitStatus
                        + ". Assuming that non-zero means that execution failed.");
            }
        } catch (IOException | NativeCommandException | ProcessingException e) {
            throw new ProcessingException("Failed to execute command: "
                    + command[0], e);
        } finally {
            try {
                
                getRuntime().getMicrobaseFS().upload(stdOutFile, jobStdOutLogFile, null);
                getRuntime().getMicrobaseFS().upload(stdErrFile, jobStdErrLogFile, null);

            } catch (Exception e) {
                throw new ProcessingException("Failed to upload output file", e);
            }
        }
    }


    private void miraAssembler() throws ProcessingException {
        String[] miraCommand = new String[]{
            "/mnt/responder_data/mira_3.4.0_prod_linux-gnu_x86_64_static/bin/mira",
            "--project=Strain-4", "--job=denovo,genome,accurate,iontor"};
        executeCommand(miraCommand, "mira");
        
    }

    private void uploadRelevantResults(String projectName) throws FSOperationNotSupportedException, FSException {
        String directoryName = projectName+"_assembly/"+projectName+"_d_results";
        File resultDirectory = new File(getWorkingDirectory(), directoryName);
        for(File file:resultDirectory.listFiles()){
                     MBFile resultFile = new MBFile(remoteS3Bucket, directoryName, file.getName());       

                      l.info("Uploading result files to: " + resultFile);
                    getRuntime().getMicrobaseFS().upload(
                    file, resultFile, null);
                 
                
            }
        }
    }
    

