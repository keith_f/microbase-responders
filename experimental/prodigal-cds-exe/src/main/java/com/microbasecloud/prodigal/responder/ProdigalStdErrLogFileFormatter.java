/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.microbasecloud.prodigal.responder;

import com.microbase.responder.prodigalcdscommon.ProdigalJobDescription;
import uk.org.microbase.filesystem.spi.MBFile;
import uk.org.microbase.responder.util.AbstractOutputPathReformatter;

/**
 *
 * @author prisni
 */
public class ProdigalStdErrLogFileFormatter extends AbstractOutputPathReformatter
    implements ProdigalOutputPathReformatter
{
  
  private ProdigalJobDescription jobDescription;
  
  @Override
  public String getFormattedBucketName() {
    return responderInfo.getLogDestinationBucket();
  }

  @Override
  public String getFormattedPath() {
    StringBuilder path = new StringBuilder(responderInfo.getLogDestinationBasePath());
    path.append("/").append(getFormattedFilename());
    path.append("/").append(message.getGuid());
    path.append("/").append(processInfo.getWorkStartedAtMs());
    return path.toString();
  }

  @Override
  public String getFormattedFilename() {
    StringBuilder resultFilename = new StringBuilder();
    resultFilename.append(jobDescription.getQueryFastaFile().getName())
        .append("-").append(jobDescription.getPredictionType().name())
        .append(".stderr");
    return resultFilename.toString();
  }
  
  @Override
  public MBFile getFormattedRemoteFile() {
    MBFile file = new MBFile(getFormattedBucketName(), getFormattedPath(), getFormattedFilename());
    return file;
  }
  

  public ProdigalJobDescription getJobDescription() {
    return jobDescription;
  }

  @Override
  public void setJobDescription(ProdigalJobDescription jobDescription) {
    this.jobDescription = jobDescription;
  }

}

