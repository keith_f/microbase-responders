/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: 25-Mar-2012, 12:40:04
 */

package com.dragnportal.hmmer.db;

import com.dragnportal.hmmer.db.data.HmmerHit;
import com.dragnportal.hmmer.db.data.HmmerReport;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author Keith Flanagan
 */
public class HmmerReportDAO
{
  
  public void store(Session session, HmmerReport report)
      throws HmmerDbException
  {
    try
    {
      session.save(report);
      session.flush();
    }
    catch(Exception e)
    {
      throw new HmmerDbException(
          "Failed to execute store()", e);
    }
    finally
    {
    }
  }
  
  public List<HmmerReport> selectAllReports(Session session)
      throws HmmerDbException
  {
    try
    {
      String qry = "from "+HmmerReport.class.getName();
      Query query = session.createQuery(qry);
        
      List<HmmerReport> reports = query.list();
      return reports;
    }
    catch(Exception e)
    {
      throw new HmmerDbException(
          "Failed to execute selectAllReports()", e);
    }
    finally
    {
    }
  }
  
  
}
