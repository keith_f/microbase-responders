/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.microbase.responder.sff;

import com.microbase.responder.sffcommon.SFFJobDescription;
import com.microbasecloud.shell.messageutil.ShellMsgGenerator;
import com.microbasecloud.shell.messageutil.ShellMsgGeneratorException;
import java.util.HashMap;
import java.util.Map;
import org.codehaus.jackson.map.ObjectMapper;
import uk.org.microbase.filesystem.spi.MBFile;

/**
 *
 * @author prisni
 */
public class SFFJobMessageGenerator implements ShellMsgGenerator<SFFJobDescription>
{
  
  @Override
  public String getMessageProcessorClassname() {
    return SFFResponder.class.getName();
  }

  @Override
  public Map<String, String> getReqPropertyNames() {
    Map<String, String> nameToDesc = new HashMap();
    
    
    
    nameToDesc.put("sff.zip.bucket", "MBFS bucket of the sff zip file");
    nameToDesc.put("sff.zip.path", "Path to the sff zip file");
    nameToDesc.put("sff.zip.filename", "Filename of the sff zip file");
    
        
    return nameToDesc;
  }

  @Override
  public String generateJson(Map<String, String> properties) 
          throws ShellMsgGeneratorException {
    try {
      SFFJobDescription bean = generateMsgBean(properties);

      // Serialise message content to JSON
      ObjectMapper mapper = new ObjectMapper();
      String json = mapper.writeValueAsString(bean);
      return json;
    }
    catch(Exception e) {
      throw new ShellMsgGeneratorException(
              "Failed to generate and serialise a message content bean "
              + "from properties: "+properties, e);
    }
  }

  @Override
  public SFFJobDescription generateMsgBean(Map<String, String> properties) 
          throws ShellMsgGeneratorException {
    try {
      SFFJobDescription bean = new SFFJobDescription();
     
      bean.setSffZipFile(
              new MBFile(
              properties.get("sff.zip.bucket"), 
              properties.get("sff.zip.path"), 
              properties.get("sff.zip.filename")));
     
      return bean;
    }
    catch(Exception e) {
      throw new ShellMsgGeneratorException(
              "Failed to generate message content bean "
              + "from properties: "+properties, e);
    }
  }

    
}
