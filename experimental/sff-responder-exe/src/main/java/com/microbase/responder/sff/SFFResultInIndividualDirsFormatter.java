/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.microbase.responder.sff;

import com.microbase.responder.sffcommon.SFFJobDescription;
import uk.org.microbase.filesystem.spi.MBFile;
import uk.org.microbase.responder.util.AbstractOutputPathReformatter;

/**
 *
 * @author prisni
 */
public class SFFResultInIndividualDirsFormatter extends AbstractOutputPathReformatter
    implements SFFOutputPathReformatter
{
  
  private SFFJobDescription jobDescription;
  
  @Override
  public String getFormattedBucketName() {
    return responderInfo.getResultDestinationBucket();
  }

  @Override
  public String getFormattedPath() {
    StringBuilder path = new StringBuilder(responderInfo.getResultDestinationBasePath());
    path.append("/").append(getFormattedFilename());
    return path.toString();
  }

  @Override
  public String getFormattedFilename() {
    StringBuilder resultFilename = new StringBuilder();
    resultFilename.append(SFFResponder.projectName).append("_").append(jobDescription.getOutputType().name())
        .append(".").append("iontor")
        .append(".").append(jobDescription.getOutputFileType().name());
    return resultFilename.toString();
  }
  
  @Override
  public MBFile getFormattedRemoteFile() {
    MBFile file = new MBFile(getFormattedBucketName(), getFormattedPath(), getFormattedFilename());
    return file;
  }
  

  public SFFJobDescription getJobDescription() {
    return jobDescription;
  }

  
  public void setJobDescription(SFFJobDescription jobDescription) {
    this.jobDescription = jobDescription;
  }

}
