/*
 * Copyright 2011 Keith Flanagan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * File created: 20-Sep-2010, 17:48:17
 */

package uk.ac.ncl.mucosa.genomepool.ws;

import com.torrenttamer.jdbcutils.DatabaseConfiguration;
import com.torrenttamer.jdbcutils.DatabaseConfigurationException;
import com.torrenttamer.jdbcutils.DistributedJdbcPool;
import com.torrenttamer.jdbcutils.DistributedJdbcPoolFactory;
import java.io.IOException;
import java.util.logging.Logger;
import uk.org.microbase.runtime.ConfigurationException;

/**
 *
 * @author Keith Flanagan
 */
public class GenomePoolConfig
{
  private static final String DEFAULT_GP_FULL_CONFIG_RESOURCE =
      GenomePoolConfig.class.getSimpleName() + ".properties";
  
  private static final String DEFAULT_GP_LITE_HIBERNATE_CONFIG_RESOURCE
      = "GenomePoolLiteConfig-hibernate.cfg.xml";

  private static final Logger logger =
      Logger.getLogger(GenomePoolConfig.class.getName());
  
  
  private String gpFullConfigResource;
  private String gpLiteConfigResource;
  
  public GenomePoolConfig()
  {
    gpFullConfigResource = DEFAULT_GP_FULL_CONFIG_RESOURCE;
    gpLiteConfigResource = DEFAULT_GP_LITE_HIBERNATE_CONFIG_RESOURCE;
  }

  public String getGpFullConfigResource()
  {
    return gpFullConfigResource;
  }

  public void setGpFullConfigResource(String gpFullConfigResource)
  {
    this.gpFullConfigResource = gpFullConfigResource;
  }

  public String getGpLiteConfigResource()
  {
    return gpLiteConfigResource;
  }

  public void setGpLiteConfigResource(String gpLiteConfigResource)
  {
    this.gpLiteConfigResource = gpLiteConfigResource;
  }
  
  



  public static DistributedJdbcPool createDataSourceFromResource(
      ClassLoader cl, String resourcePath)
      throws ConfigurationException, DatabaseConfigurationException
  {
    try
    {
      DatabaseConfiguration config = DistributedJdbcPoolFactory.
          readConfigurationFromPropertyResource(cl, resourcePath);

      DistributedJdbcPool pool = DistributedJdbcPoolFactory.
          createSingleWriteMultipleReplicaPool(config);
      return pool;
    }
    catch(IOException e)
    {
      throw new ConfigurationException(
          "Failed to read notification system database configuration file", e);
    }
    catch(DatabaseConfigurationException e)
    {
      throw new ConfigurationException(
          "Failed to configure notification system database pool", e);
    }
  }

  public static DistributedJdbcPool createDataSourceFromDefaultConfigFile(
      ClassLoader cl)
      throws ConfigurationException, DatabaseConfigurationException
  {
    return createDataSourceFromResource(cl, DEFAULT_GP_FULL_CONFIG_RESOURCE);
  }

}
