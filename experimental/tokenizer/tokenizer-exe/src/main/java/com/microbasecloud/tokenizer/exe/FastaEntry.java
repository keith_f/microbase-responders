/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published
 *  by the Free Software Foundation. The full license may be found in
 *  COPYING.LESSER in this project's root directory.
 * 
 *  Copyright 2009 jointly held by the authors listed at the top of each
 *  source file and/or their respective employers.
 */

package com.microbasecloud.tokenizer.exe;

/**
 *
 * @author Keith Flanagan
 * @author Sirintra Nakjang
 */
public class FastaEntry
{
  private String gi;
  private String description;
  private String sequence;
  private String accession;

  public FastaEntry()
  {

  }

  public String getDescription()
  {
    return description;
  }

  public void setDescription(String description)
  {
    this.description = description;
  }

  public String getGi()
  {
    return gi;
  }

  public void setGi(String gi)
  {
    this.gi = gi;
  }

  public String getSequence()
  {
    return sequence;
  }

  public void setSequence(String sequence)
  {
    this.sequence = sequence;
  }

  public String getAccession()
  {
    return accession;
  }

  public void setAccession(String accession)
  {
    this.accession = accession;
  }



  
}
