/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: 25-Mar-2012, 12:40:32
 */
package com.microbasecloud.responders.filescanner.data.nodes;

import com.microbasecloud.filescanner.common.FileStatus;
import java.io.Serializable;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import uk.ac.ncl.aries.entanglement.graph.data.Node;

/**
 *
 * @author Keith Flanagan
 */
public class ScanReport
        extends Node {

  public static class FileEntry
          implements Serializable
  {

    private FileStatus status;
    private String bucket;
    private String filePath;
    private String filename;
    private Date modified;
    private long fileLength;

    public FileEntry() {
    }

    public FileEntry(String bucket, String path, String name) {
      this.bucket = bucket;
      this.filePath = path;
      this.filename = name;
    }

    @Override
    public String toString() {
      StringBuilder sb = new StringBuilder("FileEntry{");
      sb.append("bucket=").append(bucket)
              .append(", path=").append(filePath)
              .append(", name=").append(filename)
              .append(", status=").append(status)
              .append(", lastModified=").append(modified)
              .append(", fileLength=").append(fileLength)
              .append("}");
      return sb.toString();
    }

    public String getBucket() {
      return bucket;
    }

    public void setBucket(String bucket) {
      this.bucket = bucket;
    }

    public String getFilePath() {
      return filePath;
    }

    public void setFilePath(String filePath) {
      this.filePath = filePath;
    }

    public String getFilename() {
      return filename;
    }

    public void setFilename(String filename) {
      this.filename = filename;
    }

    public Date getModified() {
      return modified;
    }

    public void setModified(Date modified) {
      this.modified = modified;
    }

    public long getFileLength() {
      return fileLength;
    }

    public void setFileLength(long fileLength) {
      this.fileLength = fileLength;
    }

    public FileStatus getStatus() {
      return status;
    }

    public void setStatus(FileStatus status) {
      this.status = status;
    }
  }
  
  
  private String previousReportUid;
  private Date scanTimestamp;
  private String targetName;
  private String bucket;
  private String filePath;
  private int totalFileStatusChanges;
  private int totalNumFiles;
  private int numNewFiles;
  private int numUpdatedFiles;
  private int numDeletedFiles;
  private int numUnchangedFiles;
  private Set<FileEntry> newFiles;
  private Set<FileEntry> updatedFiles;
  private Set<FileEntry> deletedFiles;
  private Set<FileEntry> unchangedFiles;

  public ScanReport() {
    newFiles = new HashSet<>();
    updatedFiles = new HashSet<>();
    deletedFiles = new HashSet<>();
    unchangedFiles = new HashSet<>();
  }

  @Override
  public String toString() {
    return "ScanReport{" + "uid=" + uid + ", previousReportUid=" + previousReportUid
            + ", scanTimestamp=" + scanTimestamp + ", targetName=" + targetName
            + ", bucket=" + bucket + ", filePath=" + filePath
            + ", totalFileStatusChanges=" + totalFileStatusChanges
            + ", totalNumFiles=" + totalNumFiles + ", numNewFiles=" + numNewFiles
            + ", numUpdatedFiles=" + numUpdatedFiles + ", numDeletedFiles=" + numDeletedFiles
            + ", numUnchangedFiles=" + numUnchangedFiles + '}';
  }
  
  public String getPreviousReportUid() {
    return previousReportUid;
  }

  public void setPreviousReportUid(String previousReportUid) {
    this.previousReportUid = previousReportUid;
  }

  public Date getScanTimestamp() {
    return scanTimestamp;
  }

  public void setScanTimestamp(Date scanTimestamp) {
    this.scanTimestamp = scanTimestamp;
  }

  public String getBucket() {
    return bucket;
  }

  public void setBucket(String bucket) {
    this.bucket = bucket;
  }

  public String getTargetName() {
    return targetName;
  }

  public void setTargetName(String targetName) {
    this.targetName = targetName;
  }

  public String getFilePath() {
    return filePath;
  }

  public void setFilePath(String filePath) {
    this.filePath = filePath;
  }

  public int getTotalFileStatusChanges() {
    return totalFileStatusChanges;
  }

  public void setTotalFileStatusChanges(int totalFileStatusChanges) {
    this.totalFileStatusChanges = totalFileStatusChanges;
  }

  public int getTotalNumFiles() {
    return totalNumFiles;
  }

  public void setTotalNumFiles(int totalNumFiles) {
    this.totalNumFiles = totalNumFiles;
  }

  public int getNumNewFiles() {
    return numNewFiles;
  }

  public void setNumNewFiles(int numNewFiles) {
    this.numNewFiles = numNewFiles;
  }

  public int getNumUpdatedFiles() {
    return numUpdatedFiles;
  }

  public void setNumUpdatedFiles(int numUpdatedFiles) {
    this.numUpdatedFiles = numUpdatedFiles;
  }

  public int getNumDeletedFiles() {
    return numDeletedFiles;
  }

  public void setNumDeletedFiles(int numDeletedFiles) {
    this.numDeletedFiles = numDeletedFiles;
  }

  public int getNumUnchangedFiles() {
    return numUnchangedFiles;
  }

  public void setNumUnchangedFiles(int numUnchangedFiles) {
    this.numUnchangedFiles = numUnchangedFiles;
  }

  public Set<FileEntry> getNewFiles() {
    return newFiles;
  }

  public void setNewFiles(Set<FileEntry> newFiles) {
    this.newFiles = newFiles;
  }

  public Set<FileEntry> getUpdatedFiles() {
    return updatedFiles;
  }

  public void setUpdatedFiles(Set<FileEntry> updatedFiles) {
    this.updatedFiles = updatedFiles;
  }

  public Set<FileEntry> getDeletedFiles() {
    return deletedFiles;
  }

  public void setDeletedFiles(Set<FileEntry> deletedFiles) {
    this.deletedFiles = deletedFiles;
  }

  public Set<FileEntry> getUnchangedFiles() {
    return unchangedFiles;
  }

  public void setUnchangedFiles(Set<FileEntry> unchangedFiles) {
    this.unchangedFiles = unchangedFiles;
  }
}
