/*
 * Copyright 2013 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package com.microbasecloud.responders.filescanner.data.nodes;

import java.util.Date;
import uk.ac.ncl.aries.entanglement.graph.data.Node;

/**
 * A graph node that represents a file.
 * 
 * @author Keith Flanagan
 */
public class FileNode
    extends Node
{
  private Date timestampOfDiscovery;
  
  private String bucket;
  private String path;
  private String filename;
  
  private long fileLength;
  private Date fileModifiedDate;

  public FileNode() {
    timestampOfDiscovery = new Date(System.currentTimeMillis());
  }

  public FileNode(String bucket, String path, String filename) {
    timestampOfDiscovery = new Date(System.currentTimeMillis());
    this.bucket = bucket;
    this.path = path;
    this.filename = filename;
  }

  @Override
  public String toString() {
    return "FileNode{" + "bucket=" + bucket + ", path=" + path + ", filename=" + filename + '}';
  }

  public String getBucket() {
    return bucket;
  }

  public void setBucket(String bucket) {
    this.bucket = bucket;
  }

  public String getPath() {
    return path;
  }

  public void setPath(String path) {
    this.path = path;
  }

  public String getFilename() {
    return filename;
  }

  public void setFilename(String filename) {
    this.filename = filename;
  }

  public Date getTimestampOfDiscovery() {
    return timestampOfDiscovery;
  }

  public void setTimestampOfDiscovery(Date timestampOfDiscovery) {
    this.timestampOfDiscovery = timestampOfDiscovery;
  }

  public long getFileLength() {
    return fileLength;
  }

  public void setFileLength(long fileLength) {
    this.fileLength = fileLength;
  }

  public Date getFileModifiedDate() {
    return fileModifiedDate;
  }

  public void setFileModifiedDate(Date fileModifiedDate) {
    this.fileModifiedDate = fileModifiedDate;
  }
  
}
