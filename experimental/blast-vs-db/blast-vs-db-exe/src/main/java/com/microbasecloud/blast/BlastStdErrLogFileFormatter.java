/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: 01-Dec-2012, 21:33:20
 */

package com.microbasecloud.blast;


import com.microbasecloud.blast.common.BlastJobDescription;
import uk.org.microbase.filesystem.spi.MBFile;
import uk.org.microbase.responder.util.AbstractOutputPathReformatter;

/**
 * Given a job description, generates the name to use for the STDERR log file.
 * This is in the format:
 * <code>/{bucket}/{base-dir}/{directory-for-job}/{filename}</code>
 * 
 * Where:
 * <ul>
 * <li>{bucket} is unmodified from the default configured result bucket for this
 * responder.</li>
 * <li>{base-dir} is unmodified from the default configured result base
 * directory for this responder.</li>
 * <li>{directory-for-job} is of the format described below.</li>
 * <li>{filename} is of the format described below.</li>
 * </ul>
 * 
 * The log subdirectory used is of the following format:
 * <code>[subject_filename]-[blast_db_name].[blast_type]/[MessageGUID]/[Process start timestamp]</code>
 * 
 * The filename used to store generated STDOUT content is of the format:
 * <code>[subject_filename]-[blast_db_name].[blast_type].stderr</code>
 * 
 * 
 * @author Keith Flanagan
 */
public class BlastStdErrLogFileFormatter
    extends AbstractOutputPathReformatter
    implements BlastOutputPathReformatter
{
  
  private BlastJobDescription jobDescription;
  
  @Override
  public String getFormattedBucketName() {
    return responderInfo.getLogDestinationBucket();
  }

  @Override
  public String getFormattedPath() {
    StringBuilder path = new StringBuilder(responderInfo.getLogDestinationBasePath());
    path.append("/").append(getFormattedFilename());
    path.append("/").append(message.getGuid());
    path.append("/").append(processInfo.getWorkStartedAtMs());
    return path.toString();
  }

  @Override
  public String getFormattedFilename() {
    StringBuilder resultFilename = new StringBuilder();
    resultFilename.append(jobDescription.getQueryFile().getName())
        .append("-").append(jobDescription.getDatabaseName())
        .append(".").append(jobDescription.getJobType().name())
        .append(".stderr");
    return resultFilename.toString();
  }
  
  @Override
  public MBFile getFormattedRemoteFile() {
    MBFile file = new MBFile(getFormattedBucketName(), getFormattedPath(), getFormattedFilename());
    return file;
  }
  

  public BlastJobDescription getJobDescription() {
    return jobDescription;
  }

  @Override
  public void setJobDescription(BlastJobDescription jobDescription) {
    this.jobDescription = jobDescription;
  }

}
