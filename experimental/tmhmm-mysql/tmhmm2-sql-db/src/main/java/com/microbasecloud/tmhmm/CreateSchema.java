/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.microbasecloud.tmhmm;

/**
 *
 * @author Sirintra Nakjang
 */
 
import org.hibernate.cfg.Configuration;
import org.hibernate.dialect.MySQLDialect;
public class CreateSchema
{
 
  public static void main(String[] args)
  {
    Configuration cfg = new Configuration().configure("/TmhmmResponder-hibernate.cfg.xml");
    String[] lines = cfg.generateSchemaCreationScript(new MySQLDialect());
 
    System.out.println("\n\nSQL Schema follows:");
    for (int i = 0; i < lines.length; i++)
    {
      System.out.println(lines[i] + ";");
    }
  }
}
