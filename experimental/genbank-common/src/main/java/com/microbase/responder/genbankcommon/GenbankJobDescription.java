/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.microbase.responder.genbankcommon;

import java.io.Serializable;
import uk.org.microbase.filesystem.spi.MBFile;

/**
 *
 * @author prisni
 */
public class GenbankJobDescription implements Serializable {
    
    
     
  public enum OutputType {
    gbf, val, sqn
  };
  
  private OutputType jobType;

    public OutputType getJobType() {
        return jobType;
    }

    public void setJobType(OutputType fileType) {
        this.jobType = fileType;
    }

   
    private MBFile[] inputFiles;
    private MBFile templateFile;
    private MBFile databaseZipFile;
    private String databaseName;

    public GenbankJobDescription() {
        inputFiles = new MBFile[3];
    }

    public MBFile[] getInputFiles() {
        return inputFiles;
    }

    public void setInputFiles(MBFile[] inputFiles) {
        this.inputFiles = inputFiles;
    }

    public MBFile getDatabaseZipFile() {
        return databaseZipFile;
    }

    public void setDatabaseZipFile(MBFile databaseZipFile) {
        this.databaseZipFile = databaseZipFile;
    }

    public String getDatabaseName() {
        return databaseName;
    }

    public void setDatabaseName(String databaseName) {
        this.databaseName = databaseName;
    }

    public MBFile getTemplateFile() {
        return templateFile;
    }

    public void setTemplateFile(MBFile templateFile) {
        this.templateFile = templateFile;
    }
     
}
