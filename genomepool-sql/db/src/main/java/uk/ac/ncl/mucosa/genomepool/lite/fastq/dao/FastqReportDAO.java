/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: 25-Mar-2012, 12:40:04
 */

package uk.ac.ncl.mucosa.genomepool.lite.fastq.dao;

import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import uk.ac.ncl.mucosa.genomepool.GenomePoolDbException;
import uk.ac.ncl.mucosa.genomepool.lite.fastq.data.FastqReport;

/**
 *
 * @author Keith Flanagan
 */
public class FastqReportDAO
{
  
  public void store(Session session, FastqReport report)
      throws GenomePoolDbException
  {
    try
    {
      session.save(report);
      session.flush();
    }
    catch(Exception e)
    {
      throw new GenomePoolDbException(
          "Failed to execute store()", e);
    }
    finally
    {
    }
  }
  
  public List<FastqReport> selectAllReports(Session session)
      throws GenomePoolDbException
  {
    try
    {
      String qry = "from "+FastqReport.class.getName();
      Query query = session.createQuery(qry);
        
      List<FastqReport> reports = query.list();
      return reports;
    }
    catch(Exception e)
    {
      throw new GenomePoolDbException(
          "Failed to execute selectAllReports()", e);
    }
    finally
    {
    }
  }
  
  
}
