/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: 25-Mar-2012, 12:40:04
 */

package com.microbasecloud.hmmer;

import java.util.List;
import java.util.Set;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author Keith Flanagan
 */
public class HmmerHitDAO
{
  private static final int DEFAULT_BATCH = 50000;
  
  public void store(Session session, Set<HmmerHit> hits)
      throws HmmerParserException
  {
    try
    {
      int count = 0;
      for (HmmerHit hit : hits)
      {
        session.save(hit);
        count++;
        if (count % DEFAULT_BATCH == 0)
        {
          session.flush();
          session.clear();
        }
      }
    }
    catch(Exception e)
    {
      throw new HmmerParserException(
          "Failed to execute store() operation", e);
    }
  }
  
  public List<HmmerHit> selectHitsForReport(Session session, long reportId)
      throws HmmerParserException
  {
    try
    {
      String qry = "from "+HmmerHit.class.getName()+" where report.id = :id";
      Query query = session.createQuery(qry);
      query.setParameter("id", reportId);
        
      List<HmmerHit> hits = query.list();
      return hits;
    }
    catch(Exception e)
    {
      throw new HmmerParserException(
          "Failed to execute selectHitsForReport()", e);
    }
  }
  
  
  /**
   * Returns a list of distinct query names (HMM profile names) currently
   * present in the database (for any report).
   * @param session
   * @return
   * @throws HmmerDbException 
   */
  public List<String> selectDistinctQueryNames(Session session)
      throws HmmerParserException
  {
    try
    {
      String qry = "select distinct hit.queryName from "
          + HmmerHit.class.getName()+" hit order by hit.queryName";
      Query query = session.createQuery(qry);
        
      List<String> hmmerProfileNames = query.list();
      return hmmerProfileNames;
    }
    catch(Exception e)
    {
      throw new HmmerParserException(
          "Failed to execute selectDistinctQueryNames()", e);
    }
  }
  
  /**
   * Given a queryName (hmmer profile name), returns the 
   * <code>numHits</code> best results. This method uses 
   * <code>fullSeqEVal</code> for determining the 'best' hits.
   * 
   * FIXME - probably need to limit by dataset!
   * 
   * @param session
   * @param queryName the name of the hmmer profile to search
   * @param numHits the number of best hits to return
   * @return a list of best hits for the specified profile in order of best->worst
   * @throws HmmerDbException 
   */
  public List<HmmerHit> getBestHitsForHmmQuery(
      Session session, String queryName, int numHits)
      throws HmmerParserException
  {
    try
    {
      String qry = "from "+HmmerHit.class.getName()+" hit "
          + "where hit.queryName = :queryName order by hit.fullSeqEVal";
      Query query = session.createQuery(qry);
      query.setParameter("queryName", queryName);
      query.setMaxResults(numHits);
        
      List<HmmerHit> hits = query.list();
      return hits;
    }
    catch(Exception e)
    {
      throw new HmmerParserException(
          "Failed to execute getBestHitsForHmmQuery()", e);
    }
  }
  
}
