/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.microbase.responder.genbankcommon;

import java.io.Serializable;
import uk.org.microbase.filesystem.spi.MBFile;

/**
 *
 * @author prisni
 */
public class GenbankJobResult implements Serializable{
    
    private GenbankJobDescription job;
  
  private MBFile resultFile;
  private MBFile stdOutLogFile;
  private MBFile stdErrLogFile;

    public GenbankJobResult() {
    }

    public GenbankJobDescription getJob() {
        return job;
    }

    public void setJob(GenbankJobDescription job) {
        this.job = job;
    }

    public MBFile getResultFile() {
        return resultFile;
    }

    public void setResultFile(MBFile resultFile) {
        this.resultFile = resultFile;
    }

    public MBFile getStdOutLogFile() {
        return stdOutLogFile;
    }

    public void setStdOutLogFile(MBFile stdOutLogFile) {
        this.stdOutLogFile = stdOutLogFile;
    }

    public MBFile getStdErrLogFile() {
        return stdErrLogFile;
    }

    public void setStdErrLogFile(MBFile stdErrLogFile) {
        this.stdErrLogFile = stdErrLogFile;
    }
  
  
    
}
