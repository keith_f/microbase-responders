/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: 30-Sep-2012, 12:55:35
 */

package com.microbasecloud.blast;

import com.microbasecloud.blast.common.BlastJobDescription;
import com.microbasecloud.blast.common.BlastJobDescription.BlastType;
import com.microbasecloud.shell.messageutil.ShellMsgGenerator;
import com.microbasecloud.shell.messageutil.ShellMsgGeneratorException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import org.codehaus.jackson.map.ObjectMapper;
import uk.org.microbase.filesystem.spi.MBFile;

/**
 * This program generates a Microbase notification message containing
 * a BlastJobDescription, given a suitable database and query file.
 * 
 * This program is provided for convenience and produces both the text for a
 * message, and a sample notification system command line that can be used to
 * initiate the blast responder.
 * 
 * @author Keith Flanagan
 */
public class BlastJobMessageGenerator
    implements ShellMsgGenerator<BlastJobDescription>
{
  
  @Override
  public String getMessageProcessorClassname() {
    return BlastExeResponder.class.getName();
  }

  @Override
  public Map<String, String> getReqPropertyNames() {
    Map<String, String> nameToDesc = new HashMap<>();
    
    nameToDesc.put("blast.type", "The type of BLAST to perform. "
            + "Current accepted values are: "+Arrays.asList(BlastJobDescription.BlastType.values()));
    
    nameToDesc.put("blast.qry.bucket", "MBFS bucket of the query FASTA file");
    nameToDesc.put("blast.qry.path", "Path to the query FASTA file");
    nameToDesc.put("blast.qry.filename", "Filename of the query FASTA file");
    
    nameToDesc.put("blast.db.bucket", "MBFS bucket of a BLAST database zip file");
    nameToDesc.put("blast.db.path", "Path to a BLAST database zip file");
    nameToDesc.put("blast.db.filename", "Filename of a BLAST database zip file");
    
    nameToDesc.put("blast.db.name", "The name of the BLAST database stored within "
            + "a zip file. Note that for 'multi part' BLAST databases, this name "
            + "need not necessarilly be a filename.");
    
    
    return nameToDesc;
  }

  @Override
  public String generateJson(Map<String, String> properties) 
          throws ShellMsgGeneratorException {
    try {
      BlastJobDescription bean = generateMsgBean(properties);

      // Serialise message content to JSON
      ObjectMapper mapper = new ObjectMapper();
      String json = mapper.writeValueAsString(bean);
      return json;
    }
    catch(Exception e) {
      throw new ShellMsgGeneratorException(
              "Failed to generate and serialise a message content bean "
              + "from properties: "+properties, e);
    }
  }

  @Override
  public BlastJobDescription generateMsgBean(Map<String, String> properties) 
          throws ShellMsgGeneratorException {
    try {
      BlastJobDescription bean = new BlastJobDescription();
      bean.setDatabaseName(properties.get("blast.db.name"));
      bean.setDatabaseZipFile(
              new MBFile(
              properties.get("blast.db.bucket"), 
              properties.get("blast.db.path"), 
              properties.get("blast.db.filename")));
      bean.setJobType(BlastType.valueOf(properties.get("blast.type")));
      bean.setQueryFile(
              new MBFile(
              properties.get("blast.qry.bucket"), 
              properties.get("blast.qry.path"), 
              properties.get("blast.qry.filename")));

      return bean;
    }
    catch(Exception e) {
      throw new ShellMsgGeneratorException(
              "Failed to generate message content bean "
              + "from properties: "+properties, e);
    }
  }

}
