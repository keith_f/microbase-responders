/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.microbasecloud.abacasaligner;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Logger;
import org.codehaus.jackson.map.ObjectMapper;
import uk.org.microbase.dist.responder.RegistrationException;
import uk.org.microbase.dist.responder.ResponderInfo;
import uk.org.microbase.filesystem.spi.FileMetaData;
import uk.org.microbase.filesystem.spi.MBFile;
import uk.org.microbase.filesystem.spi.MicrobaseFS;
import uk.org.microbase.notification.data.Message;
import uk.org.microbase.responder.spi.AbstractMessageProcessor;
import uk.org.microbase.responder.spi.ProcessingException;
import uk.org.microbase.responder.spi.ResponderInfoPropertiesParser;
import uk.org.microbase.util.cli.NativeCommandExecutor;

/**
 *
 * @author prisni
 */
public class AbacasResponder extends AbstractMessageProcessor {
    
    
    private static final Logger l = Logger.getLogger(AbacasResponder.class.getName());
  
  /**
   * Configuration resource for this responder. A file with this name must be
   * located on the classpath.
   */
  static final String CONFIG_RESOURCE = "/"+AbacasResponder.class.getSimpleName()+".properties";
  
  /*
   * Names of properties in the responder configuration file that define the 
   * Microbase Filesystem bucket and path where generated result files should
   * be placed.
   * 
   * Result files are stored in this form:
   * <bucket>/<path>/<filename>.blast
   */
  private static final String CONFIG_RESULT_BUCKET = "result_bucket";
  private static final String CONFIG_RESULT_BASE_PATH = "result_base_path";

  private static final String CONFIG_LOG_BUCKET = "log_bucket";
  private static final String CONFIG_LOG_BASE_PATH = "log_base_path";
  
  private static final String CONFIG_RESULT_NAME_REFORMATTER = "result_name_reformatter";
  
   /**
   * Jobs for this responder arrive in notification messages. The job description
   * is serialised in a single JSON-formatted property attached to the message.
   * This field defines the name of the message property name that contains
   * information about the work to be performed.
   */
  private static final String IN_MSG_PROP__JOB = AbacasJobDescription.class.getName();
  
  private static final String OUT_MSG_PROB__RESULT_FILE = "result_file";
  private static final String OUT_MSG_PROB__STDOUT_FILE = "stdout";
  private static final String OUT_MSG_PROB__STDERR_FILE = "stderr";
  
  /*
   * Configuration options (these should be treated as 'final'
   */
  private Properties config;
  private String confResultBucket;
  private String confResultBasePath;
  private String confLogBucket;
  private String confLogBasePath;
  private OutputFilePathReformatter resultNameReformatter;
  
  /*
   * Per-job variables. These are set every time <code>preRun</code> is called.
   */
  private AbacasJobDescription jobDescr;
  private MBFile jobRemoteResultFile;
  private MBFile jobStdOutLogFile;
  private MBFile jobStdErrLogFile;
      

//    private static final String TOPIC_IN = "ALIGNMENT-JOB-BEGIN";
//    private static final String TOPIC_OUT = "ALIGNMENT-JOB-ACCOMPLISHED";
    private FileMetaData referenceFile;
    private FileMetaData queryFile;
    private String remoteS3Bucket;
    private String projectName = "Strain-4";
    private FileMetaData abacasAlignmentS3Files;
    private String logFileS3Bucket;
    private String logFileS3Path;
    private FileMetaData[] outputS3File;

    public AbacasResponder() {
        outputS3File = new FileMetaData[9];
        
    resultNameReformatter = new ResultFilesInSingleDirFormatter();
  }
    
    
    

   
    @Override
    protected ResponderInfo createDefaultResponderInfo() throws RegistrationException {
    try
    {
      config = new Properties();
      config.load(AbacasResponder.class.getResourceAsStream(CONFIG_RESOURCE));
      
      confResultBucket = config.getProperty(CONFIG_RESULT_BUCKET);
      confResultBasePath = config.getProperty(CONFIG_RESULT_BASE_PATH);
      confLogBucket = config.getProperty(CONFIG_LOG_BUCKET);
      confLogBasePath = config.getProperty(CONFIG_LOG_BASE_PATH);

      ResponderInfo info = 
          ResponderInfoPropertiesParser.parseFromProperties(config);
      l.info("Responder config: "+info);
      return info;
    }
    catch(Exception e) 
    {
      throw new RegistrationException(
          "Failed to load responder configuration", e);
    }
  
//        ResponderInfo info = new ResponderInfo();
//        info.setEnabled(true);
//        info.setMaxDistributedCores(300);
//        info.setMaxLocalCores(50);
//        info.setPreferredCpus(1);
//        info.setQueuePopulatorLastRunTimestamp(0);
//        info.setQueuePopulatorRunEveryMs(30000);
//        info.setResponderName(AbacasResponder.class.getSimpleName());
//        info.setResponderVersion("1.0");
//        info.setResponderId(info.getResponderName() + "-"
//                + info.getResponderVersion());
//        info.setTopicOfInterest(TOPIC_IN);
//        return info;
    }
    
    private AbacasJobDescription parseJobDescrFrom(Message msg)
      throws IOException
  {
    String jobDescrRawJson = msg.getContent().get(IN_MSG_PROP__JOB);
    
    ObjectMapper mapper = new ObjectMapper();
    
    AbacasJobDescription jobDescr = mapper.readValue(jobDescrRawJson, AbacasJobDescription.class);
    return jobDescr;
  }
    
    private String generateResultName(AbacasJobDescription job)
  {
    StringBuilder resultFilename = new StringBuilder();
    resultFilename.append(job.getQueryFile().getName())
        .append("-").append(job.getDatabaseName())
        .append(".").append(job.getJobType().name());
    return resultFilename.toString(); 
  }
    
    private MBFile determineRemoteResultFile(AbacasJobDescription job)
  {
    String filename = generateResultName(job);
    String bucket = resultNameReformatter.getBucket(confResultBucket, confResultBasePath, filename);
    String path = resultNameReformatter.getPath(confResultBucket, confResultBasePath, filename);
    
    MBFile remoteFile = new MBFile(bucket, path, filename);
    return remoteFile;
  }
  
  private MBFile determineRemoteLogStdOutFile(Message msg, AbacasJobDescription job)
  {
    String filename = getProcessInfo().getWorkStartedAtMs()+"."+generateResultName(job)+".stdout";
    String bucket = confLogBucket;
    String path = confLogBasePath + "/" + getResponderInfo().getResponderId() + "/" + msg.getGuid();
    
    MBFile remoteFile = new MBFile(bucket, path, filename);
    return remoteFile;
  }
  
  private MBFile determineRemoteLogStdErrFile(Message msg, AbacasJobDescription job)
  {
    String filename = getProcessInfo().getWorkStartedAtMs()+"."+generateResultName(job)+".stderr";
    String bucket = confLogBucket;
    String path = confLogBasePath + "/" + getResponderInfo().getResponderId() + "/" + msg.getGuid();
    
    MBFile remoteFile = new MBFile(bucket, path, filename);
    return remoteFile;
  }

    public void preRun(Message msg) throws ProcessingException {
        System.out.println("Doing pre-run");
        referenceFile = parseInputReferenceFile(msg);
        queryFile = parseInputQueryFile(msg);
        
        String fileName = queryFile.getLocation().getName()+"_"+referenceFile.getLocation().getName();
        // set remote location for output
        remoteS3Bucket = referenceFile.getLocation().getBucket(); //sets output bucket to be the same as fastq
        //sets the path for the output to be uploaded to
        String remoteS3Path = "alignment for:" + projectName;
        
        String remoteBinS3Name =  fileName + ".bin";
        outputS3File[0] = new FileMetaData(remoteS3Bucket, remoteS3Path, remoteBinS3Name);
        
        String remoteCrunchS3Name =  fileName + ".crunch";
        outputS3File[1] = new FileMetaData(remoteS3Bucket, remoteS3Path, remoteCrunchS3Name);
        
        String remoteFastaS3Name =  fileName + ".fasta";
        outputS3File[2] = new FileMetaData(remoteS3Bucket, remoteS3Path, remoteFastaS3Name);
        
        String remoteGapsS3Name =  fileName + ".gaps";
        outputS3File[3] = new FileMetaData(remoteS3Bucket, remoteS3Path, remoteGapsS3Name);
        
        String remoteGapsTabS3Name =  fileName + ".gaps.tab";
        outputS3File[4] = new FileMetaData(remoteS3Bucket, remoteS3Path, remoteGapsTabS3Name);
        
        String remoteFastaTabS3Name =  fileName + ".fasta.tab";
        outputS3File[5] = new FileMetaData(remoteS3Bucket, remoteS3Path, remoteFastaTabS3Name);
        
        
        String remoteAssemblyLogS3Name = "my" + "-" + projectName + "-"
                + "alignment"; //sets the name for the output directory

        abacasAlignmentS3Files = new FileMetaData(remoteS3Bucket, remoteS3Path,
                remoteAssemblyLogS3Name); // creats a filemetadata object for the mira assembly results

        logFileS3Bucket = remoteS3Bucket;
        logFileS3Path = remoteS3Path;



    }

    public void cleanupPreviousResults(Message msg) throws ProcessingException {
        l.info("Doing cleanup");
    try
    {
      MicrobaseFS fs = getRuntime().getMicrobaseFS();
      
      if (fs.exists(jobRemoteResultFile))
      {
        l.info("Found an existing result file generated by a previous "
            + "execution. Deleting...");
        fs.deleteRemoteCopy(jobRemoteResultFile);
      }
    }
    catch(Exception e)
    {
      throw new ProcessingException("Failed to perform cleanup", e);
    }
    }

    /**
     *
     * @param msg
     * @return
     * @throws ProcessingException
     */
    @Override
    public Set<Message> processMessage(Message msg) throws ProcessingException {
        {
   l.info("Performing computational work");
    try
    { 
      MBFile queryRemoteFile = jobDescr.getQueryFile();
      MBFile subjectDbRemoteFile = jobDescr.getDatabaseZipFile();
    

            File referenceFastaFile = downloadInputFile(referenceFile);
            File queryFastaFile = downloadInputFile(queryFile);
            
            l.info("Running command ...");
            
            File[] outputsLocalFile = runAbacasAligner(referenceFastaFile, queryFastaFile);
            
            getRuntime().getMicrobaseFS().upload(
                    outputsLocalFile[0],
                    outputS3File[0].getLocation(), null);
            
            getRuntime().getMicrobaseFS().upload(
                    outputsLocalFile[1],
                    outputS3File[1].getLocation(), null);
            
            getRuntime().getMicrobaseFS().upload(
                    outputsLocalFile[2],
                    outputS3File[2].getLocation(), null);
            
            getRuntime().getMicrobaseFS().upload(
                    outputsLocalFile[3],
                    outputS3File[3].getLocation(), null);
            
            getRuntime().getMicrobaseFS().upload(
                    outputsLocalFile[4],
                    outputS3File[4].getLocation(), null);
            
            getRuntime().getMicrobaseFS().upload(
                    outputsLocalFile[5],
                    outputS3File[5].getLocation(), null);
            
            Message alignmentMsg = generateSuccessNotification(msg, jobDescr); //generate success notification
            Set<Message> messages = new HashSet(); //create a hash set of notification messages 
            messages.add(alignmentMsg); //add new success messages to the hash set
            return messages;
        } catch (Exception e) {
            throw new ProcessingException("Failed to run one or more commands: "
                    + "on: " + getRuntime().getNodeInfo().getHostname()
                    + "in: " + getWorkingDirectory(), e);

        }

        }
    }

    private File downloadInputFile(FileMetaData remoteFile) throws
            ProcessingException {
        try {
            //get a reference to the working temp dir
            File tmpDir = getWorkingDirectory();

            //downloading the input file
            File destinationFile = new File(tmpDir, remoteFile.getLocation().getName());

            getRuntime().getMicrobaseFS().downloadFileToSpecificLocation(
                    remoteFile.getLocation(), destinationFile, true);

            return destinationFile;

        } catch (Exception e) {
            throw new ProcessingException("Failed to download remote file"
                    + remoteFile.getLocation().getBucket() + " , " + remoteFile.getLocation().getPath());
        }
    }

    private File[] runAbacasAligner(File refFile, File qryFile) throws ProcessingException {

        String[] abacasCommand = new String[]{ 
            "/mnt/responder_data/abacas-mer/runabacas.sh", refFile.getAbsolutePath(),
            qryFile.getAbsolutePath()};

        executeCommand(abacasCommand, "abacas");
        File[] resultFiles = getWorkingDirectory().listFiles();
        return resultFiles;
    }

//    private void uploadRelevantResults() throws ProcessingException {
//        File resultFile = getWorkingDirectory();
//                for(File file:resultFile.listFiles()){
//            MBFile outputfile = new MBFile(remoteS3Bucket, resultFile.getPath(), file.getName());
//            try {
//                getRuntime().getMicrobaseFS().upload(
//                       file, outputfile, null);
//            } catch (FSOperationNotSupportedException | FSException ex) {
//                Logger.getLogger(AbacasResponder.class.getName()).log(Level.SEVERE, null, ex);
//            }
//        }                          
//                    
//        
//    }

    private Message generateSuccessNotification(Message parent, AbacasJobDescription job) throws IOException {
         Message successMsg = createMessage(
        parent,                           //The message to use as a parent
        getResponderInfo().getTopicOut(), //The topic of the new message
        parent.getWorkflowStepId(),        //The workflow step ID
        //An optional human-readable description
        "Successfully completed a ABACAS alignment.");
    
    
    ObjectMapper mapper = new ObjectMapper();
    String jobJson = mapper.writeValueAsString(job);
    successMsg.getContent().put(IN_MSG_PROP__JOB, mapper.writeValueAsString(job));
    successMsg.getContent().put(OUT_MSG_PROB__RESULT_FILE, mapper.writeValueAsString(jobRemoteResultFile));
    successMsg.getContent().put(OUT_MSG_PROB__STDERR_FILE, mapper.writeValueAsString(jobStdErrLogFile));
    successMsg.getContent().put(OUT_MSG_PROB__STDOUT_FILE, mapper.writeValueAsString(jobStdOutLogFile));

    
    return successMsg;

//        Message successMsg = createMessage(
//                parent,
//                //The message to use as a parent
//                TOPIC_OUT,
//                //The topic of the new message
//                parent.getWorkflowStepId(),
//                //The workflow step ID
//                //An optional human-readable description
//                "Successfully accomplished alignment job using abacas for the reads: "
//                + projectName);
////             Inform future responders about the output assembly files
//        successMsg.getContent().put("result_file.aligned_reads_report.bucket", abacasAlignmentS3Files.getLocation().getBucket());
//        successMsg.getContent().put("result_file.aligned_reads_report.path", abacasAlignmentS3Files.getLocation().getPath());
//        successMsg.getContent().put("result_file.aligned_reads_report.name", abacasAlignmentS3Files.getLocation().getName());
//
//
//        return successMsg;


    }

    private void executeCommand(String[] command, String stdOutPrefix) throws ProcessingException {
        File workDir = getWorkingDirectory();
        File stdOutFile = new File(workDir, stdOutPrefix + "-stdout.txt");
        File stdErrFile = new File(workDir, stdOutPrefix + "-stderr.txt");
        try {
            FileWriter stdOut = new FileWriter(stdOutFile);
            FileWriter stdErr = new FileWriter(stdErrFile);

            int exitStatus = NativeCommandExecutor.executeNativeCommand(workDir, command, stdOut, stdErr);

            stdOut.flush();
            stdOut.close();
            stdErr.flush();
            stdErr.close();

            if (exitStatus != 0) {
                throw new ProcessingException("Exit status of the command: "
                        + Arrays.asList(command) + "\nwas " + exitStatus
                        + ". Assuming that non-zero means that execution failed.");
            }
        } catch (Exception e) {
            throw new ProcessingException("Failed to execute command: "
                    + command[0], e);
        } finally {
            try {
                getRuntime().getMicrobaseFS().upload(stdOutFile, jobStdOutLogFile, null);
                getRuntime().getMicrobaseFS().upload(stdErrFile, jobStdErrLogFile, null);
//                MBFile standardOutputFile = new MBFile(logFileS3Bucket, logFileS3Path, stdOutFile.getName());
//                getRuntime().getMicrobaseFS().upload(stdOutFile, standardOutputFile, null);
//
//                MBFile standardErrorFile = new MBFile(logFileS3Bucket, logFileS3Path, stdErrFile.getName());
//                getRuntime().getMicrobaseFS().upload(stdErrFile, standardErrorFile, null);
            } catch (Exception e) {
                throw new ProcessingException("Failed to upload output file", e);
            }
        }
    } 

    private FileMetaData parseInputReferenceFile(Message m) {
        FileMetaData refFile = new FileMetaData(m.getContent().
                get("result_file.ref.bucket"),
                m.getContent().get("result_file.ref.path"),
                m.getContent().get("result_file.ref.filename"));
        return refFile;
    }

    private FileMetaData parseInputQueryFile(Message m) {
        FileMetaData qryFile = new FileMetaData(m.getContent().
                get("result_file.query.bucket"),
                m.getContent().get("result_file.query.path"),
                m.getContent().get("result_file.query.filename"));
        return qryFile;
    }

  }
