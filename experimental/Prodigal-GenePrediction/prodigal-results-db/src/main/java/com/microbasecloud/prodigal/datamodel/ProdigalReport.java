/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.microbasecloud.prodigal.datamodel;

import java.io.Serializable;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 *
 * @author prisni
 */
@Entity
@Table (name = "prodigal_report")
public class ProdigalReport implements Serializable{
    
    @Id
  @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;
      /**
   * Unique ID of this report
   */
  private String guid;
  
  /**
   * The unique ID of the notification message that triggered the generation
   * of this report.
   */
  private String incomingMessageId;
  
  /**
   * Details of the input file used for this report.
   */
  private String cdsListFileBucket;
  private String cdsListSourceFilePath;
  private String cdsListSourceFileName;
  
  private String translationListFileBucket;
  private String translationListSourceFilePath;
  private String translationListSourceFileName;
 

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getGuid() {
        return guid;
    }

    public void setGuid(String guid) {
        this.guid = guid;
    }

    public String getIncomingMessageId() {
        return incomingMessageId;
    }

    public void setIncomingMessageId(String incomingMessageId) {
        this.incomingMessageId = incomingMessageId;
    }

    public String getCdsListFileBucket() {
        return cdsListFileBucket;
    }

    public void setCdsListFileBucket(String cdsListFileBucket) {
        this.cdsListFileBucket = cdsListFileBucket;
    }

    public String getCdsListSourceFilePath() {
        return cdsListSourceFilePath;
    }

    public void setCdsListSourceFilePath(String cdsListSourceFilePath) {
        this.cdsListSourceFilePath = cdsListSourceFilePath;
    }

    public String getCdsListSourceFileName() {
        return cdsListSourceFileName;
    }

    public void setCdsListSourceFileName(String cdsListSourceFileName) {
        this.cdsListSourceFileName = cdsListSourceFileName;
    }

    public String getTranslationListFileBucket() {
        return translationListFileBucket;
    }

    public void setTranslationListFileBucket(String translationListFileBucket) {
        this.translationListFileBucket = translationListFileBucket;
    }

    public String getTranslationListSourceFilePath() {
        return translationListSourceFilePath;
    }

    public void setTranslationListSourceFilePath(String translationListSourceFilePath) {
        this.translationListSourceFilePath = translationListSourceFilePath;
    }

    public String getTranslationListSourceFileName() {
        return translationListSourceFileName;
    }

    public void setTranslationListSourceFileName(String translationListSourceFileName) {
        this.translationListSourceFileName = translationListSourceFileName;
    }

   
  
    
}
