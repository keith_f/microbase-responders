/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.microbase.responder.sffcommon;

import java.io.Serializable;
import uk.org.microbase.filesystem.spi.MBFile;

/**
 *
 * @author prisni
 */
public class SFFJobDescription 
implements Serializable {
    public enum OutputType{
    in, traceinfo_in;
};
    
    public enum OutputFileType{
        fastq, xml;
    };
    
    public OutputType outputType;
    public OutputFileType outputFileType;

    public OutputType getOutputType() {
        return outputType;
    }

    public void setOutputType(OutputType outputType) {
        this.outputType = outputType;
    }

    public OutputFileType getOutputFileType() {
        return outputFileType;
    }

    public void setOutputFileType(OutputFileType outputFileType) {
        this.outputFileType = outputFileType;
    }
    
    private MBFile fastQFile;
    private MBFile traceInfoXmlFile;
    private MBFile sffZipFile;

    public MBFile getFastQFile() {
        return fastQFile;
    }

    public void setFastQFile(MBFile fastQFile) {
        this.fastQFile = fastQFile;
    }

    public MBFile getTraceInfoXmlFile() {
        return traceInfoXmlFile;
    }

    public void setTraceInfoXmlFile(MBFile traceInfoXmlFile) {
        this.traceInfoXmlFile = traceInfoXmlFile;
    }
   
   public MBFile getSffZipFile() {
        return sffZipFile;
    }

    public void setSffZipFile(MBFile sffZipFile) {
        this.sffZipFile = sffZipFile;
    }
  
}
