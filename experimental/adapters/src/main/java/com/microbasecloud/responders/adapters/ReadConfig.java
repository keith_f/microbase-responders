/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: 09-Oct-2012, 20:33:48
 */

package com.microbasecloud.responders.adapters;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Logger;
import uk.org.microbase.responder.spi.MessageProcessor;

/**

 * @author Keith Flanagan
 */
public class ReadConfig
{
  private static final Logger logger =
      Logger.getLogger(ReadConfig.class.getName());

  
  
  /**
   * Reads a properties object and returns a set of Targets defined
   * by the contents.
   * 
   * @param props
   * @return 
   */
  public static Map<String, String> loadConfig(Properties props, String className)
  {
    Map<String, String> responderProps = new HashMap<>();
    for (String key : props.stringPropertyNames())
    {
      String val = props.getProperty(key);
      if (key.startsWith(className))
      {
        // +1 to ignore the '|' separator character
        responderProps.put(key.substring(className.length()+1), val);
      }
    }
    return responderProps;
  }
}


