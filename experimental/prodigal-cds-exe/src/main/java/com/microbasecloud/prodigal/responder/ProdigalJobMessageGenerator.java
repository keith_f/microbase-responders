/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.microbasecloud.prodigal.responder;

import com.microbase.responder.prodigalcdscommon.ProdigalJobDescription;
import com.microbasecloud.shell.messageutil.ShellMsgGenerator;
import com.microbasecloud.shell.messageutil.ShellMsgGeneratorException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import org.codehaus.jackson.map.ObjectMapper;
import uk.org.microbase.filesystem.spi.MBFile;

/**
 *
 * @author prisni
 */
public class ProdigalJobMessageGenerator 
 implements ShellMsgGenerator<ProdigalJobDescription>
{
  
  @Override
  public String getMessageProcessorClassname() {
    return ProdigalResponder.class.getName();
  }

  @Override
  public Map<String, String> getReqPropertyNames() {
    Map<String, String> nameToDesc = new HashMap();
    
    nameToDesc.put("prodigal.predition.type", "The type of prediction expected. "
            + "Current expected values are: "+Arrays.asList(ProdigalJobDescription.PredictionType.values()));
    
    nameToDesc.put("prodigal.query.fasta.bucket", "MBFS bucket of the query FASTA file");
    nameToDesc.put("prodigal.query.fasta.path", "Path to the query FASTA file");
    nameToDesc.put("prodigal.query.fasta.filename", "Filename of the query FASTA file");
    
    nameToDesc.put("prodigal.db.bucket", "MBFS bucket of a Prodigal database zip file");
    nameToDesc.put("prodigal.db.path", "Path to a Prodigal database zip file");
    nameToDesc.put("prodigal.db.filename", "Filename of a Prodigal database zip file");
    
    nameToDesc.put("prodigal.db.name", "The name of the Prodigal database stored within "
            + "a zip file. Note that for 'multi part' Prodigal databases, this name "
            + "need not necessarilly be a filename.");
        
    return nameToDesc;
  }

  @Override
  public String generateJson(Map<String, String> properties) 
          throws ShellMsgGeneratorException {
    try {
      ProdigalJobDescription bean = generateMsgBean(properties);

      // Serialise message content to JSON
      ObjectMapper mapper = new ObjectMapper();
      String json = mapper.writeValueAsString(bean);
      return json;
    }
    catch(Exception e) {
      throw new ShellMsgGeneratorException(
              "Failed to generate and serialise a message content bean "
              + "from properties: "+properties, e);
    }
  }

  @Override
  public ProdigalJobDescription generateMsgBean(Map<String, String> properties) 
          throws ShellMsgGeneratorException {
    try {
      ProdigalJobDescription bean = new ProdigalJobDescription();
      bean.setDatabaseName(properties.get("prodigal.db.name"));
      bean.setDatabaseZipFile(
              new MBFile(
              properties.get("prodigal.db.bucket"), 
              properties.get("prodigal.db.path"), 
              properties.get("prodigal.db.filename")));
     
      bean.setQueryFastaFile(
              new MBFile(
              properties.get("prodigal.query.bucket"), 
              properties.get("prodigal.query.path"), 
              properties.get("prodigal.query.filename")));

      return bean;
    }
    catch(Exception e) {
      throw new ShellMsgGeneratorException(
              "Failed to generate message content bean "
              + "from properties: "+properties, e);
    }
  }

}
