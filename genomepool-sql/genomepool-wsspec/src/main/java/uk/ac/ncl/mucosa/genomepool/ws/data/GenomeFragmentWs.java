/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published
 *  by the Free Software Foundation. The full license may be found in
 *  COPYING.LESSER in this project's root directory.
 * 
 *  Copyright 2009 jointly held by the authors listed at the top of each
 *  source file and/or their respective employers.
 */

package uk.ac.ncl.mucosa.genomepool.ws.data;

import java.io.Serializable;

/**
 *
 * @author Keith Flanagan
 * @author Sirintra Nakjang
 */
public class GenomeFragmentWs
        implements Serializable
{
   private String guid;
   private int giNumber;
   private int taxonId;
   private String organismName;
   private String organismShortName;

   

   private String accession;

   private String version;

   private String description;

   private String comment;

   private int numberProtein;

//   private Status status;

   private FragmentTypeWs fragmentType;

   private String fragmentSubtype;

   private String ncbiModified;

//   private FileInfo parsedFrom;

//   private String fileid;

//   private String dnaFastaFileid;

//   private String proteinFastaFileid;

//  private Set<GenomeFragmentSynonym> synonyms;

  //private Set<Gene> genes;

  private int dnaSequenceLength;


  public GenomeFragmentWs()
  {
//    synonyms = new HashSet<GenomeFragmentSynonym>();
    //genes = new HashSet<Gene>();
  }

  @Override
  public boolean equals(Object obj)
  {
    if (obj == null)
    {
      return false;
    }
    if (getClass() != obj.getClass())
    {
      return false;
    }
    final GenomeFragmentWs other = (GenomeFragmentWs) obj;
    if ((this.guid == null) ? (other.guid != null) : !this.guid.equals(other.guid))
    {
      return false;
    }
    return true;
  }

  @Override
  public int hashCode()
  {
    int hash = 7;
    hash = 29 * hash + (this.guid != null ? this.guid.hashCode() : 0);
    return hash;
  }

  

  @Override
  public String toString()
  {
    StringBuilder sb = new StringBuilder("[");
    sb.append("guid:").append(guid)
        .append(", desc:").append(description)
        .append(", accession:").append(accession)
        .append(", version:").append(version)
        .append(", gi:").append(giNumber)
        .append(", no. proteins:").append(numberProtein)
//        .append(", status:").append(status)
        .append(", fragmentType:").append(fragmentType)
        .append(", fragmentSubtype:").append(fragmentSubtype)
//        .append(", fileid:").append(fileid)
//        .append(", dnaFastaFileid:").append(dnaFastaFileid)
//        .append(", proteinFastaFileid:").append(proteinFastaFileid)
//        .append(", synonyms:").append(synonyms.size())
        //.append(", genes:").append(genes.size())
        .append(", DNA seq length:").append(dnaSequenceLength)
        .append("]");
    return sb.toString();
  }

  public String getAccession()
  {
    return accession;
  }

  public void setAccession(String accession)
  {
    this.accession = accession;
  }

  public String getDescription()
  {
    return description;
  }

  public void setDescription(String description)
  {
    this.description = description;
  }

//  public String getDnaFastaFileid()
//  {
//    return dnaFastaFileid;
//  }
//
//  public void setDnaFastaFileid(String dnaFastaFileid)
//  {
//    this.dnaFastaFileid = dnaFastaFileid;
//  }

//  public String getFileid()
//  {
//    return fileid;
//  }
//
//  public void setFileid(String fileid)
//  {
//    this.fileid = fileid;
//  }

  public String getFragmentSubtype()
  {
    return fragmentSubtype;
  }

  public void setFragmentSubtype(String fragmentSubtype)
  {
    this.fragmentSubtype = fragmentSubtype;
  }

  public FragmentTypeWs getFragmentType()
  {
    return fragmentType;
  }

  public void setFragmentType(FragmentTypeWs fragmentType)
  {
    this.fragmentType = fragmentType;
  }

  public int getGiNumber()
  {
    return giNumber;
  }

  public void setGiNumber(int giNumber)
  {
    this.giNumber = giNumber;
  }

  public String getGuid()
  {
    return guid;
  }

  public void setGuid(String guid)
  {
    this.guid = guid;
  }

  public int getNumberProtein()
  {
    return numberProtein;
  }

  public void setNumberProtein(int numberProtein)
  {
    this.numberProtein = numberProtein;
  }

//  public String getProteinFastaFileid()
//  {
//    return proteinFastaFileid;
//  }
//
//  public void setProteinFastaFileid(String proteinFastaFileid)
//  {
//    this.proteinFastaFileid = proteinFastaFileid;
//  }

//  public Status getStatus()
//  {
//    return status;
//  }
//
//  public void setStatus(Status status)
//  {
//    this.status = status;
//  }

  public String getVersion()
  {
    return version;
  }

  public void setVersion(String version)
  {
    this.version = version;
  }

//  public Set<Gene> getGenes()
//  {
//    return genes;
//  }
//
//  public void setGenes(Set<Gene> genes)
//  {
//    this.genes = genes;
//  }

//  public Set<GenomeFragmentSynonym> getSynonyms()
//  {
//    return synonyms;
//  }
//
//  public void setSynonyms(Set<GenomeFragmentSynonym> synonyms)
//  {
//    this.synonyms = synonyms;
//  }

  public String getComment()
  {
    return comment;
  }

  public void setComment(String comment)
  {
    this.comment = comment;
  }

  public String getNcbiModified()
  {
    return ncbiModified;
  }

  public void setNcbiModified(String ncbiModified)
  {
    this.ncbiModified = ncbiModified;
  }

  public int getDnaSequenceLength()
  {
    return dnaSequenceLength;
  }

  public void setDnaSequenceLength(int dnaSequenceLength)
  {
    this.dnaSequenceLength = dnaSequenceLength;
  }

  public String getOrganismName()
  {
    return organismName;
  }

  public void setOrganismName(String organismName)
  {
    this.organismName = organismName;
  }

  public int getTaxonId()
  {
    return taxonId;
  }

  public void setTaxonId(int taxonId)
  {
    this.taxonId = taxonId;
  }

//  public FileInfo getParsedFrom()
//  {
//    return parsedFrom;
//  }
//
//  public void setParsedFrom(FileInfo parsedFrom)
//  {
//    this.parsedFrom = parsedFrom;
//  }

  public String getOrganismShortName()
  {
    return organismShortName;
  }

  public void setOrganismShortName(String organismShortName)
  {
    this.organismShortName = organismShortName;
  }

  
}
