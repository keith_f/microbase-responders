/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.microbasecloud.genbankresponder;

import com.microbasecloud.prodigal.datamodel.ProdigalCDS;
import com.microbasecloud.prodigal.datamodel.ProdigalDAO;
import com.microbasecloud.prodigal.datamodel.ProdigalDBException;
import com.torrenttamer.hibernate.HibernateUtilException;
import com.torrenttamer.hibernate.SessionProvider;
import java.util.HashMap;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Session;

/**
 *
 * @author prisni
 */
public class ProdigalAnnotatorImpl implements FeatureTableAnnotator {

    public ProdigalAnnotatorImpl() {
        
    }
    
    private SessionProvider prodigalAnnotatorSessionProvider; 
    ProdigalDAO dao;

    @Override
    public Map<String, String> getAnnotations(String featureId) throws FeatureTableAnnotatorException {
        try {
            Session session = prodigalAnnotatorSessionProvider.getReadOnlySession();
            ProdigalCDS cdsInfo = dao.findCDS(session, featureId);
            Map<String, String> hmap = new HashMap<String, String>();
            hmap.put("GCcontent", cdsInfo.getCdsGCcontent());
            hmap.put("RBSMotif", cdsInfo.getCdsRBSmotif());
            return hmap;
            
        } catch (HibernateUtilException | ProdigalDBException ex) {
           throw new FeatureTableAnnotatorException("Failed to get annotations from the prodigal database: ", ex);
        }
        
        
    }

  
    @Override
    public void initialize() throws FeatureTableAnnotatorException{
        try {        
            prodigalAnnotatorSessionProvider = new SessionProvider("/ProdigalResponder-hibernate.cfg.xml");
            dao = new ProdigalDAO();
            
        } catch (HibernateUtilException ex) {
            throw new FeatureTableAnnotatorException("Could not connect to the prodigal database: ", ex);
        }
            
       
       
    }
    
    
}
