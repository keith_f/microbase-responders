/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.microbasecloud.genbankresponder;

import com.microbase.responder.genbankcommon.GenbankJobDescription;
import com.microbasecloud.shell.messageutil.ShellMsgGenerator;
import com.microbasecloud.shell.messageutil.ShellMsgGeneratorException;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import org.codehaus.jackson.map.ObjectMapper;
import uk.org.microbase.filesystem.spi.MBFile;

/**
 *
 * @author prisni
 */
public class GenbankJobMessageGenerator implements ShellMsgGenerator<GenbankJobDescription>{

    @Override
    public String getMessageProcessorClassname() {
        return GenBankGenerator.class.getName();
    }

    @Override
    public Map<String, String> getReqPropertyNames() {
        Map<String, String> nameToDesc = new HashMap<>();
    
//    nameToDesc.put("blast.type", "The type of BLAST to perform. "
//            + "Current accepted values are: "+Arrays.asList(GenbankJobDescription.OutputType.values()));
    
    nameToDesc.put("gbk.fasta.bucket", "MBFS bucket of the query FASTA file");
    nameToDesc.put("gbk.fasta.path", "Path to the query FASTA file");
    nameToDesc.put("gbk.fasta.filename", "Filename of the query FASTA file");
    
    nameToDesc.put("gbk.feature.bucket", "MBFS bucket of a features table file");
    nameToDesc.put("gbk.feature.path", "Path to a features table file");
    nameToDesc.put("gbk.feature.filename", "Filename of a features table file");
    
    nameToDesc.put("blast.db.bucket", "MBFS bucket of a BLAST database zip file");
    nameToDesc.put("blast.db.path", "Path to a BLAST database zip file");
    nameToDesc.put("blast.db.filename", "Filename of a BLAST database zip file");
    
    nameToDesc.put("blast.db.name", "The name of the BLAST database stored within "
            + "a zip file. Note that for 'multi part' BLAST databases, this name "
            + "need not necessarilly be a filename.");
    
    
    return nameToDesc;
    }

    @Override
    public String generateJson(Map<String, String> properties) throws ShellMsgGeneratorException {
        try {
      GenbankJobDescription bean = generateMsgBean(properties);

      // Serialise message content to JSON
      ObjectMapper mapper = new ObjectMapper();
      String json = mapper.writeValueAsString(bean);
      return json;
    }
    catch(Exception e) {
      throw new ShellMsgGeneratorException(
              "Failed to generate and serialise a message content bean "
              + "from properties: "+properties, e);
    }
    }

    @Override
    public GenbankJobDescription generateMsgBean(Map<String, String> properties) throws ShellMsgGeneratorException {
         try {
      GenbankJobDescription bean = new GenbankJobDescription();
      bean.setDatabaseName(properties.get("blast.db.name"));
      bean.setDatabaseZipFile(
              new MBFile(
              properties.get("blast.db.bucket"), 
              properties.get("blast.db.path"), 
              properties.get("blast.db.filename")));
//      bean.setJobType(BlastType.valueOf(properties.get("blast.type")));
//      bean.setQueryFile(
//              new MBFile(
//              properties.get("blast.qry.bucket"), 
//              properties.get("blast.qry.path"), 
//              properties.get("blast.qry.filename")));

      return bean;
    }
    catch(Exception e) {
      throw new ShellMsgGeneratorException(
              "Failed to generate message content bean "
              + "from properties: "+properties, e);
    }
  }
    }
    

