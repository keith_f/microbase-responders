/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.microbase.responder.sff;

import com.microbase.responder.sffcommon.SFFJobDescription;
import uk.org.microbase.responder.util.OutputPathReformatter;

/**
 *
 * @author prisni
 */
public interface SFFOutputPathReformatter extends OutputPathReformatter
{
  public void setJobDescription(SFFJobDescription job);
}
