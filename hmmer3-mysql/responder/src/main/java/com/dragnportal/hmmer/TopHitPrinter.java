/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: 26-Mar-2012, 13:27:12
 */

package com.dragnportal.hmmer;

import com.dragnportal.hmmer.db.Constants;
import com.dragnportal.hmmer.db.HmmerDbException;
import com.dragnportal.hmmer.db.HmmerHitDAO;
import com.dragnportal.hmmer.db.HmmerReportDAO;
import com.dragnportal.hmmer.db.data.HmmerHit;
import com.dragnportal.hmmer.db.data.HmmerReport;
import com.torrenttamer.hibernate.SessionProvider;
import java.util.*;
import org.hibernate.Session;

/**
 * Given a populated database of parsed hmmer reports, obtains a list of
 * all profiles (queryName) entries, and prints the best hit to each.
 * The output is in queryName order.
 * 
 * FIXME the current version of this utility assumes there is just one
 * strain's worth of data in the database!
 * 
 * @author Keith Flanagan
 */
public class TopHitPrinter
{
  private static final int DEFAULT_MAX_BEST_HTIS = 1;
  
  public static void main(String[] args) throws HmmerDbException
  {
    int maxBestHits = DEFAULT_MAX_BEST_HTIS;
//    if (args.length != 1)
//    {
//      System.out.println("USAGE: <Organism ID>");
//      System.exit(1);
//    }
    
    Session session = null;
    try
    {
      SessionProvider hmmSessionProvider = 
          new SessionProvider(Constants.DEFAULT_HMM_DB_RESOURCE);
      session = hmmSessionProvider.getReadOnlySession();
      HmmerReportDAO reportDao = new HmmerReportDAO();
      HmmerHitDAO hitDao = new HmmerHitDAO();
      
      
      System.out.println("Obtaining list of query profile targets ...");
      List<String> queryNames = hitDao.selectDistinctQueryNames(session);
      System.out.println("Found "+queryNames.size()+" hmmer profiles with results in the database");
      
      System.out.println("Querying database for best hits of each profile");
      Appendable output = new StringBuilder();
      output.append("QueryName\tTargetName\tFullSeqEVal\n");
      int profileQueryCount = 0;
      int bestHitCount = 0;
      for (String queryName : queryNames)
      {
        System.out.println("Querying profile: "+(profileQueryCount++)+" of "+queryNames.size()+" ("+queryName+")");
        
        List<HmmerHit> bestHits = 
            hitDao.getBestHitsForHmmQuery(session, queryName, maxBestHits);
        bestHitCount += bestHits.size();
        
        for (HmmerHit hit : bestHits)
        {
          output.append(hit.getQueryName()).append("\t");
          output.append(hit.getTargetName()).append("\t");
          output.append(String.valueOf(hit.getFullSeqEVal())).append("\n");
        }
      }
      
      System.out.println("Report follows:");
      System.out.println("--------------------------------------------------");
      System.out.println(output.toString());
      System.out.println("--------------------------------------------------");
      System.out.println("Queried all profiles. Number of hits in output: "+bestHitCount);
     
      
    }
    catch(Exception e)
    {
      SessionProvider.silentRollback(session);
      throw new HmmerDbException("Failed to query DRAGN database(s) ", e);
    }
    finally
    {
      SessionProvider.silentClose(session);
    }
  }
  
  private static void reportTst(List<HmmerHit> hits, StringBuilder reportTxt)
  {
    double eValThresh = 0.0005;
    Set<String> expectedHmms = new HashSet<String>();
    expectedHmms.add("TstPart1");
    expectedHmms.add("TstPart2");
    expectedHmms.add("TstPart3");
    expectedHmms.add("TstPart4");
    expectedHmms.add("TstPart5");
    expectedHmms.add("TstPart6");
    String description = "The TSST toxin is a superantigen found in ~30% of strains. "
        + "It binds to T cell receptors causing polyclonal T-cell activation "
        + "leading to elevated serum levels of pro-imflammatory cytokines.  "
        + "The toxin is most commonly associated with toxic shock syndrome "
        + "(Foster 2005). Symptoms include rash, hypotension, fever and "
        + "multiorgan dysfunction. \n\n" 
        + "Immune evasion by staphylococci. Foster, T. J. Nat Rev Microbiol. 2005 Dec ; 3(12): 948-58";
    
    Set<String> foundHmms = new HashSet<String>();
    Map<String, Double> foundHmmsToBestEVals = new HashMap<String, Double>();
    for (HmmerHit hit : hits)
    {
      String qryName = hit.getQueryName();
      double eVal = hit.getFullSeqEVal();
      if (expectedHmms.contains(qryName))
      {
        foundHmms.add(qryName);
        if (!foundHmmsToBestEVals.containsKey(qryName))
          //The first time we've found a hit to this profile.
        {
          foundHmmsToBestEVals.put(qryName, eVal);
        }
        else
        {
          double prev_eVal = foundHmmsToBestEVals.get(qryName);
          if (eVal < prev_eVal)
            //We've found a better hit.
          {
            foundHmmsToBestEVals.put(qryName, eVal);  
          }
        }
      }
    }

    reportTxt.append("TST summary:\n");
    reportTxt.append("  * Found "+foundHmms.size()+" of "+expectedHmms.size()+" expected HMMER hits\n");
    
    for (Double eval : foundHmmsToBestEVals.values())
    {
      if (eval >= eValThresh)
      {
        reportTxt.append("  * However, the e-value for one or more hits was too high.\n");
        return;
      }
    }
    
    reportTxt.append("  * ");
    reportTxt.append(description);
    reportTxt.append("\n");
    
    
  }
}
