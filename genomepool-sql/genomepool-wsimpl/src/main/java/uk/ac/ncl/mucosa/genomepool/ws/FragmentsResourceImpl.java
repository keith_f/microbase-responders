/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: Jan 15, 2012, 11:48:11 PM
 */
package uk.ac.ncl.mucosa.genomepool.ws;

import com.torrenttamer.jdbcutils.DistributedJdbcPool;
import com.torrenttamer.jdbcutils.JdbcUtils;
import java.sql.Connection;
import java.util.List;
import org.restlet.Server;
import org.restlet.data.Form;
import org.restlet.data.Protocol;
import org.restlet.resource.ServerResource;
import uk.ac.ncl.mucosa.genomepool.full.dao.GenomeFragmentDAO;
import uk.ac.ncl.mucosa.genomepool.full.data.GenomeFragment;
import uk.ac.ncl.mucosa.genomepool.ws.data.GenomeFragmentWs;
import uk.ac.ncl.mucosa.genomepool.ws.data.PagedItems;

/**
 *
 * @author Keith Flanagan
 */
public class FragmentsResourceImpl
    extends ServerResource
    implements FragmentsResource
{
  private static final int DEFAULT_OFFSET = 0;
  private static final int DEFAULT_LIMIT = Integer.MAX_VALUE;
  
  private final DistributedJdbcPool gpDbPool;

  public static void main(String[] args)
      throws Exception
  {
    // Create the HTTP server and listen on port 8182  
    new Server(Protocol.HTTP, 8182, FragmentsResourceImpl.class).start();
  }

  public FragmentsResourceImpl()
      throws GenomePoolException, Exception
  {
    try
    {
      /*
       * NOTE: This line is *only* required because Tomcat fails to load the 
       * driver in the proper way. If not using Tomcat, remove...
       */
      Class.forName("com.mysql.jdbc.Driver").newInstance(); 
      
      gpDbPool = GenomePoolConfig.
              createDataSourceFromDefaultConfigFile(getClass().getClassLoader());
    }
    catch (GenomePoolException ex)
    {
      ex.printStackTrace();
      throw ex;
    }
    catch(Exception ex)
    {
      ex.printStackTrace();
      throw ex;
    }
  }
  
  


  @Override
  public PagedItems<GenomeFragmentWs> getFragments()
      throws GenomePoolWsException
  {
    Form form = getRequest().getResourceRef().getQueryAsForm();
    int offset = ParamParser.parseOffset(form, DEFAULT_OFFSET);
    int limit = ParamParser.parseLimit(form, DEFAULT_LIMIT);
    List<Integer> taxonIds = 
        ParamParser.parseIntList(form, Parameters.TAXON_ID.paramName());
    
    Connection txn = null;
    try
    {
      txn = gpDbPool.getReadOnlyConnection();
      GenomeFragmentDAO dao = new GenomeFragmentDAO();
      
      PagedItems<GenomeFragmentWs> result = new PagedItems<GenomeFragmentWs>();
      result.setOffset(offset);
      result.setLimit(limit);
      
      if (taxonIds.isEmpty())
        //Standard paged query over all GenomeFragments
      {
        int allGfs = dao.count(txn);
        result.setTotalItems(allGfs);
        List<GenomeFragment> gfs = 
            dao.list(txn, offset, limit);
        List<GenomeFragmentWs> gfsWs = DbToWsConverters.fragmentDbToWs(gfs);
        result.setItems(gfsWs);
      }
      else
        //Request for a specific set of taxons
      {
        int allGfs = dao.countByTaxonIds(txn, taxonIds);
        result.setTotalItems(allGfs);
        List<GenomeFragment> gfs = 
            dao.listByTaxonIds(txn, taxonIds, offset, limit);
        List<GenomeFragmentWs> gfsWs = DbToWsConverters.fragmentDbToWs(gfs);
        result.setItems(gfsWs);
      }
      
      return result;
    }
    catch(Exception e)
    {
      JdbcUtils.silentRollback(txn);
      throw new GenomePoolWsException(
          "Database operation failed: "+e.getMessage(), e);
    }
    finally
    {
      JdbcUtils.silentClose(txn);
    }
  }

}
