/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.microbasecloud.interproscan;

import java.io.Serializable;
import javax.persistence.*;

/**
 *
 * @author Sirintra Nakjang
 */
@Entity
@Table( name = "interproscan_hits" )
public class InterproscanHit
    implements Serializable
{
  @Id
  @GeneratedValue(strategy=GenerationType.AUTO)
  private Long id;
 
  @ManyToOne(cascade = CascadeType.ALL)
  private InterproscanReport report;
 
  private String queryAcc;
  private String crc;
  private int seqlength;
  private String method;
  private String hitAcc;
  private String hitDescription;
  private int startDomain;
  private int endDomain;
  private Double evalue;
  private String status;
  private String iprAcc;
  private String iprDescription;
  private String goDescription;

  public InterproscanHit()
  {
  }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public InterproscanReport getReport() {
        return report;
    }

    public void setReport(InterproscanReport report) {
        this.report = report;
    }

    public String getQueryAcc() {
        return queryAcc;
    }

    public void setQueryAcc(String queryAcc) {
        this.queryAcc = queryAcc;
    }

    public String getCrc() {
        return crc;
    }

    public void setCrc(String crc) {
        this.crc = crc;
    }

    public int getSeqlength() {
        return seqlength;
    }

    public void setSeqlength(int seqlength) {
        this.seqlength = seqlength;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public String getHitAcc() {
        return hitAcc;
    }

    public void setHitAcc(String hitAcc) {
        this.hitAcc = hitAcc;
    }

    public String getHitDescription() {
        return hitDescription;
    }

    public void setHitDescription(String hitDescription) {
        this.hitDescription = hitDescription;
    }

    public int getStartDomain() {
        return startDomain;
    }

    public void setStartDomain(int startDomain) {
        this.startDomain = startDomain;
    }

    public int getEndDomain() {
        return endDomain;
    }

    public void setEndDomain(int endDomain) {
        this.endDomain = endDomain;
    }

    public Double getEvalue() {
        return evalue;
    }

    public void setEvalue(Double evalue) {
        this.evalue = evalue;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getIprAcc() {
        return iprAcc;
    }

    public void setIprAcc(String iprAcc) {
        this.iprAcc = iprAcc;
    }

    public String getIprDescription() {
        return iprDescription;
    }

    public void setIprDescription(String iprDescription) {
        this.iprDescription = iprDescription;
    }

    public String getGoDescription() {
        return goDescription;
    }

    public void setGoDescription(String goDescription) {
        this.goDescription = goDescription;
    }
 
}