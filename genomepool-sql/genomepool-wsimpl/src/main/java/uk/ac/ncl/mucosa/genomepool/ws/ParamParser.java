/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: Jan 21, 2012, 10:18:09 PM
 */

package uk.ac.ncl.mucosa.genomepool.ws;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.restlet.data.Form;
import org.restlet.data.Parameter;

/**
 * Parses common parameter values.
 * 
 * @author Keith Flanagan
 */
public class ParamParser
{
  private static final Logger logger = 
      Logger.getLogger(ParamParser.class.getSimpleName());
  
  public static String parseId(Form form)
  {
    String val = form.getFirstValue(RESTParams.ID.paramName());
    return val;
  }
  
  public static int parseOffset(Form form, int defaultOffset)
  {
    int val = parseInt(
        form.getFirstValue(RESTParams.OFFSET.paramName()), defaultOffset);
    logger.log(Level.INFO, "Returning offset: {0}", val);
    return val;
  }
  
  public static int parseLimit(Form form, int defaultOffset)
  {
    int val = parseInt(
        form.getFirstValue(RESTParams.LIMIT.paramName()), defaultOffset);
    logger.log(Level.INFO, "Returning limit: {0}", val);
    return val;
  }
  
  public static int parseMin(Form form, int defaultOffset)
  {
    int val = parseInt(
        form.getFirstValue(RESTParams.MIN.paramName()), defaultOffset);
    logger.log(Level.INFO, "Returning min: {0}", val);
    return val;
  }  
  
  public static int parseMax(Form form, int defaultOffset)
  {
    int val = parseInt(
        form.getFirstValue(RESTParams.MAX.paramName()), defaultOffset);
    logger.log(Level.INFO, "Returning max: {0}", val);
    return val;
  }
  
  /**
   * Given a parameter name, returns all values, or an empty list if no
   * such parameter name existed in the query.
   * @param form
   * @param paramName
   * @return 
   */
  public static List<String> parseStringList(Form form, String paramName)
  {
    boolean ignoreCase = true;
    String[] valArr = form.getValuesArray(paramName, ignoreCase);
    List<String> values = new ArrayList<String>(valArr.length);
    if (valArr != null)
    {
      values.addAll(Arrays.asList(valArr));
    }
    return values;
  }
  
  public static List<Integer> parseIntList(Form form, String paramName)
  {
    boolean ignoreCase = true;
    String[] valArr = form.getValuesArray(paramName, ignoreCase);
    List<Integer> values = new ArrayList<Integer>(valArr.length);
    if (valArr != null)
    {
      for (String valStr : valArr)
      {
        try
        {
          values.add(Integer.parseInt(valStr));
        }
        catch(NumberFormatException e)
        {
          logger.log(Level.INFO, 
              "NumberFormatException: {0}. Ignoring list entry: {1}", 
              new Object[]{e.getMessage(), valStr});
        }
      }
    }
    return values;
  }
  
  private static int parseInt(String valStr, int defaultOffset)
  {
    int val = defaultOffset;
    try
    {
      if (valStr != null)
      {
        val = Integer.parseInt(valStr);
      }
    }
    catch(NumberFormatException e)
    {
      logger.log(Level.INFO, "NumberFormatException: {0}", e.getMessage());
    }
    return val;
  }
}
