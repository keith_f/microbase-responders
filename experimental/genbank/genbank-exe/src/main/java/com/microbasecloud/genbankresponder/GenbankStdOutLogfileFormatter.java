/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.microbasecloud.genbankresponder;

import com.microbase.responder.genbankcommon.GenbankJobDescription;
import uk.org.microbase.filesystem.spi.MBFile;

/**
 *
 * @author prisni
 */
public class GenbankStdOutLogfileFormatter extends AbstractOutputPathReformatter
    implements GenbankOutputPathReformatter
{
  
  private GenbankJobDescription jobDescription;
  
  @Override
  public String getFormattedBucketName() {
    return responderInfo.getLogDestinationBucket();
  }

  @Override
  public String getFormattedPath() {
    StringBuilder path = new StringBuilder(responderInfo.getLogDestinationBasePath());
    path.append("/").append(getFormattedFilename());
    path.append("/").append(message.getGuid());
    path.append("/").append(processInfo.getWorkStartedAtMs());
    return path.toString();
  }

  @Override
  public String getFormattedFilename() {
    StringBuilder resultFilename = new StringBuilder();
//    resultFilename.append(jobDescription.getQueryFile().getName())
//        .append("-").append(jobDescription.getDatabaseName())
//        .append(".").append(jobDescription.getJobType().name())
//        .append(".stdout");
    return resultFilename.toString();
  }
  
  @Override
  public MBFile getFormattedRemoteFile() {
    MBFile file = new MBFile(getFormattedBucketName(), getFormattedPath(), getFormattedFilename());
    return file;
  }
  

  public GenbankJobDescription getJobDescription() {
    return jobDescription;
  }

  @Override
  public void setJobDescription(GenbankJobDescription jobDescription) {
    this.jobDescription = jobDescription;
  }

}
