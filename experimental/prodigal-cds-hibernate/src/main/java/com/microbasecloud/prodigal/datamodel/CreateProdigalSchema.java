/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.microbasecloud.prodigal.datamodel;
import java.io.File;
import org.hibernate.cfg.Configuration;
import org.hibernate.dialect.MySQLDialect;
/**
 *
 * @author prisni
 */
public class CreateProdigalSchema {
    
    public static void main(String[] args)
  {
    String str=  args[0];
    File fileName = new File(str);
    Configuration cfg = new Configuration().configure(fileName);
    String[] lines = cfg.generateSchemaCreationScript(new MySQLDialect());

    System.out.println("\n\nSQL Schema follows:");
    for (int i = 0; i < lines.length; i++)
    {
      System.out.println(lines[i] + ";");
    }
  }
}

