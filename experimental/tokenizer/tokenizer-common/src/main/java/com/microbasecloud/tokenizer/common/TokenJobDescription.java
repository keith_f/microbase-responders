/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package com.microbasecloud.tokenizer.common;

import uk.org.microbase.filesystem.spi.MBFile;

/**
 *
 * @author Keith Flanagan
 */
public class TokenJobDescription
{
  private MBFile fragmentFile;
  
  private String organismGuid;
  private String fragmentGuid;
  private String graphName;
  private String graphBranch;
  
  private int tokenSize;

  public TokenJobDescription() {
  }

  public int getTokenSize() {
    return tokenSize;
  }

  public void setTokenSize(int tokenSize) {
    this.tokenSize = tokenSize;
  }

  public String getFragmentGuid() {
    return fragmentGuid;
  }

  public void setFragmentGuid(String fragmentGuid) {
    this.fragmentGuid = fragmentGuid;
  }

  public MBFile getFragmentFile() {
    return fragmentFile;
  }

  public void setFragmentFile(MBFile fragmentFile) {
    this.fragmentFile = fragmentFile;
  }

  public String getGraphName() {
    return graphName;
  }

  public void setGraphName(String graphName) {
    this.graphName = graphName;
  }

  public String getGraphBranch() {
    return graphBranch;
  }

  public void setGraphBranch(String graphBranch) {
    this.graphBranch = graphBranch;
  }

  public String getOrganismGuid() {
    return organismGuid;
  }

  public void setOrganismGuid(String organismGuid) {
    this.organismGuid = organismGuid;
  }
  
}
