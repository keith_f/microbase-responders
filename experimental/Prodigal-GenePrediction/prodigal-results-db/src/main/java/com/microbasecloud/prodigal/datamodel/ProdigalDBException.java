/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.microbasecloud.prodigal.datamodel;

/**
 *
 * @author prisni
 */
public class ProdigalDBException extends Exception {

    public ProdigalDBException() {
    }

    public ProdigalDBException(String message) {
        super(message);
    }

    public ProdigalDBException(String message, Throwable cause) {
        super(message, cause);
    }

    public ProdigalDBException(Throwable cause) {
        super(cause);
    }
    
}
