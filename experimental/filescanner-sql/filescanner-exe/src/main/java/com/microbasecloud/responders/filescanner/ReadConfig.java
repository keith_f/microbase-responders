/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: 09-Oct-2012, 20:33:48
 */

package com.microbasecloud.responders.filescanner;

import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Logger;

/**
 * Reads a Properties object that defines one or more remote targets
 * that need to be scanned.
 * 
 * An example configuration file that defines two locations:
 * <pre>
 * new_gbk_files.bucket = my-amazon-bucket-name
 * new_gbk_files.path = /path/to/gbks
 * new_gbk_files.extensions = .gbk
 * new_gbk_files.scan_every_seconds = 120
 * new_gbk_files.message_topic = NEW_GENBANK_FILE_FOUND
 * 
 * fasta_files.bucket = my-amazon-bucket-name
 * fasta_files.path = /path/to/fasta/files
 * fasta_files.extensions = .fna .faa
 * fasta_files.scan_every_seconds = 60
 * fasta_files.message_topic = NEW_FASTA_FILE_FOUND
 * </pre>
 * @author Keith Flanagan
 */
public class ReadConfig
{
  private static final Logger logger =
      Logger.getLogger(ReadConfig.class.getName());
  
  private static final String PROP_BUCKET = "bucket";
  private static final String PROP_PATH = "path";
  private static final String PROP_EXTENSIONS = "extensions";
  private static final String PROP_SCAN_EVERY_SEC = "scan_every_seconds";
  private static final String PROP_MESSAGE_TOPIC_NEW = "message_topic.new";
  private static final String PROP_MESSAGE_TOPIC_MOD = "message_topic.modified";
  private static final String PROP_MESSAGE_TOPIC_DEL = "message_topic.deleted";
  private static final String PROP_MESSAGE_TOPIC_UNC = "message_topic.unchanged";
  private static final String PROP_MESSAGE_TOPIC_DIG = "message_topic.digest";
  
  
  /**
   * Reads a properties object and returns a set of Targets defined
   * by the contents.
   * 
   * @param props
   * @return 
   */
  public static Set<Target> loadConfig(Properties props)
  {
    Map<String, Target> targets = new HashMap<>();

    for (String key : props.stringPropertyNames())
    {
      String val = props.getProperty(key);
      
      //Here, we split the Properties key: <target name>.<config property>
      String targetName = key.substring(0, key.indexOf("."));
      String propertyType = key.substring(key.indexOf(".")+1); // Start after the dot
      logger.info("Target name: "+targetName+", type: "+propertyType + ", val: "+val);
      
      Target target = targets.get(targetName);
      if (target == null) {
        target = new Target();
        target.setTargetName(targetName);
        targets.put(targetName, target);
      }
      
      switch (propertyType) {
        case PROP_BUCKET:
          target.getTargetDir().setBucket(val);
          break;
        case PROP_PATH:
          target.getTargetDir().setPath(val);
          break;
        case PROP_EXTENSIONS:
          for (String ext : val.split(" ")) {
            target.getExtensions().add(ext);
          }
          break;
        case PROP_SCAN_EVERY_SEC:
          target.setScanEveryMs(Integer.parseInt(val) * 1000);
          break;
        case PROP_MESSAGE_TOPIC_NEW:
          target.setNewFileTopic(val);
          break;
        case PROP_MESSAGE_TOPIC_MOD:
          target.setUpdatedFileTopic(val);
          break;
        case PROP_MESSAGE_TOPIC_DEL:
          target.setDeletedFileTopic(val);
          break;
        case PROP_MESSAGE_TOPIC_UNC:
          target.setUnchangedFileTopic(val);
          break;
        case PROP_MESSAGE_TOPIC_DIG:
          target.setDigestTopic(val);
          break;
      }
    }
    return new HashSet<>(targets.values());
  }
}


