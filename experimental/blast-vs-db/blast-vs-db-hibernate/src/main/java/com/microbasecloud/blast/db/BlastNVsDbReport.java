/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: 25-Mar-2012, 12:40:32
 */

package com.microbasecloud.blast.db;

import java.io.Serializable;
import javax.persistence.*;

/**
 *
 * @author Keith Flanagan
 */
@Entity
@Table( name = "blastn_reports" )
public class BlastNVsDbReport 
    implements Serializable
{
  @Id
  @GeneratedValue(strategy= GenerationType.AUTO)
  private Long id;

  private String uid;
  
  private String messageUid;
  
  private String commandLine;
  
  @OneToOne(cascade = CascadeType.ALL)
  private FileInfoDb blastDatabaseFile;
  
  @OneToOne(cascade = CascadeType.ALL)
  private FileInfoDb querySequenceFile;
  
  @OneToOne(cascade = CascadeType.ALL)
  private FileInfoDb blastReportFile;      
      
  public BlastNVsDbReport()
  {
  }

  @Override
  public String toString()
  {
    return "BlastNVsDbReport{" + "id=" + id + ", uid=" + uid 
        + ", messageUid=" + messageUid + ", commandLine=" + commandLine 
        + ", blastDatabaseFile=" + blastDatabaseFile 
        + ", querySequenceFile=" + querySequenceFile 
        + ", blastReportFile=" + blastReportFile + '}';
  }

  public Long getId()
  {
    return id;
  }

  public void setId(Long id)
  {
    this.id = id;
  }

  public String getUid()
  {
    return uid;
  }

  public void setUid(String uid)
  {
    this.uid = uid;
  }

  public String getMessageUid()
  {
    return messageUid;
  }

  public void setMessageUid(String messageUid)
  {
    this.messageUid = messageUid;
  }

  public String getCommandLine()
  {
    return commandLine;
  }

  public void setCommandLine(String commandLine)
  {
    this.commandLine = commandLine;
  }

  public FileInfoDb getBlastDatabaseFile()
  {
    return blastDatabaseFile;
  }

  public void setBlastDatabaseFile(FileInfoDb blastDatabaseFile)
  {
    this.blastDatabaseFile = blastDatabaseFile;
  }

  public FileInfoDb getQuerySequenceFile()
  {
    return querySequenceFile;
  }

  public void setQuerySequenceFile(FileInfoDb querySequenceFile)
  {
    this.querySequenceFile = querySequenceFile;
  }

  public FileInfoDb getBlastReportFile()
  {
    return blastReportFile;
  }

  public void setBlastReportFile(FileInfoDb blastReportFile)
  {
    this.blastReportFile = blastReportFile;
  }
}
