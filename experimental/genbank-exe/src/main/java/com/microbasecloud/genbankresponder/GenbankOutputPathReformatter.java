/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.microbasecloud.genbankresponder;

import com.microbase.responder.genbankcommon.GenbankJobDescription;
import uk.org.microbase.responder.util.OutputPathReformatter;

/**
 *
 * @author prisni
 */
public interface GenbankOutputPathReformatter extends OutputPathReformatter {
    
    public void setJobDescription(GenbankJobDescription job);
}
