/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: 25-Mar-2012, 12:40:32
 */

package uk.ac.ncl.mucosa.genomepool.graph.fastq.nodes;

import uk.ac.ncl.aries.entanglement.graph.data.Node;

/**
 *
 * @author Keith Flanagan
 */
public class FastqSequenceEntryNode 
    extends Node
{ 
  private String readId;
  
  private String dnaSequence;
 
  public FastqSequenceEntryNode()
  {
  }

  public String getDnaSequence()
  {
    return dnaSequence;
  }

  public void setDnaSequence(String dnaSequence)
  {
    this.dnaSequence = dnaSequence;
  }

  public String getReadId()
  {
    return readId;
  }

  public void setReadId(String readId)
  {
    this.readId = readId;
  }
  
}
