/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: 26-Mar-2012, 00:36:11
 */

package com.dragnportal.hmmer.db;

import com.dragnportal.hmmer.db.data.HmmerHit;
import com.dragnportal.hmmer.db.data.HmmerReport;
import com.torrenttamer.hibernate.SessionProvider;
import java.util.Set;
import org.hibernate.Session;

/**
 *
 * @author Keith Flanagan
 */
public class Importer
{
  public static void insertHits(SessionProvider hmmerSessionProvider, 
      HmmerReport report, Set<HmmerHit> hits)
      throws HmmerDbException
  {
    Session session = null;
    try
    {
      for (HmmerHit hit : hits)
      {
        hit.setReport(report);
      }
      
      session = hmmerSessionProvider.getWriteableSession();
      session.beginTransaction();
      HmmerHitDAO dao = new HmmerHitDAO();
      dao.store(session, hits);
      System.out.println("Committing...");
      session.getTransaction().commit();
      System.out.println("Committed.");
    }
    catch(Exception e)
    {
      SessionProvider.silentRollback(session);
      throw new HmmerDbException("Failed to save "
          + hits.size()+" hits to the DB", e);
    }
    finally
    {
      SessionProvider.silentClose(session);
    }
  }
}
