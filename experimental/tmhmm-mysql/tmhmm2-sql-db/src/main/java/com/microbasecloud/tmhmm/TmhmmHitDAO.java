/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.microbasecloud.tmhmm;

import java.util.List;
import java.util.Set;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author Sirintra Nakjang
 */
public class TmhmmHitDAO {
    private static final int DEFAULT_BATCH = 50000;
 
  public void store(Session session, Set<TmhmmHit> hits)
      throws TmhmmParserException
  {
    try
    {
      int count = 0;
      for (TmhmmHit hit : hits)
      {
        session.save(hit);
        count++;
        if (count % DEFAULT_BATCH == 0)
        {
          session.flush();
          session.clear();
        }
      }
    }
    catch(Exception e)
    {
      throw new TmhmmParserException(
          "Failed to execute store() operation", e);
    }
  }
 
  public List<TmhmmHit> selectHitsForReport(Session session, long reportId)
      throws TmhmmParserException
  {
    try
    {
      String qry = "from "+TmhmmHit.class.getName()+" where report.id = :id";
      Query query = session.createQuery(qry);
      query.setParameter("id", reportId);
 
      List<TmhmmHit> hits = query.list();
      return hits;
    }
    catch(Exception e)
    {
      throw new TmhmmParserException(
          "Failed to execute selectHitsForReport()", e);
    }
  }
}
