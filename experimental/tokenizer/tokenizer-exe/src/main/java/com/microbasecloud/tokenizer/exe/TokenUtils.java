/*
 * Copyright 2010 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: 23-Aug-2010, 15:09:11
 */

package com.microbasecloud.tokenizer.exe;

import com.microbasecloud.tokenizer.db.data.TokenOccurrence;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Keith Flanagan
 */
public class TokenUtils
{ 
  public static List<TokenOccurrence> tokenise(
      String productSequence, int windowSize)
  {
    List<TokenOccurrence> tokens = new ArrayList<>();
    for (int i=0; i<productSequence.length() - windowSize + 1; i++)
    {
      TokenOccurrence token = new TokenOccurrence();
      String tokenStr = productSequence.substring(i, i+windowSize);
      token.setToken(tokenStr);
//      token.setTokenLength(windowSize);
      token.setStartCoord(i);
      tokens.add(token);
    }
    return tokens;
  }
}
