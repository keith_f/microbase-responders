/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: 09-May-2012, 22:36:58
 */
package uk.ac.ncl.mucosa.genomepool.job.fastq;

import com.microbasecloud.responders.filescanner.data.nodes.FileNode;
import com.torrenttamer.mongodb.dbobject.DbObjectMarshaller;
import com.torrenttamer.mongodb.dbobject.DbObjectMarshallerException;
import java.io.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.logging.Logger;
import org.openide.util.Exceptions;
import uk.ac.ncl.aries.entanglement.revlog.RevisionLog;
import uk.ac.ncl.aries.entanglement.revlog.RevisionLogException;
import uk.ac.ncl.aries.entanglement.revlog.commands.CreateEdge;
import uk.ac.ncl.aries.entanglement.revlog.commands.CreateNode;
import uk.ac.ncl.aries.entanglement.revlog.commands.GraphOperation;
import uk.ac.ncl.aries.entanglement.util.TxnUtils;
import uk.ac.ncl.mucosa.genomepool.GenomePoolDbException;
import uk.ac.ncl.mucosa.genomepool.graph.fastq.edges.DataParsedFrom;
import uk.ac.ncl.mucosa.genomepool.graph.fastq.edges.PartOfFastqReport;
import uk.ac.ncl.mucosa.genomepool.graph.fastq.nodes.FastqReportNode;
import uk.ac.ncl.mucosa.genomepool.graph.fastq.nodes.FastqScoreEntryNode;
import uk.ac.ncl.mucosa.genomepool.graph.fastq.nodes.FastqSequenceEntryNode;
import uk.ac.ncl.mucosa.genomepool.job.ParseDataException;
import uk.org.microbase.util.UidGenerator;

/**
 * Parses a FASTQ file.
 * 
 * A FASTQ file has the following format:
 * 1) An ID line
 * 2) One or more lines of DNA sequence
 * 3) A '+' symbol
 * 4) One or more lines of quality scores
 * 
 * The next line should then be an ID line, and so on.
 * The length of the combined DNA sequences line(s) for a particular read should
 * be identical to the combined length of the quality score line(s) fro the read.
 * 
 * EG:
 * <code>
 * @SOME_ID
 * ATGCATGC...
 * ATGCATGC...
 * +
 * Score line(s)
 * Score line(s)
 * @SOME_ID (next ID)
 * ... and so on ...
 * </code>
 * 
 * The input to this parser should be a single FASTQ file. The outputs are as
 * follows:
 * <ul>
 * <li>a FastqReport and set of FastqScoreEntry and FastqSequence entries are
 * generated and stored to a MySQL database</li>.
 * <li>a DNA FASTA file is generated, containing the read IDs and associated
 * DNA sequences</li>
 * <li>a protein FASTA file is generate that contains 6-frame translations of
 * each read</li>.
 * 
 * 
 * @author Keith Flanagan
 */
public class FastqParserToEntanglement
{
  private static final Logger logger =
          Logger.getLogger(FastqParserToEntanglement.class.getName());
  private static final int BATCH_SIZE = 100;
  
  private static final String SEPARATOR = "+";
  public enum FastqState
  {
    ID_LINE, SEQUENCE, SCORES
  }
  
  private final DbObjectMarshaller marshaller; 
  private final RevisionLog revLog;
  private int txnSubmitId;
  
  //The ID of the graph node that contains the file we're parsing
  private final String fileNodeUid;
  
  /*
   * Temporary per-read variables
   */
  private FastqState state;
  private String readId;
  private StringBuilder readSeq;
  private StringBuilder readScores;
  

  /**
   * Overall database bean that represents the entire file
   */
  private FastqReportNode report;
  
  /**
   * A batch of sequence objects - this set is periodically stored in the
   * database as part of a batch operation
   */
  private final List<FastqSequenceEntryNode> parsedSeqs;
  
  /**
   * A batch of score objects - this set is periodically stored in the
   * database as part of a batch operation
   */
  private final List<FastqScoreEntryNode> parsedScores;
  
  private final List<GraphOperation> graphOps;
  
  public FastqParserToEntanglement(DbObjectMarshaller marshaller, RevisionLog revLog, String fileNodeUid)
  {
    this.revLog = revLog;
    this.marshaller = marshaller;
    this.fileNodeUid = fileNodeUid;
    this.parsedSeqs = new ArrayList<>();
    this.parsedScores = new ArrayList<>();
    graphOps = new LinkedList<>();
  }
  
  /**
   * Parse a new FASTQ file. These files may be quite large, so each read is
   * parsed and stored to the database in a streaming fashion. 
   * 
   * As well as database storage, two FASTA files are (optionally) produced.
   * The first of these is a FASTA output of the reads in the FASTQ file. The
   * second contains 6-frame translations of the reads.
   * 
   * @param fastqStream an InputStream providing the FASTQ data
   * @param session a writable database connection used for storing the parsed
   * objects in a relational DB
   * @param dnaFastaStream if specified, parsed sequence data is written to this
   * stream in FASTA format
   * @param translationFastaStream if specified, 6-frame protein translations of
   * sequence data reads are written to this stream in FASTA format.
   * @param description an optional human-readable description that is attached
   * to FASTQ report entries when they are stored in a database
   * @param fasqFileURI an optional URI that is stored as meta-data alongside
   * the FASTQ entry when the database output is used. Can be used to store a
   * reference to the original FASTQ report file for provenence.
   * @return a FastqReport object that can be used to query the database.
   * @throws ParseDataException 
   */
  public FastqReportNode parse(InputStream fastqStream, 
      String graphId, String graphBranch,
      OutputStream dnaFastaStream, OutputStream translationFastaStream,
      String description)
      throws ParseDataException
  {
    String txnId = null;
    try
    {
      graphOps.clear();
      
      txnId = TxnUtils.beginNewTransaction(revLog, graphId, graphBranch);
      txnSubmitId = 0;
      
      report = new FastqReportNode();
      report.setUid(UidGenerator.generateUid());
      report.setAdded(new Date(System.currentTimeMillis()));
      report.setDescription(description);
//      report.setFastqFileURI(fastqFileURI);
      report.setNumReads(0);
      
      // Create a report graph node
      graphOps.add(new CreateNode(marshaller.serialize(report)));
      // Link the report to the original file node
      DataParsedFrom parsedFrom = new DataParsedFrom(report.getUid(), fileNodeUid);
      parsedFrom.setFromType(report.getClass().getSimpleName());
      parsedFrom.setToType(FileNode.class.getSimpleName());
      graphOps.add(new CreateEdge(marshaller.serialize(parsedFrom)));
      
      
      state = FastqState.ID_LINE;
      //Iterate each line in the file.
      BufferedReader br = new BufferedReader(new InputStreamReader(fastqStream));
      int lineCountDebug = 0;
      for (String line = br.readLine(); line != null; line = br.readLine())
      {
        lineCountDebug++;
        if (lineCountDebug % 100 == 0) {
          logger.info("Linecount: "+lineCountDebug+", parsedSeqs.size(): "+parsedSeqs.size());
        }
        

        
        
        switch(state)
        {
          case ID_LINE:
            dealIdLine(line);
            break;
          case SEQUENCE:
            dealSeqLine(line);
            break;
          case SCORES :
            dealScoreLine(line);
            
            //Periodically store a batch of items to the DB and files.
            if (state == FastqState.ID_LINE && parsedSeqs.size() >= BATCH_SIZE)
            {
              storeBatch(revLog, graphId, graphBranch, txnId, txnSubmitId, dnaFastaStream, translationFastaStream);
            }
            
            dealIdLine(line);
            break;
        }
        

      }

      //Stores final batch of items to the DB and files.
      if (!parsedSeqs.isEmpty()) {
        storeBatch(revLog, graphId, graphBranch, txnId, txnSubmitId, dnaFastaStream, translationFastaStream); 
      }
      
      //Commit the transaction
      TxnUtils.commitTransaction(revLog, graphId, graphBranch, txnId);
      
      return report;
    }
    catch (Exception e)
    {
      try {
        TxnUtils.rollbackTransaction(revLog, graphId, graphBranch, txnId);
      } catch (RevisionLogException ex) {
        logger.info("Also failed to roll back the transaction!");
        Exceptions.printStackTrace(ex);
      }
      throw new ParseDataException("Failed to parse or store FASTQ file", e);
    }
  }
  
  private void dealIdLine(String line)
  {
    if (state != FastqState.ID_LINE)
    {
      return;
    }
    readId = line.substring(1); //Remove first '@' character - take the rest as ID.
    readSeq = new StringBuilder();
    readScores = new StringBuilder();
    state = FastqState.SEQUENCE;
  }
  
  private void dealSeqLine(String line)
  {
    if (line.length() == 1 && line.equals(SEPARATOR))
    {
      //Reached the end of the sequence data. Change state to start reading 
      //scores
      state = FastqState.SCORES;
    }
    else
    {
      readSeq.append(line);
    }
  }
  
  private void dealScoreLine(String line) throws DbObjectMarshallerException
  {
    if (readSeq.length() == readScores.length())
    {
      //We've reached the end of this entry
      FastqSequenceEntryNode seqEntry = new FastqSequenceEntryNode();
      seqEntry.setUid(UidGenerator.generateUid());
      seqEntry.setDnaSequence(readSeq.toString());
      seqEntry.setReadId(readId);
//      seqEntry.setReport(report);
      parsedSeqs.add(seqEntry);
      
      FastqScoreEntryNode scoreEntry = new FastqScoreEntryNode();
      scoreEntry.setUid(UidGenerator.generateUid());
      scoreEntry.setQualityScores(readScores.toString());
      scoreEntry.setReadId(readId);
//      scoreEntry.setReport(report);
      parsedScores.add(scoreEntry);
      
      state = FastqState.ID_LINE; //Reset state for next entry
    }
    else
    {
      readScores.append(line);
    }
  }
  
  /**
   * Stores the content of <code>parsedSeqs</code> and 
   * <code>parsedScores</code> to a database and/or files.
   */
  private void storeBatch(RevisionLog revLog, String graphId, String branchId,
      String txnUid, int txnSubmitId,
      OutputStream dnaFastaStream, OutputStream proteinFastaStream)
      throws ParseDataException
  {
    logger.info("Storing batch now.");
    /*
     * Write data to DB
     */
    if (revLog != null)
    {
      try
      {
        for (FastqSequenceEntryNode seqEntry : parsedSeqs) {
          graphOps.add(new CreateNode(marshaller.serialize(seqEntry)));
          PartOfFastqReport partOf = new PartOfFastqReport(seqEntry, report);
          graphOps.add(new CreateEdge(marshaller.serialize(partOf)));
        }
        
        for (FastqScoreEntryNode scoreEntry : parsedScores) {
          graphOps.add(new CreateNode(marshaller.serialize(scoreEntry)));
          PartOfFastqReport partOf = new PartOfFastqReport(scoreEntry, report);
          graphOps.add(new CreateEdge(marshaller.serialize(partOf)));
        }
        
        revLog.submitRevisions(graphId, branchId, txnUid, txnSubmitId, graphOps);
        txnSubmitId++;
        graphOps.clear();

      }
      catch(Exception e)
      {
        throw new ParseDataException(
            "Failed to write FASTQ entries to a database", e);
      }
    }
    
    /*
     * Write DNA FASTA data to OutputStream
     */
    if (dnaFastaStream != null)
    {
      try
      {
        for (FastqSequenceEntryNode entry : parsedSeqs)
        {
          StringBuilder fastaEntry = new StringBuilder();

          fastaEntry.append(">").append(entry.getReadId()).append("\n");
          fastaEntry.append(entry.getDnaSequence()).append("\n");

          //Write FASTA-formatted DNA to OutputStream
          byte[] fastaBytes = fastaEntry.toString().getBytes();
          dnaFastaStream.write(fastaBytes);
          
          
          //Also do 6-frame translation and write to the OutputStream
          if (proteinFastaStream != null)
          {
            InputStream ramStream = new ByteArrayInputStream(fastaBytes);
            SixFrameTranslateUtil.translateSixFrames(ramStream, proteinFastaStream);
          }
        }
      }
      catch(IOException e)
      {
        throw new ParseDataException(
            "Failed to write DNA FASTA sequence(s) to stream", e);
      }
    }
    
    
    report.setNumReads(report.getNumReads()+parsedSeqs.size());
    if (parsedSeqs.size() != parsedScores.size())
    {
      throw new ParseDataException(
          "Completed parsing of a batch of reads, but number of sequences != "
          + "number of scores: "+parsedSeqs.size()+" != "+parsedScores.size());
    }
    parsedSeqs.clear();
    parsedScores.clear();
  }
  
}
