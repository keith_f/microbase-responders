/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.microbase.responder.assemblermiraresponder;

import com.microbase.responder.assemblermiracommon.AssemblerMIRAJobDescription;

/**
 *
 * @author prisni
 */
public interface AssemblerMIRAOutputPathReformatter extends OutputPathReformatter{
    public void setJobDescription(AssemblerMIRAJobDescription job);
}
