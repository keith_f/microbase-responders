/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.microbasecloud.genbankresponder;

import java.util.Set;

/**
 *
 * @author prisni
 */
public interface RegionFinder {
    
    public Set<String> findRegionOf(String fragmentId) throws FeatureTableAnnotatorException;
    
    public void initialize() throws FeatureTableAnnotatorException;
    
}
