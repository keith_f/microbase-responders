/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: 25-Mar-2012, 12:40:32
 */

package com.microbasecloud.blast.db;

import java.io.Serializable;
import javax.persistence.*;

/**
 *
 * @author Keith Flanagan
 */
@Entity
@Table( name = "blast_hits" )
public class BlastNVsDbHit 
    implements Serializable
{
  @Id
  @GeneratedValue(strategy= GenerationType.AUTO)
  private Long id;
  
  @ManyToOne(cascade = CascadeType.ALL)
  private BlastNVsDbReport report;
  
  private String queryName;
  private String subjectName;
  private float percentIdentity;
  private int alignedLength;
  private int numMismatches;
  private int numGaps;
  private int querySeqStart;
  private int querySeqEnd;
  private int subjectSeqStart;
  private int subjectSeqEnd;
  private double eValue;
  private double bitScore;
  
      
      
  public BlastNVsDbHit()
  {
  }

  @Override
  public String toString()
  {
    return "BlastNVsDbHit{" + "id=" + id + ", report=" + report 
        + ", queryName=" + queryName + ", subjectName=" + subjectName 
        + ", percentIdentity=" + percentIdentity 
        + ", alignedLength=" + alignedLength
        + ", numMismatches=" + numMismatches + ", numGaps=" + numGaps 
        + ", querySeqStart=" + querySeqStart 
        + ", querySeqEnd=" + querySeqEnd
        + ", subjectSeqStart=" + subjectSeqStart
        + ", subjectSeqEnd=" + subjectSeqEnd
        + ", eValue=" + eValue + ", bitScore=" + bitScore + '}';
  }

  public Long getId()
  {
    return id;
  }

  public void setId(Long id)
  {
    this.id = id;
  }

  public BlastNVsDbReport getReport()
  {
    return report;
  }

  public void setReport(BlastNVsDbReport report)
  {
    this.report = report;
  }

  public String getQueryName()
  {
    return queryName;
  }

  public void setQueryName(String queryName)
  {
    this.queryName = queryName;
  }

  public String getSubjectName()
  {
    return subjectName;
  }

  public void setSubjectName(String subjectName)
  {
    this.subjectName = subjectName;
  }

  public float getPercentIdentity()
  {
    return percentIdentity;
  }

  public void setPercentIdentity(float percentIdentity)
  {
    this.percentIdentity = percentIdentity;
  }

  public int getAlignedLength()
  {
    return alignedLength;
  }

  public void setAlignedLength(int alignedLength)
  {
    this.alignedLength = alignedLength;
  }

  public int getNumMismatches()
  {
    return numMismatches;
  }

  public void setNumMismatches(int numMismatches)
  {
    this.numMismatches = numMismatches;
  }

  public int getNumGaps()
  {
    return numGaps;
  }

  public void setNumGaps(int numGaps)
  {
    this.numGaps = numGaps;
  }

  public int getQuerySeqStart()
  {
    return querySeqStart;
  }

  public void setQuerySeqStart(int querySeqStart)
  {
    this.querySeqStart = querySeqStart;
  }

  public int getQuerySeqEnd()
  {
    return querySeqEnd;
  }

  public void setQuerySeqEnd(int querySeqEnd)
  {
    this.querySeqEnd = querySeqEnd;
  }

  public int getSubjectSeqStart()
  {
    return subjectSeqStart;
  }

  public void setSubjectSeqStart(int subjectSeqStart)
  {
    this.subjectSeqStart = subjectSeqStart;
  }

  public int getSubjectSeqEnd()
  {
    return subjectSeqEnd;
  }

  public void setSubjectSeqEnd(int subjectSeqEnd)
  {
    this.subjectSeqEnd = subjectSeqEnd;
  }

  public double geteValue()
  {
    return eValue;
  }

  public void seteValue(double eValue)
  {
    this.eValue = eValue;
  }

  public double getBitScore()
  {
    return bitScore;
  }

  public void setBitScore(double bitScore)
  {
    this.bitScore = bitScore;
  }
  
}
