/*
 * Copyright 2011 Keith Flanagan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * File created: 20-Sep-2010, 17:48:17
 */

package uk.ac.ncl.mucosa.genomepool.notification;

import org.codehaus.jackson.annotate.JsonIgnore;
import uk.org.microbase.notification.data.Message;

/**
 *
 * @author Keith Flanagan
 */
public class NewFaaFileNotification
{
  private static final String ACCESSION_ID = "faa_accessionid";

  private static final String BUCKET = "faa_bucket";
  private static final String PATH = "faa_path";
  private static final String NAME = "faa_name";

  protected final Message message;

  public NewFaaFileNotification(Message message)
  {
    this.message = message;
  }

  public Message getMessage()
  {
    return message;
  }
 

  @JsonIgnore
  public String getFragmentAccessionId()
  {
    return message.getContent().get(ACCESSION_ID);
  }

  @JsonIgnore
  public void setFragmentAccessionId(String accessionId)
  {
    message.getContent().put(ACCESSION_ID, accessionId);
  }

  @JsonIgnore
  public String getBucket()
  {
    return message.getContent().get(BUCKET);
  }

  @JsonIgnore
  public void setBucket(String bucket)
  {
    message.getContent().put(BUCKET, bucket);
  }

  @JsonIgnore
  public String getName()
  {
    return message.getContent().get(NAME);
  }

  @JsonIgnore
  public void setName(String name)
  {
    message.getContent().put(NAME, name);
  }

  @JsonIgnore
  public String getPath()
  {
    return message.getContent().get(PATH);
  }

  @JsonIgnore
  public void setPath(String path)
  {
    message.getContent().put(PATH, path);
  }


}
