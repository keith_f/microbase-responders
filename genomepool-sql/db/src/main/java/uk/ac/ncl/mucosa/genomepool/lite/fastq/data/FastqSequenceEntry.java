/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: 25-Mar-2012, 12:40:32
 */

package uk.ac.ncl.mucosa.genomepool.lite.fastq.data;

import java.io.Serializable;
import javax.persistence.*;

/**
 *
 * @author Keith Flanagan
 */
@Entity
@Table( name = "fastq_seq_entries" )
public class FastqSequenceEntry 
    implements Serializable
{
  @Id
  @GeneratedValue(strategy= GenerationType.AUTO)
  private Long id;
  
  @ManyToOne(cascade = CascadeType.ALL)
  private FastqReport report;
  
  private String readId;
  
  @Lob
  private String dnaSequence;
 
  public FastqSequenceEntry()
  {
  }

  public Long getId()
  {
    return id;
  }

  public void setId(Long id)
  {
    this.id = id;
  }

  public FastqReport getReport()
  {
    return report;
  }

  public void setReport(FastqReport report)
  {
    this.report = report;
  }

  public String getDnaSequence()
  {
    return dnaSequence;
  }

  public void setDnaSequence(String dnaSequence)
  {
    this.dnaSequence = dnaSequence;
  }

  public String getReadId()
  {
    return readId;
  }

  public void setReadId(String readId)
  {
    this.readId = readId;
  }
  
}
