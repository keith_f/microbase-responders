/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package com.microbasecloud.tokenizer.common;

/**
 *
 * @author Keith Flanagan
 */
public class TokenJobResult
{
  private TokenJobDescription job;
  
  private int totalProteinsProcessed;
  private int totalTokensGenerated;
  
  private double workTimeSeconds;
  private double proteinsPerSecond;
  private double tokensPerSecond;

  public TokenJobDescription getJob() {
    return job;
  }

  public void setJob(TokenJobDescription job) {
    this.job = job;
  }

  public int getTotalProteinsProcessed() {
    return totalProteinsProcessed;
  }

  public void setTotalProteinsProcessed(int totalProteinsProcessed) {
    this.totalProteinsProcessed = totalProteinsProcessed;
  }

  public int getTotalTokensGenerated() {
    return totalTokensGenerated;
  }

  public void setTotalTokensGenerated(int totalTokensGenerated) {
    this.totalTokensGenerated = totalTokensGenerated;
  }

  public double getWorkTimeSeconds() {
    return workTimeSeconds;
  }

  public void setWorkTimeSeconds(double workTimeSeconds) {
    this.workTimeSeconds = workTimeSeconds;
  }

  public double getProteinsPerSecond() {
    return proteinsPerSecond;
  }

  public void setProteinsPerSecond(double proteinsPerSecond) {
    this.proteinsPerSecond = proteinsPerSecond;
  }

  public double getTokensPerSecond() {
    return tokensPerSecond;
  }

  public void setTokensPerSecond(double tokensPerSecond) {
    this.tokensPerSecond = tokensPerSecond;
  }
}
