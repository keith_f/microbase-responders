/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: 07-Aug-2012, 17:29:39
 */

package com.microbasecloud.hmmer;

import com.torrenttamer.hibernate.HibernateUtilException;
import com.torrenttamer.hibernate.SessionProvider;
import com.torrenttamer.util.UidGenerator;
import java.io.*;
import java.util.HashSet;
import java.util.Set;
import java.util.StringTokenizer;
import org.hibernate.Session;

/**
 * A file parser capable of reading HMMER-3.X output reports that are generated
 * with the --tblout option. eg:
 * 
 * hmmsearch --tblout report.txt TstPart1.hmm COLSplit.fasta
 * 
 * @author Keith Flanagan
 */
public class HmmerTblOutParser
{
  public HmmerTblOutParser()
  {
    
  }
  
  /**
   * Use this method for testing the parser from the command line
   */
  public static void main(String[] args) 
      throws HmmerParserException, HibernateUtilException
  {
    if (args.length != 1)
    {
      System.out.println("USAGE: <hmmer --tblout formatted file>");
      System.exit(1);
    }
    String name = args[0];
    
    File file = new File(name);
    
    HmmerTblOutParser parser = new HmmerTblOutParser();
    Set<HmmerHit> hits = parser.parseFromFile(file);
    
    System.out.println("Hits found: "+hits.size());
    
    System.out.println("Going to upload hits to specified database");
    SessionProvider hmmerSessionProvider = 
        new SessionProvider("/HmmerResponder-hibernate.cfg.xml");
          //new SessionProvider(Constants.DEFAULT_HMM_DB_RESOURCE);
 
    /*
     * Create a parent report object. Since we're not running this through
     * Microbase, just create a partial report object containing a unique ID
     * and a filename.
     */
    HmmerReport report = new HmmerReport();
    report.setGuid(UidGenerator.generateUid()); //Unique ID for this HMMER report
    report.setSourceFileName(file.getName()); //Record name of result file
    
    //Associate all this parsed hits with this report
    for (HmmerHit hit : hits)
    {
      hit.setReport(report);
    }
    
    /*
     * Store report and all associated HmmerHit objects to the database.
     */
    Session session = null;
    try
    {
      session = hmmerSessionProvider.getWriteableSession();
      session.beginTransaction();
      HmmerHitDAO dao = new HmmerHitDAO();
      dao.store(session, hits); //Stores the HmmerHit instances as well as the HmmerReport
      System.out.println("Committing...");
      session.getTransaction().commit();
      System.out.println("Committed.");
    }
    catch(Exception e)
    {
      SessionProvider.silentRollback(session);
      throw new HmmerParserException("Failed to save "
          + hits.size()+" hits to the DB", e);
    }
    finally
    {
      SessionProvider.silentClose(session);
    }

    System.out.println("Done.");
  }
  
  public Set<HmmerHit> parseFromFile(File hmmerReport) 
      throws HmmerParserException
  {
    try
    {
      FileInputStream fis = new FileInputStream(hmmerReport);
      return parse(fis);
    }
    catch(Exception e)
    {
      throw new HmmerParserException(
          "Failed to parse HMMER output from file: "+hmmerReport.getAbsolutePath(), e);
    }
  }
  
  public Set<HmmerHit> parse(InputStream is) throws HmmerParserException
  {
    try
    {
      Set<HmmerHit> hits = new HashSet<HmmerHit>();
      BufferedReader br = new BufferedReader(new InputStreamReader(is));
      String line;
      while ((line = br.readLine()) != null)
      {
        if (line.startsWith("#"))
        {
          continue; //Skip comment lines
        }
        else if (line.trim().length() == 0)
        {
          continue; //Skip blank lines
        }
        HmmerHit hit = parseHitLine(line);
        hits.add(hit);
      }
      return hits;
    }
    catch(Exception e)
    {
      throw new HmmerParserException("Failed to parse content", e);
    }
  }
  
  private HmmerHit parseHitLine(String line)
  {
    StringTokenizer st = new StringTokenizer(line, " ");
    int count = st.countTokens();
//    System.out.println("Found "+count+" tokens");
    
    String targetName = st.nextToken();
    String targetAcc = st.nextToken();
    String queryName = st.nextToken();
    String queryAcc = st.nextToken();
    double fullSeqEVal = Double.parseDouble(st.nextToken());
    double fullSeqScore = Double.parseDouble(st.nextToken());
    double fullSeqBias = Double.parseDouble(st.nextToken());
    
    double best1DomSeqEVal = Double.parseDouble(st.nextToken());
    double best1DomSeqScore = Double.parseDouble(st.nextToken());
    double best1DomSeqBias = Double.parseDouble(st.nextToken());
    
    double domNumExp = Double.parseDouble(st.nextToken());
    int domNumReg = Integer.parseInt(st.nextToken());
    int domNumClu = Integer.parseInt(st.nextToken());
    int domNumOv = Integer.parseInt(st.nextToken());
    int domNumEnv = Integer.parseInt(st.nextToken());
    int domNumDom = Integer.parseInt(st.nextToken());
    int domNumRep = Integer.parseInt(st.nextToken());
    int domNumInc = Integer.parseInt(st.nextToken());
    
    StringBuilder descr = new StringBuilder();
    while(st.hasMoreTokens())
    {
      descr.append(st.nextToken());
      if (st.hasMoreTokens())
      {
        descr.append(" ");
      }
    }
    
    HmmerHit hit = new HmmerHit();
    hit.setTargetName(targetName);
    hit.setTargetAcc(targetAcc);
    hit.setQueryName(queryName);
    hit.setQueryAcc(queryAcc);
    hit.setFullSeqEVal(fullSeqEVal);
    hit.setFullSeqScore(fullSeqScore);
    hit.setFullSeqBias(fullSeqBias);
    
    hit.setBest1DomSeqEVal(best1DomSeqEVal);
    hit.setBest1DomSeqScore(best1DomSeqScore);
    hit.setBest1DomSeqBias(best1DomSeqBias);
    
    hit.setDomNumExp(domNumExp);
    hit.setDomNumReg(domNumReg);
    hit.setDomNumClu(domNumClu);
    hit.setDomNumOv(domNumOv);
    hit.setDomNumEnv(domNumEnv);
    hit.setDomNumDom(domNumDom);
    hit.setDomNumRep(domNumRep);
    hit.setDomNumInc(domNumInc);
    
    hit.setDescr(descr.toString());
    
    return hit;
  }
}
