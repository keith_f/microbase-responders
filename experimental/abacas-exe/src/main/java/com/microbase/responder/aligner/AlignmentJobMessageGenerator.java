/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.microbase.responder.aligner;

import com.microbase.responder.abacascommon.AlignmentJobDescription;
import com.microbasecloud.shell.messageutil.ShellMsgGenerator;
import com.microbasecloud.shell.messageutil.ShellMsgGeneratorException;
import java.util.HashMap;
import java.util.Map;
import org.codehaus.jackson.map.ObjectMapper;
import uk.org.microbase.filesystem.spi.MBFile;

/**
 *
 * @author prisni
 */
public class AlignmentJobMessageGenerator implements ShellMsgGenerator<AlignmentJobDescription> {

    @Override
    public String getMessageProcessorClassname() {
        return AbacasResponder.class.getName();
    }

    @Override
    public Map<String, String> getReqPropertyNames() {
        Map<String, String> nameToDesc = new HashMap();
       
    nameToDesc.put("query.fasta.bucket", "MBFS bucket of the query FASTA file");
    nameToDesc.put("query.fasta.path", "Path to the query FASTA file");
    nameToDesc.put("query.fasta.filename", "Filename of the query FASTA file");
    
    nameToDesc.put("reference.fasta.bucket", "MBFS bucket of the reference FASTA file");
    nameToDesc.put("reference.fasta.path", "Path to the reference FASTA file");
    nameToDesc.put("reference.fasta.filename", "Filename of the reference FASTA file"); 
    
    return nameToDesc;
    }

    @Override
    public String generateJson(Map<String, String> properties) throws ShellMsgGeneratorException {
        try {
      AlignmentJobDescription bean = generateMsgBean(properties);

      // Serialise message content to JSON
      ObjectMapper mapper = new ObjectMapper();
      String json = mapper.writeValueAsString(bean);
      return json;
    }
    catch(Exception e) {
      throw new ShellMsgGeneratorException(
              "Failed to generate and serialise a message content bean "
              + "from properties: "+properties, e);
    }
    }

    @Override
    public AlignmentJobDescription generateMsgBean(Map<String, String> properties) throws ShellMsgGeneratorException {
         try {
      AlignmentJobDescription bean = new AlignmentJobDescription();
      
      bean.setQueryFile(new MBFile(properties.get("query.fasta.bucket"), 
              properties.get("query.fasta.path"),
              properties.get("query.fasta.filename")));
      bean.setQueryFile(new MBFile(properties.get("reference.fasta.bucket"), 
              properties.get("reference.fasta.path"),
              properties.get("reference.fasta.filename")));
      
      return bean;
    }
    catch(Exception e) {
      throw new ShellMsgGeneratorException(
              "Failed to generate message content bean "
              + "from properties: "+properties, e);
    }
  }
    
}
