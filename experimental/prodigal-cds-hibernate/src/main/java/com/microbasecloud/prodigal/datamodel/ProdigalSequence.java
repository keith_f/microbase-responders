/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.microbasecloud.prodigal.datamodel;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author prisni
 */
@Entity
@Table (name = "prodigal_sequence")
public class ProdigalSequence implements Serializable {
   @Id
  @GeneratedValue(strategy= GenerationType.AUTO)
  private Long id;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }
   
    
    private int seqNum;
    private int seqLength;
    private long giNum;
    private String gbkNum;
//    private String species;
//    private String contigNum;
    private String prodigalVer;
    private String runType;
    private String model;
    private double seqGCcontent;
    private int transTable; //This should be a foreign key to a table of translation tables
    private boolean usesSd;

     @ManyToOne(cascade = CascadeType.ALL, optional = false )
     private ProdigalReport report;
    
    
    @Override
    public String toString() {
        return "ProdigalSequence{" + "seqNum=" + seqNum + ", seqLength=" +
                seqLength + ", giNum=" + giNum + ", gbkNum=" + gbkNum +
                ", prodigalVer=" + prodigalVer + ", runType=" + runType + 
                ", model=" + model + ", seqGCcontent=" + seqGCcontent + 
                ", transTable=" + transTable + ", usesSd=" + usesSd + '}';
    }

    public int getSeqNum() {
        return seqNum;
    }

    public void setSeqNum(int seqNum) {
        this.seqNum = seqNum;
    }

    public int getSeqLength() {
        return seqLength;
    }

    public void setSeqLength(int seqLength) {
        this.seqLength = seqLength;
    }

    public long getGiNum() {
        return giNum;
    }

    public void setGiNum(long giNum) {
        this.giNum = giNum;
    }

    public String getGbkNum() {
        return gbkNum;
    }

    public void setGbkNum(String gbkNum) {
        this.gbkNum = gbkNum;
    }

//    public String getSpecies() {
//        return species;
//    }
//
//    public void setSpecies(String species) {
//        this.species = species;
//    }
//
//    public String getContigNum() {
//        return contigNum;
//    }
//
//    public void setContigNum(String contigNum) {
//        this.contigNum = contigNum;
//    }

    public String getProdigalVer() {
        return prodigalVer;
    }

    public void setProdigalVer(String prodigalVer) {
        this.prodigalVer = prodigalVer;
    }

    public String getRunType() {
        return runType;
    }

    public void setRunType(String runType) {
        this.runType = runType;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public double getSeqGCcontent() {
        return seqGCcontent;
    }

    public void setSeqGCcontent(double seqGCcontent) {
        this.seqGCcontent = seqGCcontent;
    }

    public int getTransTable() {
        return transTable;
    }

    public void setTransTable(int transTable) {
        this.transTable = transTable;
    }

    public boolean isUsesSd() {
        return usesSd;
    }

    public void setUsesSd(boolean usesSd) {
        this.usesSd = usesSd;
    }

    public ProdigalReport getReport() {
        return report;
    }

    public void setReport(ProdigalReport report) {
        this.report = report;
    }
    
}
