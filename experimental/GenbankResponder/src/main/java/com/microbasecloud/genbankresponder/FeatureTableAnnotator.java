/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.microbasecloud.genbankresponder;

import java.util.Map;

/**
 *
 * @author prisni
 */
public interface FeatureTableAnnotator {
    
    /**
     *
     * @param featureId
     * @return
     * @throws FeatureTableAnnotatorException
     */
    public Map<String, String> getAnnotations(String featureId) throws FeatureTableAnnotatorException;
    
      /**
     *
     * @throws FeatureTableAnnotatorException
     * @throws HibernateUtilException
     */
    
    public void initialize() throws FeatureTableAnnotatorException;
    
    
    
}
