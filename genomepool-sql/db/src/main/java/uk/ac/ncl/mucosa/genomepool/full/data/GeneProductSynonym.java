/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */

package uk.ac.ncl.mucosa.genomepool.full.data;

import java.io.Serializable;

/**
 *
 * @author Sirintra Nakjang
 */
public class GeneProductSynonym 
        implements Serializable
{
  //private GeneProduct geneProduct;
  private String geneProductGuid;

  private String synonym;

  private String product;

  private String note;

  public GeneProductSynonym()
  {
  }

//  public GeneProduct getGeneProduct()
//  {
//    return geneProduct;
//  }
//
//  public void setGeneProduct(GeneProduct geneProduct)
//  {
//    this.geneProduct = geneProduct;
//  }


  public String getNote()
  {
    return note;
  }

  public void setNote(String note)
  {
    this.note = note;
  }

  public String getProduct()
  {
    return product;
  }

  public void setProduct(String product)
  {
    this.product = product;
  }

  public String getSynonym()
  {
    return synonym;
  }

  public void setSynonym(String synonym)
  {
    this.synonym = synonym;
  }

  public String getGeneProductGuid()
  {
    return geneProductGuid;
  }

  public void setGeneProductGuid(String geneProductGuid)
  {
    this.geneProductGuid = geneProductGuid;
  }

  
}
