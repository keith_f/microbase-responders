/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.microbase.responder.prodigalcdscommon;

import java.io.Serializable;
import uk.org.microbase.filesystem.spi.MBFile;

/**
 *
 * @author prisni
 */
public class ProdigalJobDescription 
implements Serializable
{
    
  private MBFile queryFastaFile;
  private MBFile databaseZipFile;
  private String databaseName;
  
  public enum PredictionType {
    cds, translations;
  };
  
  private PredictionType predictionType;

    public PredictionType getPredictionType() {
        return predictionType;
    }

    public void setPredictionType(PredictionType predictionType) {
        this.predictionType = predictionType;
    }

  public ProdigalJobDescription()
  {
  }

   public MBFile getDatabaseZipFile()
  {
    return databaseZipFile;
  }

  public void setDatabaseZipFile(MBFile databaseZipFile)
  {
    this.databaseZipFile = databaseZipFile;
  }

  public String getDatabaseName()
  {
    return databaseName;
  }

  public void setDatabaseName(String databaseName)
  {
    this.databaseName = databaseName;
  }

  public MBFile getQueryFastaFile()
  {
    return queryFastaFile;
  }

  public void setQueryFastaFile(MBFile queryFastaFile)
  {
    this.queryFastaFile = queryFastaFile;
  }

}

