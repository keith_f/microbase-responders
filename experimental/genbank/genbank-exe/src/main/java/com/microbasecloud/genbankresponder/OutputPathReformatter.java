/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.microbasecloud.genbankresponder;

import uk.org.microbase.dist.processes.ProcessInfo;
import uk.org.microbase.dist.responder.ResponderInfo;
import uk.org.microbase.filesystem.spi.MBFile;
import uk.org.microbase.notification.data.Message;
import uk.org.microbase.runtime.ClientRuntime;

/**
 *
 * @author prisni
 */
public interface OutputPathReformatter {

    public String getFormattedBucketName();

    public String getFormattedPath();

    public String getFormattedFilename();

    public MBFile getFormattedRemoteFile();

    public void setRuntime(ClientRuntime runtime);

    public void setProcessInfo(ProcessInfo processInfo);

    public void setResponderInfo(ResponderInfo responderInfo);

    public void setMessage(Message message);

    public void configure(ClientRuntime runtime, ResponderInfo responderInfo,
            ProcessInfo processInfo, Message message);
}
