/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package com.microbasecloud.tokenizer.db.data;

/**
 *
 * @author Keith Flanagan
 */
public class ProteinNode
{
  private int taxonId;
  private String organismGuid;
  private String fragmentGuid;
  private String productGi;
  private String sequence;
  private String description;

  public ProteinNode() {
  }

  @Override
  public String toString() {
    return "ProteinNode{" + "taxonId=" + taxonId + ", organismGuid=" + organismGuid 
            + ", fragmentGuid=" + fragmentGuid + ", productGi=" + productGi 
            + ", sequence=" + sequence + ", description=" + description + '}';
  }
  
  

  public String getSequence() {
    return sequence;
  }

  public void setSequence(String sequence) {
    this.sequence = sequence;
  }

  public String getProductGi() {
    return productGi;
  }

  public void setProductGi(String productGi) {
    this.productGi = productGi;
  }

  public int getTaxonId() {
    return taxonId;
  }

  public void setTaxonId(int taxonId) {
    this.taxonId = taxonId;
  }

  public String getDescription() {
    return description;
  }

  public void setDescription(String description) {
    this.description = description;
  }

  public String getFragmentGuid() {
    return fragmentGuid;
  }

  public void setFragmentGuid(String fragmentGuid) {
    this.fragmentGuid = fragmentGuid;
  }

  public String getOrganismGuid() {
    return organismGuid;
  }

  public void setOrganismGuid(String organismGuid) {
    this.organismGuid = organismGuid;
  }
  
}
