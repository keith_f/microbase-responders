/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.microbasecloud.prodigal.dataparser;

import com.microbasecloud.prodigal.datamodel.ProdigalCDS;
import com.microbasecloud.prodigal.datamodel.ProdigalSequence;
import java.io.*;
import java.util.*;

public class ProdigalStdOutputFileParser {

    public static void main(String[] args) throws FileNotFoundException,
            ProdigalParserException {
//        try{
//            FileInputStream fstream = new FileInputStream("/home/prisni/Downloads/new_outputs.txt");
//            DataInputStream in = new DataInputStream(fstream);
//            BufferedReader br = new BufferedReader(new InputStreamReader(in));
//            String strLine;
//            
//            while((strLine = br.readLine()) != null){
//                System.out.println(strLine);
//            }
//            in.close();
//        } catch(Exception e){
//            System.err.println("Error: "+e.getMessage());
//        }
//        

        //read the file and its content
        
        File file = new File("/home/prisni/new_outputs.txt");
        System.out.println("Found your file...hooray!\n");

        try {

            System.out.println("Reading file\n");


            List<ProdigalSequence> sequences = new ArrayList<ProdigalSequence>();
            Map<String, ProdigalCDS> cdss = new HashMap<String, ProdigalCDS>();
            parseCDSFile(file, sequences, cdss);
            

        } catch (FileNotFoundException e) {
            System.out.printf("ERROR no file found\n");
        }
    }

    private static void parseCDSFile(File cdsFile,
            List<ProdigalSequence> sequences, Map<String,
                    ProdigalCDS> cdss)
            throws FileNotFoundException, ProdigalParserException {
        ProdigalOutputFileDetailExtractor proD = 
                new ProdigalOutputFileDetailExtractor();
        Scanner sc = new Scanner(cdsFile);
        System.out.println("Reading file\n");

        ProdigalSequence currentSequence = null;
        ProdigalCDS currentCds = null;

        while (sc.hasNextLine()) {
            String line = sc.nextLine();

            if (proD.isCurrentLineHeader(line)) {
                currentSequence = new ProdigalSequence();
                sequences.add(currentSequence);
                proD.extractHeaderInfo(currentSequence, line);
            } else if (proD.isCurrentLineStrandInfo(line.replaceAll("\\s", ""))) {
                currentCds = new ProdigalCDS();
                currentCds.setParentSequence(currentSequence);
                proD.extractStrandInfo(currentCds, line.replaceAll("\\s", ""));
            } else if (proD.isCurrentLineCDSInfo(line.replaceAll("\\s", ""))) {
                proD.extractCDSInfo(currentCds, line.replaceAll("\\s", ""));
                cdss.put(currentCds.getCdsID(), currentCds);
            }

        }

        sc.close();

        System.out.println("Found " + sequences.size() + " sequence definitions");
        System.out.println("Found " + cdss.size() + " CDS entries");
    }
  
}
