/*
 * Copyright 2011 Keith Flanagan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * File created: 20-Sep-2010, 17:48:17
 */

package uk.ac.ncl.mucosa.genomepool.cli;

import java.util.logging.Logger;

/**
 * This program connects to a GenomePool instance and instructs it to
 * manually submit .faa files existing in the Microbase FS for parsing.
 *
 * @author Keith Flanagan
 */
public class ManualSubmitFaa
{
  private static final Logger logger =
      Logger.getLogger(ManualSubmitFaa.class.getName());

  //TODO - base on ManualSubmitGbk
}
