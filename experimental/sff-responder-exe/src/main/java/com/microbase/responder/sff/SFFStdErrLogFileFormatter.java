/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.microbase.responder.sff;

import com.microbase.responder.sffcommon.SFFJobDescription;
import uk.org.microbase.filesystem.spi.MBFile;
import uk.org.microbase.responder.util.AbstractOutputPathReformatter;

/**
 *
 * @author prisni
 */
public class SFFStdErrLogFileFormatter extends AbstractOutputPathReformatter
    implements SFFOutputPathReformatter
{
  
  private SFFJobDescription jobDescription;
  
  @Override
  public String getFormattedBucketName() {
    return responderInfo.getLogDestinationBucket();
  }

  @Override
  public String getFormattedPath() {
    StringBuilder path = new StringBuilder(responderInfo.getLogDestinationBasePath());
    path.append("/").append(getFormattedFilename());
    path.append("/").append(message.getGuid());
    path.append("/").append(processInfo.getWorkStartedAtMs());
    return path.toString();
  }

  @Override
  public String getFormattedFilename() {
    StringBuilder resultFilename = new StringBuilder();
    resultFilename.append(jobDescription.getSffZipFile().getName())
        .append(".stderr");
    return resultFilename.toString();
  }
  
  @Override
  public MBFile getFormattedRemoteFile() {
    MBFile file = new MBFile(getFormattedBucketName(), getFormattedPath(), getFormattedFilename());
    return file;
  }
  

  public SFFJobDescription getJobDescription() {
    return jobDescription;
  }

 
  public void setJobDescription(SFFJobDescription jobDescription) {
    this.jobDescription = jobDescription;
  }

}
