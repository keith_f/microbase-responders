/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: 06-Oct-2012, 21:44:44
 */

package com.microbasecloud.responders.filescanner;

import com.microbasecloud.filescanner.common.Constants;
import com.microbasecloud.filescanner.common.FileStatus;
import com.microbasecloud.responders.filescanner.notifications.FileStateChangedNotification;
import com.microbasecloud.responders.filescanner.data.edges.FileFoundBy;
import com.microbasecloud.responders.filescanner.data.edges.HasPreviousReport;
import com.microbasecloud.responders.filescanner.data.nodes.FileNode;
import com.microbasecloud.responders.filescanner.data.nodes.ScanReport;
import com.microbasecloud.responders.filescanner.data.nodes.ScanReport.FileEntry;
import com.microbasecloud.responders.filescanner.data.nodes.ScanReportDAO;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.Mongo;
import com.torrenttamer.mongodb.MongoDbFactory;
import com.torrenttamer.mongodb.dbobject.DbObjectMarshaller;
import com.torrenttamer.util.UidGenerator;
import java.util.*;
import java.util.logging.Level;
import java.util.logging.Logger;
import uk.ac.ncl.aries.entanglement.ObjectMarshallerFactory;
import uk.ac.ncl.aries.entanglement.graph.EdgeDAO;
import uk.ac.ncl.aries.entanglement.graph.GraphDAOFactory;
import uk.ac.ncl.aries.entanglement.graph.NodeDAO;
import uk.ac.ncl.aries.entanglement.player.GraphCheckoutNamingScheme;
import uk.ac.ncl.aries.entanglement.player.LogPlayer;
import uk.ac.ncl.aries.entanglement.player.LogPlayerMongoDbImpl;
import uk.ac.ncl.aries.entanglement.revlog.RevisionLog;
import uk.ac.ncl.aries.entanglement.revlog.RevisionLogDirectToMongoDbImpl;
import uk.ac.ncl.aries.entanglement.revlog.RevisionLogException;
import uk.ac.ncl.aries.entanglement.revlog.commands.CreateEdge;
import uk.ac.ncl.aries.entanglement.revlog.commands.CreateNode;
import uk.ac.ncl.aries.entanglement.revlog.commands.CreateNodeIfNotExists;
import uk.ac.ncl.aries.entanglement.revlog.commands.GraphOperation;
import uk.ac.ncl.aries.entanglement.util.TxnUtils;
import uk.ac.ncl.aries.entanglement.util.experimental.GraphOpPostCommitPlayer;
import uk.org.microbase.filesystem.spi.FileMetaData;
import uk.org.microbase.notification.data.Message;
import uk.org.microbase.notification.spi.NotificationException;
import uk.org.microbase.runtime.ClientRuntime;
import uk.org.microbase.util.serialization.JsonSerializer;
import uk.org.microbase.util.serialization.MbSerializerException;

/**
 *
 * @author Keith Flanagan
 */
public class MbfsScannerWorker
    implements Runnable
{
  private static final Logger logger =
      Logger.getLogger(MbfsScannerWorker.class.getName());
  
  private static final String PUBLISHER = FileScannerResponder.class.getSimpleName();
  
  private final ClientRuntime runtime;
  private final String hostname;
  private final String database;
  private final String graphName;
  private final String graphBranch;
  private final Target target;
  
  private final DbObjectMarshaller marshaller;
  
  
  public MbfsScannerWorker(ClientRuntime runtime, 
          String hostname, String database, String graphName, String graphBranch,
          Target target)
  {
    this.runtime = runtime;
    this.hostname = hostname;
    this.database = database;
    this.graphName = graphName;
    this.graphBranch = graphBranch;
    this.target = target;
    
    marshaller = ObjectMarshallerFactory.create(runtime.getClassLoader());
  }
  
  /**
   * Utility method to compare two dates down to an accuracy of 1 second.
   * This is needed because whilst Java Dates typically measure milliseconds,
   * the SQL temporal type 'timestamp' uses second-level accuracy.
   * This method returns true if the dates are equal to the nearset second.
   * 
   * @param first
   * @param second
   * @return 
   */
  private boolean datesEqual(Date first, Date second)
  {
    logger.info("First timestamp: "+first.getTime()
        +", second timestamp: "+second.getTime());
    GregorianCalendar firstCal = new GregorianCalendar();
    firstCal.setTime(first);
    
    GregorianCalendar secondCal = new GregorianCalendar();
    secondCal.setTime(second);
    
    firstCal.set(Calendar.MILLISECOND, 0);
    secondCal.set(Calendar.MILLISECOND, 0);
    
//    logger.info("First: "+firstCal.toString()+", second: "+secondCal.toString());
    return firstCal.equals(secondCal);
  }
   
  @Override
  public void run()
  {
//    Session session = null;
    Mongo mongo = null;
    DB db;
    RevisionLog revLog = null;
    NodeDAO nodeDao;
    EdgeDAO edgeDao;
    
    String txnId = null;
    int txnSubmitId = 0;
    List<GraphOperation> graphOps = new ArrayList<>();
    
    try
    {      
      logger.log(Level.INFO,
          "Performing file scan of remote filesystem: {0}", 
          new Object[]{target.getTargetDir()});
      
      // Perform scan of requested target - ignore files not of interest.
      Set<FileMetaData> files = runtime.
          getMicrobaseFS().listRemoteFileMetaData(target.getTargetDir());
      for (Iterator<FileMetaData> itr = files.iterator(); itr.hasNext();) {
        FileMetaData file = itr.next();
//        logger.log(Level.INFO, "Found remote file: {0}; {1}; {2}", 
//            new Object[]{file.getBucket(), file.getPath(), file.getName()});
        boolean matched = false;
        for (String ext : target.getExtensions()) {
          if (file.getLocation().getName().endsWith(ext)) {
            matched = true;
//            logger.info("Remote matched extension: "+ext);
            break;
          }
        }
        if (!matched) {
          itr.remove();
        }
      }
     
      
      logger.info("Obtaining connection to database");
      MongoDbFactory dbFactory = new MongoDbFactory(hostname, database);
      mongo = dbFactory.createMongoConnection();
      db = mongo.getDB(database);

      revLog = new RevisionLogDirectToMongoDbImpl(runtime.getClassLoader(), mongo, db);
      GraphCheckoutNamingScheme collectionNamer = new GraphCheckoutNamingScheme(graphName, graphBranch);
      DBCollection nodeCol = db.getCollection(collectionNamer.getNodeCollectionName());
      DBCollection edgeCol = db.getCollection(collectionNamer.getEdgeCollectionName());
      nodeDao = GraphDAOFactory.createDefaultNodeDAO(runtime.getClassLoader(), mongo, db, nodeCol, edgeCol);
      edgeDao = GraphDAOFactory.createDefaultEdgeDAO(runtime.getClassLoader(), mongo, db, nodeCol, edgeCol);
      
      LogPlayer player = new LogPlayerMongoDbImpl(
              runtime.getClassLoader(), marshaller, graphName, graphBranch, 
              revLog, nodeDao, edgeDao);
      GraphOpPostCommitPlayer autoUpdater = new GraphOpPostCommitPlayer(player);
      revLog.addListener(autoUpdater);
      
      ScanReportDAO reportDao = new ScanReportDAO(runtime.getClassLoader(), mongo, db, nodeCol);
      
      
      // Obtain the previous scan report for this target (is there is one)
      logger.info("Obtaining previous scan report for this target (if present)");
      ScanReport previousReport = reportDao.selectLatestReportForTarget(target.getTargetName());
      
      // Create a new report object for the current scan
      ScanReport currentReport = new ScanReport();
      currentReport.setUid(UidGenerator.generateUid());
      currentReport.setTargetName(target.getTargetName());
      currentReport.setBucket(target.getTargetDir().getBucket());
      currentReport.setFilePath(target.getTargetDir().getPath());
      currentReport.setScanTimestamp(new Date(System.currentTimeMillis()));
      if (previousReport != null) {
        currentReport.setPreviousReportUid(previousReport.getUid());
      }
      
      // Obtain previous file entries (if there were any)
      logger.info("Obtaining previous file entries (if any)");
      List<FileEntry> prevFileEntries = new LinkedList<>();
      if (previousReport != null) {
//        prevFileEntries.addAll(fileDao.selectFilesForReport(session, previousReport));
        prevFileEntries.addAll(previousReport.getDeletedFiles());
        prevFileEntries.addAll(previousReport.getNewFiles());
        prevFileEntries.addAll(previousReport.getUnchangedFiles());
        prevFileEntries.addAll(previousReport.getUpdatedFiles());
      }
      Map<String, FileEntry> nameToPrevFileEntry = new HashMap<>();
      for (FileEntry dbEntry : prevFileEntries) {
        nameToPrevFileEntry.put(dbEntry.getFilename(), dbEntry);
      }
      logger.log(Level.INFO, "There were {0} file entries from a previous scan",
          prevFileEntries.size());
      
      // Match current file listing against previous scan
      Set<FileEntry> allFiles = new HashSet<>();
      Set<FileEntry> newFiles = new HashSet<>();
      Set<FileEntry> updatedFiles = new HashSet<>();
      Set<FileEntry> deletedFiles = new HashSet<>();
      Set<FileEntry> unchangedFiles = new HashSet<>();
      Set<String> currentScanFilenames = new HashSet<>();
      for (FileMetaData file : files) 
      {
        logger.info("Classifying file: "+file);
        currentScanFilenames.add(file.getLocation().getName());
        System.out.println(file.getLocation().getName());
        FileEntry prevFileEntry = nameToPrevFileEntry.get(file.getLocation().getName());

        FileEntry newFileEntry = new FileEntry(
            target.getTargetDir().getBucket(), target.getTargetDir().getPath(), file.getLocation().getName());
        newFileEntry.setModified(file.getLastModified());
//        newFileEntry.setReport(currentReport);
        newFileEntry.setFileLength(file.getLength());
        allFiles.add(newFileEntry);
        
        if (prevFileEntry == null) {
          // This file wasn't in the previous scan
          logger.info("File: "+target.getTargetDir()+" is new wrt last scan.");
          newFileEntry.setStatus(FileStatus.NEW);
          newFiles.add(newFileEntry);
        } else if (!datesEqual(prevFileEntry.getModified(), file.getLastModified())) {
          // Exists in the previous report but modification time has changed
          logger.info("File: "+target.getTargetDir()
              +" is known, but the modification time has "
              + "changed: previous entry: "+prevFileEntry.getModified()
              + ", now: "+file.getLastModified());
          newFileEntry.setStatus(FileStatus.UPDATED);
          updatedFiles.add(newFileEntry);
        } else if (prevFileEntry.getFileLength() !=  file.getLength()) {
          // Exists in the previous report but file size has changed
          logger.info("File: "+target.getTargetDir()+" is known, but the file size has changed.");
          newFileEntry.setStatus(FileStatus.UPDATED);
          updatedFiles.add(newFileEntry);          
        } else {
          logger.info("File: "+target.getTargetDir()+" is known and has not changed.");
          newFileEntry.setStatus(FileStatus.UNCHANGED);
          unchangedFiles.add(newFileEntry);
        }
      }
      
      // Find deleted files
      Set<String> deletedFilenames = new HashSet<>(nameToPrevFileEntry.keySet());
      deletedFilenames.removeAll(currentScanFilenames);
      
      /*
       * Deleted files are those that did occur in the previous scan, but don't
       * now exist in the current scan. OR, they did occur in the previous scan
       * but were marked as 'DELETED'.
       */
      for (String deletedName : deletedFilenames) {
        FileEntry prevEntry = nameToPrevFileEntry.get(deletedName);
        if (prevEntry.getStatus().equals(FileStatus.DELETED)) {
          // This file 'existed' in the previous scan - but only as a DELETED event
          continue;
        }
        
        FileEntry newFileEntry = new FileEntry(
            target.getTargetDir().getBucket(), target.getTargetDir().getPath(), deletedName);
//        newFileEntry.setReport(currentReport);
        newFileEntry.setStatus(FileStatus.DELETED);
        deletedFiles.add(newFileEntry);
        allFiles.add(newFileEntry);
      }
      
      //Set report summary
      currentReport.setTotalFileStatusChanges(files.size() + deletedFiles.size());
      
      currentReport.setTotalNumFiles(files.size());
      currentReport.setNumNewFiles(newFiles.size());
      currentReport.setNumUpdatedFiles(updatedFiles.size());
      currentReport.setNumUnchangedFiles(unchangedFiles.size());
      currentReport.setNumDeletedFiles(deletedFiles.size());

      currentReport.setNewFiles(newFiles);
      currentReport.setUpdatedFiles(updatedFiles);
      currentReport.setUnchangedFiles(unchangedFiles);
      currentReport.setDeletedFiles(deletedFiles);
      
      // Write objects to database (graph revision log)
      graphOps.add(new CreateNodeIfNotExists(marshaller.serialize(currentReport)));
      if (previousReport != null) {
        HasPreviousReport hasPrevReport = new HasPreviousReport(currentReport, previousReport);
        graphOps.add(new CreateEdge(marshaller.serialize(hasPrevReport)));
      }
      /*
       * For each new file, create a node. Note that, if a file is deleted
       * and then another file of the same name and location is added in future,
       * the system will treat the new file as a totally new file - it will get
       * a new graph node, and any subsequent analyses will hang off that node.
       */
      //Map stores FileEntry obtained from scan and maps it to graph node ID
      Map<FileEntry, String> fileEntryToNode = new HashMap<>();
      for (FileEntry newFileEntry : newFiles) {
        FileNode newFileNode = new FileNode(newFileEntry.getBucket(), 
                newFileEntry.getFilePath(), newFileEntry.getFilename());
        newFileNode.setUid(UidGenerator.generateUid());
        newFileNode.setFileModifiedDate(newFileEntry.getModified());
        newFileNode.setFileLength(newFileEntry.getFileLength());
        graphOps.add(new CreateNode(marshaller.serialize(newFileNode)));
        
        FileFoundBy foundByEdge = new FileFoundBy(newFileNode, currentReport);
        graphOps.add(new CreateEdge(marshaller.serialize(foundByEdge)));
        
        fileEntryToNode.put(newFileEntry, newFileNode.getUid());
      }
      
      //revLog.submitRevision(graphName, graphBranch, txnId, txnSubmitId++, new TransactionBegin(txnId));
      txnId = TxnUtils.beginNewTransaction(revLog, graphName, graphBranch);
      revLog.submitRevisions(graphName, graphBranch, txnId, txnSubmitId++, graphOps);

      

      //Create notifications for each event type, if they are required.
      Set<Message> msgs = new HashSet<>();
      msgs.addAll(createIndividualFileNotifications(nameToPrevFileEntry, fileEntryToNode, currentReport, newFiles, target.getNewFileTopic()));
      msgs.addAll(createIndividualFileNotifications(nameToPrevFileEntry, fileEntryToNode, currentReport, updatedFiles, target.getUpdatedFileTopic()));
      msgs.addAll(createIndividualFileNotifications(nameToPrevFileEntry, fileEntryToNode, currentReport, unchangedFiles, target.getUnchangedFileTopic()));
      msgs.addAll(createIndividualFileNotifications(nameToPrevFileEntry, fileEntryToNode, currentReport, deletedFiles, target.getDeletedFileTopic()));
      if (target.getDigestTopic() != null) {
        Set<FileEntry> allExceptUnchanged = new HashSet<>(allFiles);
        allExceptUnchanged.removeAll(unchangedFiles);
        // Publish summary message if there is at least one file change event
        if (!allExceptUnchanged.isEmpty()) {
          msgs.add(createDigestMessage(currentReport, allExceptUnchanged, target.getDigestTopic()));
        }
      }
      
      runtime.getNotification().publish(msgs);
//      session.getTransaction().commit();
//      revLog.submitRevision(graphName, graphBranch, txnId, txnSubmitId++, new TransactionCommit(txnId));
      TxnUtils.commitTransaction(revLog, graphName, graphBranch, txnId);

      logger.info("Finished.");
    }
    catch(Exception e)
    {
      logger.info("File scanning failed of: "
          +target.getTargetDir().getBucket()+"; "+target.getTargetDir().getPath());
      e.printStackTrace();
      
      
//      SessionProvider.silentRollback(session);
      
      try {
        TxnUtils.rollbackTransaction(revLog, graphName, graphBranch, txnId);
      }
      catch(RevisionLogException e2) {
        logger.info("Also failed to roll back the transaction. This is a separate problem!");
        e2.printStackTrace();
      }

    }
    finally
    {
      if (mongo != null) {
       mongo.close();
      }
    }
  }
  
  
  private Set<Message> createIndividualFileNotifications(
      Map<String, FileEntry> nameToPrevFileEntry,
      Map<FileEntry, String> fileEntryToNode,
      ScanReport report, Set<FileEntry> entries, String topicGuid)
      throws NotificationException, MbSerializerException
  {
    
    Set<Message> msgs = new HashSet<>();
    if (topicGuid == null) {
      return msgs;
    }
    
    for (FileEntry entry : entries) {
      //This is the main content of the message
      FileStateChangedNotification stateChange = new FileStateChangedNotification();
      stateChange.setFileMetaData(new FileMetaData(
          entry.getBucket(), entry.getFilePath(), entry.getFilename(), 
          entry.getFileLength(), entry.getModified()));
      stateChange.setStatusChange(entry.getStatus());
      stateChange.setScanReportId(report.getUid());
      stateChange.setNodeUid(fileEntryToNode.get(entry));

      String newStateChangeJson = JsonSerializer.serializeToString(stateChange);

      Map<String, String> content = new HashMap<>();
      content.put(Constants.MSG_KEY_NEW_FILE_STATE, newStateChangeJson);
      
      // Also include the previous state info (if present)
      if (nameToPrevFileEntry.containsKey(entry.getFilename())) {
        FileEntry prevEntry = nameToPrevFileEntry.get(entry.getFilename());
        FileStateChangedNotification prevState = new FileStateChangedNotification();
        prevState.setFileMetaData(new FileMetaData(
            prevEntry.getBucket(), prevEntry.getFilePath(), prevEntry.getFilename(),
            prevEntry.getFileLength(), prevEntry.getModified()));
        prevState.setStatusChange(prevEntry.getStatus());
        prevState.setScanReportId(report.getUid());        
        
        String prevStateChangeJson = JsonSerializer.serializeToString(prevState);
        content.put(Constants.MSG_KEY_PREV_FILE_STATE, prevStateChangeJson);
      }

      Message msg = new Message();
      msg.setGuid(UidGenerator.generateUid());
      msg.setTopicGuid(topicGuid);
      msg.setPublisherGuid(PUBLISHER);
      msg.setContent(content);
      
      msgs.add(msg);
    }
    return msgs;
  }
  
  private Message createDigestMessage(ScanReport report, Set<FileEntry> allFiles, String topicGuid) 
      throws MbSerializerException
  {
    Map<String, String> content = new HashMap<>();
    
    Message msg = new Message();
    msg.setGuid(UidGenerator.generateUid());
    msg.setTopicGuid(topicGuid);
    msg.setPublisherGuid(PUBLISHER);
    msg.setContent(content);
    
    
    for (FileEntry entry : allFiles) {
      //This is the main content of the message
      FileStateChangedNotification stateChange = new FileStateChangedNotification();
      stateChange.setFileMetaData(new FileMetaData(
          entry.getBucket(), entry.getFilePath(), entry.getFilename(), 
          entry.getFileLength(), entry.getModified()));
      stateChange.setStatusChange(entry.getStatus());
      stateChange.setScanReportId(report.getUid());

      String stateChangeJson = JsonSerializer.serializeToString(stateChange);

      content.put(entry.getFilename(), stateChangeJson);      
    }
    return msg;
  } 
}
