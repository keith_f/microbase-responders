/*
 * Copyright 2010 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: 20-Sep-2010, 17:48:17
 */

package uk.ac.ncl.mucosa.genomepool.full.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Queries involving organisms stored in the GenomePool.
 * Data for the queries in this class actually comes from the genome_fragments
 * table.
 * @author Keith Flanagan
 */
public class OrganismDAO
{

  public OrganismDAO()
  {
  }


  public boolean exists(Connection txn, String organismGuid)
      throws SQLException
  {
    PreparedStatement stmt = null;
    try
    {
      stmt = txn.prepareStatement(
        "SELECT count(*) as count FROM genome_fragments WHERE organism_guid = ?");
      ResultSet rs = stmt.executeQuery();
      rs.next();
      return rs.getInt("count") > 0;
    }
    catch(Exception e)
    {
      throw new SQLException(
          "Failed to execute exists() for organism: "+organismGuid, e);
    }
    finally
    {
      stmt.close();
    }
  }


  public List<Integer> getTaxonIds(Connection txn)
      throws SQLException
  {
    PreparedStatement stmt = null;
    try
    {
      stmt = txn.prepareStatement(
        "SELECT DISTINCT taxon_id FROM genome_fragments");
      List<Integer> result = new ArrayList<Integer>();
      ResultSet rs = stmt.executeQuery();
      while(rs.next())
      {
        result.add(rs.getInt(1));
      }
      return result;
    }
    catch(Exception e)
    {
      throw new SQLException(
          "Failed to execute getTaxonIds()", e);
    }
    finally
    {
      stmt.close();
    }
  }

  public List<String> getOrganismNames(Connection txn)
      throws SQLException
  {
    PreparedStatement stmt = null;
    try
    {
      stmt = txn.prepareStatement(
        "SELECT DISTINCT organism_name FROM genome_fragments "
        + "ORDER BY organism_name");
      List<String> result = new ArrayList<String>();
      ResultSet rs = stmt.executeQuery();
      while(rs.next())
      {
        result.add(rs.getString(1));
      }
      return result;
    }
    catch(Exception e)
    {
      throw new SQLException(
          "Failed to execute getTaxonIds()", e);
    }
    finally
    {
      stmt.close();
    }
  }
  
  public List<String> getOrganismNames(Connection txn, int offset, int limit)
      throws SQLException
  {
    PreparedStatement stmt = null;
    try
    {
      stmt = txn.prepareStatement(
        "SELECT DISTINCT organism_name FROM genome_fragments "
        + "ORDER BY organism_name LIMIT ? OFFSET ?");
      stmt.setInt(1, limit);
      stmt.setInt(2, offset);
      List<String> result = new ArrayList<String>();
      ResultSet rs = stmt.executeQuery();
      while(rs.next())
      {
        result.add(rs.getString(1));
      }
      return result;
    }
    catch(Exception e)
    {
      throw new SQLException(
          "Failed to execute getOrganismNames()", e);
    }
    finally
    {
      stmt.close();
    }
  }
  
  
  /**
   * Returns a mapping of taxon ID -> organism name for all distinct 
   * organism names (also ordered by name to allow paging).
   * @param txn
   * @param offset
   * @param limit
   * @return
   * @throws SQLException 
   */
  public Map<Integer, String> getTaxonToOrganismNames(Connection txn, int offset, int limit)
      throws SQLException
  {
    PreparedStatement stmt = null;
    try
    {
      stmt = txn.prepareStatement(
        "SELECT DISTINCT taxon_id, organism_name FROM genome_fragments "
        + "ORDER BY organism_name LIMIT ? OFFSET ?");
      stmt.setInt(1, limit);
      stmt.setInt(2, offset);
      Map<Integer, String> result = new HashMap<Integer, String>();
      ResultSet rs = stmt.executeQuery();
      while(rs.next())
      {
        result.put(rs.getInt("taxon_id"), rs.getString("organism_name"));
      }
      return result;
    }
    catch(Exception e)
    {
      throw new SQLException(
          "Failed to execute getTaxonToOrganismNames()", e);
    }
    finally
    {
      stmt.close();
    }
  }
  
  public int countDistinctTaxonIds(Connection txn)
      throws SQLException
  {
    PreparedStatement stmt = null;
    try
    {
      stmt = txn.prepareStatement(
        "SELECT COUNT(DISTINCT taxon_id) as count FROM genome_fragments");
      ResultSet rs = stmt.executeQuery();
      rs.next();
      return rs.getInt("count");
    }
    catch(Exception e)
    {
      throw new SQLException(
          "Failed to execute countDistinctTaxonIds()", e);
    }
    finally
    {
      stmt.close();
    }
  }

}
