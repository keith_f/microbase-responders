/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.mucosa.genomepool.full.data;

import java.io.Serializable;

/**
 *
 * @author Sirintra Nakjang
 */
public class GeneSynonym
        implements Serializable
{
  public enum SynonymType
  {
    GENE_ID, LOCUS_TAG, GENE_NAME;
  }

  //private Gene gene;
  private String geneGuid;

  private SynonymType synonymType ;

  private String synonym ;

  public GeneSynonym()
  {
  }

//  public Gene getGene()
//  {
//    return gene;
//  }
//
//  public void setGene(Gene gene)
//  {
//    this.gene = gene;
//  }

  public SynonymType getSynonymType()
  {
    return synonymType;
  }

  public void setSynonymType(SynonymType synonymType)
  {
    this.synonymType = synonymType;
  }

  public String getSynonym()
  {
    return synonym;
  }

  public void setSynonym(String synonym)
  {
    this.synonym = synonym;
  }

  public String getGeneGuid()
  {
    return geneGuid;
  }

  public void setGeneGuid(String geneGuid)
  {
    this.geneGuid = geneGuid;
  }

  
}
