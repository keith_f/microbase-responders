/*
 * Copyright 2012 Keith Flanagan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package uk.ac.ncl.mucosa.genomepool.notification;

import uk.org.microbase.filesystem.spi.MBFile;

/**
 *
 * @author Keith Flanagan
 */
public class NewFastqFileNotification
{
  private String fastqFileNodeUid;
  private String fastqParserReportNodeUid;
  
  private MBFile parsedDnaFastaFile;
  private MBFile parsedProteinFastaFile;

  public NewFastqFileNotification() {
  }

  public String getFastqParserReportNodeUid() {
    return fastqParserReportNodeUid;
  }

  public void setFastqParserReportNodeUid(String fastqParserReportNodeUid) {
    this.fastqParserReportNodeUid = fastqParserReportNodeUid;
  }

  public String getFastqFileNodeUid() {
    return fastqFileNodeUid;
  }

  public void setFastqFileNodeUid(String fastqFileNodeUid) {
    this.fastqFileNodeUid = fastqFileNodeUid;
  }

  public MBFile getParsedDnaFastaFile() {
    return parsedDnaFastaFile;
  }

  public void setParsedDnaFastaFile(MBFile parsedDnaFastaFile) {
    this.parsedDnaFastaFile = parsedDnaFastaFile;
  }

  public MBFile getParsedProteinFastaFile() {
    return parsedProteinFastaFile;
  }

  public void setParsedProteinFastaFile(MBFile parsedProteinFastaFile) {
    this.parsedProteinFastaFile = parsedProteinFastaFile;
  }

}
