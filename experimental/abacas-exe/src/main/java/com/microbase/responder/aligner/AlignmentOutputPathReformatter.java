/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.microbase.responder.aligner;

import com.microbase.responder.abacascommon.AlignmentJobDescription;
import uk.org.microbase.responder.util.OutputPathReformatter;

/**
 *
 * @author prisni
 */
public interface AlignmentOutputPathReformatter extends OutputPathReformatter {
    
    public void setJobDescription(AlignmentJobDescription job);
    
}
