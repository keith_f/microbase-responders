/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.microbasecloud.responders.filescanner.data.nodes;

import com.microbasecloud.responders.filescanner.data.FileScannerDBException;
import com.mongodb.BasicDBObject;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.DBCursor;
import com.mongodb.DBObject;
import com.mongodb.Mongo;
import com.torrenttamer.mongodb.dbobject.DbObjectMarshaller;
import com.torrenttamer.mongodb.dbobject.DeserialisingIterable;
import java.util.logging.Logger;
import uk.ac.ncl.aries.entanglement.ObjectMarshallerFactory;
import uk.ac.ncl.aries.entanglement.graph.AbstractGraphEntityDAO;

/**
 *
 * @author Keith Flanagan
 */
public class ScanReportDAO 
    extends AbstractGraphEntityDAO {
  private static final Logger logger =
          Logger.getLogger(ScanReportDAO.class.getName());
  

  /*
   * Field names
   */
  private static final String PROP_UID = "uid";
  private static final String PROP_TIMESTAMP = "scanTimestamp";
  private static final String PROP_TARGET_NAME = "targetName";
  
  /*
   * Predefined sort orders
   */
  private static final DBObject SORT_BY_TIMESTAMP_ASC = new BasicDBObject(PROP_TIMESTAMP, 1);
  private static final DBObject SORT_BY_TIMESTAMP_DESC = new BasicDBObject(PROP_TIMESTAMP, -1);
  
  private final DbObjectMarshaller marshaller;
    
  public ScanReportDAO(ClassLoader classLoader, Mongo m, DB db, DBCollection col) {
    super(classLoader, m, db, col);
    marshaller = ObjectMarshallerFactory.create(classLoader);
  }
  
  public ScanReport selectLatestReportForTarget(String targetName)
      throws FileScannerDBException
  {
    DBObject query = null;
    try {
      query = new BasicDBObject();
      query.put(PROP_TARGET_NAME, targetName);

      DBCursor cursor = col.find(query).sort(SORT_BY_TIMESTAMP_DESC).limit(1);
      if (!cursor.hasNext()) {
//        logger.info("++++++++++++++++++++++++++++++++ No previous report exists!");
        return null;
      }
      DBObject prevReport = cursor.next();
//      logger.info("++++++++++++++++++++++++++++++++ Previous report is: "+prevReport);
      return marshaller.deserialize(prevReport, ScanReport.class);
    }
    catch(Exception e) {
      throw new FileScannerDBException("Failed to perform database operation:\n"
          + "Query: "+query, e);
    }
  }
  
  public ScanReport selectLatestReportForUid(String uid)
      throws FileScannerDBException
  {
    DBObject query = null;
    try {
      query = new BasicDBObject();
      query.put(PROP_UID, uid);

      DBCursor cursor = col.find(query).limit(1);
      if (!cursor.hasNext()) {
        return null;
      }
      return marshaller.deserialize(cursor.next(), ScanReport.class);
    }
    catch(Exception e) {
      throw new FileScannerDBException("Failed to perform database operation:\n"
          + "Query: "+query, e);
    }
  }
 
  
  public Iterable<ScanReport> selectReportsForTarget(String targetName)
      throws FileScannerDBException
  {
    DBObject query = null;
    try {
      query = new BasicDBObject();
      query.put(PROP_TARGET_NAME, targetName);

      DBCursor cursor = col.find(query).sort(SORT_BY_TIMESTAMP_ASC);
      return new DeserialisingIterable<>(cursor, marshaller, ScanReport.class);
    }
    catch(Exception e) {
      throw new FileScannerDBException("Failed to perform database operation:\n"
          + "Query: "+query, e);
    }
  }

}
