/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: 25-Mar-2012, 12:40:24
 */

package uk.ac.ncl.mucosa.genomepool.graph.fastq.nodes;

import java.util.Date;
import uk.ac.ncl.aries.entanglement.graph.data.Node;

/**
 *
 * @author Keith Flanagan
 */

public class FastqReportNode 
    extends Node
{ 
  private String fastqFileURI;

  private int numReads;
  
  private Date added;
  
  private String description;

  public FastqReportNode()
  {
  }

  public String getDescription()
  {
    return description;
  }

  public void setDescription(String description)
  {
    this.description = description;
  }

  public Date getAdded()
  {
    return added;
  }

  public void setAdded(Date added)
  {
    this.added = added;
  }

  public String getFastqFileURI()
  {
    return fastqFileURI;
  }

  public void setFastqFileURI(String fastqFileURI)
  {
    this.fastqFileURI = fastqFileURI;
  }

  public int getNumReads()
  {
    return numReads;
  }

  public void setNumReads(int numReads)
  {
    this.numReads = numReads;
  }
  
}
