/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package com.microbasecloud.tokenizer.exe;

import com.microbasecloud.tokenizer.common.TokenJobDescription;
import com.microbasecloud.tokenizer.common.TokenJobResult;
import uk.ac.ncl.aries.mongodb.util.BeanToGraphOperationMapper;
import com.microbasecloud.tokenizer.db.TokenDbException;
import com.microbasecloud.tokenizer.db.TxnUtils;
import com.microbasecloud.tokenizer.db.data.GenomeFragmentNode;
import com.microbasecloud.tokenizer.db.data.OrganismNode;
import com.microbasecloud.tokenizer.db.data.ProteinNode;
import com.microbasecloud.tokenizer.db.data.TokenNode;
import com.microbasecloud.tokenizer.db.data.TokenOccurrence;
import com.mongodb.DB;
import com.mongodb.DBCollection;
import com.mongodb.Mongo;
import com.torrenttamer.util.UidGenerator;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.net.UnknownHostException;
import java.util.Arrays;
import java.util.HashSet;
import java.util.LinkedList;
import java.util.List;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openide.util.Exceptions;
import uk.ac.ncl.aries.mongodb.player.EdgeDAO;
import uk.ac.ncl.aries.mongodb.player.GraphCheckoutNamingScheme;
import uk.ac.ncl.aries.mongodb.player.NodeDAO;
import uk.ac.ncl.aries.mongodb.player.PlayerDAOFactory;
import uk.ac.ncl.aries.mongodb.revlog.RevisionLog;
import uk.ac.ncl.aries.mongodb.revlog.RevisionLogDirectToMongoDbImpl;
import uk.ac.ncl.aries.mongodb.revlog.RevisionLogException;
import uk.ac.ncl.aries.mongodb.revlog.commands.CreateEdgeBetweenNamedNodes;
import uk.ac.ncl.aries.mongodb.revlog.commands.GraphOperation;
import uk.ac.ncl.aries.mongodb.shell.DbFactory;
import uk.org.microbase.dist.responder.RegistrationException;
import uk.org.microbase.dist.responder.ResponderInfo;
import uk.org.microbase.filesystem.spi.MBFile;
import uk.org.microbase.filesystem.spi.MicrobaseFS;
import uk.org.microbase.notification.data.Message;
import uk.org.microbase.responder.spi.AbstractMessageProcessor;
import uk.org.microbase.responder.spi.ProcessingException;
import uk.org.microbase.responder.spi.ResponderInfoPropertiesParser;
import uk.org.microbase.util.cli.NativeCommandExecutor;

/**
 * A MessageProcessor implementation capable of executing tools from
 * the NCBI BLAST package.
 * 
 * 
 * @author Keith Flanagan
 */
public class TokenExeResponder
  extends AbstractMessageProcessor
{
  private static final Logger l = Logger.getLogger(TokenExeResponder.class.getName());
  
  public static String PROP_HOSTNAME = "mongo_hostname";
  public static String PROP_DB_NAME = "database_name";
  public static String PROP_GRAPH_NAME = "graph_name";
  public static String PROP_GRAPH_BRANCH_NAME = "graph_branch_name";
  
  /**
   * Configuration resource for this responder. A file with this name must be
   * located on the classpath.
   */
  static final String CONFIG_RESOURCE = TokenExeResponder.class.getSimpleName()+".properties";
  
  /*
   * Per-job properties
   */
  private Properties config;
  private Mongo mongo;
  private DB db;
  private String graphName;
  private String branchName;
  private RevisionLog revLog;
  private NodeDAO nodeDao;
  private EdgeDAO edgeDao;
  private int totalTokens;
  private int totalProteins;
  
  /*
   * Per-job variables. These are set every time <code>preRun</code> is called.
   */
  private TokenJobDescription jobDescr;
    

  public TokenExeResponder()
  {
  }
  
  
  
  @Override
  protected ResponderInfo createDefaultResponderInfo()
      throws RegistrationException
  {
    try
    {
      config = new Properties();
      // Use properties from the Microbase global configuration file, if present
      config.putAll(getRuntime().getRawConfigProperties());
      try (InputStream is = getRuntime().getClassLoader().getResourceAsStream(CONFIG_RESOURCE)) {
        if (is == null) {
          throw new RegistrationException("Failed to find configuration resource: "+CONFIG_RESOURCE);
        }
        config.load(is);
      }

      ResponderInfo info = ResponderInfoPropertiesParser.parseFromProperties(config);

      l.info("Responder config: "+info);
      return info;
    }
    catch(Exception e) 
    {
      throw new RegistrationException(
          "Failed to load responder configuration", e);
    }
  }

  public void connect() throws UnknownHostException, RevisionLogException  {
    String hostname = config.getProperty(PROP_HOSTNAME);
    String database = config.getProperty(PROP_DB_NAME);
    graphName = config.getProperty(PROP_GRAPH_NAME);
    branchName = config.getProperty(PROP_GRAPH_BRANCH_NAME);
    
    l.info("Connecting to: "+hostname+"/"+database+", graph: "+graphName+"/"+branchName);
    
    DbFactory dbFactory = new DbFactory(hostname, database);
    mongo = dbFactory.createMongoConnection();
    db = mongo.getDB(database);
    
    revLog = new RevisionLogDirectToMongoDbImpl(mongo, db);
    
    GraphCheckoutNamingScheme collectionNamer = new GraphCheckoutNamingScheme(graphName, branchName);
    DBCollection nodeCol = db.getCollection(collectionNamer.getNodeCollectionName());
    DBCollection edgeCol = db.getCollection(collectionNamer.getEdgeCollectionName());
    nodeDao = PlayerDAOFactory.createDefaultNodeDAO(mongo, db, nodeCol, edgeCol);
    edgeDao = PlayerDAOFactory.createDefaultEdgeDAO(mongo, db, nodeCol, edgeCol);
    
    l.info("Connected!");
  }
  
  
  @Override
  public void preRun(Message message)
      throws ProcessingException
  {
    l.info("Doing preRun");
    try {
      /*
       * Parse message as a responder specific job description.
       */
      jobDescr = parseMessageWithJsonContent(message, TokenJobDescription.class);
      
      connect();
      
      totalTokens = 0;
      totalProteins = 0;
    }
    catch(Exception e) {
      throw new ProcessingException("Failed to parse message", e);
    }
  }

  @Override
  public void cleanupPreviousResults(Message message)
      throws ProcessingException
  {
    l.info("Doing cleanup");
    try
    {
      //TODO implement me...
    }
    catch(Exception e)
    {
      throw new ProcessingException("Failed to perform cleanup", e);
    }
  }

  @Override
  public Set<Message> processMessage(Message inMsg)
      throws ProcessingException
  {
    l.info("Performing computational work");
    long startTimeMs = System.currentTimeMillis();
    
    
    String graphId = jobDescr.getGraphName();
    String branchId = jobDescr.getGraphBranch();
    
    String txnId = null;
    int txnSubmitId = 0;
    int totalGraphOpsCount = 0;
    List<GraphOperation> graphOps = new LinkedList<>();
    try
    {
      l.info("Downloading input file: "+jobDescr.getFragmentFile());

      File fastaFile = downloadFileToWorkingDirectory(jobDescr.getFragmentFile());
      
      txnId = TxnUtils.beginNewTransaction(revLog, graphId, branchId, txnSubmitId++);
      
      // Create an organism node if one doesn't already exist
      OrganismNode orgNode = new OrganismNode();
      orgNode.setOrganismGuid(jobDescr.getOrganismGuid());
      graphOps.add(BeanToGraphOperationMapper.addNamedNodeWithAnnotationBean(
            orgNode.getOrganismGuid(), orgNode.getClass().getSimpleName(), 
            OrganismNode.class.getSimpleName(), orgNode));
//      graphOps.addAll(BeanToGraphOperationMapper.addBeanAsAnnotationToNamedNode(
//            orgNode.getOrganismGuid(), orgNode.getClass().getName(), 
//            OrganismNode.class.getSimpleName(), orgNode));
      
      
      // Create a fragment node if one doesn't already exist
      GenomeFragmentNode fragNode = new GenomeFragmentNode();
      fragNode.setFragmentGuid(jobDescr.getFragmentGuid());
      fragNode.setOrganismGuid(jobDescr.getOrganismGuid());
      graphOps.add(BeanToGraphOperationMapper.addNamedNodeWithAnnotationBean(
            fragNode.getFragmentGuid(), fragNode.getClass().getSimpleName(), 
            GenomeFragmentNode.class.getSimpleName(), fragNode));
//      graphOps.addAll(BeanToGraphOperationMapper.addBeanAsAnnotationToNamedNode(
//            fragNode.getFragmentGuid(), fragNode.getClass().getName(), 
//            GenomeFragmentNode.class.getSimpleName(), fragNode));
      
      // Create a link between fragment --> organism
      CreateEdgeBetweenNamedNodes fragBelongsToOrg = 
            new CreateEdgeBetweenNamedNodes(
            "belongs-to-organism", UidGenerator.generateUidAsBytes(),
            fragNode.getFragmentGuid(), orgNode.getOrganismGuid());
      graphOps.add(fragBelongsToOrg);
       
      totalGraphOpsCount += writeGraphRevisions(graphId, branchId, txnId, txnSubmitId, graphOps);
      txnSubmitId++;
      
      // Loop over each protein in the input FASTA file
      FileInputStream fis = new FileInputStream(fastaFile);
      List<FastaEntry> entries = FastaUtils.readIdsFromFasta(fis);
      
//      l.log(Level.INFO, "Read {0} from the fasta file: {1}", 
//              new Object[]{entries.size(), fastaFile.getAbsolutePath()});
      totalProteins = totalProteins + entries.size();
      for (FastaEntry fastaEntry : entries) {
        // Create a protein node to represent this sequence
        ProteinNode protNode = new ProteinNode();
        protNode.setProductGi(fastaEntry.getGi());
//        protNode.setTaxonId(null);
        protNode.setOrganismGuid(jobDescr.getOrganismGuid());
        protNode.setFragmentGuid(jobDescr.getFragmentGuid());
        protNode.setDescription(fastaEntry.getDescription());
        protNode.setSequence(fastaEntry.getSequence());
        graphOps.add(BeanToGraphOperationMapper.addNamedNodeWithAnnotationBean(
              protNode.getProductGi(), protNode.getClass().getSimpleName(), 
              ProteinNode.class.getSimpleName(), protNode));
//        graphOps.addAll(BeanToGraphOperationMapper.addBeanAsAnnotationToNamedNode(
//              protNode.getProductGi(), protNode.getClass().getName(), 
//              ProteinNode.class.getSimpleName(), protNode));
        
        // Now perform tokenization of this protein
        graphOps.addAll(doTokenization(
                orgNode, fragNode, protNode, jobDescr.getTokenSize()));
        
        int written = writeGraphRevisions(graphId, branchId, txnId, txnSubmitId, graphOps);
        txnSubmitId++;
        totalGraphOpsCount = totalGraphOpsCount + written;
      }

      l.log(Level.INFO, "Committing {0} revisions to the graph history.", totalGraphOpsCount);
      TxnUtils.commitTransaction(revLog, graphId, branchId, txnId, txnSubmitId++);

      long endTimeMs = System.currentTimeMillis();
      double seconds = (endTimeMs - startTimeMs) / 1000d;
      double tokensPerSecond = totalTokens / seconds;
      double proteinsPerSecond = totalProteins / seconds;
      l.log(Level.INFO, "Committed fragment: {0} which contained {1} proteins, "
              + "resulting in {2} tokens in {3} graph operations and took {4} seconds ({5} proteins/second; {6} tokens/second)", 
              new Object[]{fragNode.getFragmentGuid(), totalProteins, totalTokens, 
                totalGraphOpsCount, seconds, proteinsPerSecond, tokensPerSecond});
      
      
      Message successMsg = generateSuccessNotification(inMsg, seconds, proteinsPerSecond, tokensPerSecond);
      Set<Message> messages = new HashSet<>();
      messages.add(successMsg);
      return messages;
    }
    catch(Exception e)
    {
      try {
        TxnUtils.rollbackTransaction(revLog, graphId, branchId, txnId, txnSubmitId++);
      } catch (TokenDbException ex) {
        throw new ProcessingException(
          "Something failed while submitting one or more graph revisions. "
                + "We also failed to roll back the transaction...", ex);
      }
      throw new ProcessingException(
          "Something failed while submitting one or more graph revisions", e);
    }
    finally
    {
      mongo.close();
    }
  }
  
  private int writeGraphRevisions(String graphId, String branchId,
          String txnId, int txnSubmitId, List<GraphOperation> graphOps)
          throws RevisionLogException
  {
//    l.log(Level.INFO, "Submitting {0} revisions to the revision history", graphOps.size());
    revLog.submitRevisions(graphId, branchId, txnId, txnSubmitId, graphOps);
    int totalOps = graphOps.size();
    graphOps.clear();
    return totalOps;
  }
  
  
  private List<GraphOperation> doTokenization(OrganismNode orgNode, 
          GenomeFragmentNode fragNode, ProteinNode protNode, int windowSize)
      throws ProcessingException
  {
    List<GraphOperation> graphOpsForToken = new LinkedList<>();
    try
    {
//      System.out.println("Processing protein: "+totalProteins);
//      System.out.println(protNode.getSequence());
      List<TokenOccurrence> occs = TokenUtils.tokenise(protNode.getSequence(), windowSize);
//      System.out.println("*********************************** TOKEN OCCURRENCES: "+occs.size()+"\n");
      
      /*
       * For each occurrence:
       *  1) Ensure that a 'token' node exists
       *  2) Link the token node to the existing protein node
       *  3) Link the token node to the existing organism node
       *  4) Link the token node to the existing fragment node
       */
      for (TokenOccurrence occ : occs) {
        TokenNode tokenNode = new TokenNode();
        tokenNode.setTokenString(occ.getToken());
        tokenNode.setTokenSize(windowSize);
        
        // Create a token node if one doesn't already exist
        String beanName = tokenNode.getTokenString();
        graphOpsForToken.add(BeanToGraphOperationMapper.addNamedNodeWithAnnotationBean(
              beanName, tokenNode.getClass().getSimpleName(),
              TokenNode.class.getSimpleName(), tokenNode));
//        graphOpsForToken.addAll(BeanToGraphOperationMapper.addBeanAsAnnotationToNamedNode(
//              beanName, tokenNode.getClass().getName(),
//              TokenNode.class.getSimpleName(), tokenNode));
        
        // Create link to protein node
        String fromName = tokenNode.getTokenString();
        String toName = String.valueOf(protNode.getProductGi());
        CreateEdgeBetweenNamedNodes tokenPresentInProtein = 
              new CreateEdgeBetweenNamedNodes(
              "present-in-protein", UidGenerator.generateUidAsBytes(),
              fromName, toName);
        graphOpsForToken.add(tokenPresentInProtein);
        
        // Create link to organism node
        fromName = tokenNode.getTokenString();
        toName = String.valueOf(orgNode.getOrganismGuid());
        CreateEdgeBetweenNamedNodes tokenExistsInOrg = 
              new CreateEdgeBetweenNamedNodes(
              "exists-in-organism", UidGenerator.generateUidAsBytes(),
              fromName, toName);
        graphOpsForToken.add(tokenExistsInOrg);
        
        // Create link to fragment node
        //Commented to save on disk space (we can infer this relation)
//        fromName = tokenNode.getTokenString();
//        toName = String.valueOf(fragNode.getFragmentGuid());
//        CreateEdgeBetweenNamedNodes tokenExistsInFrag = 
//              new CreateEdgeBetweenNamedNodes(
//              "exists-in-fragment", UidGenerator.generateUidAsBytes(),
//              fromName, toName);
//        graphOpsForToken.add(tokenExistsInFrag);
      }
      
      totalTokens = totalTokens + occs.size();
      return graphOpsForToken;
    }
    catch(Exception e)
    {
      throw new ProcessingException(
          "Failed to execute the tokenization of: "+protNode, e);
    }
  }

  
  
  

  private Message generateSuccessNotification(Message parent, double seconds,
          double proteinsPerSecond, double tokensPerSecond)
      throws IOException
  {
    /*
     * Prepare the content for the 'success' notification message.
     */
    
    TokenJobResult resultContent = new TokenJobResult();
    resultContent.setJob(jobDescr);
    resultContent.setTotalProteinsProcessed(totalProteins);
    resultContent.setTotalTokensGenerated(totalTokens);
    
    
    resultContent.setWorkTimeSeconds(seconds);
    resultContent.setProteinsPerSecond(proteinsPerSecond);
    resultContent.setTokensPerSecond(tokensPerSecond);
    
    
    /*
     * Create the success message with the above content. Link this success
     * message to the parent message (the job descriptor) for provenance.
     */
    Message successMsg = createMessageWithJsonContent(
        parent,                           //The message to use as a parent
        getResponderInfo().getTopicOut(), //The topic of the new message
        parent.getWorkflowStepId(),       //The workflow step ID
        resultContent,                    //The result content item
        //An optional human-readable description
        "Successfully completed the tokenisation of a genome fragment.");
    

    
    return successMsg;
  }
  
  
}
