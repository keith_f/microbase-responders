/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.microbasecloud.genbankresponder;

/**
 *
 * @author prisni
 */
public class FeatureTableAnnotatorException extends Exception {

    public FeatureTableAnnotatorException() {
    }

    public FeatureTableAnnotatorException(String message) {
        super(message);
    }

    public FeatureTableAnnotatorException(String message, Throwable cause) {
        super(message, cause);
    }

    public FeatureTableAnnotatorException(Throwable cause) {
        super(cause);

    }
}
