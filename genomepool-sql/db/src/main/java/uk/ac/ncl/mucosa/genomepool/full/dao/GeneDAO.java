/*
 * Copyright 2010 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: 20-Sep-2010, 17:48:17
 */

package uk.ac.ncl.mucosa.genomepool.full.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import uk.ac.ncl.mucosa.genomepool.full.data.Gene;

/**
 *
 * @author Keith Flanagan
 */
public class GeneDAO
{

  public GeneDAO()
  {
  }

  public Gene getByGuid(Connection txn, String guid)
      throws SQLException
  {
    PreparedStatement stmt = null;
    try
    {
      stmt = txn.prepareStatement(
        "SELECT * FROM genes WHERE guid = ?");
      stmt.setString(1, guid);
      ResultSet rs = stmt.executeQuery();
      List<Gene> genes = resultSetToGenes(rs);
      if (genes.size() == 1)
      {
        return genes.get(0);
      }
      else if (genes.isEmpty())
      {
        return null;
      }
      else
      {
        throw new SQLException(genes.size()+" Gene instances had "
            + "the same GUID: "+guid + ". The database is inconsistent.");
      }
    }
    catch(Exception e)
    {
      throw new SQLException(
          "Failed to execute getByGuid()", e);
    }
    finally
    {
      stmt.close();
    }
  }
  
  public List<Gene> getByFragmentGuid(Connection txn, 
      String fragmentGuid, int offset, int limit)
      throws SQLException
  {
    PreparedStatement stmt = null;
    try
    {
      stmt = txn.prepareStatement(
        "SELECT * FROM genes WHERE fragment_guid = ? "
          + "ORDER BY start_locus LIMIT ? OFFSET ?");
      stmt.setString(1, fragmentGuid);
      stmt.setInt(2, limit);
      stmt.setInt(3, offset);
      
      ResultSet rs = stmt.executeQuery();
      List<Gene> genes = resultSetToGenes(rs);
      return genes;
    }
    catch(Exception e)
    {
      throw new SQLException(
          "Failed to execute getByFragmentGuid()", e);
    }
    finally
    {
      stmt.close();
    }
  }
  
  /**
   * Returns the Gene entries that overlap a part or the whole of specified DNA 
   * coordinates of a genome fragment. i.e., if a genes starts in, ends in, or
   * is wholly contained by the region startPos --> endPos, then it is returned.
   * 
   * @param txn
   * @param fragmentGuid
   * @param startPos
   * @param endPos
   * @return
   * @throws SQLException 
   */
  public List<Gene> getGenesOverlappingCoordinates(Connection txn, 
      String fragmentGuid, int startPos, int endPos)
      throws SQLException
  {
    PreparedStatement stmt = null;
    try
    {
      stmt = txn.prepareStatement(
        "SELECT * FROM genes WHERE fragment_guid = ? "
          + "AND ((start_locus >= ? AND start_locus <= ?) "
          + "OR (end_locus >= ? AND end_locus <= ?)) "
          + "ORDER BY start_locus");
      int col = 1;
      stmt.setString(col++, fragmentGuid);
      stmt.setInt(col++, startPos);
      stmt.setInt(col++, endPos);
      stmt.setInt(col++, startPos);
      stmt.setInt(col++, endPos);
      
      ResultSet rs = stmt.executeQuery();
      List<Gene> genes = resultSetToGenes(rs);
      return genes;
    }
    catch(Exception e)
    {
      throw new SQLException(
          "Failed to execute getGenesOverlappingCoordinates()", e);
    }
    finally
    {
      stmt.close();
    }
  }


  private List<Gene> resultSetToGenes(ResultSet rs)
      throws SQLException
  {
    List<Gene> list = new ArrayList<Gene>();
    while(rs.next())
    {
      Gene gene = new Gene();

      gene.setEndLocus(rs.getInt("end_locus"));
      gene.setFragmentGuid(rs.getString("fragment_guid"));
      gene.setGeneId(rs.getString("gene_id"));
      gene.setGeneName(rs.getString("gene_name"));
      gene.setGuid(rs.getString("guid"));
      gene.setLocusTag(rs.getString("locus_tag"));
      gene.setOrientation(rs.getString("orientation"));
      gene.setProducts(null);  //FIXME not yet supported
      gene.setStartLocus(rs.getInt("start_locus"));
      gene.setSynonyms(null); //FIXME not yet supported
      gene.setTaxonId(rs.getInt("taxon_id"));

      list.add(gene);
    }
    return list;
  }
}
