/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package uk.ac.ncl.mucosa.genomepool.ws.data;

import java.io.Serializable;

/**
 *
 * @author Sirintra Nakjang
 */
public class ProductAnnotationWs
        implements Serializable
{
  private String geneProductGuid;
  private String dbReference;

  private String identifier;

  public ProductAnnotationWs()
  {
  }

  public String getDbReference()
  {
    return dbReference;
  }

  public void setDbReference(String dbReference)
  {
    this.dbReference = dbReference;
  }

  public String getIdentifier()
  {
    return identifier;
  }

  public void setIdentifier(String identifier)
  {
    this.identifier = identifier;
  }

  public String getGeneProductGuid()
  {
    return geneProductGuid;
  }

  public void setGeneProductGuid(String geneProductGuid)
  {
    this.geneProductGuid = geneProductGuid;
  }

  
}
