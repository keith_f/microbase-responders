/*
 * Copyright 2013 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 */

package uk.ac.ncl.mucosa.genomepool.graph.fastq.edges;

import uk.ac.ncl.aries.entanglement.graph.data.Edge;
import uk.ac.ncl.aries.entanglement.graph.data.Node;

/**
 *
 * @author Keith Flanagan
 */
public class PartOfFastqReport
    extends Edge
{

  public PartOfFastqReport() {
  }

  public PartOfFastqReport(String fromUid, String toUid) {
    super(fromUid, toUid);
  }

  public PartOfFastqReport(Node from, Node to) {
    super(from, to);
  }

  public PartOfFastqReport(String fromType, String fromName, String toType, String toName) {
    super(fromType, fromName, toType, toName);
  }

}
