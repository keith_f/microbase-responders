/*
 * Copyright 2010 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: 20-Sep-2010, 17:48:17
 */

package uk.ac.ncl.mucosa.genomepool.full.dao;

import java.sql.Clob;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import uk.ac.ncl.mucosa.genomepool.full.data.FragmentType;
import uk.ac.ncl.mucosa.genomepool.full.data.GenomeFragment;

/**
 *
 * @author Keith Flanagan
 */
public class DnaSequencesDAO
{

  public DnaSequencesDAO()
  {
  }


  public boolean exists(Connection txn, String fragmentGuid)
      throws SQLException
  {
    PreparedStatement stmt = null;
    try
    {
      stmt = txn.prepareStatement(
        "SELECT count(*) as count FROM dna_sequences WHERE fragment_guid = ?");
      stmt.setString(1, fragmentGuid);
      ResultSet rs = stmt.executeQuery();
      rs.next();
      return rs.getInt("count") > 0;
    }
    catch(Exception e)
    {
      throw new SQLException(
          "Failed to execute exists() for fragment: "+fragmentGuid, e);
    }
    finally
    {
      stmt.close();
    }
  }

  public int count(Connection txn)
      throws SQLException
  {
    PreparedStatement stmt = null;
    try
    {
      stmt = txn.prepareStatement(
        "SELECT count(*) as count FROM dna_sequences");
      ResultSet rs = stmt.executeQuery();
      rs.next();
      return rs.getInt("count");
    }
    catch(Exception e)
    {
      throw new SQLException(
          "Failed to execute count()", e);
    }
    finally
    {
      stmt.close();
    }
  }


  public List<String> getFragmentGuidsWithDnaSequenceEntries(Connection txn)
      throws SQLException
  {
    PreparedStatement stmt = null;
    try
    {
      stmt = txn.prepareStatement(
        "SELECT fragment_guid FROM dna_sequences");
      List<String> result = new ArrayList<String>();
      ResultSet rs = stmt.executeQuery();
      while(rs.next())
      {
        result.add(rs.getString(1));
      }
      return result;
    }
    catch(Exception e)
    {
      throw new SQLException(
          "Failed to execute getFragmentGuidsWithDnaSequenceEntries()", e);
    }
    finally
    {
      stmt.close();
    }
  }
  
  public String getDnaSequenceForFragment(Connection txn, String fragmentGuid)
      throws SQLException
  {
    PreparedStatement stmt = null;
    try
    {
      stmt = txn.prepareStatement(
        "SELECT dna_sequence FROM dna_sequences WHERE fragment_guid = ?");
      stmt.setString(1, fragmentGuid);
      ResultSet rs = stmt.executeQuery();
      if (rs.next()) {
        String seq = rs.getString("dna_sequence");
        return seq;
//        Clob clob = rs.getClob("dna_sequence");
//        clob.getSubString(pos, length);
      }
      return null;
    }
    catch(Exception e)
    {
      throw new SQLException(
          "Failed to execute getSequenceForFragment()", e);
    }
    finally
    {
      stmt.close();
    }
  }
  
  /**
   * 
   * @param txn
   * @param fragmentGuid
   * @param startBasePlusOne remember that this must be the start base, plus one
   * because SQL starts counting at 1 instead of 0...
   * @param lenBases
   * @return
   * @throws SQLException 
   */
  public String getDnaSequenceForFragment(
          Connection txn, String fragmentGuid, int startBasePlusOne, int lenBases)
      throws SQLException
  {
    PreparedStatement stmt = null;
    try
    {
      stmt = txn.prepareStatement(
        "SELECT dna_sequence FROM dna_sequences WHERE fragment_guid = ?");
      stmt.setString(1, fragmentGuid);
      ResultSet rs = stmt.executeQuery();
      if (rs.next()) {
        Clob clob = rs.getClob("dna_sequence");
        return clob.getSubString(startBasePlusOne, lenBases);
      }
      return null;
    }
    catch(Exception e)
    {
      throw new SQLException(
          "Failed to execute getSequenceForFragment()", e);
    }
    finally
    {
      stmt.close();
    }
  }





}
