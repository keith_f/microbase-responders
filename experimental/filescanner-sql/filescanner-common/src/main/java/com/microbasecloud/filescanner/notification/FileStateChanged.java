/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: 14-Oct-2012, 16:23:56
 */

package com.microbasecloud.filescanner.notification;

import com.microbasecloud.filescanner.common.FileStatus;
import java.io.Serializable;
import uk.org.microbase.filesystem.spi.FileMetaData;

/**
 * Can be serialized and attached to a notification message as a content
 * item to inform other workflow components of an update to a file's
 * state (new, deleted, updated, unchanged).
 * @author Keith Flanagan
 */
public class FileStateChanged implements Serializable
{
  private FileMetaData fileMetaData;
  private FileStatus statusChange;
  
  private long scanReportId;

  public FileStateChanged()
  {
  }

  public FileMetaData getFileMetaData()
  {
    return fileMetaData;
  }

  public void setFileMetaData(FileMetaData fileMetaData)
  {
    this.fileMetaData = fileMetaData;
  }

  public FileStatus getStatusChange()
  {
    return statusChange;
  }

  public void setStatusChange(FileStatus statusChange)
  {
    this.statusChange = statusChange;
  }

  public long getScanReportId()
  {
    return scanReportId;
  }

  public void setScanReportId(long scanReportId)
  {
    this.scanReportId = scanReportId;
  }  
}
