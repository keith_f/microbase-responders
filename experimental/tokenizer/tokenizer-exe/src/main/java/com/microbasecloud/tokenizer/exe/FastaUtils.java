/*
 *  This program is free software; you can redistribute it and/or modify
 *  it under the terms of the GNU Lesser General Public License as published
 *  by the Free Software Foundation. The full license may be found in
 *  COPYING.LESSER in this project's root directory.
 * 
 *  Copyright 2009 jointly held by the authors listed at the top of each
 *  source file and/or their respective employers.
 */

package com.microbasecloud.tokenizer.exe;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Keith Flanagan
 * @author Sirintra Nakjang
 */
public class FastaUtils
{
  public static List<FastaEntry> readIdsFromFasta(InputStream is)
      throws IOException
  {
    BufferedReader br = null;
    try
    {
      List<FastaEntry> fastaEntries = new ArrayList<>();
      //final String fastaId = ">"+id;
      StringBuilder seqText = null;// = new StringBuilder();
      //boolean foundSeq = false;
      br = new BufferedReader(new InputStreamReader(is));
      FastaEntry currentEntry = null;
      for (String line = br.readLine(); ; line = br.readLine())
      {
        if (line == null) 
        {
          //We've reached the end of the file. Remember to set the sequence text
          //of the current entry (if there is one)
          if (currentEntry != null) {
            currentEntry.setSequence(seqText.toString());
          }
          break;
        } 
        else if (line.startsWith(">"))
        {
          /*
           * We've found the start of a new sequence. Before reacting to that,
           * first commit any current sequence to the list.
           */
          if (currentEntry != null) {
            currentEntry.setSequence(seqText.toString());
          }
          
          /*
           * Now, deal with the new sequence entry that we've just discovered.
           */
          String idLine = line.substring(1);
          String[] split = idLine.split(" ");
          String description = idLine.substring(split[0].length());

          currentEntry = new FastaEntry();
          currentEntry.setGi(split[0].substring(0, 11));
          currentEntry.setDescription(description);
          fastaEntries.add(currentEntry);

          seqText = new StringBuilder();
          //foundSeq = true;
        }
        else if (seqText != null)
        {
          seqText.append(line);
        }
      }

      return fastaEntries;
    }
    finally
    {
      if (br != null)
      {
        br.close();
      }
    }
  }
}
