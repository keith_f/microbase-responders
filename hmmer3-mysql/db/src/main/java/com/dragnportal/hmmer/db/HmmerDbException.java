/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: 26-Mar-2012, 00:58:42
 */

package com.dragnportal.hmmer.db;

/**
 *
 * @author Keith Flanagan
 */
public class HmmerDbException
    extends Exception
{

  public HmmerDbException(Throwable thrwbl)
  {
    super(thrwbl);
  }

  public HmmerDbException(String string, Throwable thrwbl)
  {
    super(string, thrwbl);
  }

  public HmmerDbException(String string)
  {
    super(string);
  }

  public HmmerDbException()
  {
  }

}
