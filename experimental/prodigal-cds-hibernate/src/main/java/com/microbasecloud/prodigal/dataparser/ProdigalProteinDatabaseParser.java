/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.microbasecloud.prodigal.dataparser;

import com.microbasecloud.prodigal.datamodel.ProdigalDAO;
import com.microbasecloud.prodigal.datamodel.ProdigalReport;
import com.microbasecloud.prodigal.datamodel.ProdigalTranslation;
import com.torrenttamer.hibernate.HibernateUtilException;
import com.torrenttamer.hibernate.SessionProvider;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.List;
import org.hibernate.Session;

/**
 *
 * @author prisni
 */
public class ProdigalProteinDatabaseParser {

    public static void main(String[] args) throws FileNotFoundException, HibernateUtilException, Exception {

        

        ProdigalReport report = new ProdigalReport();
        report.setTranslationListSourceFileName("1234");
        File file = new File("/home/prisni/new_prot_trans.txt");
    }
    
    public static void parse(File file, ProdigalReport report) 
            throws FileNotFoundException, HibernateUtilException, Exception
    {
        ProdigalOutputFileDetailExtractor prodG = new ProdigalOutputFileDetailExtractor();
        List<ProdigalTranslation> proteins = prodG.extractProteins(report, file);
        System.out.println("Found your protein file...hooray!\n");
        SessionProvider prodigalSessionProvider =
                new SessionProvider("/ProdigalResponder-hibernate.cfg.xml");
        Session session = null;

        try {
            session = prodigalSessionProvider.getWriteableSession();
            session.beginTransaction();
            ProdigalDAO dao = new ProdigalDAO();
            dao.storeTranslations(session, proteins); //Stores the HmmerHit instances as well as the HmmerReport
            System.out.println("Committing...");
            session.getTransaction().commit();
            System.out.println("Committed.");
        } catch (Exception e) {
            SessionProvider.silentRollback(session);
            throw new Exception("Failed to save "
                    + proteins.size() + " hits to the DB", e);
        } finally {
            SessionProvider.silentClose(session);
        }



    }
}
