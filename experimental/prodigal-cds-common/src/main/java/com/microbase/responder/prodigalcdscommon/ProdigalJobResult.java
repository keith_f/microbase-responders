/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.microbase.responder.prodigalcdscommon;

import java.io.Serializable;
import uk.org.microbase.filesystem.spi.MBFile;

/**
 *
 * @author prisni
 */
public class ProdigalJobResult
        implements Serializable {

    private ProdigalJobDescription job;
    private MBFile resultCDSFile;
    private MBFile resultTranslationsFile;
    private MBFile stdOutLogFile;
    private MBFile stdErrLogFile;

    public ProdigalJobResult() {
    }

    public ProdigalJobDescription getJob() {
        return job;
    }

    public void setJob(ProdigalJobDescription job) {
        this.job = job;
    }

    public MBFile getResultCDSFile() {
        return resultCDSFile;
    }

    public void setResultCDSFile(MBFile resultCDSFile) {
        this.resultCDSFile = resultCDSFile;
    }

    public MBFile getResultTranslationsFile() {
        return resultTranslationsFile;
    }

    public void setResultTranslationsFile(MBFile resultTranslationsFile) {
        this.resultTranslationsFile = resultTranslationsFile;
    }

    public MBFile getStdOutLogFile() {
        return stdOutLogFile;
    }

    public void setStdOutLogFile(MBFile stdOutLogFile) {
        this.stdOutLogFile = stdOutLogFile;
    }

    public MBFile getStdErrLogFile() {
        return stdErrLogFile;
    }

    public void setStdErrLogFile(MBFile stdErrLogFile) {
        this.stdErrLogFile = stdErrLogFile;
    }
}
