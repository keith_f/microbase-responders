/*
 * Copyright 2011 Keith Flanagan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * File created: 20-Sep-2010, 17:48:17
 */

package uk.ac.ncl.mucosa.genomepool.ws;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import uk.ac.ncl.mucosa.genomepool.ws.GenomePoolException;
import uk.org.microbase.filesystem.spi.MBFile;

/**
 *
 * @author Keith Flanagan
 */
@WebService
public interface GenomePool
{
  /**
   * Allows the manual addition of fragment data to the GenomePool database.
   * @param resultSetId the ID of the result set that this entry should be
   * placed in.
   * @param gbkFile details of a Microbase Filesystem entry to add. This file
   * must be in GenBank format.
   * @throws GenomePoolException
   */
  @WebMethod(operationName = "addGenbankFile")
  public void addGenbankFile(
      @WebParam(name = "resultSetId") String resultSetId,
      @WebParam(name = "fileInfo") MBFile gbkFile)
      throws GenomePoolException;

  /**
   * Allows the manual addition of protein data to the 'raw' protein sequence
   * database.
   * @param resultSetId the ID of the result set that this entry should be
   * placed in.
   * @param fragmentId the genome fragment ID that the set of protein sequences
   * contained in the file should be assigned to. This information is not
   * available in the FASTA format, so should must be provided separately.
   * @param faaFile details of a Microbase Filesystem entry to add. This file
   * must be in FASTA format.
   * @throws GenomePoolException
   */
  @WebMethod(operationName = "addProteinFastaFile")
  public void addProteinFastaFile(
      @WebParam(name = "resultSetId") String resultSetId,
      @WebParam(name = "fragmentId") String fragmentId,
      @WebParam(name = "fileInfo") MBFile faaFile)
      throws GenomePoolException;


  @WebMethod(operationName = "countGenomeEntries")
  public int countGenomeEntries()
      throws GenomePoolException;

  @WebMethod(operationName = "getProteinSequenceFor")
  public String getProteinSequenceFor(
      @WebParam(name = "proteinId") String proteinId)
      throws GenomePoolException;

  @WebMethod(operationName = "getProteinSequenceForOrganism")
  public String getProteinSequenceForOrganism(
      @WebParam(name = "organismId") String organismId)
      throws GenomePoolException;

  @WebMethod(operationName = "getProteinSequenceForFragment")
  public String getProteinSequenceForFragment(
      @WebParam(name = "fragmentId") String fragmentId)
      throws GenomePoolException;



}


