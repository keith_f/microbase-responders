/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.microbase.responder.sff;

import com.microbase.responder.sffcommon.SFFJobDescription;
import com.microbase.responder.sffcommon.SFFJobResult;
import java.io.File;
import java.io.FileWriter;
import java.io.InputStream;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Properties;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import uk.org.microbase.dist.responder.RegistrationException;
import uk.org.microbase.dist.responder.ResponderInfo;
import uk.org.microbase.filesystem.spi.MBFile;
import uk.org.microbase.filesystem.spi.MicrobaseFS;
import uk.org.microbase.notification.data.Message;
import uk.org.microbase.responder.spi.AbstractMessageProcessor;
import uk.org.microbase.responder.spi.ProcessingException;
import uk.org.microbase.responder.spi.ResponderInfoPropertiesParser;
import uk.org.microbase.util.cli.NativeCommandExecutor;

    
/**
 *
 * @author prisni
 */
public class SFFResponder extends AbstractMessageProcessor{
    
     private static final Logger l = Logger.getLogger(SFFResponder.class.getName());
    /**
     * Configuration resource for this responder. A file with this name must be
     * located on the classpath.
     */
    static final String CONFIG_RESOURCE = SFFResponder.class.getSimpleName() + ".properties";
    private static final String CONFIG_RESULT_NAME_REFORMATTER = "result_name_reformatter";
    /*
     * Configuration options (these should be treated as 'final'
     */
    private Properties config;
    private SFFOutputPathReformatter resultFilenameReformatter;
    private SFFOutputPathReformatter stdOutFilenameReformatter;
    private SFFOutputPathReformatter stdErrFilenameReformatter;
    /*
     * Per-job variables. These are set every time <code>preRun</code> is called.
     */
    private SFFJobDescription jobDescr;
    private MBFile jobRemoteResultFile;
    private MBFile jobStdOutLogFile;
    private MBFile jobStdErrLogFile;

    public SFFResponder() {

        /*
         * Set some default file name formatters here.
         * TODO eventually, we'll want these to be configurable.
         */
        resultFilenameReformatter = new SFFResultInIndividualDirsFormatter();
        stdOutFilenameReformatter = new SFFStdOutLogFileFormatter();
        stdErrFilenameReformatter = new SFFStdErrLogFileFormatter();

    }
    
//    private static final String TOPIC_IN = "FASTQ-GENERATION-JOB";
//    private static final String TOPIC_OUT = "ASSEMBLY-JOB-BEGIN";
//    
//    
//    private FileMetaData sff;
    public static String projectName = "Strain-4";
////    private FileMetaData[] fastQS3File;
////    private FileMetaData traceXMLS3File;
//    private FileMetaData[] outputS3Files;
//    private String logFileS3Bucket;
//    private String logFileS3Path;
//    public SFFResponder() {
//        outputS3Files = new FileMetaData[3];
//    }

    /**
     *
     * @return
     */
    @Override
    protected ResponderInfo createDefaultResponderInfo() throws RegistrationException {
        try {
            config = new Properties();
            // Use properties from the Microbase global configuration file, if present
            config.putAll(getRuntime().getRawConfigProperties());
            try (InputStream is = getRuntime().getClassLoader().getResourceAsStream(CONFIG_RESOURCE)) {
                if (is == null) {
                    throw new RegistrationException("Failed to find configuration resource: " + CONFIG_RESOURCE);
                }
                config.load(is);
            }

            ResponderInfo info = ResponderInfoPropertiesParser.parseFromProperties(config);

            l.info("Responder config: " + info);
            return info;
        } catch (Exception e) {
            throw new RegistrationException(
                    "Failed to load responder configuration", e);
        }
    }

    
//        ResponderInfo info = new ResponderInfo();
//        info.setEnabled(true);
//        info.setMaxDistributedCores(300);
//        info.setMaxLocalCores(50);
//        info.setPreferredCpus(1);
//        info.setQueuePopulatorLastRunTimestamp(0);
//        info.setQueuePopulatorRunEveryMs(30000);
//        info.setResponderName(SFFResponder.class.getSimpleName());
//        info.setResponderVersion("1.0");
//        info.setResponderId(info.getResponderName() + "-"
//                + info.getResponderVersion());
//        info.setTopicOfInterest(TOPIC_IN);
//        return info;
//    }

    public void preRun(Message message) throws ProcessingException
            {
        l.info("Doing preRun");
        try {
            /*
             * Parse message as a responder specific job description.
             */
            jobDescr = parseMessageWithJsonContent(message, SFFJobDescription.class);

            /*
             * Configure the result file formatters
             */
            resultFilenameReformatter.configure(getRuntime(), getResponderInfo(), getProcessInfo(), message);
            resultFilenameReformatter.setJobDescription(jobDescr);
            stdOutFilenameReformatter.configure(getRuntime(), getResponderInfo(), getProcessInfo(), message);
            stdOutFilenameReformatter.setJobDescription(jobDescr);
            stdErrFilenameReformatter.configure(getRuntime(), getResponderInfo(), getProcessInfo(), message);
            stdErrFilenameReformatter.setJobDescription(jobDescr);

            jobRemoteResultFile = resultFilenameReformatter.getFormattedRemoteFile();
            jobStdOutLogFile = stdOutFilenameReformatter.getFormattedRemoteFile();
            jobStdErrLogFile = stdErrFilenameReformatter.getFormattedRemoteFile();

        } catch (Exception e) {
            throw new ProcessingException("Failed to parse message", e);
        }
    }
//    {
//        System.out.println("Doing pre-run");
//        sff = parseZipFileFromMessage(msg);
//        // set remote location for output
//        String remoteS3Bucket = sff.getLocation().getBucket();
//        String remoteS3Path = "fastQ-from:" + sff.getLocation().getName();
//        String remoteFastQS3Name = projectName + "_in.iontor" +
//                ".fastq";
//        outputS3Files[0] = new FileMetaData(remoteS3Bucket, remoteS3Path,
//                remoteFastQS3Name);
//         String remoteTraceXMLS3Name = projectName + "_traceinfo_in.iontor" +
//                ".xml";
//        outputS3Files[1] = new FileMetaData(remoteS3Bucket, remoteS3Path,
//                remoteTraceXMLS3Name);
//        logFileS3Bucket = remoteS3Bucket;
//        logFileS3Path = remoteS3Path;
//    }

    @Override
    public void cleanupPreviousResults(Message msg) throws ProcessingException {
        l.info("Doing cleanup");
        try {
            MicrobaseFS fs = getRuntime().getMicrobaseFS();

            if (fs.exists(jobRemoteResultFile)) {
                l.info("Found an existing result file generated by a previous "
                        + "execution. Deleting...");
                fs.deleteRemoteCopy(jobRemoteResultFile);
            }
        } catch (Exception e) {
            throw new ProcessingException("Failed to perform cleanup", e);
        }
    }

    @Override
    public Set<Message> processMessage(Message msg) throws ProcessingException {
        l.info("Performing computational work");
            try {
                MBFile sffZippedFile = jobDescr.getSffZipFile();
                File remoteSffFile = downloadAndUnzipFileToSharedSpace(sffZippedFile);              
                l.info("SFF file is: " + remoteSffFile);
                
                l.info("Running command ...");

                File[] outputsLocalFile = runSffExtract(remoteSffFile);

                l.info("Uploading generated fastq and xml files: " + outputsLocalFile[0].getAbsolutePath()+
                        "and"+outputsLocalFile[1].getAbsolutePath()+" to: " + jobRemoteResultFile);

                getRuntime().getMicrobaseFS().upload(
                        outputsLocalFile[0], jobRemoteResultFile, null);

                getRuntime().getMicrobaseFS().upload(
                        outputsLocalFile[1], jobRemoteResultFile, null);
//        File sffFileZip = downloadInputFile(sff);
//            try {
//                InputStream zippedFIS = new FileInputStream(sffFileZip);
//                ZipUtils.extractZip(zippedFIS, getWorkingDirectory());
//                zippedFIS.close();
            Message sffSuccessMsg = generateSuccessNotification(msg, jobDescr); //generate success notification
                Set<Message> messages = new HashSet(); //create a hash set of notification messages 
                messages.add(sffSuccessMsg); //add new success messages to the hash set
                return messages;
            } catch (Exception e) {
                throw new ProcessingException("Failed to run one or more commands: "
                        + "on: " + getRuntime().getNodeInfo().getHostname()
                        + "in: " + getWorkingDirectory(), e);

            }
                                  
            // MIRA responder takes an sff file which is zipped. We extract the sff file(s)
            // We're assuming that this zipped file has only one sff file 
            // and has the same name as the zipped file
//            String sff_filename = parseSffFileNameFromMessage(msg);
//            
//            File sffFile = new File(getWorkingDirectory(), sff_filename);
//            
//            File[] outputsLocalFile = runSffExtract(sffFile);
//            uploadExtractedFiles(outputsLocalFile);
//            Message sffSuccessMsg = generateSuccessNotification(msg);
//            Set<Message> messages = new HashSet();
//            messages.add(sffSuccessMsg);
//            return messages;
    
    }
            

//    private File downloadInputFile(FileMetaData remoteFile) 
//            throws ProcessingException {
//         try {
//            //get a reference to the working temp dir
//            File tmpDir = getWorkingDirectory();
//
//            //downloading the fasta file
//            File destinationFile = new File(tmpDir, remoteFile.getLocation().getName());
//
//            getRuntime().getMicrobaseFS().downloadFileToSpecificLocation(
//                    remoteFile.getLocation(), destinationFile, true);
//
//            return destinationFile;
//
//        } catch (Exception e) {
//            throw new ProcessingException("Failed to download remote file"
//                    + remoteFile.getLocation().getBucket() + " , " + remoteFile.getLocation().getPath(), e);
//        }
//    }
//    /**
//     * File descriptor for zip file in S3 bucket
//     * @param m
//     * @return
//     * @throws ProcessingException 
//     */
//
//    private FileMetaData parseZipFileFromMessage(Message m) throws ProcessingException{
//        try {
//            FileMetaData sffZipFile = new FileMetaData(m.getContent()
//                    .get("sffZip_bucket"),
//                    m.getContent().get("sffZip_path"),
//                    m.getContent().get("sffZip_filename"));
//            return sffZipFile;
//        } catch (Exception e) {
//            throw new ProcessingException("Failed to parse message", e);
//        }
//    }
    
//    /**
//     * name of the file within that zipped file
//     * @param m
//     * @return
//     * @throws ProcessingException 
//     */
//    
//    private String parseSffFileNameFromMessage(Message m) throws ProcessingException{
//        try {
//            return m.getContent().get("sff_filename");
//        } catch (Exception ex) {
//            throw new ProcessingException("Failed to parse message", ex);
//        }
//    }

//    
    private Message generateSuccessNotification(Message parent, SFFJobDescription job) {
        /*
         * Prepare the content for the 'success' notification message.
         */

        SFFJobResult resultContent = new SFFJobResult();
        resultContent.setJob(job);
        resultContent.setResultFastQFile(jobRemoteResultFile);
        resultContent.setResultTraceInfoXMLFile(jobRemoteResultFile);
        resultContent.setStdErrLogFile(jobStdErrLogFile);
        resultContent.setStdOutLogFile(jobStdOutLogFile);

        /*
         * Create the success message with the above content. Link this success
         * message to the parent message (the job descriptor) for provenance.
         */
        Message successMsg = createMessageWithJsonContent(
                parent, //The message to use as a parent
                getResponderInfo().getTopicOut(), //The topic of the new message
                parent.getWorkflowStepId(), //The workflow step ID
                resultContent, //The result content item
                //An optional human-readable description
                "Successfully completed alignment of reads.");

        return successMsg;
       
//            Message successMsg = createMessage(
//                    parent,
//                    //The message to use as a parent
//                    TOPIC_OUT,
//                    //The topic of the new message
//                    parent.getWorkflowStepId(),
//                    //The workflow step ID
//                    //An optional human-readable description
//                    "Successfully accomplished extracting fastq and trace info xml using ssf_extract for the sff file: "
//                    + sff.getLocation().getName());
////            successMsg.getContent().put("sff_bucket", sff.getBucket());
////            successMsg.getContent().put("sff_path", sff.getPath());
////            successMsg.getContent().put("sff_filename", sff.getName());
//            
//            // Inform future responders about the fastq output file
//            successMsg.getContent().put("result_file.fastQ.bucket", outputS3Files[0].getLocation().getBucket());
//            successMsg.getContent().put("result_file.fastQ.path",  outputS3Files[0].getLocation().getPath());
//            successMsg.getContent().put("result_file.fastQ.filename",  outputS3Files[0].getLocation().getName());
//            
//            // Inform future responders about the trace info xml output file
//            successMsg.getContent().put("result_file.traceXML.bucket",  outputS3Files[1].getLocation().getBucket());
//            successMsg.getContent().put("result_file.traceXML.path", outputS3Files[1].getLocation().getPath());
//            successMsg.getContent().put("result_file.traceXML.filename", outputS3Files[1].getLocation().getName());
//            return successMsg;
        }
//    
//    
    private void executeCommand(String[] command, String commandType)
            throws ProcessingException {
        File workDir = getWorkingDirectory();
        StringBuilder namePrefix = new StringBuilder();
        namePrefix.append(getProcessInfo().getMessageId()).append(".");
        namePrefix.append(getProcessInfo().getProcessGuid()).append(".");
        namePrefix.append(getProcessInfo().getHostname()).append(".");
        namePrefix.append(commandType).append(".");
        File stdOutFile = new File(workDir, namePrefix.toString() + "stdout.txt");
        File stdErrFile = new File(workDir, namePrefix.toString() + "stderr.txt");
        try {
            FileWriter stdOut = new FileWriter(stdOutFile);
            FileWriter stdErr = new FileWriter(stdErrFile);

            int exitStatus = NativeCommandExecutor.executeNativeCommand(
                    workDir, command, stdOut, stdErr);

            stdOut.flush();
            stdOut.close();
            stdErr.flush();
            stdErr.close();

            if (exitStatus != 0) {
                throw new ProcessingException("Exit status of the command: "
                        + Arrays.asList(command)
                        + " \nwas " + exitStatus
                        + ". Assuming that non-zero means that execution failed.");
            }
        } catch (Exception e) {
            throw new ProcessingException("Failed to execute command: " + command[0], e);
        } finally {
            try {
                getRuntime().getMicrobaseFS().upload(stdOutFile, jobStdOutLogFile, null);
                getRuntime().getMicrobaseFS().upload(stdErrFile, jobStdErrLogFile, null);
            } catch (Exception e) {
                throw new ProcessingException(
                        "Failed to upload STDOUT/STDERR content", e);
            }
        }
    }

//
    private File[] runSffExtract(File sffFile) throws ProcessingException {
        try{
            
               String[] sffExtractCommand = new String[]{"/mnt/responder_data/sff_extract_0_3_0", "-s",
                 outputS3Files[0].getLocation().getName(), "-x",
                 outputS3Files[1].getLocation().getName(), sffFile.getAbsolutePath()};
            executeCommand(sffExtractCommand, "sff_extract");
            File fastQResultFile = new File(getWorkingDirectory(), 
                    outputS3Files[0].getLocation().getName());
            File traceInfoXmlResultFile = new File(getWorkingDirectory(),
                    outputS3Files[1].getLocation().getName());
            return new File[]{fastQResultFile, traceInfoXmlResultFile};
            
    } catch (Exception ex) {
            throw new ProcessingException("Failed to execute blast", ex);

    }
  
    }    
//
    private void uploadExtractedFiles(File[] outputsLocalFile) {
        try {
            getRuntime().getMicrobaseFS().upload(
                         outputsLocalFile[0],
                         outputS3Files[0].getLocation(), null);
                 getRuntime().getMicrobaseFS().upload(
                         outputsLocalFile[1],
                         outputS3Files[1].getLocation(), null);
        } catch (Exception ex) {
            Logger.getLogger(SFFResponder.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}

    
