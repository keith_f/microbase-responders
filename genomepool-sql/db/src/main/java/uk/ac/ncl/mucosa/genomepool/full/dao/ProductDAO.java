/*
 * Copyright 2010 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: 20-Sep-2010, 17:48:17
 */

package uk.ac.ncl.mucosa.genomepool.full.dao;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import uk.ac.ncl.mucosa.genomepool.full.data.Gene;
import uk.ac.ncl.mucosa.genomepool.full.data.GeneProduct;
import uk.ac.ncl.mucosa.genomepool.full.data.GeneProductSynonym;
import uk.ac.ncl.mucosa.genomepool.full.data.ProductAnnotation;

/**
 *
 * @author Keith Flanagan
 */
public class ProductDAO
{

  public ProductDAO()
  {
  }

  public boolean existsByGuid(Connection txn, String guid)
      throws SQLException
  {
    PreparedStatement stmt = null;
    try
    {
      stmt = txn.prepareStatement(
        "SELECT count(*) as count FROM gene_products WHERE guid = ?");
      stmt.setString(1, guid);
      ResultSet rs = stmt.executeQuery();
      rs.next();
      return rs.getInt("count") > 0;
    }
    catch(Exception e)
    {
      throw new SQLException(
          "Failed to execute exists() for GeneProduct: "+guid, e);
    }
    finally
    {
      stmt.close();
    }
  }

  public int count(Connection txn)
      throws SQLException
  {
    PreparedStatement stmt = null;
    try
    {
      stmt = txn.prepareStatement(
        "SELECT count(*) as count FROM gene_products");
      ResultSet rs = stmt.executeQuery();
      rs.next();
      return rs.getInt("count");
    }
    catch(Exception e)
    {
      throw new SQLException(
          "Failed to execute count()", e);
    }
    finally
    {
      stmt.close();
    }
  }

  public int countByOrganism(Connection txn, int taxonId)
      throws SQLException
  {
    PreparedStatement stmt = null;
    try
    {
      stmt = txn.prepareStatement(
        "SELECT count(*) as count FROM gene_products where taxon_id = ?");
      stmt.setInt(1, taxonId);
      ResultSet rs = stmt.executeQuery();
      rs.next();
      return rs.getInt("count");
    }
    catch(Exception e)
    {
      throw new SQLException(
          "Failed to execute count() where taxonid: "+taxonId, e);
    }
    finally
    {
      stmt.close();
    }
  }


  public int countByFragment(Connection txn, String fragmentGuid)
      throws SQLException
  {
    PreparedStatement stmt = null;
    try
    {
      stmt = txn.prepareStatement(
        "SELECT count(*) as count FROM gene_products where fragment_guid = ?");
      stmt.setString(1, fragmentGuid);
      ResultSet rs = stmt.executeQuery();
      rs.next();
      return rs.getInt("count");
    }
    catch(Exception e)
    {
      throw new SQLException(
          "Failed to execute count() where fragment_guid: "+fragmentGuid, e);
    }
    finally
    {
      stmt.close();
    }
  }

  public GeneProduct getByGuid(Connection txn, String guid)
      throws SQLException
  {
    PreparedStatement stmt = null;
    try
    {
      stmt = txn.prepareStatement(
        "SELECT * FROM gene_products WHERE guid = ?");
      stmt.setString(1, guid);
      ResultSet rs = stmt.executeQuery();
      List<GeneProduct> products = resultSetToGeneProducts(txn, rs);
      if (products.size() == 1)
      {
        return products.get(0);
      }
      else if (products.isEmpty())
      {
        return null;
      }
      else
      {
        throw new SQLException(products.size()+" GeneProduct instances had "
            + "the same GUID: "+guid + ". The database is inconsistent.");
      }
    }
    catch(Exception e)
    {
      throw new SQLException(
          "Failed to execute getByGuid()", e);
    }
    finally
    {
      stmt.close();
    }
  }


  public List<GeneProduct> getByTaxonId(Connection txn, int taxonId)
      throws SQLException
  {
    PreparedStatement stmt = null;
    try
    {
      stmt = txn.prepareStatement(
        "SELECT * FROM gene_products WHERE taxon_id = ?");
      stmt.setInt(1, taxonId);
      ResultSet rs = stmt.executeQuery();
      return resultSetToGeneProducts(txn, rs);
    }
    catch(Exception e)
    {
      throw new SQLException(
          "Failed to execute getByTaxonId()", e);
    }
    finally
    {
      stmt.close();
    }
  }


  public GeneProduct findByGi(Connection txn, int gi)
      throws SQLException
  {
    PreparedStatement stmt = null;
    try
    {
      stmt = txn.prepareStatement(
        "SELECT * FROM gene_products WHERE gi = ?");
      stmt.setInt(1, gi);
      ResultSet rs = stmt.executeQuery();
      List<GeneProduct> products = resultSetToGeneProducts(txn, rs);
      if (products.size() == 1)
      {
        return products.get(0);
      }
      else if (products.isEmpty())
      {
        return null;
      }
      else
      {
        throw new SQLException(products.size()+" GenomeFragment instances had "
            + "the same GI: "+gi + ". The database is inconsistent.");
      }
    }
    catch(Exception e)
    {
      throw new SQLException(
          "Failed to execute getByGi()", e);
    }
    finally
    {
      stmt.close();
    }
  }

  public String findProductSequenceByGi(Connection txn, int gi)
      throws SQLException
  {
    PreparedStatement stmt = null;
    try
    {
      stmt = txn.prepareStatement(
        "SELECT product_sequence FROM gene_products WHERE gi = ?");
      stmt.setInt(1, gi);
      ResultSet rs = stmt.executeQuery();
      if (!rs.next())
      {
        throw new SQLException("No such product with gi: "+gi);
      }
      return rs.getString("product_sequence");
    }
    catch(Exception e)
    {
      throw new SQLException(
          "Failed to execute getByGi()", e);
    }
    finally
    {
      stmt.close();
    }
  }

  public List<GeneProduct> findByFragmentGuid(
      Connection txn, String fragmentGuid)
      throws SQLException
  {
    PreparedStatement stmt = null;
    try
    {
      stmt = txn.prepareStatement(
        "SELECT * FROM gene_products WHERE fragment_guid = ?");
      stmt.setString(1, fragmentGuid);
      ResultSet rs = stmt.executeQuery();
      return resultSetToGeneProducts(txn, rs);
    }
    catch(Exception e)
    {
      throw new SQLException(
          "Failed to execute getByFragmentGuid()", e);
    }
    finally
    {
      stmt.close();
    }
  }

  
  public Set<GeneProductSynonym> getGeneProductSynonyms(
      Connection txn, String geneProductGuid)
      throws SQLException
  {
    PreparedStatement stmt = null;
    try
    {
      stmt = txn.prepareStatement(
        "SELECT * FROM gene_product_synonyms WHERE gene_product_guid = ?");
      stmt.setString(1, geneProductGuid);
      ResultSet rs = stmt.executeQuery();
      return resultSetToGeneProductSynonyms(rs);
    }
    catch(Exception e)
    {
      throw new SQLException(
          "Failed to execute getGeneProductSynonyms()", e);
    }
    finally
    {
      stmt.close();
    }
  }

  public Set<ProductAnnotation> getProductAnnotations(
      Connection txn, String geneProductGuidguid)
      throws SQLException
  {
    PreparedStatement stmt = null;
    try
    {
      stmt = txn.prepareStatement(
        "SELECT * FROM product_annotations WHERE gene_product_guid = ?");
      stmt.setString(1, geneProductGuidguid);
      ResultSet rs = stmt.executeQuery();
      return resultSetToGeneProductAnnotations(rs);
    }
    catch(Exception e)
    {
      throw new SQLException(
          "Failed to execute getGeneProductSynonyms()", e);
    }
    finally
    {
      stmt.close();
    }
  }

  /**
   *
   * @param rs a result set of from the gene_products table
   *
   * @throws SQLException
   */
  private List<GeneProduct> resultSetToGeneProducts(
      Connection txn, ResultSet rs)
      throws SQLException
  {
    List<GeneProduct> list = new ArrayList<GeneProduct>();
    while(rs.next())
    {
      GeneProduct prod = new GeneProduct();

      
      prod.setFragmentGuid(rs.getString("fragment_guid"));
      prod.setGeneGuid(rs.getString("gene_guid"));
      prod.setGi(rs.getInt("gi"));
      prod.setGuid(rs.getString("guid"));
      prod.setProductDescr(rs.getString("description"));
      prod.setProductSequence(rs.getString("product_sequence"));
      prod.setProductType(rs.getString("product_type"));
      prod.setProteinId(rs.getString("protein_id"));
      prod.setProteinTagId(rs.getString("tagid"));
      
      prod.setTaxonId(rs.getInt("taxon_id"));

      Set<GeneProductSynonym> synonyms = getGeneProductSynonyms(txn, prod.getGuid());
      prod.setSynonyms(synonyms);
      Set<ProductAnnotation> annotations = getProductAnnotations(txn, prod.getGuid());
      prod.setAnnotations(annotations);

      list.add(prod);
    }
    return list;
  }

  private static Set<GeneProductSynonym> resultSetToGeneProductSynonyms(ResultSet rs)
      throws SQLException
  {
    Set<GeneProductSynonym> list = new HashSet<GeneProductSynonym>();
    while(rs.next())
    {
      GeneProductSynonym syn = new GeneProductSynonym();

      syn.setGeneProductGuid(rs.getString("gene_product_guid"));
      syn.setNote(rs.getString("note"));
      syn.setProduct(rs.getString("product"));
      syn.setSynonym(rs.getString("synonym"));


      list.add(syn);
    }
    return list;
  }

  private static Set<ProductAnnotation> resultSetToGeneProductAnnotations(ResultSet rs)
      throws SQLException
  {
    Set<ProductAnnotation> list = new HashSet<ProductAnnotation>();
    while(rs.next())
    {
      ProductAnnotation ann = new ProductAnnotation();

      ann.setDbReference(rs.getString("gene_product_guid"));
      ann.setGeneProductGuid(rs.getString("dbreference"));
      ann.setIdentifier(rs.getString("identifier"));

      list.add(ann);
    }
    return list;
  }
}
