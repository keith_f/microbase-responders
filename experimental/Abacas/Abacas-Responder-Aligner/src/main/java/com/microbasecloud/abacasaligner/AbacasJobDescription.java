package com.microbasecloud.abacasaligner;

/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */


import java.io.Serializable;
import uk.org.microbase.filesystem.spi.MBFile;

/**
 *
 * @author prisni
 */
public class AbacasJobDescription implements Serializable {
    
    public enum ResultFileType {
    bin, crunch, fasta, gaps, gapstab, fastatab ;
  };
    private ResultFileType jobType;
    private MBFile queryFile;
    private MBFile databaseZipFile;
    private String databaseName;
    
    public AbacasJobDescription()
  {
  }
    
    public ResultFileType getJobType()
  {
    return jobType;
  }

  public void setJobType(ResultFileType jobType)
  {
    this.jobType = jobType;
  }
    
    public MBFile getDatabaseZipFile()
  {
    return databaseZipFile;
  }

  public void setDatabaseZipFile(MBFile databaseZipFile)
  {
    this.databaseZipFile = databaseZipFile;
  }

  public String getDatabaseName()
  {
    return databaseName;
  }

  public void setDatabaseName(String databaseName)
  {
    this.databaseName = databaseName;
  }

  public MBFile getQueryFile()
  {
    return queryFile;
  }

  public void setQueryFile(MBFile queryFile)
  {
    this.queryFile = queryFile;
  }
        
}
