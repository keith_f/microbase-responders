/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.microbasecloud.interproscan;

/**
 *
 * @author sirintra Nakjang
 */
public class InterproscanParserException extends Exception{
    
  public InterproscanParserException(Throwable cause)
  {
    super(cause);
  }
 
  public InterproscanParserException(String message, Throwable cause)
  {
    super(message, cause);
  }
 
  public InterproscanParserException(String message)
  {
    super(message);
  }
 
  public InterproscanParserException()
  {
  }
}
