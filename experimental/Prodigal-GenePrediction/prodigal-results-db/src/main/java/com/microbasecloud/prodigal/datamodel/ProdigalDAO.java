/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.microbasecloud.prodigal.datamodel;

import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author prisni
 */
public class ProdigalDAO {

    private static final int DEFAULT_BATCH = 50000;

    public void storeTranslations(Session session, List<ProdigalTranslation> proteins) throws ProdigalDBException {
        try {
            int count = 0;
            for (ProdigalTranslation pd : proteins) {
                session.save(pd);
                count++;
                if (count % DEFAULT_BATCH == 0) {
                    session.flush();
                    session.clear();
                }

            }


        } catch (Exception e) {
            throw new ProdigalDBException("Failed to store", e);
        }
    }

    public void storeCdss(Session session, List<ProdigalCDS> cdss) throws ProdigalDBException {
        try {
            int count = 0;
            for (ProdigalCDS cd : cdss) {
                session.save(cd);
                count++;
                if (count % DEFAULT_BATCH == 0) {
                    session.flush();
                    session.clear();
                }

            }
        } catch (Exception e) {
            throw new ProdigalDBException("Failed to store", e);
        }
    }
    
    public List<ProdigalCDS> listCdssbySeqGi(Session session, long seqGi) throws ProdigalDBException{
        try{
            String queryString = "from "+ProdigalCDS.class.getName()+" where parentSequence.giNum = :gi";
            Query query = session.createQuery(queryString);
            query.setLong("gi", seqGi);
            List<ProdigalCDS> results = query.list();
            return results;
        }catch (Exception e){
            throw new ProdigalDBException("Failed to retrieve CDS", e);
        }
        
        
    }
    
    public ProdigalCDS findCDS(Session session, String id) throws ProdigalDBException {
        try {
            String queryString = "from " + ProdigalCDS.class.getName() + " where uniqueID = :id";
            Query query = session.createQuery(queryString);
            query.setString("id", id);
            List<ProdigalCDS> results = query.list();
            return results.isEmpty() ? null : results.iterator().next();
        } catch (Exception e) {
            throw new ProdigalDBException("Failed to find CDS", e);
        }

    }
}
