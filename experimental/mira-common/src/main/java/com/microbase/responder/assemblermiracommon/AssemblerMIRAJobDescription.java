/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.microbase.responder.assemblermiracommon;

import java.io.Serializable;
import uk.org.microbase.filesystem.spi.MBFile;

    
/**
 *
 * @author prisni
 */
public class AssemblerMIRAJobDescription implements Serializable {

   
    private MBFile fastQFile;
    private MBFile traceInfoXmlFile;
    private MBFile outputFiles;
   

    public AssemblerMIRAJobDescription() {
        
    }

    public MBFile getFastQFile() {
        return fastQFile;
    }

    public void setFastQFile(MBFile fastQFile) {
        this.fastQFile = fastQFile;
    }

    public MBFile getTraceInfoXmlFile() {
        return traceInfoXmlFile;
    }

    public void setTraceInfoXmlFile(MBFile traceInfoXmlFile) {
        this.traceInfoXmlFile = traceInfoXmlFile;
    }
    
    public MBFile getOutputFiles() {    
        return outputFiles;
    }

    public void setOutputFiles(MBFile outputFiles) {
        this.outputFiles = outputFiles;
    }

    
    
}
