/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.microbasecloud.tmhmm;

import com.torrenttamer.hibernate.HibernateUtilException;
import com.torrenttamer.hibernate.SessionProvider;
import com.torrenttamer.util.UidGenerator;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.HashSet;
import java.util.Set;
import java.util.StringTokenizer;
import org.hibernate.Session;

/**
 *
 * @author Sirintra Nakjang
 */
public class TmhmmTblOutParser {
    
  public TmhmmTblOutParser()
  {
 
  }
 
  /**
   * Use this method for testing the parser from the command line
   */
  public static void main(String[] args)
      throws TmhmmParserException, HibernateUtilException
  {
    if (args.length != 1)
    {
      System.out.println("USAGE: <tmhmm inputfile>");
      System.exit(1);
    }
    String name = args[0];
 
    File file = new File(name);
    parse(file);
  }
  
  public static void parse(File file) throws TmhmmParserException, HibernateUtilException
  { 
    TmhmmTblOutParser parser = new TmhmmTblOutParser();
    Set<TmhmmHit> hits = parser.parseFromFile(file);
 
    System.out.println("Hits found: "+hits.size());
    System.out.println("Going to upload hits to specified database");
    SessionProvider tmhmmSessionProvider =
      new SessionProvider("/TmhmmResponder-hibernate.cfg.xml");
 
    // Create a parent report object. Since we're not running this through
    // Microbase, just create a partial report object containing a unique ID
    // and a filename.
    TmhmmReport report = new TmhmmReport();
    report.setGuid(UidGenerator.generateUid()); //Unique ID for this Tmhmm report
    report.setSourceFileName(file.getName()); //Record name of result file
 
    //Associate all this parsed hits with this report
    for (TmhmmHit hit : hits)
    {
      hit.setReport(report);
    }
 
    /*
     * Store report and all associated HmmerHit objects to the database.
     */
    Session session = null;
    try
    {
      session = tmhmmSessionProvider.getWriteableSession();
      session.beginTransaction();
      TmhmmHitDAO dao = new TmhmmHitDAO();
      dao.store(session, hits); //Stores the HmmerHit instances as well as the HmmerReport
      System.out.println("Committing...");
      session.getTransaction().commit();
      System.out.println("Committed.");
    }
    catch(Exception e)
    {
      SessionProvider.silentRollback(session);
      throw new TmhmmParserException("Failed to save "
        + hits.size()+" hits to the DB", e);
    }
    finally
    {
      SessionProvider.silentClose(session);
    }
 
    System.out.println("Done.");
  }
 
  public Set<TmhmmHit> parseFromFile(File tmhmmReport)
      throws TmhmmParserException
  {
    try
    {
      FileInputStream fis = new FileInputStream(tmhmmReport);
      return parse(fis);
    }
    catch(Exception e)
    {
      throw new TmhmmParserException(
          "Failed to parse TMHMM output from file: "+tmhmmReport.getAbsolutePath(), e);
    }
  }
  public Set<TmhmmHit> parse(InputStream is) throws TmhmmParserException
  {
    try
    {
      Set<TmhmmHit> hits = new HashSet<TmhmmHit>();
      BufferedReader br = new BufferedReader(new InputStreamReader(is));
      String line;
      while ((line = br.readLine()) != null)
      {
        if (line.startsWith("#"))
        {
          continue; //Skip comment lines
        }
        else if (line.trim().length() == 0)
        {
          continue; //Skip blank lines
        }
        TmhmmHit hit = parseHitLine(line);
        hits.add(hit);
      }
      return hits;
    }
    catch(Exception e)
    {
      throw new TmhmmParserException("Failed to parse content", e);
    }
  }
  
  private TmhmmHit parseHitLine(String line)
  {
    StringTokenizer st = new StringTokenizer(line, "\t=");
    int count = st.countTokens();
//    System.out.println("Found "+count+" tokens");
    String queryAcc = st.nextToken();
    st.nextToken();
    int seqlength = Integer.parseInt(st.nextToken());
    st.nextToken();
    float expaa = Float.parseFloat(st.nextToken());
    st.nextToken();
    float first60 = Float.parseFloat(st.nextToken());
    st.nextToken();
    int prehel = Integer.parseInt(st.nextToken());
    st.nextToken();
    String topology = st.nextToken();        
    
    TmhmmHit hit = new TmhmmHit();
    hit.setQueryAcc(queryAcc);
    hit.setSeqlength(seqlength);
    hit.setExpectedAminoAcid(expaa);
    hit.setFirst60(first60);
    hit.setPredictedHelix(prehel);
    hit.setTopology(topology);
  
    return hit;
  }
    
}
