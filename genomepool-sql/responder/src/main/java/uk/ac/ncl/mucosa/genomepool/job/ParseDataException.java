/*
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU Lesser General Public License as published 
 * by the Free Software Foundation. The full license may be found in
 * COPYING.LESSER in this project's root directory.
 * 
 * Copyright 2007 jointly held by the authors listed at the top of each
 * source file and/or their respective employers.
 */
package uk.ac.ncl.mucosa.genomepool.job;

/**
 * 
 ** @author Sirintra Nakjang
 */
public class ParseDataException extends Exception
{

  public ParseDataException()
  {
  }

  public ParseDataException(String message)
  {
    super(message);
  }

  public ParseDataException(Throwable cause)
  {
    super(cause);
  }

  public ParseDataException(String message, Throwable cause)
  {
    super(message, cause);
  }

}
