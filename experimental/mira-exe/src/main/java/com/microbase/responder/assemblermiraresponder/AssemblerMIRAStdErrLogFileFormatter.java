/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.microbase.responder.assemblermiraresponder;

import com.microbase.responder.assemblermiracommon.AssemblerMIRAJobDescription;
import uk.org.microbase.filesystem.spi.MBFile;

/**
 *
 * @author prisni
 */
public class AssemblerMIRAStdErrLogFileFormatter extends AbstractOutputPathReformatter
    implements AssemblerMIRAOutputPathReformatter {
    
    private AssemblerMIRAJobDescription jobDescription;
  
  @Override
  public String getFormattedBucketName() {
    return responderInfo.getLogDestinationBucket();
  }

  @Override
  public String getFormattedPath() {
    StringBuilder path = new StringBuilder(responderInfo.getLogDestinationBasePath());
    path.append("/").append(getFormattedFilename());
    path.append("/").append(message.getGuid());
    path.append("/").append(processInfo.getWorkStartedAtMs());
    return path.toString();
  }

  @Override
  public String getFormattedFilename() {
    StringBuilder resultFilename = new StringBuilder();
//    resultFilename.append(jobDescription.getInputFiles().getName())
//        .append("-").append(jobDescription.getDatabaseName())
//        .append(".").append(jobDescription.getJobType().name())
//        .append(".stderr");
    return resultFilename.toString();
  }
  
  @Override
  public MBFile getFormattedRemoteFile() {
    MBFile file = new MBFile(getFormattedBucketName(), getFormattedPath(), getFormattedFilename());
    return file;
  }
  

  public AssemblerMIRAJobDescription getJobDescription() {
    return jobDescription;
  }

  @Override
  public void setJobDescription(AssemblerMIRAJobDescription jobDescription) {
    this.jobDescription = jobDescription;
    
}
    
}
