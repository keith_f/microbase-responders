/*
 * Copyright 2011 Keith Flanagan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 * File created: 20-Sep-2010, 17:48:17
 */

package uk.ac.ncl.mucosa.genomepool.notification;

import java.util.HashSet;
import java.util.Set;
import uk.org.microbase.notification.data.Message;
import uk.org.microbase.util.serialization.JsonSerializer;
import uk.org.microbase.util.serialization.MbSerializerException;

/**
 *
 * @author Keith Flanagan
 */
public class FaaFileParsedNotification
    extends NewFaaFileNotification
{
  private static final String PROTEIN_GIS = "protein_gis";

  public FaaFileParsedNotification(Message message)
  {
    super(message);
  }

  public Set<String> getProteinGis() throws MbSerializerException
  {
    String serialised = message.getContent().get(PROTEIN_GIS);
//    System.out.println("Serialised: "+serialised);
    Set<String> proteinGis = (Set<String>)
        JsonSerializer.deserializeFromString(serialised, Set.class);
    return proteinGis;
  }

  public void setProteinGis(Set<String> proteinGis) throws MbSerializerException
  {
    String content = JsonSerializer.serializeToString(proteinGis);
    message.getContent().put(PROTEIN_GIS, content);
  }

  public static void main(String[] args) throws MbSerializerException
  {
    Set<String> test = new HashSet<String>();
    test.add("foo");
    test.add("bar");
    test.add("baz");

    FaaFileParsedNotification msg= new FaaFileParsedNotification(new Message());
    msg.setProteinGis(test);

    System.out.println(msg.getProteinGis());
  }
}
