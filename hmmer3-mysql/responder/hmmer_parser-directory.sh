#!/bin/bash

if [ -z $1 ] ; then
  echo "Usage:"
  echo "hmmer_parser-directory.sh /path/to/result/directory"
  exit 1
fi

result_dir=$1

for file in `ls $result_dir` ; do
  echo $result_dir/$file
  ./hmmer_parser.sh $result_dir/$file
done

