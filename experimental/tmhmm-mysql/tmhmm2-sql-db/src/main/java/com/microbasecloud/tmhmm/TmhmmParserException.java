/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.microbasecloud.tmhmm;

/**
 *
 * @author sirintra Nakjang
 */
public class TmhmmParserException extends Exception{
    
  public TmhmmParserException(Throwable cause)
  {
    super(cause);
  }
 
  public TmhmmParserException(String message, Throwable cause)
  {
    super(message, cause);
  }
 
  public TmhmmParserException(String message)
  {
    super(message);
  }
 
  public TmhmmParserException()
  {
  }
}
