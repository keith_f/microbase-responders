/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: 10-May-2012, 22:38:24
 */

package uk.ac.ncl.mucosa.genomepool.job.fastq;

import java.io.*;
import org.biojava.bio.Annotation;
import org.biojava.bio.seq.*;
import org.biojava.bio.seq.io.SymbolTokenization;
import org.biojava.bio.symbol.AlphabetManager;
import org.biojava.bio.symbol.SymbolList;
import org.biojavax.bio.seq.RichSequence;
import uk.org.microbase.util.file.FileUtils;

/**
 * Adapted from: http://biojava.org/wiki/BioJava:Cookbook:Translation:SixFrames
 * 
 * Uses BioJava to produce a 6-frame translation from an input File or
 * InputStream.
 * 
 * @author Keith Flanagan
 */
public class SixFrameTranslateUtil
{
  public static void translateSixFrames(File dnaInFile, File proteinOutFile)
      throws IOException
  {
    InputStream is = null;
    OutputStream os  = null;
    try
    {
      is = new FileInputStream(dnaInFile);
      os = new FileOutputStream(proteinOutFile);
    
      translateSixFrames(is, os);
    }
    finally
    {
      FileUtils.closeOutputStreams(os);
      FileUtils.closeInputStreams(is);
    }
  }
  
  /**
   * Given an InputStream that provides FASTA-formatted DNA sequences,
   * performs a 6-frame translation and writes output to the specified
   * OutputStream.
   * 
   * @param dnaIS the InputStream to use containing the FASTA-formatted DNA
   * sequences to translate.
   * @param translationOS an OutputStream to use for the translated output.
   * 
   * @throws IOException 
   */
  public static void translateSixFrames(
      InputStream dnaIS, OutputStream translationOS) 
      throws IOException
  {
    try
    {
      String type = "DNA";
      SymbolTokenization toke = AlphabetManager.alphabetForName(type).getTokenization("token");

      BufferedReader br = new BufferedReader(new InputStreamReader(dnaIS));
      

      SequenceIterator seqi = RichSequence.IOTools.readFasta(br,
          toke, null);

      // for each sequence
      while (seqi.hasNext())
      {
        Sequence seq = seqi.nextSequence();

        StringBuilder fastaTranslations = new StringBuilder();
        
        // for each frame
        for (int i = 0; i < 3; i++)
        {
          SymbolList prot;
          Sequence trans;

          // take the reading frame
          // remember that in a SymbolList the first element has index= 1
          // remember that if the length of the list evenly divisible
          // by three an IllegalArgumentException will be thrown
          SymbolList syms = seq.subList(i + 1,
              seq.length() - (seq.length() - i) % 3);

          // if it is DNA transcribe it to RNA
          if (syms.getAlphabet() == DNATools.getDNA())
          {
            syms = DNATools.toRNA(syms);
          }

          // output forward translation to STDOUT
          prot = RNATools.translate(syms);
          trans = SequenceTools.createSequence(prot, "", 
              seq.getName() + "_frame_+" + i,
              Annotation.EMPTY_ANNOTATION);
          /*
           * This method is deprecated since BioJava 1.5
           * SeqIOTools.writeFasta(System.out, trans);
           */
//          RichSequence.IOTools.writeFasta(translationOS, trans, null);
          fastaTranslations.append(">").append(trans.getName()).append("\n");
          fastaTranslations.append(trans.seqString()).append("\n");
          
          

          // output reverse frame translation to STDOUT
          syms = RNATools.reverseComplement(syms);
          prot = RNATools.translate(syms);
          trans = SequenceTools.createSequence(prot, "", 
              seq.getName() + "_frame_-" + i,
              Annotation.EMPTY_ANNOTATION);
          /*
           * This method is deprecated since BioJava 1.5
           * SeqIOTools.writeFasta(System.out, trans);
           */
//          RichSequence.IOTools.writeFasta(translationOS, trans, null);
          fastaTranslations.append(">").append(trans.getName()).append("\n");
          fastaTranslations.append(trans.seqString()).append("\n");
        }
        
        translationOS.write(fastaTranslations.toString().getBytes());
      }
      translationOS.flush();
    }
    catch (Exception e)
    {
      throw new IOException("Failed to perform a 6-frame translation", e);
    }
  }
}
