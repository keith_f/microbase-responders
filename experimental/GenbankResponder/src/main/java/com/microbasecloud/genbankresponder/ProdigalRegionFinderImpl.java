/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.microbasecloud.genbankresponder;

import com.microbasecloud.prodigal.datamodel.ProdigalCDS;
import com.microbasecloud.prodigal.datamodel.ProdigalDAO;
import com.microbasecloud.prodigal.datamodel.ProdigalDBException;
import com.torrenttamer.hibernate.HibernateUtilException;
import com.torrenttamer.hibernate.SessionProvider;
import java.util.HashSet;
import java.util.Set;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.hibernate.Session;

/**
 *
 * @author prisni
 */
public class ProdigalRegionFinderImpl implements RegionFinder{
    private SessionProvider prodigalRegionFinderProvider; 
    ProdigalDAO dao;

    @Override
    public Set<String> findRegionOf(String fragmentId) throws FeatureTableAnnotatorException {
        try {
            Session session = prodigalRegionFinderProvider.getReadOnlySession();
            ProdigalCDS regionInfo = dao.findCDS(session, fragmentId);
            Set<String> hset = new HashSet<String> ();
            hset.add(regionInfo.getCdsStartLoc());
            hset.add(regionInfo.getCdsEndLoc());
            return hset;
        } catch (HibernateUtilException | ProdigalDBException ex) {
            throw new FeatureTableAnnotatorException("Failed to get annotations from the prodigal database: ", ex);
        }
    }

    @Override
    public void initialize() throws FeatureTableAnnotatorException {
        try {        
            prodigalRegionFinderProvider = new SessionProvider("/ProdigalResponder-hibernate.cfg.xml");
            dao = new ProdigalDAO();
            
        } catch (HibernateUtilException ex) {
            throw new FeatureTableAnnotatorException("Could not connect to the prodigal database: ", ex);
        }
    }
    
}
