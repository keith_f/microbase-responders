/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.microbasecloud.abacasaligner;

/**
 *
 * @author prisni
 */
public interface OutputFilePathReformatter {
    
  public String getBucket(String configuredBucketName, String configuredPath, String outputFilename);
  public String getPath(String configuredBucketName, String configuredPath, String outputFilename);
  public String getName(String configuredBucketName, String configuredPath, String outputFilename);
  

    
}
