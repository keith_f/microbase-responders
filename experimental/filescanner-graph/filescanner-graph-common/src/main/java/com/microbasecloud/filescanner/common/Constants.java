/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: 13-Oct-2012, 22:26:24
 */

package com.microbasecloud.filescanner.common;

/**
 *
 * @author Keith Flanagan
 */
public class Constants
{
  /**
   * Database configuration resource for this responder.
   */
  public static final String HIBERNATE_RESOURCE =
      "/FileScannerResponder-hibernate.cfg.xml";
  
  /**
   * The name of a distributed Map used for storing the timestamps of the last
   * time specific remote targets have been scanned.
   */
  public static final String HZ_LAST_RUN_MAP = "FileScannerResponder.last_runs";
  
  /**
   * Used as a message content 'key' for notification messages about individual
   * file events.
   */
  public static final String MSG_KEY_NEW_FILE_STATE = "file_state_changed.new";
  
  /**
   * Used as a message content 'key' for notification messages about individual
   * file events.
   */
  public static final String MSG_KEY_PREV_FILE_STATE = "file_state_changed.previous";
}
