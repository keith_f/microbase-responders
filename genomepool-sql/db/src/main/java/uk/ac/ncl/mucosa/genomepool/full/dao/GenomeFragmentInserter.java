/*
 * Copyright 2011 Keith Flanagan
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */

package uk.ac.ncl.mucosa.genomepool.full.dao;


import com.torrenttamer.jdbcutils.JdbcUtils;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.logging.Logger;
import uk.ac.ncl.mucosa.genomepool.full.data.Gene;
import uk.ac.ncl.mucosa.genomepool.full.data.GeneProduct;
import uk.ac.ncl.mucosa.genomepool.full.data.GeneProductSynonym;
import uk.ac.ncl.mucosa.genomepool.full.data.GeneSynonym;
import uk.ac.ncl.mucosa.genomepool.full.data.GenomeFragment;
import uk.ac.ncl.mucosa.genomepool.full.data.ProductAnnotation;

/**
 * Performs the complex task of inserting a genome fragment into a database.
 * 
 * @author Keith Flanagan
 */
public class GenomeFragmentInserter
{
  private static final Logger logger =
      Logger.getLogger(GenomeFragmentInserter.class.getName());

//  public static void insert(Connection txn, 
//      String messageGuid,
//      GenomeFragment frag,
//      List<Gene> genes,
//      String dnaData) throws SQLException
//  {
//      insertGenomeFragment(txn, frag);
//      insertGenes(txn, genes);
//      insertDnaData(txn, frag.getTaxonId(), frag.getGuid(), dnaData);
//      insertNotificationDataMapping(txn, messageGuid, frag);
//  }
  
  public static void insert(Connection txn,
      String messageGuid, String workflowExeId, String workflowStepId,
      String fileBucket, String filePath, String fileName,
      GenomeFragment frag,
      List<Gene> genes,
      String dnaData) throws SQLException
  {
    int workflowId = 
        insertMsgDataMappingEntry(
        txn, messageGuid, workflowExeId, workflowStepId,
        fileBucket, filePath, fileName);
    
    insertGenomeFragment(txn, workflowId, frag);
    insertGenes(txn, genes);
    insertDnaData(txn, frag.getTaxonId(), frag.getGuid(), dnaData);
//    insertNotificationDataMapping(txn, messageGuid, frag);
  }
  
  /**
   * Used to delete data items relating to a specific workflow execution / step.
   * For example, if a message needs to be re-processed, then all data items
   * relating to a previous processing attempt need to be deleted. 
   * 
   * @param txn
   * @param workflowExeId
   * @param workflowStepId
   * @return 
   */
  public static int deleteMappingEntry(Connection txn, 
      String workflowExeId, String workflowStepId)
      throws SQLException
  {
    PreparedStatement stmt = null;

    try
    {
      final String sql =
          "DELETE FROM resultset_mapping "
          + "WHERE workflow_exe_id = ? AND workflow_step_id = ?";


      stmt = txn.prepareStatement(sql);
      int col = 1;
      stmt.setString(col++, workflowExeId);
      stmt.setString(col++, workflowStepId);

      int rowCount = stmt.executeUpdate();
      return rowCount;
    }
    catch(SQLException e)
    {
      JdbcUtils.reportEmbeddedSQLException(e);
      throw new SQLException(
          "Failed to delete entries relating to: "
            + workflowExeId + ", " + workflowStepId, e);
    }
    finally
    {
//      PostgresUtils.silentClose(stmt);
      JdbcUtils.silentClose(stmt);
    }
  }
  
  private static int insertMsgDataMappingEntry(Connection txn, 
      String messageGuid, String workflowExeId, String workflowStepId,
      String fileBucket, String filePath, String fileName)
      throws SQLException
  {
    PreparedStatement stmt = null;

    try
    {
      final String sql =
          "INSERT INTO resultset_mapping ("
          + "msg_guid, workflow_exe_id, workflow_step_id, "
          + "file_bucket, file_path, file_name) "
          + "VALUES (?,?,?,?,?,?)";


      stmt = txn.prepareStatement(sql, PreparedStatement.RETURN_GENERATED_KEYS);
      int col = 1;
      stmt.setString(col++, messageGuid);
      stmt.setString(col++, workflowExeId);
      stmt.setString(col++, workflowStepId);
      stmt.setString(col++, fileBucket);
      stmt.setString(col++, filePath);
      stmt.setString(col++, fileName);
      

      int rowCount = stmt.executeUpdate();
      if (rowCount != 1)
      {
        throw new SQLException("Failed to insert resultset_mapping entry");
      }
      ResultSet keySet = stmt.getGeneratedKeys(); //Requires RETURN_GENERATED_KEYS
      if (keySet.next())
      {
        int id = keySet.getInt(1);
        return id;
      }
      else
      {
        throw new SQLException("Failed to get auto-generated database ID");
      }
    }
    catch(SQLException e)
    {
      JdbcUtils.reportEmbeddedSQLException(e);
      throw new SQLException("Failed to insert GenomeFragment entry", e);
    }
    finally
    {
//      PostgresUtils.silentClose(stmt);
//      stmt.close();
      JdbcUtils.silentClose(stmt);
    }
  }

  /**
   * Inserts a newly parsed GenomeFragment into the database. 
   * @param txn
   * @param workflowId an ID from the resultset_mapping table. This represents
   * the combined workflow execution ID and step ID
   * @param frag
   * @throws SQLException 
   */
  private static void insertGenomeFragment(Connection txn, 
      int workflowId, GenomeFragment frag)
      throws SQLException
  {
    PreparedStatement stmt = null;

    try
    {
      final String sql =
          "insert into genome_fragments ("
          + "workflow_id, "
          + "guid, gi, taxon_id, organism_name, "
          + "organism_short_name, accession, version, description, "
          + "comment, number_protein, fragment_type, fragment_subtype, "
          + "dna_sequence_length) "
          + "values(?,?,?,?,?,?,?,?,?,?,?,?,?,?)";


      stmt = txn.prepareStatement(sql);
      int col = 1;
      stmt.setInt(col++, workflowId);
      stmt.setString(col++, frag.getGuid());
      stmt.setInt(col++, frag.getGiNumber());
      stmt.setInt(col++, frag.getTaxonId());
      stmt.setString(col++, frag.getOrganismName());
      stmt.setString(col++, frag.getOrganismShortName());
      stmt.setString(col++, frag.getAccession());
      stmt.setString(col++, frag.getVersion());
      stmt.setString(col++, frag.getDescription());
      stmt.setString(col++, frag.getComment());
      stmt.setInt(col++, frag.getNumberProtein());
      stmt.setString(col++, frag.getFragmentType().toString());
      stmt.setString(col++, frag.getFragmentSubtype());
      stmt.setInt(col++, frag.getDnaSequenceLength());

      stmt.execute();
    }
    catch(SQLException e)
    {
      JdbcUtils.reportEmbeddedSQLException(e);
      throw new SQLException("Failed to insert GenomeFragment entry", e);
    }
    finally
    {
//      PostgresUtils.silentClose(stmt);
//      stmt.close();
      JdbcUtils.silentClose(stmt);
    }
  }

  private static void insertDnaData(Connection txn,
      int taxonId, String fragmentGuid, String dnaData)
      throws SQLException
  {
    PreparedStatement stmt = null;

    try
    {
      final String sql =
          "insert into dna_sequences ("
          + "taxon_id, fragment_guid, dna_sequence) "
          + "values(?,?,?)";


      stmt = txn.prepareStatement(sql);
      int col = 1;
      stmt.setInt(col++, taxonId);
      stmt.setString(col++, fragmentGuid);
      stmt.setString(col++, dnaData);

      stmt.execute();
    }
    catch(SQLException e)
    {
      JdbcUtils.reportEmbeddedSQLException(e);
      throw new SQLException("Failed to insert DNA data entry", e);
    }
    finally
    {
//      PostgresUtils.silentClose(stmt);
//      stmt.close();
      JdbcUtils.silentClose(stmt);
    }
  }

  private static void insertGenes(Connection txn, List<Gene> genes)
      throws SQLException
  {
    PreparedStatement stmt = null;

    try
    {
      final String sql =
          "insert into genes ("
          + "guid, gene_id, gene_name, "
          + "taxon_id, fragment_guid, "
          + "locus_tag, start_locus, end_locus, orientation) "
          + "values(?,?,?,?,?,?,?,?,?)";

      stmt = txn.prepareStatement(sql);

      Set<GeneProduct> productSuperSet = new HashSet<GeneProduct>();
      Set<GeneSynonym> synonymSuperSet = new HashSet<GeneSynonym>();
      for (Gene gene : genes)
      {
        int col = 1;
        stmt.setString(col++, gene.getGuid());
        stmt.setString(col++, gene.getGeneId());
        stmt.setString(col++, gene.getGeneName());
        stmt.setInt(col++, gene.getTaxonId());
        stmt.setString(col++, gene.getFragmentGuid());
        stmt.setString(col++, gene.getLocusTag());
        stmt.setInt(col++, gene.getStartLocus());
        stmt.setInt(col++, gene.getEndLocus());
        stmt.setString(col++, gene.getOrientation());

        stmt.addBatch();

        //Prepare child objects for bulk insertion later
        productSuperSet.addAll(gene.getProducts());
        synonymSuperSet.addAll(gene.getSynonyms());
      }
      stmt.executeBatch();

      //finally, bulk-insert all collection objects
      insertGeneProducts(txn, productSuperSet);
      insertGeneSynonyms(txn, synonymSuperSet);
    }
    catch(SQLException e)
    {
      JdbcUtils.reportEmbeddedSQLException(e);
      throw new SQLException("Failed to insert Gene entries", e);
    }
    finally
    {
//      PostgresUtils.silentClose(stmt);
//      stmt.close();
      JdbcUtils.silentClose(stmt);
    }
  }

  private static void insertGeneProducts(Connection txn, Set<GeneProduct> products)
      throws SQLException
  {
    PreparedStatement stmt = null;

    try
    {
      final String sql =
          "insert into gene_products ("
          + "guid, gene_guid, gi, protein_id, tagid, "
          + "taxon_id, fragment_guid, "
          + "product_type, description, product_sequence) "
          + "values(?,?,?,?,?,?,?,?,?,?)";

      stmt = txn.prepareStatement(sql);

      Set<ProductAnnotation> annotationSuperSet = new HashSet<ProductAnnotation>();
      Set<GeneProductSynonym> synonymSuperSet = new HashSet<GeneProductSynonym>();
      for (GeneProduct product : products)
      {
        int col = 1;
        stmt.setString(col++, product.getGuid());
        stmt.setString(col++, product.getGeneGuid());
        stmt.setInt(col++, product.getGi());
        stmt.setString(col++, product.getProteinId());
        stmt.setString(col++, product.getProteinTagId());
        stmt.setInt(col++, product.getTaxonId());
        stmt.setString(col++, product.getFragmentGuid());
        stmt.setString(col++, product.getProductType());
        stmt.setString(col++, product.getProductDescr());
        stmt.setString(col++, product.getProductSequence());

        stmt.addBatch();

        //Prepare child objects for bulk insertion later
        annotationSuperSet.addAll(product.getAnnotations());
        synonymSuperSet.addAll(product.getSynonyms());
      }
      stmt.executeBatch();

      //finally, bulk-insert all collection objects
      insertGeneProductAnnotations(txn, annotationSuperSet);
      insertGeneProductSynonyms(txn, synonymSuperSet);
    }
    catch(SQLException e)
    {
      JdbcUtils.reportEmbeddedSQLException(e);
      throw new SQLException("Failed to insert GeneProduct entries", e);
    }
    finally
    {
//      PostgresUtils.silentClose(stmt);
//      stmt.close();
      JdbcUtils.silentClose(stmt);
    }
  }

  private static void insertGeneProductAnnotations(
      Connection txn, Set<ProductAnnotation> annotations)
      throws SQLException
  {
    PreparedStatement stmt = null;

    try
    {
      final String sql =
          "insert into product_annotations ("
          + "gene_product_guid, dbreference, identifier)"
          + "values(?,?,?)";

      stmt = txn.prepareStatement(sql);

      for (ProductAnnotation ann : annotations)
      {
        int col = 1;
        stmt.setString(col++, ann.getGeneProductGuid());
        stmt.setString(col++, ann.getDbReference());
        stmt.setString(col++, ann.getIdentifier());

        stmt.addBatch();
      }
      stmt.executeBatch();
    }
    catch(SQLException e)
    {
      JdbcUtils.reportEmbeddedSQLException(e);
      throw new SQLException("Failed to insert ProductAnnotation entries", e);
    }
    finally
    {
//      PostgresUtils.silentClose(stmt);
//      stmt.close();
      JdbcUtils.silentClose(stmt);
    }
  }

  private static void insertGeneProductSynonyms(
      Connection txn, Set<GeneProductSynonym> synonyms)
      throws SQLException
  {
    PreparedStatement stmt = null;

    try
    {
      final String sql =
          "insert into gene_product_synonyms ("
          + "gene_product_guid, synonym, product, note)"
          + "values(?,?,?,?)";

      stmt = txn.prepareStatement(sql);

      for (GeneProductSynonym productSynonym : synonyms)
      {
        int col = 1;
        stmt.setString(col++, productSynonym.getGeneProductGuid());
        stmt.setString(col++, productSynonym.getSynonym());
        stmt.setString(col++, productSynonym.getProduct());
        stmt.setString(col++, productSynonym.getNote());

        stmt.addBatch();
      }
      stmt.executeBatch();
    }
    catch(SQLException e)
    {
      JdbcUtils.reportEmbeddedSQLException(e);
      throw new SQLException("Failed to insert GeneProductSynonym entries", e);
    }
    finally
    {
//      PostgresUtils.silentClose(stmt);
//      stmt.close();
      JdbcUtils.silentClose(stmt);
    }
  }

  private static void insertGeneSynonyms(
      Connection txn, Set<GeneSynonym> geneSynonyms)
      throws SQLException
  {
    PreparedStatement stmt = null;

    try
    {
      final String sql =
          "insert into gene_synonyms ("
          + "gene_guid, synonym_type, synonym) "
          + "values(?,?,?)";

      stmt = txn.prepareStatement(sql);

      for (GeneSynonym geneSynonym : geneSynonyms)
      {
        int col = 1;
        stmt.setString(col++, geneSynonym.getGeneGuid());
        stmt.setString(col++, geneSynonym.getSynonymType().toString());
        stmt.setString(col++, geneSynonym.getSynonym());

        stmt.addBatch();
      }
      stmt.executeBatch();
    }
    catch(SQLException e)
    {
      JdbcUtils.reportEmbeddedSQLException(e);
      throw new SQLException("Failed to insert Gene entries", e);
    }
    finally
    {
//      PostgresUtils.silentClose(stmt);
//      stmt.close();
      JdbcUtils.silentClose(stmt);
    }
  }
  
//  private static void insertNotificationDataMapping(
//      Connection txn, String messageGuid, GenomeFragment frag)
//      throws SQLException
//  {
//    if (messageGuid == null)
//    {
//      logger.info("Not storing a notification message -> fragment ID mapping "
//          + "for fragment: "+frag.getGuid()+", because the notification "
//          + "message ID was NULL.");
//      return;
//    }
//    
//    PreparedStatement stmt = null;
//
//    try
//    {
//      final String sql =
//          "INSERT INTO notification_data_mapping ("
//          + "message_guid, fragment_guid) "
//          + "VALUES(?,?)";
//
//      stmt = txn.prepareStatement(sql);
//      
//      int col = 1;
//      stmt.setString(col++, messageGuid);
//      stmt.setString(col++, frag.getGuid());
//      stmt.execute();
//    }
//    catch(SQLException e)
//    {
//      JdbcUtils.reportEmbeddedSQLException(e);
//      throw new SQLException(
//          "Failed to insert notification data mapping entry", e);
//    }
//    finally
//    {
////      stmt.close();
//      JdbcUtils.silentClose(stmt);
//    }
//  }

}
