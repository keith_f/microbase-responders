/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: Mar 9, 2012, 4:14:16 PM
 */

package uk.ac.ncl.mucosa.genomepool.job.gbk;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;
import java.util.logging.Logger;
import uk.ac.ncl.mucosa.genomepool.full.data.Gene;
import uk.ac.ncl.mucosa.genomepool.full.data.GeneProduct;
import uk.ac.ncl.mucosa.genomepool.full.data.GenomeFragment;

/**
 *
 * @author Keith Flanagan
 */
public class FastaUtils
{
  private static final Logger logger =
      Logger.getLogger(FastaUtils.class.getName());

  public static void writeDnaFastaFile(GbkParsedData data, File file)
      throws IOException
  {
    BufferedWriter bw = new BufferedWriter(new FileWriter(file));

    GenomeFragment frag = data.getGenomeFragment();
    //Version string is accession.version
    String version = frag.getVersion();
    String gi = String.valueOf(frag.getGiNumber());

    String seq = data.getDnaSequence();

    logger.info("Writing DNA sequence to fasta file:");
    //bw.append(">").append(gi).append("|").append(version).append("\n");
    if (gi != null)
    {
      bw.append(">").append(gi).append("\n");
    }
    else
    {
      bw.append(">").append(version).append("\n");
    }
    bw.append(seq);
    bw.append("\n");

    bw.flush();
    bw.close();
  }

  public static void writeProteinFastaFile(GbkParsedData data, File file)
      throws IOException
  {
    BufferedWriter bw = new BufferedWriter(new FileWriter(file));

    GenomeFragment frag = data.getGenomeFragment();
    List<Gene> genes = data.getGenes();
    for(Gene gene : genes)
    {
      for (GeneProduct product : gene.getProducts())
      {
        String tagId = product.getProteinTagId();
        String gi = String.valueOf(product.getGi());
        String seq = product.getProductSequence();

        if (seq != null)
        {
          //bw.append(">").append(tagId).append("|").append(gi).append("\n");
          if (gi != null)
          {
            bw.append(">").append(tagId).append("\n");
          }
          else
          {
            bw.append(">").append(gi).append("\n");
          }
          bw.append(seq).append("\n");
        }
      }
    }

    bw.flush();
    bw.close();
  }

}
