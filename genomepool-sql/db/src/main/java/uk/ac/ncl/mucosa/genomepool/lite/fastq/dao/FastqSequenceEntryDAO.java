/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: 25-Mar-2012, 12:40:04
 */

package uk.ac.ncl.mucosa.genomepool.lite.fastq.dao;

import java.util.Collection;
import java.util.List;
import org.hibernate.Query;
import org.hibernate.Session;
import uk.ac.ncl.mucosa.genomepool.GenomePoolDbException;
import uk.ac.ncl.mucosa.genomepool.lite.fastq.data.FastqSequenceEntry;

/**
 *
 * @author Keith Flanagan
 */
public class FastqSequenceEntryDAO
{
  private static final int DEFAULT_BATCH = 50000;
  
  public void store(Session session, Collection<FastqSequenceEntry> entries)
      throws GenomePoolDbException
  {
    try
    {
      int count = 0;
      for (FastqSequenceEntry entry : entries)
      {
        session.save(entry);
        count++;
        if (count % DEFAULT_BATCH == 0)
        {
          session.flush();
          session.clear();
        }
      }
    }
    catch(Exception e)
    {
      throw new GenomePoolDbException(
          "Failed to execute store()", e);
    }
    finally
    {
    }
  }
  
  public List<FastqSequenceEntry> selectHitsForReport(
      Session session, long reportId)
      throws GenomePoolDbException
  {
    try
    {
      String qry = "from "+FastqSequenceEntry.class.getName()+" where report.id = :id";
      Query query = session.createQuery(qry);
      query.setParameter("id", reportId);
        
      List<FastqSequenceEntry> entries = query.list();
      return entries;
    }
    catch(Exception e)
    {
      throw new GenomePoolDbException(
          "Failed to execute selectHitsForReport()", e);
    }
    finally
    {
    }
  }
  
}
