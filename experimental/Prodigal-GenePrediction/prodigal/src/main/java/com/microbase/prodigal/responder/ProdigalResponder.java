/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.microbase.prodigal.responder;

import java.io.File;
import java.io.FileWriter;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;
import uk.org.microbase.dist.responder.ResponderInfo;
import uk.org.microbase.filesystem.spi.FileMetaData;
import uk.org.microbase.filesystem.spi.MBFile;
import uk.org.microbase.notification.data.Message;
import uk.org.microbase.responder.spi.AbstractMessageProcessor;
import uk.org.microbase.responder.spi.ProcessingException;
import uk.org.microbase.util.cli.NativeCommandExecutor;
/**
 *
 * @author prisni
 */
public class ProdigalResponder extends AbstractMessageProcessor {

    private static final String TOPIC_IN = "GENE-PREDICTION-JOB";
    private static final String TOPIC_OUT = "GENE-PREDICTION-ACCOMPLISHED";
    /**
     * file to store input fna file to Remote file system location for output
     *
     * @return
     */
    private FileMetaData seq;
    private FileMetaData genePredictionS3File;
    private FileMetaData translationsPredictionS3File;
    private String logFileS3Bucket;
    private String logFileS3Path;
    

    @Override
    protected ResponderInfo createDefaultResponderInfo() {
        ResponderInfo info = new ResponderInfo();
        info.setEnabled(true);
        info.setMaxDistributedCores(300);
        info.setMaxLocalCores(50);
        info.setPreferredCpus(1);
        info.setQueuePopulatorLastRunTimestamp(0);
        info.setQueuePopulatorRunEveryMs(30000);
        info.setResponderName(ProdigalResponder.class.getSimpleName());
        info.setResponderVersion("1.0");
        info.setResponderId(info.getResponderName() + "-"
                + info.getResponderVersion());
        info.setTopicOfInterest(TOPIC_IN);
        return info;
    }

    @Override
    public void preRun(Message msg) throws ProcessingException {
        System.out.println("Doing pre-run");
        seq = parseMessage(msg);
        // set remote location for output
        String remoteS3Bucket = seq.getLocation().getBucket();
        String remoteS3Path = "gene prediction for: " + seq.getLocation().getName();
        String remoteGeneS3Name = "my" + "-" + seq.getLocation().getName() + "-" + "genes";
        String remoteTranslationsS3Name = "my" + "-" + seq.getLocation().getName() + "-" + "translations";

        genePredictionS3File = new FileMetaData(remoteS3Bucket, remoteS3Path,
                remoteGeneS3Name);
        translationsPredictionS3File = new FileMetaData(remoteS3Bucket, remoteS3Path,
                remoteTranslationsS3Name);
        logFileS3Bucket = remoteS3Bucket;
        logFileS3Path = remoteS3Path;

    }

    @Override
    public void cleanupPreviousResults(Message msg) throws ProcessingException {
    }

    @Override
    public Set<Message> processMessage(Message msg) throws ProcessingException {
        try {

            File seqFile = downloadInputFile(seq);
            File predictedResult = predictProdigal(seqFile);
            MBFile genesFile = new MBFile(genePredictionS3File.getLocation().
                    getBucket(), genePredictionS3File.getLocation().getPath(),
                    genePredictionS3File.getLocation().getName());
            getRuntime().getMicrobaseFS().upload(
                    predictedResult, genesFile, null);
            File predictedTranslation = predictTranslations(seqFile);
            MBFile transFile = new MBFile(translationsPredictionS3File.getLocation().
                    getBucket(), translationsPredictionS3File.getLocation().getPath(),
                    translationsPredictionS3File.getLocation().getName());
            getRuntime().getMicrobaseFS().upload(
                    predictedTranslation, transFile, null);
            Message prodigalPredictMsg = generateSuccessNotification(msg);
            Set<Message> messages = new HashSet<Message>();
            messages.add(prodigalPredictMsg);

            return messages;
        } catch (Exception e) {
            throw new ProcessingException("Failed to run one or more commands: "
                    + "on: " + getRuntime().getNodeInfo().getHostname()
                    + "in: " + getWorkingDirectory(), e);

        }
    }

    private FileMetaData parseMessage(Message m) throws ProcessingException {
        try {
            FileMetaData seqFile = new FileMetaData(m.getContent().get("seq_bucket"),
                    m.getContent().get("seq_path"),
                    m.getContent().get("seq_filename"));
            return seqFile;
        } catch (Exception e) {
            throw new ProcessingException("Failed to parse message", e);
        }
    }

    private File downloadInputFile(FileMetaData remoteFile) throws
            ProcessingException {
        try {
            //get a reference to the working temp dir
            File tmpDir = getWorkingDirectory();

            //downloading the fasta file
            File destinationFile = new File(tmpDir, remoteFile.getLocation().getName());

            getRuntime().getMicrobaseFS().downloadFileToSpecificLocation(remoteFile.getLocation(), destinationFile, true);
//                    (
//                    remoteFile.getBucket(), remoteFile.getPath(),
//                    remoteFile.getName(), destinationFile, true);

            return destinationFile;

        } catch (Exception e) {
            throw new ProcessingException("Failed to download remote file"
                    + remoteFile.getLocation().getBucket() + " , " + remoteFile.getLocation().getPath());
        }
    }

    private File predictProdigal(File seqFile) throws ProcessingException {
        try {
            File predictedResultFile = new File(getWorkingDirectory(),
                    genePredictionS3File.getLocation().getName());
            String[] prodigalCommand = new String[]{"/mnt/responder_data/prodigal_260/prodigal.v2_60.linux", "-c", "-i",
                seqFile.getAbsolutePath(), "-o", predictedResultFile.getAbsolutePath()};

            executeCommand(prodigalCommand, "prodigal");


            return predictedResultFile;
        } catch (Exception e) {
            throw new ProcessingException("Failed to execute prodigal 2.0", e);
        }
    }

    private File predictTranslations(File seqFile) throws ProcessingException {
        try {
            File predictedResultFile = new File(getWorkingDirectory(),
                    translationsPredictionS3File.getLocation().getName());

            String[] prodigalCommandTranslations = new String[]{"/mnt/responder_data/prodigal_260/prodigal.v2_60.linux", "-c", "-i",
                seqFile.getAbsolutePath(), "-a", predictedResultFile.getAbsolutePath()};

            executeCommand(prodigalCommandTranslations, "translations");

            return predictedResultFile;
        } catch (Exception e) {
            throw new ProcessingException("Failed to execute prodigal 2.0", e);
        }
    }

    private Message generateSuccessNotification(Message parent) {
        {
            Message successMsg = createMessage(
                    parent,
                    //The message to use as a parent
                    TOPIC_OUT,
                    //The topic of the new message
                    parent.getWorkflowStepId(),
                    //The workflow step ID
                    //An optional human-readable description
                    "Successfully accomplished gene prediction using PRODIGAL 2.0 for the sequence: "
                    + seq.getLocation().getName());
//            successMsg.getContent().put("seq_bucket", seq.getBucket());
//            successMsg.getContent().put("seq_path", seq.getPath());
//            successMsg.getContent().put("seq_filename", seq.getName());
            
            // Inform future responders about the predicted genes output file
            successMsg.getContent().put("result_file.predicted_genes.bucket", genePredictionS3File.getLocation().getBucket());
            successMsg.getContent().put("result_file.predicted_genes.path", genePredictionS3File.getLocation().getPath());
            successMsg.getContent().put("result_file.predicted_genes.filename", genePredictionS3File.getLocation().getName());
            
            // Inform future responders about the predicted translations output file
            successMsg.getContent().put("result_file.predicted_translations.bucket", translationsPredictionS3File.getLocation().getBucket());
            successMsg.getContent().put("result_file.predicted_translations.path", translationsPredictionS3File.getLocation().getPath());
            successMsg.getContent().put("result_file.predicted_translations.filename", translationsPredictionS3File.getLocation().getName());
            return successMsg;
        }

    }

    private void executeCommand(String[] command, String stdOutPrefix) throws ProcessingException {
        File workDir = getWorkingDirectory();
        File stdOutFile = new File(workDir, stdOutPrefix + "-stdout.txt");
        File stdErrFile = new File(workDir, stdOutPrefix + "-stderr.txt");
        try {
            FileWriter stdOut = new FileWriter(stdOutFile);
            FileWriter stdErr = new FileWriter(stdErrFile);

            int exitStatus = NativeCommandExecutor.executeNativeCommand(workDir, command, stdOut, stdErr);

            stdOut.flush();
            stdOut.close();
            stdErr.flush();
            stdErr.close();

            if (exitStatus != 0) {
                throw new ProcessingException("Exit status of the command: "
                        + Arrays.asList(command) + "\nwas " + exitStatus
                        + ". Assuming that non-zero means that execution failed.");
            }
        } catch (Exception e) {
            throw new ProcessingException("Failed to execute command: "
                    + command[0], e);
        } finally {
            try {
                 MBFile standardOutputFile = new MBFile(logFileS3Bucket, logFileS3Path, stdOutFile.getName());
                getRuntime().getMicrobaseFS().upload(stdOutFile, standardOutputFile, null);
//                        (stdOutFile, logFileS3Bucket, 
//                        logFileS3Path, stdOutFile.getLocation().getName(), null);
                MBFile standardErrorFile = new MBFile(logFileS3Bucket, logFileS3Path, stdErrFile.getName());
                getRuntime().getMicrobaseFS().upload(stdErrFile, standardErrorFile, null);
            } catch (Exception e) {
                throw new ProcessingException("Failed to upload output file", e);
            }
        }
    }
}
