/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.microbasecloud.prodigal.datamodel;

import java.io.Serializable;
import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Lob;
import javax.persistence.ManyToOne;
import javax.persistence.Table;

/**
 *
 * @author prisni
 */
@Entity
@Table ( name = "prodigal_protein")
public class ProdigalTranslation implements Serializable {
    @Id
    @GeneratedValue(strategy= GenerationType.AUTO)
    private Long id;
    
   @Lob
   @Column (length = 65536)
   private String proteinTranslation;
   private String proteinID;
   private boolean proteinPartial;
   private String proteinStartCodon;
   private String proteinRBSmotif;
   private String proteinRBSspacer;
   private double proteinGCcontent;
   private long proteinGInumber;
   private String proteinGBKnumber;

    @ManyToOne(cascade = CascadeType.ALL, optional = false)
    private ProdigalReport report; 
    
    @Override
    public String toString() {
        return "ProdigalTranslation{" + "id=" + id + ", proteinTranslation=" +
                proteinTranslation + ", proteinID=" + proteinID +
                ", proteinPartial=" + proteinPartial + ", proteinStartCodon=" +
                proteinStartCodon + ", proteinRBSmotif=" + proteinRBSmotif + 
                ", proteinRBSspacer=" + proteinRBSspacer + 
                ", proteinGCcontent=" + proteinGCcontent +
                ", proteinGInumber=" + proteinGInumber +
                ", proteinGBKnumber=" + proteinGBKnumber + '}';
    }
   

    public String getProteinTranslation() {
        return proteinTranslation;
    }

    public void setProteinTranslation(String proteinTranslation) {
        this.proteinTranslation = proteinTranslation;
    }

    public String getProteinID() {
        return proteinID;
    }

    public void setProteinID(String proteinID) {
        this.proteinID = proteinID;
    }

    public boolean getProteinPartial() {
        return proteinPartial;
    }

    public void setProteinPartial(boolean proteinPartial) {
        this.proteinPartial = proteinPartial;
    }

    public String getProteinStartCodon() {
        return proteinStartCodon;
    }

    public void setProteinStartCodon(String proteinStartCodon) {
        this.proteinStartCodon = proteinStartCodon;
    }

    public String getProteinRBSmotif() {
        return proteinRBSmotif;
    }

    public void setProteinRBSmotif(String proteinRBSmotif) {
        this.proteinRBSmotif = proteinRBSmotif;
    }

    public String getProteinRBSspacer() {
        return proteinRBSspacer;
    }

    public void setProteinRBSspacer(String proteinRBSspacer) {
        this.proteinRBSspacer = proteinRBSspacer;
    }

    public double getProteinGCcontent() {
        return proteinGCcontent;
    }

    public void setProteinGCcontent(double proteinGCcontent) {
        this.proteinGCcontent = proteinGCcontent;
    }

    public long getProteinGInumber() {
        return proteinGInumber;
    }

    public void setProteinGInumber(long proteinGInumber) {
        this.proteinGInumber = proteinGInumber;
    }

    public String getProteinGBKnumber() {
        return proteinGBKnumber;
    }

    public void setProteinGBKnumber(String proteinGBKnumber) {
        this.proteinGBKnumber = proteinGBKnumber;
    }
   
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public ProdigalReport getReport() {
        return report;
    }

    public void setReport(ProdigalReport report) {
        this.report = report;
    }

    
}
