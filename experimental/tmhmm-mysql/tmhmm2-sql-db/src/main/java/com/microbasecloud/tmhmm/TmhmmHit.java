/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.microbasecloud.tmhmm;

import java.io.Serializable;
import javax.persistence.*;

/**
 *
 * @author Sirintra Nakjang
 */
@Entity
@Table( name = "tmhmm_hits" )
public class TmhmmHit
    implements Serializable
{
  @Id
  @GeneratedValue(strategy=GenerationType.AUTO)
  private Long id;
 
  @ManyToOne(cascade = CascadeType.ALL)
  private TmhmmReport report;
 
  private String queryAcc;
  private int seqlength;
  private float expectedAminoAcid;
  private float first60;
  private int predictedHelix;
  private String topology; 

  public TmhmmHit()
  {
  }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public TmhmmReport getReport() {
        return report;
    }

    public void setReport(TmhmmReport report) {
        this.report = report;
    }

    public String getQueryAcc() {
        return queryAcc;
    }

    public void setQueryAcc(String queryAcc) {
        this.queryAcc = queryAcc;
    }

    public int getSeqlength() {
        return seqlength;
    }

    public void setSeqlength(int seqlength) {
        this.seqlength = seqlength;
    }

    public float getExpectedAminoAcid() {
        return expectedAminoAcid;
    }

    public void setExpectedAminoAcid(float expectedAminoAcid) {
        this.expectedAminoAcid = expectedAminoAcid;
    }

    public float getFirst60() {
        return first60;
    }

    public void setFirst60(float first60) {
        this.first60 = first60;
    }

    public int getPredictedHelix() {
        return predictedHelix;
    }

    public void setPredictedHelix(int predictedHelix) {
        this.predictedHelix = predictedHelix;
    }

    public String getTopology() {
        return topology;
    }

    public void setTopology(String topology) {
        this.topology = topology;
    }
  
  
}