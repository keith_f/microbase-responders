/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 *
 */
package uk.ac.ncl.mucosa.genomepool.ws.data;

import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 * Represents a portion of a genome fragment's  DNA string 
 * @author Keith Flanagan
 */
public class DnaBlockWs
        implements Serializable
{
  private String fragmentGuid;
  private int fromBase;
  private int toBase;

  private String dna;

  public DnaBlockWs()
  {
  }

  @Override
  public boolean equals(Object obj)
  {
    if (obj == null)
    {
      return false;
    }
    if (getClass() != obj.getClass())
    {
      return false;
    }
    final DnaBlockWs other = (DnaBlockWs) obj;
    if ((this.fragmentGuid == null) ? (other.fragmentGuid != null) : !this.fragmentGuid.equals(other.fragmentGuid))
    {
      return false;
    }
    if (this.fromBase != other.fromBase)
    {
      return false;
    }
    if (this.toBase != other.toBase)
    {
      return false;
    }
    return true;
  }

  @Override
  public int hashCode()
  {
    int hash = 5;
    hash = 67 * hash + (this.fragmentGuid != null ? this.fragmentGuid.hashCode() : 0);
    hash = 67 * hash + this.fromBase;
    hash = 67 * hash + this.toBase;
    return hash;
  }

  @Override
  public String toString()
  {
    return "DnaBlockWs{" + "fragmentGuid=" + fragmentGuid + ", fromBase=" + fromBase + ", toBase=" + toBase + ", dna=" + dna + '}';
  }

  public String getDna()
  {
    return dna;
  }

  public void setDna(String dna)
  {
    this.dna = dna;
  }

  public String getFragmentGuid()
  {
    return fragmentGuid;
  }

  public void setFragmentGuid(String fragmentGuid)
  {
    this.fragmentGuid = fragmentGuid;
  }

  public int getFromBase()
  {
    return fromBase;
  }

  public void setFromBase(int fromBase)
  {
    this.fromBase = fromBase;
  }

  public int getToBase()
  {
    return toBase;
  }

  public void setToBase(int toBase)
  {
    this.toBase = toBase;
  }

}
