/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.microbasecloud.responders.filescanner.data;

import java.util.List;
import java.util.Set;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author Keith Flanagan
 */
public class ScanReportDAO {
    private static final int DEFAULT_BATCH = 50000;
 
  public void store(Session session, Set<ScanReport> entries)
      throws FileScannerDBException
  {
    try
    {
      int count = 0;
      for (ScanReport entry : entries)
      {
        session.save(entry);
        count++;
        if (count % DEFAULT_BATCH == 0)
        {
          session.flush();
          session.clear();
        }
      }
    }
    catch(Exception e)
    {
      throw new FileScannerDBException(
          "Failed to execute store() operation", e);
    }
  }
  
  public List<ScanReport> selectReportsForTarget(
      Session session, String targetName)
      throws FileScannerDBException
  {
    try
    {
      String qry = "from "+ScanReport.class.getName()
          + " where targetName = :targetName "
          + "order by scanTimestamp";
          
      Query query = session.createQuery(qry);
      query.setParameter("targetName", targetName);
 
      List<ScanReport> records = query.list();
      return records;
    }
    catch(Exception e)
    {
      throw new FileScannerDBException(
          "Failed to execute selectReportsForTarget()", e);
    }
  }
 
  public ScanReport selectLatestReportForTarget(
      Session session, String targetName)
      throws FileScannerDBException
  {
    try
    {
      String qry = "from "+ScanReport.class.getName()
          +" where targetName = :targetName "
          + "order by scanTimestamp desc";
      Query query = session.createQuery(qry);
      query.setParameter("targetName", targetName);
      query.setFetchSize(1);
        
      List<ScanReport> records = query.list();
      return records.isEmpty() ? null : records.iterator().next();
    }
    catch(Exception e)
    {
      throw new FileScannerDBException(
          "Failed to execute selectLatestReportForTarget()", e);
    }
  }
}
