/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.microbase.responder.sffcommon;

import java.io.Serializable;
import uk.org.microbase.filesystem.spi.MBFile;

/**
 *
 * @author prisni
 */
public class SFFJobResult implements Serializable {
    
  private SFFJobDescription job;
  
  private MBFile resultFastQFile;
  private MBFile resultTraceInfoXMLFile;
  private MBFile stdOutLogFile;
  private MBFile stdErrLogFile;

    public SFFJobResult() {
    }

    public SFFJobDescription getJob() {
        return job;
    }

    public void setJob(SFFJobDescription job) {
        this.job = job;
    }

    public MBFile getResultFastQFile() {
        return resultFastQFile;
    }

    public void setResultFastQFile(MBFile resultFastQFile) {
        this.resultFastQFile = resultFastQFile;
    }

    public MBFile getResultTraceInfoXMLFile() {
        return resultTraceInfoXMLFile;
    }

    public void setResultTraceInfoXMLFile(MBFile resultTraceInfoXMLFile) {
        this.resultTraceInfoXMLFile = resultTraceInfoXMLFile;
    }

    public MBFile getStdOutLogFile() {
        return stdOutLogFile;
    }

    public void setStdOutLogFile(MBFile stdOutLogFile) {
        this.stdOutLogFile = stdOutLogFile;
    }

    public MBFile getStdErrLogFile() {
        return stdErrLogFile;
    }

    public void setStdErrLogFile(MBFile stdErrLogFile) {
        this.stdErrLogFile = stdErrLogFile;
    }
  
    
}
