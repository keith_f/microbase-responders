/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.microbasecloud.responders.filescanner.data;

import java.util.List;
import java.util.Set;
import org.hibernate.Query;
import org.hibernate.Session;

/**
 *
 * @author Keith Flanagan
 */
public class FileEntryDAO {
    private static final int DEFAULT_BATCH = 50000;
 
  public void store(Session session, Set<FileEntry> entries)
      throws FileScannerDBException
  {
    try
    {
      int count = 0;
      for (FileEntry fileEntry : entries)
      {
        session.save(fileEntry);
        count++;
        if (count % DEFAULT_BATCH == 0)
        {
          session.flush();
          session.clear();
        }
      }
    }
    catch(Exception e)
    {
      throw new FileScannerDBException(
          "Failed to execute store() operation", e);
    }
  }
  
  public List<FileEntry> selectFilesForReport(Session session,
      ScanReport report)
      throws FileScannerDBException
  {
    try
    {
      String qry = "from "+FileEntry.class.getName()
          +" where report.id = :reportId";
      Query query = session.createQuery(qry);
      query.setParameter("reportId", report.getId());
 
      List<FileEntry> entries = query.list();
      return entries;
    }
    catch(Exception e)
    {
      throw new FileScannerDBException(
          "Failed to execute selectFilesForReport()", e);
    }
  }
  
  public FileEntry selectEntry(Session session,
      String bucket, String path, String name)
      throws FileScannerDBException
  {
    try
    {
      String qry = "from "+FileEntry.class.getName()
          +" where bucket = :bucket "
          + "and filePath = :filePath "
          + "and filename = :filename";
      Query query = session.createQuery(qry);
      query.setParameter("bucket", bucket);
      query.setParameter("filePath", path);
      query.setParameter("filename", name);
 
      List<FileEntry> records = query.list();
      return records.isEmpty() ? null : records.iterator().next();
    }
    catch(Exception e)
    {
      throw new FileScannerDBException(
          "Failed to execute selectHitsForBucket()", e);
    }
  }
 
  public List<FileEntry> selectFilesForBucket(Session session,
      String bucket, String path)
      throws FileScannerDBException
  {
    try
    {
      String qry = "from "+FileEntry.class.getName()
          +" where bucket = :bucket "
          + "and filePath = :filePath";
      Query query = session.createQuery(qry);
      query.setParameter("bucket", bucket);
      query.setParameter("filePath", path);
 
      List<FileEntry> entries = query.list();
      return entries;
    }
    catch(Exception e)
    {
      throw new FileScannerDBException(
          "Failed to execute selectFilesForBucket()", e);
    }
  }
}
