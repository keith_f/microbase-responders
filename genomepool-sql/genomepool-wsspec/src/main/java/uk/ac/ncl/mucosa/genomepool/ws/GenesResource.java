/*
 * Copyright 2012 Keith Flanagan
 * 
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 * 
 *     http://www.apache.org/licenses/LICENSE-2.0
 * 
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 * 
 * File created: Jan 21, 2012, 1:41:06 PM
 */

package uk.ac.ncl.mucosa.genomepool.ws;

import org.restlet.resource.Get;
import uk.ac.ncl.mucosa.genomepool.ws.data.DNALocatedPagedItems;
import uk.ac.ncl.mucosa.genomepool.ws.data.GeneWs;
import uk.ac.ncl.mucosa.genomepool.ws.data.GenomeFragmentWs;
import uk.ac.ncl.mucosa.genomepool.ws.data.PagedItems;

/**
 * 
 * @author Keith Flanagan
 */
public interface GenesResource
{
  public static enum Parameters {
    FRAGMENT_GUID ("fragment_guid");
    
    private final String paramName;

    Parameters(String paramName)
    {
      this.paramName = paramName;
    }

    public String paramName()
    {
      return paramName;
    }
  };
  
  /**
   * Returns a pageable list of GeneWs objects.
   * Required parameters:
   *   * fragment_guid (single) - specifies the fragment whose GeneWs entries
   *     should be returned.
   *        
   * @return
   * @throws GenomePoolWsException 
   */
  @Get
  public DNALocatedPagedItems<GeneWs> getGenes()
      throws GenomePoolWsException;
}
