/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.microbase.responder.assemblermiraresponder;

import com.microbase.responder.assemblermiracommon.AssemblerMIRAJobDescription;
import uk.org.microbase.filesystem.spi.MBFile;

/**
 *
 * @author prisni
 */
public class AssemblerMIRAResultInIndividualDirsFormatter extends AbstractOutputPathReformatter
    implements AssemblerMIRAOutputPathReformatter {

    private AssemblerMIRAJobDescription jobDescription;
  
  @Override
  public String getFormattedBucketName() {
    return responderInfo.getResultDestinationBucket();
  }

  @Override
  public String getFormattedPath() {
    StringBuilder path = new StringBuilder(responderInfo.getResultDestinationBasePath());
    path.append("/").append(getFormattedFilename());
    return path.toString();
  }

  @Override
  public String getFormattedFilename() {
    StringBuilder resultFilename = new StringBuilder();
//    resultFilename.append(jobDescription.getInputFiles().getName())
//        .append("-").append(jobDescription.getDatabaseName())
//        .append(".").append(jobDescription.getJobType().name());
    return resultFilename.toString();
  }
  
  @Override
  public MBFile getFormattedRemoteFile() {
    MBFile file = new MBFile(getFormattedBucketName(), getFormattedPath(), getFormattedFilename());
    return file;
  }
  

  public AssemblerMIRAJobDescription getJobDescription() {
    return jobDescription;
  }

  @Override
  public void setJobDescription(
          AssemblerMIRAJobDescription jobDescription) {
    this.jobDescription = jobDescription;
  }
}
