/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package com.microbase.responder.assemblermiracommon;

import java.io.Serializable;
import uk.org.microbase.filesystem.spi.MBFile;

/**
 *
 * @author prisni
 */
public class AssemblerMIRAJobResult implements Serializable {
    private AssemblerMIRAJobDescription job;
  
  private MBFile resultFile;
  private MBFile stdOutLogFile;
  private MBFile stdErrLogFile;

    public AssemblerMIRAJobResult() {
    }

    public AssemblerMIRAJobDescription getJob() {
        return job;
    }

    public void setJob(AssemblerMIRAJobDescription job) {
        this.job = job;
    }

    public MBFile getResultFile() {
        return resultFile;
    }

    public void setResultFile(MBFile resultFile) {
        this.resultFile = resultFile;
    }

    public MBFile getStdOutLogFile() {
        return stdOutLogFile;
    }

    public void setStdOutLogFile(MBFile stdOutLogFile) {
        this.stdOutLogFile = stdOutLogFile;
    }

    public MBFile getStdErrLogFile() {
        return stdErrLogFile;
    }

    public void setStdErrLogFile(MBFile stdErrLogFile) {
        this.stdErrLogFile = stdErrLogFile;
    }
  
    
}
